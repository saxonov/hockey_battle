﻿using GameSparks.Core;

namespace HockeyManager.Game.Impl
{
    public interface IGSResponceHandler
    {
        void UpdateFromGSResponce(GSData scriptData);
    }
}
