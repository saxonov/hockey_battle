﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.GameSparks.Impl;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.GameSparks
{
    [RequireComponent(typeof(GameSparksUnity),typeof(GSClientLogger),typeof(GSAuthorizationBehavior))]
    public class GSManager:MonoBehaviour,IGSManager
    {
        [HideInInspector]
        public IGSResponceHandler[] responceControllers;
        [HideInInspector]
        public ICheckMessages[] checkMessageControllers;

        [HideInInspector]
        public UnityEvent OnUserAuthenticatedEvent = new UnityEvent();

        [HideInInspector] public bool isNewUser = false;

        private static bool _oneSignalInited;
        private string _pushUserId;
        private string _pushNotificationToken;

        public float checkInternetDelaySeconds = 5f;
        private float _internetCheckDelaySeconds;

        private List<GSData> _lastActionsData;
       
        private bool _isRestarting;

        private bool _internetExist = true;

        private GSClientLogger _logger;
        private GSAuthorizationBehavior _authtorization;

        private void CheckInternetConnection()
        {
            bool internetAvailable;
            switch (Application.internetReachability)
            {
                case NetworkReachability.ReachableViaLocalAreaNetwork:
                    internetAvailable = true;
                    break;
                case NetworkReachability.ReachableViaCarrierDataNetwork:
                    internetAvailable = true;
                    break;
                default:
                    internetAvailable = false;
                    break;
            }

            if (!internetAvailable)
            {
                if (_internetExist)
                {
                    EventsManager.Instance.Call(GlobalEventTypes.InternetConnectionLost);
                }
                _internetExist = false;
            }
            else
            {
                if (!_internetExist)
                {
                    EventsManager.Instance.Call(GlobalEventTypes.InternetConnectionExist);
                }
                _internetExist = true;
            }
        }

        public bool InternetExist
        {
            get { return _internetExist; }
        }

        void Awake()
        {
            _internetCheckDelaySeconds = checkInternetDelaySeconds;
        }

        void Start()
        {
            GS.TraceMessages = true;

            _logger = GetComponent<GSClientLogger>();

            GetOneSignalPushRegIds();

            StartCoroutine(LoginCoroutine(2f));
        }
        
        #region  OneSignal Push Notifications 

     //   private const string NotificationTokenExistPrefKey = "PUSH_NOTIFICATION_REGISTERED";

        private const string OneSignalID = "132a2160-d5a5-4234-b7fc-6bd71f25bafc";


        /// <summary>
        /// not use? send from server
        /// </summary>
        /// <param name="sendAfterSeconds"></param>
        public void SendPushNotification(int sendAfterSeconds)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("include_player_ids", new List<string>() { _pushUserId });
            data.Add("contents", new Dictionary<string, string>() { { "en", "Test message" } });
            data.Add("send_after", DateTime.Now.AddSeconds(sendAfterSeconds).ToString("U"));

            OneSignal.PostNotification(data, (success) =>
            {
                Debug.Log("send push notification success!");
            }, (fail) =>
            {

                Debug.LogWarning("send push notification fail!");
            });

        }


        private void GetOneSignalPushRegIds()
        {
            if (!_oneSignalInited)
            {
                OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

                OneSignal.StartInit(OneSignalID)
                    .InFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                    .EndInit();
                _oneSignalInited = true;
                OneSignal.ClearOneSignalNotifications();
            }
        }

        private void InitializePushNotifications()
        {
            OneSignal.IdsAvailable(((id, token) =>
            {
                _pushNotificationToken = token;
                _pushUserId = id;

                PushRegistrationRequest request = new PushRegistrationRequest();
                request.SetPushId(_pushUserId);
                request.Send((responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        Debug.Log("push token: " + _pushNotificationToken);
                        Debug.Log("id: " + _pushUserId);
                    }
                });
            }));
        }

        #endregion

        void OnDisable()
        {
            GSMessageHandler._AllMessages -= OnServerMessagesHandler;
        }

        void Update()
        {
            GameTimer.Update();

            _internetCheckDelaySeconds -= Time.deltaTime;
            if (_internetCheckDelaySeconds <= 0)
            {
                _internetCheckDelaySeconds = checkInternetDelaySeconds;
                CheckInternetConnection();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rewardsCount">check rewards cunt on server, if not equal then update</param>
        public void SendRestartEvent(int rewardsCount)
        {

            if (_isRestarting)
            {
                return;
            }

            _isRestarting = true;
            var request = new LogEventRequest();
            request.SetEventKey("RESTART_EVENT");
            request.SetEventAttribute("REWARDS_COUNT", rewardsCount);
            RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _lastActionsData = responce.ScriptData.GetGSDataList(GS_ScriptData.LastActionsData);
                }
                UpdateFromTeamJournal();
                _isRestarting = false;
            });
        }

        public void RunCustomLogEventRequest(LogEventRequest request, UnityAction<LogEventResponse> Callback,bool isBackgroundRequest = false)
        {
            if (!isBackgroundRequest)
            {
                EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended, request.GetHashCode());
            }
            Debug.LogFormat("<p>GS: Send LogEvent request:{0}</p>", request.JSONString);
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    Debug.LogFormat("<p>GS: Get LogEvent error:{0}</p>", responce.JSONString);
                    // SaveClientLogs("GS: LogEvent ServerError! info: " + request.JSONString);
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);

                }
                else
                {
                    Debug.LogFormat("<p>GS: Get LogEvent responce:{0}</p>", responce.JSONString);
                    if (responce.ScriptData != null)
                    {
                        if (responce.ScriptData.ContainsKey(GS_ScriptData.Timestamp))
                        {
                            GameTimer.TimeStamp = responce.ScriptData.GetLong(GS_ScriptData.Timestamp).Value;
                        }
                    }
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved, request.GetHashCode());
                }

                Callback(responce);
            });
        }

        public void RunLogEventRequest(LogEventRequest request, UnityAction<LogEventResponse> Callback = null, string errorMsg = null, bool isBackgroundRequest = false)
        {
            if (!isBackgroundRequest)
            {
                EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended,request.GetHashCode());
            }

            Debug.LogFormat("<p>GS: Send LogEvent request:{0}</p>", request.JSONString);
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    if (!String.IsNullOrEmpty(errorMsg))
                    {
                        responce.Errors.BaseData.Add("msg", errorMsg);
                    }
                    Debug.LogFormat("<p>GS: Get LogEvent error:{0}</p>", responce.JSONString);
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                }
                else
                {
                    Debug.LogFormat("<p>GS: Get LogEvent responce:{0}</p>",responce.JSONString);
                    if (responce.ScriptData != null)
                    {
                        if (responce.ScriptData.ContainsKey(GS_ScriptData.Timestamp))
                        {
                            GameTimer.TimeStamp = responce.ScriptData.GetLong(GS_ScriptData.Timestamp).Value;
                        }

                        for (int i = 0; i < responceControllers.Length; i++)
                        {
                            responceControllers[i].UpdateFromGSResponce(responce.ScriptData);
                        }
                     
                    }

                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,request.GetHashCode());
                }
                if (Callback != null)
                {
                    Callback(responce);
                }
            });
        }

        public void RunListVirtualGoodsRequest(ListVirtualGoodsRequest request, UnityAction<ListVirtualGoodsResponse> callback)
        {
            EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended, request.GetHashCode());
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                }
                else
                {
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved, request.GetHashCode());
                }
                if (callback != null)
                {
                    callback(responce);
                }
            });
        }

        public void RunLeaderboardRequest(AroundMeLeaderboardRequest request, UnityAction<AroundMeLeaderboardResponse> callback)
        {
            EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended, request.GetHashCode());
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                }
                else
                {
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved, request.GetHashCode());
                }
                if (callback != null)
                {
                    callback(responce);
                }
            });
        }

        public void RunBuyVirtualEventRequest(BuyVirtualGoodsRequest request, UnityAction<BuyVirtualGoodResponse> Callback = null, string errorMsg = null)
        {
            Debug.LogFormat("<p>GS: Buy virtual good request data: {0}</p>", request.JSONString);
            EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended, request.GetHashCode());
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    if (!String.IsNullOrEmpty(errorMsg))
                    {
                        responce.Errors.BaseData.Add("msg", errorMsg);
                    }
                    Debug.LogFormat("<p>GS: Buy virtual good error data: {0}</p>", responce.JSONString);
                    _logger.SaveClientLogs("Buy Virtual Goods Server Error!");
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                }
                else
                {
                    Debug.LogFormat("<p>GS: Buy virtual good responce data: {0}</p>", responce.JSONString);
                    if (responce.ScriptData != null)
                    {

                        for (int i = 0; i < responceControllers.Length; i++)
                        {
                           responceControllers[i].UpdateFromGSResponce(responce.ScriptData);
                        }
                       
                    }
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved, request.GetHashCode());
                }
                if (Callback != null)
                {
                    Callback(responce);
                }
            });
        }

        private void OnServerMessagesHandler(GSMessage msg)
        {
            if (msg == null || msg.BaseData == null) return;
            //if(!GS.Authenticated)return;
            //  Debug.Log("Message code:" + msg.JSONData["extCode"]);
            if (!msg.HasErrors)
            {


                if (msg.BaseData.ContainsKey("ts"))
                {
                    GameTimer.TimeStamp = msg.BaseData.GetLong("ts").Value;
                }
                GSData data = msg.BaseData.GetGSData("data");
                string msgCode = msg.BaseData.GetString("extCode");
                Debug.LogFormat("<p>GS: Messages data : {0}</p>", msg.JSONString);
                for (int i = 0; i < checkMessageControllers.Length; i++)
                {
                    if (checkMessageControllers[i].CanCheckMessage(msgCode, data))
                    {
                        checkMessageControllers[i].CheckMessage(data, msgCode);
                    }

                }

                if (msgCode == GS_Messages.JournalActionAdded)
                {
                    CheckAndLoadJournalActions();
                    //DismissMessage(msg);
                }

                // DismissMessage(msg);

            }
            else
            {
                Debug.LogWarning(msg.Errors.JSON);
            }
        }

        private void CheckAndLoadJournalActions()
        {
            if (_isRestarting) return;
            Debug.Log("Load team journal!");
            var request = new LogEventRequest();
            request.SetEventKey("CHECK_AND_LOAD_JOURNAL");
            request.SetDurable(true);
            RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (responce.ScriptData != null)
                    {
                        _lastActionsData = responce.ScriptData.GetGSDataList(GS_ScriptData.LastActionsData);
                    }
                    UpdateFromTeamJournal();
                }

            });

        }

        public void UpdateFromTeamJournal()
        {
            if (_lastActionsData != null)
            {
                foreach (var gsData in _lastActionsData)
                {

                    string msgType = gsData.GetString("type");
                    GSData data = gsData.GetGSData("data");
                    Debug.LogFormat("<p>GS: Team journal Message data: {0}</p>", gsData.JSON);
                    if (data != null && data.ContainsKey("ts"))
                    {
                        GameTimer.TimeStamp = data.GetLong("ts").Value;
                    }

                    for (int i = 0; i < checkMessageControllers.Length; i++)
                    {
                        if (checkMessageControllers[i].CanCheckMessage(msgType, data))
                        {
                            checkMessageControllers[i].CheckMessage(data, msgType);
                        }
                    }
                }
                if (_lastActionsData.Count > 0)
                {
                    Debug.Log("Updates from team journal complete!");
                }
                _lastActionsData.Clear();
                _lastActionsData = null;

            }
        }

        IEnumerator LoginCoroutine(float delay = 1f)
        {
            yield return new WaitForSeconds(delay);
            while (!GS.Available)
            {
                _internetExist = false;
                yield return new WaitForSeconds(3f);
                EventsManager.Instance.Call(GlobalEventTypes.InternetConnectionLost);

            }
            if (!_internetExist)
            {
                _internetExist = true;
                EventsManager.Instance.Call(GlobalEventTypes.InternetConnectionExist);
            }
            _authtorization = GetComponent<GSAuthorizationBehavior>();
            _authtorization.Login(OnLoginResultHandler);
        }

        private void OnLoginResultHandler(AuthenticationResponse responce)
        {
            if (!responce.HasErrors)
            {
          
                if (responce.ScriptData.GetLong(GS_ScriptData.Timestamp).HasValue)
                {
                    GameTimer.TimeStamp = responce.ScriptData.GetLong(GS_ScriptData.Timestamp).Value;
                }

                GSData loginData = responce.ScriptData.GetGSData(GS_ScriptData.LoginData);

                bool isNewTeam = (responce.NewPlayer.HasValue && responce.NewPlayer.Value) || 
                                 (loginData.GetBoolean("isNewTeam").HasValue && loginData.GetBoolean("isNewTeam").Value);

                GameData.SetUserID(responce.UserId,isNewTeam);

                if (loginData.GetBoolean("pushRegistered").HasValue && !loginData.GetBoolean("pushRegistered").Value)
                {
                    InitializePushNotifications();
                }

                GameData.GSConstData.Initialize(loginData);

                OneSignal.ClearOneSignalNotifications();

                for (int i = 0; i < responceControllers.Length; i++)
                {
                    responceControllers[i].UpdateFromGSResponce(responce.ScriptData);
                }

                //get team journal last actions 
                _lastActionsData = responce.ScriptData.GetGSDataList(GS_ScriptData.LastActionsData);

                GSMessageHandler._AllMessages += OnServerMessagesHandler;

                OnUserAuthenticatedEvent.Invoke();
            }
        }

        public GSClientLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = GetComponent<GSClientLogger>();
                }
                return _logger;
            }


        }
    }
}
