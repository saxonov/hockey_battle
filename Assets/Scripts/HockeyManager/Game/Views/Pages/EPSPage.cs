﻿using System;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Equipments;
using HockeyManager.Game.Views.Popups;
using HockeyManager.Game.Views.Renderers;
using HockeyManager.HockeyManager.Game.Models.EPS;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Pages
{
   // [ExecuteInEditMode]
    public class EPSPage :MonoBehaviour,IPageChangeHandler
    {
        [SerializeField] private Canvas canvHolder;

        private IGameSettings settings;

        [Header("Equipments")]
        [SerializeField]private PulloverColorTuner pulloverColorTunner;
        [SerializeField] private RectTransform equipmentsBack;
        [SerializeField] private RectTransform fitBoxTransform;
        [SerializeField] private RectTransform cloakRoomHolder;
        [SerializeField] private Equipment[] _equipments;
        [SerializeField] private GameObject equipmentPrefabRenderer;
        [SerializeField] private Text equipmentUpgradeTitle;
        [SerializeField] private RectTransform equipmentUpgradeHolder;
      
        private UpgradeInfoRenderer _equipmentUpgradeRenderer;

        [Space(10)] [Header("Personal")]
        [SerializeField] private ScrollRect personalList;
       // [SerializeField] private GameObject employeePrefabRenderer;
        [SerializeField] private EmployeeItemRenderer[] employeesRenderers = new EmployeeItemRenderer[4];

        [Space(10)] [Header("Skills")]
        [SerializeField] private GameObject skillItemRendererPrefab; 
        [SerializeField] private RectTransform skillRenderersList;
        [SerializeField] private Button resetSkillsButton;
        [SerializeField] private Button applySkillsButton;
        [SerializeField] private Text leftSkillPoints;
        [SerializeField] private Text usedSkillPoints;
        private bool _skillsInitialized = false;

        [Space(10)]
        [SerializeField]private InnerPageNavigation navBar;

        private EquipmentModel _selectedEquipment;
        private EPSController _epsController;
        private MapController _map;

        private bool _cloakRoomBuilded = false;
       // private bool _personalBuilded = false;

    
        private TeamController _teamController;

        void Start()
        {
            navBar.EnableIndicator(3, _epsController.SkillPoints > 0);
        }
      
        void Awake()
        {
            settings = GameSettings.Instance;

            _map = GameRoot.Instance.GetController<MapController>();
            _epsController = GameRoot.Instance.GetController<EPSController>();
            _teamController = GameRoot.Instance.GetController<TeamController>();

            personalList.content.HideChildren();
            HideEquipments();
            EventsManager.Instance.Add<EquipmentModel>(Equipment.OnEquipmentModelChangedHandler, OnEquipmentModelChangedHandler);
            EventsManager.Instance.Add(EPSController.SkillPointsAvailable, OnSkillPointsChanged);
            EventsManager.Instance.Add<EquipmentModel>(Equipment.OnEquipmentSelectedHandler,OnEquipmentSelectedHandler);
            EventsManager.Instance.Add<EmployeeModel>(EmployeeItemRenderer.EmployeeSelectedEvent, OnEmployeeSelectedHander);
           // EmployeeItemRenderer.OnEmployeeSelected += OnEmployeeSelectedHander;
            UpgradeInfoRenderer.OnEquipmentUpgradeClicked += OnEquipmentItemClicked;

            Equipment.hudHolder = fitBoxTransform;

            EventsManager.Instance.Add<SkillModel>(SkillItemRenderer.OnAddPointClickEvent,OnAddSkillPointHandler);

            PageNavigatorManager.Instance.AddPageChangeHandler(this);
        }



        void OnDestroy()
        {
            _epsController = null;
            _teamController = null;
            EventsManager.Instance.Remove(EPSController.SkillPointsAvailable, OnSkillPointsChanged);
            EventsManager.Instance.Remove<EquipmentModel>(Equipment.OnEquipmentModelChangedHandler,OnEquipmentModelChangedHandler);
            EventsManager.Instance.Remove<EmployeeModel>(EmployeeItemRenderer.EmployeeSelectedEvent, OnEmployeeSelectedHander);
            UpgradeInfoRenderer.OnEquipmentUpgradeClicked -= OnEquipmentItemClicked;
            EventsManager.Instance.Remove<SkillModel>(SkillItemRenderer.OnAddPointClickEvent, OnAddSkillPointHandler);
            EventsManager.Instance.Remove<EquipmentModel>(Equipment.OnEquipmentSelectedHandler, OnEquipmentSelectedHandler);
            
        }


        private void OnEquipmentModelChangedHandler(EquipmentModel model)
        {
            if (model.Id == EquipmentType.Pullover)
            {
                pulloverColorTunner.gameObject.SetActive(model.Level > 1);
            }
        }

        private void OnEquipmentItemClicked(EquipmentModel model)
        {
            if (!model.IsUpgrading)
            {
                if (!_equipmentUpgradeStarted)
                {
                    _equipmentUpgradeStarted = true;
                    UpgradeEquipment(model);
                }
               
            }
            else
            {
                FinishUpgradeEquipment(model, false);
            }
        }

        private void OnSkillPointsChanged()
        {
            navBar.EnableIndicator(3, _epsController.SkillPoints > 0);
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Skills)
            {
                SkillsChanged();
            }
        }






        #region CloakRoom

        private bool _equipmentUpgradeStarted = false;

        private void HideEquipments()
        {
            foreach (var equipment in _equipments)
            {
                equipment.gameObject.SetActive(false);
            }
        }

        private void BuildCloakRoom()
        {
            _epsController.GetEquipments((equipments) =>
            {
                foreach (var equipment in equipments)
                {
                    for (int i = 0; i < _equipments.Length; i++)
                    {
                        if (_equipments[i].gameObject.name == equipment.Id.ToString())
                        {
                            _equipments[i].gameObject.SetActive(true);
                            _equipments[i].SetModel(equipment);
                            if (equipment.Id == EquipmentType.Pullover)
                            {
                                pulloverColorTunner.gameObject.SetActive(equipment.Level > 1);
                            }
                        }
                    }
                }
            });
        }


        private void FitEquipmentsPage()
        {
            Rect rectSize = RectTransformUtility.PixelAdjustRect(fitBoxTransform, canvHolder);
           // float aspect = equipmentsBack.sizeDelta.y / equipmentsBack.sizeDelta.x;
            float scalex = rectSize.width / equipmentsBack.sizeDelta.x;
            float scaley = rectSize.height / equipmentsBack.sizeDelta.y;
            float scale = Mathf.Max(scalex, scaley);
            equipmentsBack.localScale = Vector3.one * scale;
        }
        
        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (prev == NavigationPageTypes.Skills && PageNavigatorManager.Instance.CancelMovePage)
            {
                Debug.Log("Need apply skills from page " + selected);
                PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                .SetTitle("game.main.popup.title.savechangesatclose").SetCallbacks(() =>
                {
                    _epsController.UpgradeSkills(SkillsChanged,BuildSKills);
                    PageNavigatorManager.Instance.CancelMovePage = false;
                    PageNavigatorManager.Instance.GotoPage(selected);
                }, () =>
                {
                    _epsController.ResetSkillPoints(SkillsChanged, true);
                    PageNavigatorManager.Instance.CancelMovePage = false;
                    PageNavigatorManager.Instance.GotoPage(selected);
                });
                return;
            }

            switch (selected)
            {
                case NavigationPageTypes.EquipmentUpgradeInfo:

                    cloakRoomHolder.gameObject.SetActive(false);
                    AddEquipmentsUpgradeRenderer();

                    break;
                case NavigationPageTypes.Personal:
                    
                    cloakRoomHolder.gameObject.SetActive(false);
                    UpdatePersonalList();
                   
                    break;
                case NavigationPageTypes.Skills:

                    cloakRoomHolder.gameObject.SetActive(false);
                    if (!_skillsInitialized)
                    {
                        _skillsInitialized = true;
                        BuildSKills();
                        applySkillsButton.onClick.AddListener(OnApplySkillsClickHandler);
                        resetSkillsButton.onClick.AddListener(OnResetSkillsClickHandler);
                    }
                    SkillsChanged();

                    break;
                case NavigationPageTypes.EPS:
                case NavigationPageTypes.CloakRoom:

                    navBar.SelectButton(1, true);
                    cloakRoomHolder.gameObject.SetActive(true);
                    if (!_cloakRoomBuilded)
                    {
                        _cloakRoomBuilded = true;
                        FitEquipmentsPage();
                        BuildCloakRoom();

                    }
                    OnSkillPointsChanged();

                    break;
                default:
                    if (_equipmentUpgradeRenderer != null)
                    {
                        Destroy(_equipmentUpgradeRenderer.gameObject);
                        _equipmentUpgradeRenderer = null;
                    }
                    cloakRoomHolder.gameObject.SetActive(false);
                    // ClearPersonalList();
                    break;
            }
        }

        private void AddEquipmentsUpgradeRenderer()
        {
            if (_selectedEquipment == null)
            {
                throw new Exception("Equipment not selected");
            }

            if (_equipmentUpgradeRenderer != null)
            {
                Destroy(_equipmentUpgradeRenderer.gameObject);
                _equipmentUpgradeRenderer = null;
            }
            equipmentUpgradeTitle.text = "";

            UnityAction callback = () =>
            {
                equipmentUpgradeTitle.text = settings.GetEquipmentData(_selectedEquipment.Id).GetName();
                 _equipmentUpgradeRenderer = equipmentUpgradeHolder.AddChild(equipmentPrefabRenderer, false).GetComponent<UpgradeInfoRenderer>();
                _equipmentUpgradeRenderer.gameObject.SetActive(true);
                _equipmentUpgradeRenderer.SetModel(_selectedEquipment);
            };

         
            if (_selectedEquipment.UpgradeModel == null || _selectedEquipment.Level == _selectedEquipment.UpgradeModel.Level)
            {
                _epsController.LoadEquipmentUpgrade(_selectedEquipment.Id, callback);
            }
            else
            {
                callback();
            }
        }
        
        private void FinishUpgradeEquipment(EquipmentModel equipment,bool upgradeImmidiate)
        {
            string eqpName = settings.GetEquipmentData(equipment.Id).GetName();
            string time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(equipment.EndUpgradeTimeSeconds));
            string title = LocaleManager.Instance.GetText("game.main.upgrade") + ". " + eqpName + ".";
            int goldPrice = GameData.GetTimerGoldPrice(equipment.EndUpgradeTimeSeconds);

            UnityAction callback = () =>
            {
                if (_teamController.CanBuyForGold(goldPrice))
                {
                    _epsController.EndUpgradeEquipmentProcess((result) =>
                    {
                        if (result)
                        {
                            SoundManager.Instance.PlaySFX("finishforgold");
                        }
                        // PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.EPS);
                    });
                }
                else
                {
                    PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                }
            };

            if (upgradeImmidiate)
            {
                callback();
            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.FinishUpgradePopup)
                  .SetTitle(title)
                  .SetTextIconControls(goldPrice.ToString(), time)
                  .SetCallbacks(callback)
                  .SetMinutesTickUpdate((popup) =>
                  {
                      goldPrice = GameData.GetTimerGoldPrice(equipment.EndUpgradeTimeSeconds);
                      time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(equipment.EndUpgradeTimeSeconds));
                      popup.SetTextIconControls(goldPrice.ToString(), time);
                  });
            }
        }



        private void UpgradeEquipment(EquipmentModel model)
        {
         
            if (model != null)
            {
                if (NeedFinishEquipmentUpgrade())
                {
                    _equipmentUpgradeStarted = false;
                    return;
                }
                
                int baseLevel = _map.GetBaseEquipementMaxMinLevel();
                if (baseLevel <= model.Level)
                {
                    _equipmentUpgradeStarted = false;
                    int lvl = baseLevel == -1 ? 1 : _map.GetBuildingModel(MapBuildingsType.Base).Level + 1;
                    string baseName = settings.GetBuildingData(MapBuildingsType.Base).GetName();
                    var title = LocaleManager.Instance.GetText("game.main.popup.title.needlevelitem", baseName);
                    title += LocaleManager.SPACE + LocaleManager.Instance.GetText("game.common.shortlevel2", lvl);
                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                        .SetTitle(title)
                        .SetButtonLabels("game.common.tobasebutton")
                        .SetCallbacks(() =>
                        {
                            MapManager.Instance.GotoBuilding(GameData.BaseBuildingPosition);
                            MapManager.Instance.SelectBase();
                        });
                    return;
                }
                if (_teamController.CanBuyForCoins(model.UpgradeModel.Price))
                {
                    SoundManager.Instance.PlaySFX("upgradeequip");
                    _epsController.SendUpgradeEquipmentRequest(model,false, () =>
                    {
                        
                        ActiveProcessesManager.Instance.AddUpgradeProcess(model);
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.CloakRoom);
                    });
                    _equipmentUpgradeStarted = false;

                }
                else if (_teamController.CanBuyForGold(model.UpgradeModel.UpgradeGoldPrice))
                {
                    
                    PopupsManager.Instance.AddPopup(PopupType.NotEnoughFansCoinsPopup).
                    SetTitle("game.main.popup.title.notenoughmoney").
                    SetTextIconControls(model.UpgradeModel.UpgradeGoldPrice).
                    SetButtonLabels("game.common.upgradebutton", "game.common.closebutton").SetCallbacks(() =>
                    {
                        SoundManager.Instance.PlaySFX("finishforgold");
                        SoundManager.Instance.PlaySFX("upgradeequip");
                        _epsController.SendUpgradeEquipmentRequest(model,true);
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.CloakRoom);

                    });
                    _equipmentUpgradeStarted = false;
                }
                else
                {
                    _equipmentUpgradeStarted = false;
                    PopupsManager.Instance.AddPopup(PopupType.NotEnoughMoneyPopup)
                    .SetTitle("game.main.popup.title.notenoughmoney")
                    .SetButtonLabels(LocaleManager.Instance.GetText("game.main.building.shop").ToUpper())
                    .SetCallbacks(() =>
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    });
                }
            }
        }


        private void OnEquipmentSelectedHandler(EquipmentModel eqp)
        {
          _selectedEquipment = eqp;
            if (_selectedEquipment.IsUpgrading)
            {
                FinishUpgradeEquipment(_selectedEquipment,false);
            }
            else
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.EquipmentUpgradeInfo);
            }
        }

        private bool NeedFinishEquipmentUpgrade()
        {
            EquipmentModel currentUpgrade = _epsController.GetCurrentEquipmentUpgrade();
            if (currentUpgrade != null)
            {
                string title = "game.main.popup.title.cannotupgrade";
                string slvl = LocaleManager.Instance.GetText("game.common.shortlevel2", currentUpgrade.Level + 1);
                string upgradeName = settings.GetEquipmentData(currentUpgrade.Id).GetName();
                PopupsManager.Instance.AddPopup(PopupType.LockedToChangePopup)
                    .SetTitle(title)
                    .SetText(LocaleManager.Instance.GetText("game.main.popup.textfield.upgradeprocess", upgradeName + LocaleManager.SPACE + slvl))
                    .SetTextIconControls(GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds))
                    .SetCallbacks(() =>
                    {
                        FinishUpgradeEquipment(currentUpgrade, true);

                    }).SetMinutesTickUpdate((popup) =>
                    {
                        popup.SetTextIconControls(
                            GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds));
                    });
                return true;
            }
            return false;
        }

        #endregion

        #region Personal

        private bool _employeeUpgradeStarted = false;
       

        private void OnEmployeeSelectedHander(EmployeeModel model)
        {
            if (!model.IsUpgrading)
            {
                if (!_employeeUpgradeStarted)
                {
                    _employeeUpgradeStarted = true;
                    UpgradeEmployee(model);
                }
               
            }
            else
            {
                FinishUpgradeEmployee(model,false);
            }
        }


        private void UpdatePersonalList()
        {

           
            //if(_personalBuilded)return;
            //_personalBuilded = true;
            _epsController.GetEmployees((employees) =>
            {

               // personalList.content.RemoveChildren();
                for (int i = 0; i < employeesRenderers.Length; i++)
                {
                   // EmployeeItemRenderer itemRenderer = personalList.content.AddChild(employeePrefabRenderer, false).GetComponent<EmployeeItemRenderer>();
                    employeesRenderers[i].SetModel(employees[i],_teamController.TeamData.level);
                    
                }
                personalList.verticalNormalizedPosition = 1f;

                CheckTutorialLastStep();

            });
        }

        public void OnTacticBonusClickHandler()
        {
            EmployeeModel mainCouch = _epsController.GetEmployee(EmployeeType.MainCouch);
            TacticBonusSelectorPopup popup = PopupsManager.Instance.AddPopup(PopupType.ChangeTacticBonusPopup) as TacticBonusSelectorPopup;
            popup.SetData(mainCouch)
                .SetCallbacks(() =>
                {
                    if (!mainCouch.BonusState.Equals(popup.SelectedTactic))
                    {
                        _epsController.ChangeMainCouchTactic(popup.SelectedTactic,mainCouch);
                    }
                });
        }

        private void CheckTutorialLastStep()
        {
            if (!GameData.IsTutorialPassed)
            {
                if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.FinishMainCouch))
                {
                    EmployeeModel model = _epsController.GetEmployee(EmployeeType.MainCouch);
                    if (!model.IsUpgrading)
                    {
                        if (TutorialManager.Instance.IsNextStepEmpty())
                        {
                            TutorialManager.Instance.NextStep(2);
                        }
                        else
                        {
                            TutorialManager.Instance.NextStep();
                        }
                      
                    }
                }

            }
        }

        private void ClearPersonalList()
        {
           // _personalBuilded = false;
            foreach (Transform child in personalList.content.transform)
            {
                Destroy(child.gameObject);
            }
            
        }

       

        private void UpgradeEmployee(EmployeeModel model)
        {
            if (model != null)
            {
                if (NeedFinishEmployeeUpgrade())
                {
                    _employeeUpgradeStarted = false;
                    return;
                }

                int teamLevel = _teamController.TeamData.level;
                if (teamLevel < model.UnlockLevel)
                {
                    _employeeUpgradeStarted = false;
                    string title = LocaleManager.Instance.GetText("game.main.popup.title.needteamlevel", LocaleManager.Instance.GetText("game.common.shortlevel2", model.UnlockLevel));

                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                        .SetTitle(title)
                        .HideButtons(true, false);
                       
                    return;
                }
                if (_teamController.CanBuyForCoins(model.UpgradePrice))
                {
                    SoundManager.Instance.PlaySFX("hire");
                    _epsController.SendUpgradeEmployeeRequest(model, false, () =>
                    {
                        _employeeUpgradeStarted = false;
                        ActiveProcessesManager.Instance.AddUpgradeProcess(model);
                    });

                }
                else if (_teamController.CanBuyForGold(model.UpgradeGoldPrice))
                {
                    PopupsManager.Instance.AddPopup(PopupType.NotEnoughFansCoinsPopup).
                    SetTitle("game.main.popup.title.notenoughmoney").
                    SetTextIconControls(model.UpgradeGoldPrice).
                    SetButtonLabels("game.main.teach", "game.common.closebutton").
                    SetCallbacks(() =>
                    {
                        SoundManager.Instance.PlaySFX("finishforgold");
                        SoundManager.Instance.PlaySFX("hire");
                        _epsController.SendUpgradeEmployeeRequest(model, true);
                        

                    });
                    _employeeUpgradeStarted = false;
                }
                else
                {
                    PopupsManager.Instance.AddPopup(PopupType.NotEnoughMoneyPopup)
                   .SetTitle("game.main.popup.title.notenoughmoney")
                   .SetButtonLabels(LocaleManager.Instance.GetText("game.main.building.shop").ToUpper())
                   .SetCallbacks(() =>
                   {
                       PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                   });
                    _employeeUpgradeStarted = false;
                }
            }
        }

        private void FinishUpgradeEmployee(EmployeeModel model,bool upgradeimmidiate)
        {
            string emplName = settings.GetEmployeeData(model.Id).GetName();
            string time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(model.EndUpgradeTimeSeconds));
            string title = LocaleManager.Instance.GetText("game.main.teaching") + ". " + emplName + ".";
            int goldPrice = GameData.GetTimerGoldPrice(model.EndUpgradeTimeSeconds);

            UnityAction callback = () =>
            {
                if (_teamController.CanBuyForGold(goldPrice))
                {
                    _epsController.EndUpgradeEmployeeRequest(() =>
                    {
                        SoundManager.Instance.PlaySFX("finishforgold");
                        // PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Personal);
                    });
                }
                else
                {
                    PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                }
            };

            if (upgradeimmidiate)
            {
                callback();
            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.FinishUpgradePopup)
                  .SetTitle(title)
                  .SetTextIconControls(goldPrice.ToString(), time)
                  .LockButtons(false,!GameData.IsTutorialPassed)
                  .SetCallbacks(callback)
                  .SetMinutesTickUpdate((popup) =>
                  {
                      goldPrice = GameData.GetTimerGoldPrice(model.EndUpgradeTimeSeconds);
                      time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(model.EndUpgradeTimeSeconds));
                      popup.SetTextIconControls(goldPrice.ToString(), time);
                  });
            }
        }

        private bool NeedFinishEmployeeUpgrade()
        {
            EmployeeModel currentUpgrade = _epsController.GetCurrentEmployeeUpgrade();
            if (currentUpgrade != null)
            {
                string title = "game.main.popup.title.cannothire";
                string slvl = LocaleManager.Instance.GetText("game.common.shortlevel2", currentUpgrade.NextUpgradeLevel);
                string upgradeName = settings.GetEmployeeData(currentUpgrade.Id).GetName();
                PopupsManager.Instance.AddPopup(PopupType.LockedToChangePopup)
                    .SetTitle(title)
                    .SetText(LocaleManager.Instance.GetText("game.main.popup.textfield.hireprocess", upgradeName + LocaleManager.SPACE + slvl))
                    .SetTextIconControls(GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds))
                    .SetCallbacks(() =>
                    {
                        FinishUpgradeEmployee(currentUpgrade, true);

                    }).SetMinutesTickUpdate((popup) =>
                    {
                        popup.SetTextIconControls(
                            GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds));
                    });
                return true;
            }
            return false;
        }
        #endregion

        #region Skills

        private void OnResetSkillsClickHandler()
        {
           
                PopupsManager.Instance.AddPopup(PopupType.ResetSkillPointsPopup)
                .SetTextIconControls(GameData.GSConstData.ResetSkillsGoldPrice)
                .SetCallbacks(() =>
                {
                    if (_teamController.CanBuyForGold(GameData.GSConstData.ResetSkillsGoldPrice))
                    {
                        _epsController.ResetSkillPoints(SkillsChanged);

                    }
                    else
                    {
                        PageNavigatorManager.Instance.CancelMovePage = false;
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    }
                });
        }

        private void OnApplySkillsClickHandler()
        {
            applySkillsButton.gameObject.SetActive(false);
            _epsController.UpgradeSkills(SkillsChanged, BuildSKills);
            PageNavigatorManager.Instance.CancelMovePage = false;
        }


        private void OnAddSkillPointHandler(SkillModel model)
        {
            _epsController.AddPoint(model);
            SkillsChanged();
        }



        private void BuildSKills()
        {
            skillRenderersList.RemoveChildren();
            _epsController.LoadSkills((skills) =>
            {  
                for (int i = 0; i < skills.Length; i++)
                {
                    RectTransform rchild = skillRenderersList.AddChild(skillItemRendererPrefab, false);
                    rchild.GetComponent<SkillItemRenderer>().SetModel(skills[i]);
                }
                SkillsChanged();
            });
        }

        private void SkillsChanged()
        {
            leftSkillPoints.text = LocaleManager.Instance.GetText("game.main.skills.pointsleft",_epsController.SkillPoints); 
            usedSkillPoints.text = LocaleManager.Instance.GetText("game.main.skills.totalpoints",_epsController.UsedSkillPoints);
            applySkillsButton.gameObject.SetActive(_epsController.SkillPointsChanged);
            resetSkillsButton.gameObject.SetActive(_epsController.CanResetSkillPoints());
            PageNavigatorManager.Instance.CancelMovePage = _epsController.SkillPointsChanged;
            // totalSkillPointsLabel.text = _epsController.SkillPoints.ToString();
            //distributedSkillPointsLabel.text = _epsController.DistributedPoints().ToString();

            if (!GameData.IsTutorialPassed)
            {
                if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.DistributeSkillPoints))
                {
                    if (_epsController.SkillPointsChanged)
                    {
                        TutorialManager.Instance.ShowBackground();
                        TutorialManager.Instance.CurrentView.HideArrow();
                    }
                    if (_epsController.SkillPoints == 0)
                    {
                        TutorialManager.Instance.NextStep();
                    }
                }
            }

        }

        #endregion

       
    }
}
