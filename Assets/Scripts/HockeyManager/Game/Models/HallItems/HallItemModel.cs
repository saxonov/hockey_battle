﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine.Events;

namespace HockeyManager.Game.Models.HallItems
{

    public struct SlotSaveData
    {
        public string sc;
        public int id;
        public bool active;

        public SlotSaveData(int id,string sc,bool active)
        {
            this.id = id;
            this.sc = sc;
            this.active = active;
        }
    }

    public class HallItemModel:IGSParse,IEffect,IComparable<HallItemModel>
    {
        public UnityEvent OnModelChanged = new UnityEvent();

        public const string EnergyBoosterEffect = "energyTime%";
        public const string AttackBoosterEffect = "attack%";
        public const string DeffenceBoosterEffect = "deffence%";
        public const string FansBoosterEffect = "fans%";

        private string _shortCode;
        private int _id;
        private int _slot;
        private bool _active;
        private bool _isOwned;
        private int _currencyType;
        private int _moneyPrice = -1;
        private int _goldPrice = -1;
        private bool _disabled = false;

        private GSData _effect;

        private int _timeLimit;
        
        public int TimeLimit
        {
            get { return _timeLimit; }
        }

        public int CurrencyType
        {
            get { return _currencyType; }
            set { _currencyType = value; }
        }

        public bool IsOwned
        {
            get { return _isOwned; }
        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public int Slot
        {
            get { return _slot; }
            set { _slot = value; }
        }

        public int Id
        {
            get { return _id; }
        }

        public string ShortCode
        {
            get { return _shortCode; }
        }

        public bool Disabled
        {
            get { return _disabled; }
        }

        public int GoldPrice
        {
            get { return _goldPrice; }
        }

        public int MoneyPrice
        {
            get { return _moneyPrice; }
        }

        public HallItemModel(GSData data)
        {
            _id = data.GetInt("id").Value;
            _shortCode = data.GetString("name");
            Parse(data);

         
        }

        public void Parse(GSData data)
        {

            _active = data.GetBoolean("active").HasValue && data.GetBoolean("active").Value;
            _isOwned = data.GetBoolean("isOwned").HasValue && data.GetBoolean("isOwned").Value;
            
            _timeLimit = data.GetInt("timeLimit").Value;
            _slot = data.GetInt("slot").Value;

            _disabled = data.GetBoolean("disabled").Value;
            _moneyPrice = data.GetInt("coinsPrice").HasValue ? data.GetInt("coinsPrice").Value : -1;
            _goldPrice = data.GetInt("goldPrice").HasValue ? data.GetInt("goldPrice").Value : -1;
            _currencyType = data.GetInt("currencyType").HasValue ? data.GetInt("currencyType").Value : 0;

            _effect = data.GetGSData("effect");
            OnModelChanged.Invoke();
        }

        public float GetEffectValue(string prop)
        {
            if (_effect != null && _effect.ContainsKey(prop))
            {
                return _effect.GetInt(prop).Value;
            }
            return 0f;
        }

        public float GetEffectValue(params string[] prop)
        {
            return 0f;
        }
        
        public int CompareTo(HallItemModel other)
        {
            if (_isOwned && _currencyType < other.CurrencyType)
            {
                return -1;
            }
            return 1;

        }
    }
}
