﻿using UnityEngine;
using System.Collections;

namespace SCTools
{
    public class OptimizedBehavior : MonoBehaviour
    {
        private Transform _cachedTransform;


        public Transform CachedTransform
        {
            get
            {
                if (_cachedTransform == null)
                {
                    _cachedTransform = gameObject.transform;
                }
                return _cachedTransform;
            }
        }
    }


}
