﻿using System;
using HockeyManager.Game.Models.Competitions;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.UI.Popups;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Views.Popups
{
    public class CompetitionRewardsPopup:BasePopup
    {
        [Serializable]
        public struct WinnerPostiionRewards
        {
            public TextIconControl moneyReward;
            public TextIconControl goldRewards;
        }

        [SerializeField]private TextIconControl titlePanel;
        [SerializeField] private WinnerPostiionRewards[] positionRewards;

        private CompetitionModel _model;

        protected override void UpdateData()
        {
            _model = (CompetitionModel) _data;

            string title = null;
            Sprite icon = GameSettings.Instance.GetIconTextCompetitionType(_model.Type,out title);

            titlePanel.SetText(title);
            titlePanel.SetIcon(icon);

            for (int i = 0; i < _model.Rewards.Count; i++)
            {
                positionRewards[i].moneyReward.SetText(_model.Rewards[i].coins);
                positionRewards[i].goldRewards.SetText(_model.Rewards[i].gold);
            }

            SetText(LocaleManager.Instance.GetText("game.main.competitions.popup.title3.1arg", positionRewards.Length));

        }
    }
}
