﻿using HockeyManager.Game;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Views.Components;
using HockeyManager.TutorialSystem.Base;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.TutorialSystem
{
    [RequireComponent(typeof(Image))]
    public class TutorialFooterButton:TutorialObject
    {
#pragma warning disable CS0649 // Полю "TutorialFooterButton.footerNavButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private FooterNavButton footerNavButton;
#pragma warning restore CS0649 // Полю "TutorialFooterButton.footerNavButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        void Start()
        {
            if (GameData.IsTutorialPassed)
            {
                Destroy(gameObject);
            }
        }
        

        public override void OnUpState()
        { 
            footerNavButton.Select();
            footerNavButton.OnPointerClick(null);
        }

        public override void OnDownState()
        {
            footerNavButton.Select();
        }
    }
}
