﻿using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using HockeyManager.TutorialSystem;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class PvPPage:MonoBehaviour
    {
        private IGameSettings settings;

        [Header("Matches")]

        [SerializeField] private RectTransform battleStates;
        [SerializeField] private RectTransform acceptFooter;
        [SerializeField] private Text battleInfoFooterText;
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button cancelButton;
        [SerializeField] private TextIconControl acceptBonusPanel;
        [SerializeField] private TextIconControl cancelPenaltyPanel;
        [SerializeField] private Text battleTimerText;
        [SerializeField] private Text enemyTeamInfoText;
        [SerializeField] private LogoImage enemyTeamLogo;
        
        [SerializeField] private HSVShaderTuner enemyTeamLogoTuner;
        [SerializeField] private Text myTeamInfoText;
        [SerializeField] private LogoImage myTeamLogo;
        [SerializeField] private HSVShaderTuner myTeamLogoTuner;
       
        [SerializeField] private RectTransform teamsList;
        [SerializeField] private GameObject enemyTeamRendererPrefab;
        [SerializeField] private RectTransform footer;
        [SerializeField] private Button refreshButton;


        [SerializeField] private Text mainDescriptionTitle;

        [SerializeField] private RectTransform pvpContent;

        [Space(5)] [SerializeField] private InnerPageNavigation navBar;

        private TeamController _team;
        private MatchBattleController _controller;

        private long timeLeftSeconds = 0;
        private string timeStr;
        private bool _pvpLogosBuilded = false;

        void Awake()
        {
            settings = GameSettings.Instance;
            pvpContent.gameObject.SetActive(false);

            _team = GameRoot.Instance.GetController<TeamController>();
            _controller = GameRoot.Instance.GetController<MatchBattleController>();
            
            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);

            EventsManager.Instance.Add(GlobalEventTypes.GameUnpaused,OnGameUnpaused);
            EventsManager.Instance.Add<string>(EnemyItemRenderer.EnemyTeamSelectedToAttack,OnEnemyAttackHandler);
            EventsManager.Instance.Add(MatchBattleController.MatchStartedEvent, OnMatchStartedHandler);
            EventsManager.Instance.Add<GSData>(MatchBattleController.MatchYouWinnerEvent1arg, OnMatchWinHandler);
            EventsManager.Instance.Add<GSData>(MatchBattleController.MatchYouLooseEvent1arg, OnMatchLooseHandler);
            EventsManager.Instance.Add(MatchBattleController.MatchCanceledEvent, OnMatchCanceledHandler);
            EventsManager.Instance.Add<MatchPlayerModel>(MatchBattleController.MatchYouChallengedEvent, OnMatchChallengedHandler);
        }

        private void OnGameUnpaused()
        {
            //if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Matches)
            //{
            //   Debug.Log("MatchPage unpaused!");
            //   ChangeBattleState();
            //}    
        }


        private void OnMatchStartedHandler()
        {
            if (_controller.CurrentMatchBattleState == MatchBattleStates.Battle)
            {
                SoundManager.Instance.PlaySFXOnGameObject("battlestarted", gameObject);

                if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Matches)
                {
                    //PopupsManager.Instance.RemovePopup(PopupType.MatchNotificationPopup);
                    PopupsManager.Instance.AddPopup(PopupType.MatchNotificationPopup)
                        .SetData(_controller.MatchBattleModel.GetEnemyTeam())
                        .SetTitle(
                            LocaleManager.Instance.GetText("game.common.battleinprocess.1arg",
                                LocaleManager.Instance.GetText("game.main.matchpage.title")).ToUpper(), false)
                        .SetCallbacks(() => { PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Matches); });
                }
                else
                {
                    ChangeBattleState();
                }
            }
           
            
        }

        public void OnShow()
        {
            //ResetStates();
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Matches)
            {
                navBar.SelectButton(1,true);
                _controller.GetMatchInfo(ChangeBattleState);
            }
        }

        public void OnHide()
        {
            _pvpLogosBuilded = false;
            pvpContent.gameObject.SetActive(false);
        }

        void Start()
        {
            refreshButton.onClick.AddListener(OnRefreshTeamsList);
            acceptButton.onClick.AddListener(OnMatchAcceptHandler);
            cancelButton.onClick.AddListener(OnMatchCancelHandler);

            mainDescriptionTitle.gameObject.SetActive(false);

            OnGameReadyHandler();
           
        }

        private void OnGameReadyHandler()
        {
           // EventsManager.Instance.Remove(GlobalEventTypes.UserAuthenticated, OnGameReadyHandler);
            if (_team.CurrentActionState == GameActionState.PlayMatch)
            {
                _controller.GetMatchInfo(InitializeBattleState,true);
            }
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Add(TutorialManager.TutorialComplete, OnTutorialCompleteHandler);
            }
            //_controller.CheckMatchBattleRestartActivity();
        }

        private void InitializeBattleState()
        {
            ChangeBattleState();
        }


        private void OnTutorialCompleteHandler()
        {
            EventsManager.Instance.Remove(TutorialManager.TutorialComplete,OnTutorialCompleteHandler);
           _controller.GetMatchInfo(ChangeBattleState);
        }

        private void OnDestroy()
        {
            refreshButton.onClick.RemoveListener(OnRefreshTeamsList);
            acceptButton.onClick.RemoveListener(OnMatchAcceptHandler);
            cancelButton.onClick.RemoveListener(OnMatchCancelHandler);

            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);

            EventsManager.Instance.Remove(GlobalEventTypes.GameUnpaused, OnGameUnpaused);
            EventsManager.Instance.Remove<string>(EnemyItemRenderer.EnemyTeamSelectedToAttack, OnEnemyAttackHandler);
            EventsManager.Instance.Remove<GSData>(MatchBattleController.MatchYouWinnerEvent1arg, OnMatchWinHandler);
            EventsManager.Instance.Remove(MatchBattleController.MatchStartedEvent, OnMatchStartedHandler);
            EventsManager.Instance.Remove<GSData>(MatchBattleController.MatchYouLooseEvent1arg, OnMatchLooseHandler);
            EventsManager.Instance.Remove(MatchBattleController.MatchCanceledEvent, OnMatchCanceledHandler);
            EventsManager.Instance.Remove<MatchPlayerModel>(MatchBattleController.MatchYouChallengedEvent, OnMatchChallengedHandler);
        }


        private void OnMatchChallengedHandler(MatchPlayerModel enemyTeam)
        {
            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Matches)
            {
                //PopupsManager.Instance.RemovePopup(PopupType.MatchWinLoosePopup);
                //PopupsManager.Instance.RemovePopup(PopupType.MatchNotificationPopup);
                SoundManager.Instance.PlaySFX("challenged");
                PopupsManager.Instance.AddPopup(PopupType.MatchNotificationPopup)
                    .SetData(enemyTeam)
                    .SetTitle(LocaleManager.Instance.GetText("game.main.match.notify.youchallengedtitle").ToUpper())
                    .SetCallbacks(() => { PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Matches); });
            }
            else
            {
                SoundManager.Instance.PlaySFX("challenged");
                ChangeBattleState();
            }
        }


        private void OnTikTakHandler()
        {
           
                switch (_controller.CurrentMatchBattleState)
                {
                    case MatchBattleStates.Attacked:
                    case MatchBattleStates.Attack:
                        if (_controller.MatchBattleModel != null)
                        {
                            timeLeftSeconds = GameTimer.TimeLeftSeconds(_controller.MatchBattleModel.EndAcceptTime);
                            timeStr = MathUtils.SecondsToTimeString(timeLeftSeconds);

                            battleTimerText.text = LocaleManager.Instance.GetText("game.main.matchpage.matchstarttimer").ToUpper() + "\n" + timeStr.GetUITextSizeFormated(80);

                            if (_controller.CurrentMatchBattleState == MatchBattleStates.Attacked)
                            {
                                int firstMinute = _controller.MatchBattleModel.AcceptTimeSeconds - 60;
                                if (timeLeftSeconds >= firstMinute)
                                {
                                    acceptBonusPanel.SetText(_controller.MatchBattleModel.AcceptDeffenceBonusBefore.ToString().AddPostfix('%').AddPrefix('+'));
                                }
                                else
                                {
                                    acceptBonusPanel.SetText(_controller.MatchBattleModel.AcceptDeffenceBonusAfter.ToString().AddPostfix('%').AddPrefix('+'));
                                }
                            }

                        }

                        break;
                    case MatchBattleStates.Battle:

                        if (_controller.MatchBattleModel != null)
                        {
                            timeLeftSeconds = GameTimer.TimeLeftSeconds(_controller.MatchBattleModel.EndBattleTime);
                            timeStr = MathUtils.SecondsToTimeString(timeLeftSeconds);
                            
                            battleTimerText.text = LocaleManager.Instance.GetText("game.main.matchpage.matchendtimer").ToUpper() + "\n" + timeStr.GetUITextSizeFormated(80);
                        }
                        break;
                }
            
        }

        private void OnMatchCanceledHandler()
        {
            bool isInMatchPage = PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Matches;
            PopupsManager.Instance.AddPopup(PopupType.MatchCanceledPopup).HideButtons(isInMatchPage, false)
                .SetCallbacks(() =>
                {
                    //_controller.GainCancelPenaltyReward(model);
                    //VisualEffectsManager.Instance.FlyRewards(popup.gameObject.transform.position, model.RewardData);
                    PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Matches);

                }, () =>
                {
                    //  _controller.GainCancelPenaltyReward(model);
                    // VisualEffectsManager.Instance.FlyRewards(popup.gameObject.transform.position, model.RewardData);
                    if (isInMatchPage)
                    {
                        ChangeBattleState();
                    }
                    // PopupsManager.Instance.RemovePopup();
                });
        }

        private void OnMatchLooseHandler(GSData result)
        {
            string score = result.GetString("score");
            int lostFans = result.GetInt("fansLost").Value;
            int coinsLost = result.GetInt("coinsLost").Value;
           // PopupsManager.Instance.RemovePopup(PopupType.MatchNotificationPopup);
            string title = LocaleManager.Instance.GetText("game.main.matchcompletepopup.loosetitle");
            PopupsManager.Instance.AddPopup(PopupType.MatchWinLoosePopup)
                .SetTitle(title.ToUpper().GetUITextColorFormated(UISettings.BlockTextColor), false)
                .SetText(score, false)
                .SetHolderVisibility("PenaltyBox", true)
                .SetVisibilityTextIconControl("CoinsPenaltyPanel", Mathf.Abs(coinsLost) > 0)
                .SetTextIconControls(coinsLost, lostFans);

            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Matches)
            {
                _controller.GetMatchInfo(ChangeBattleState);
            }
        }

        private void OnMatchWinHandler(GSData result)
        {
            string score = result.GetString("score");
           // PopupsManager.Instance.RemovePopup(PopupType.MatchNotificationPopup);
            SoundManager.Instance.PlaySFXOnGameObject("battlewinner",gameObject);
            string title = LocaleManager.Instance.GetText("game.main.matchcompletepopup.wintitle").ToUpper();
            PopupsManager.Instance.AddPopup(PopupType.MatchWinLoosePopup)
                .SetTitle(title.GetUITextColorFormated(UISettings.GreenTextColor),false)
                .SetText(score, false);
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Matches)
            {
                _controller.GetMatchInfo(ChangeBattleState);
            }
        }

        private void ResetStates()
        {
            mainDescriptionTitle.gameObject.SetActive(false);
            battleStates.gameObject.SetActive(false);
            acceptFooter.gameObject.SetActive(false);
            battleInfoFooterText.gameObject.SetActive(false);
            teamsList.gameObject.SetActive(false);
            footer.gameObject.SetActive(false);
        }

        private void ChangeBattleState()
        {
            MatchBattleModel battle = _controller.MatchBattleModel;
            MatchBattleStates state = _controller.CurrentMatchBattleState;
            Debug.Log("MatchBatltleState changed: " + state);
            switch (state)
            {
                case MatchBattleStates.Selection:

                    _pvpLogosBuilded = false;
                    if (_controller.MatchSelectionModel != null) 
                    {
                        refreshButton.interactable = true;
                        battleStates.gameObject.SetActive(false);
                        teamsList.gameObject.SetActive(true);
                        footer.gameObject.SetActive(true);
                        mainDescriptionTitle.gameObject.SetActive(true);
                        mainDescriptionTitle.color = Color.white;
                        if (_team.TeamData.fans == 0)
                        {
                            mainDescriptionTitle.color = UISettings.BlockTextColor;
                            mainDescriptionTitle.text = LocaleManager.Instance.GetText("game.main.matchpage.nofans.description");

                        }
                        else
                        {
                            mainDescriptionTitle.text = LocaleManager.Instance.GetText("game.main.matchpage.description");
                        }

                       

                        int count = 0;
                        teamsList.RemoveChildren();
                        List<MatchPlayerModel> teams = _controller.MatchSelectionModel.Enemies;
                        foreach (var matchPlayerModel in teams)
                        {
                            count++;
                            EnemyItemRenderer itemRenderer = teamsList.AddChild(enemyTeamRendererPrefab, false).GetComponent<EnemyItemRenderer>();
                            itemRenderer.ShowSelection((count%2) == 0);
                            itemRenderer.SetData(matchPlayerModel, _controller.MatchSelectionModel.EnergyCost, _controller.MatchSelectionModel.BattleTimeSeconds);
                        }
                    }
                break;
                case MatchBattleStates.Attacked:
                case MatchBattleStates.Attack:
                case MatchBattleStates.Battle:

                    if (battle != null)
                    {
                        mainDescriptionTitle.color = Color.white;
                        mainDescriptionTitle.gameObject.SetActive(true);
                        battleStates.gameObject.SetActive(true);
                        acceptFooter.gameObject.SetActive(state == MatchBattleStates.Attacked);
                        battleInfoFooterText.gameObject.SetActive(state == MatchBattleStates.Attack || state == MatchBattleStates.Battle);
                        teamsList.gameObject.SetActive(false);
                        footer.gameObject.SetActive(false);

                        if (state == MatchBattleStates.Attacked)
                        {
                            acceptButton.interactable = true;
                            mainDescriptionTitle.text = LocaleManager.Instance.GetText("game.main.matchpage.matchchallengedtoyou").ToUpper().GetUITextSizeFormated(80);
                        }
                        else if (state == MatchBattleStates.Attack)
                        {
                            mainDescriptionTitle.text = LocaleManager.Instance.GetText("game.main.matchpage.youchallengedmatch.title").ToUpper().GetUITextSizeFormated(80);
                            battleInfoFooterText.text = LocaleManager.Instance.GetText("game.main.matchpage.youchallengedmatch.desc");
                        }
                        else if (state == MatchBattleStates.Battle)
                        {
                            mainDescriptionTitle.text = LocaleManager.Instance.GetText("game.common.battleinprocess.1arg", LocaleManager.Instance.GetText("game.main.matchpage.title")).ToUpper().GetUITextSizeFormated(80);
                            battleInfoFooterText.text = LocaleManager.Instance.GetText("game.main.matchpage.matchstarted.desc");
                        }

                        BuildPvpLogos(battle);

                        if (state != MatchBattleStates.Battle)
                        {
                            cancelPenaltyPanel.SetText(-battle.CancelPenaltyPrice);
                        }
                       
                        //OnTikTakHandler();
                    }
                    break;
            }

            if (!pvpContent.gameObject.activeSelf && PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Matches)
            {
                pvpContent.gameObject.SetActive(true);
            }
        }

        private void BuildPvpLogos(MatchBattleModel battle)
        {

            MatchPlayerModel enemy = battle.GetEnemyTeam();
            MatchPlayerModel me = battle.GetMyTeam();

            if (!_pvpLogosBuilded)
            {
               
                enemyTeamLogo.sprite = settings.GetLogo256(enemy.TeamInfo.Logo);

                enemyTeamLogoTuner.ChangeHUE(enemy.TeamInfo.LogoColor);
                enemyTeamLogoTuner.ChangeSaturation(enemy.TeamInfo.LogoSaturation);


               
                myTeamLogo.sprite = settings.GetLogo256(me.TeamInfo.Logo);

                myTeamLogoTuner.ChangeHUE(me.TeamInfo.LogoColor);
                myTeamLogoTuner.ChangeSaturation(me.TeamInfo.LogoSaturation);
                _pvpLogosBuilded = true;

                Debug.LogFormat("PVPPage Logos Changes - me:{0} , enemy:{1}",me.TeamInfo.UserId,enemy.TeamInfo.UserId);
            }
           

            string attackedLvl = LocaleManager.Instance.GetText("game.common.shortlevel2", enemy.Level).GetUITextSizeFormated(60).GetUITextColorFormated(UISettings.GreenTextColor);
            string leaderPos = LocaleManager.Instance.GetText("game.common.rankposition", enemy.LeaderBoardPosition).GetUITextSizeFormated(50);
            enemyTeamInfoText.text = enemy.TeamInfo.Name.GetUITextSizeFormated(70) + "\n" + attackedLvl + "\n" + leaderPos;

            string myLevel = LocaleManager.Instance.GetText("game.common.shortlevel2", me.Level).GetUITextSizeFormated(60).GetUITextColorFormated(UISettings.GreenTextColor);
            string myPos = LocaleManager.Instance.GetText("game.common.rankposition", me.LeaderBoardPosition).GetUITextSizeFormated(50);
            myTeamInfoText.text = me.TeamInfo.Name.GetUITextSizeFormated(70) + "\n" + myLevel + "\n" + myPos;

            _pvpLogosBuilded = true;
        }
        
        private void OnRefreshTeamsList()
        {
            refreshButton.interactable = false;
            _controller.GetMatchInfo(ChangeBattleState, true);
        }

        private void OnMatchAcceptHandler()
        {
            acceptButton.interactable = false;
            _controller.AcceptMatch(ChangeBattleState);
        }

        private void OnMatchCancelHandler()
        {
            //PopupsManager.Instance.RemovePopup(PopupType.MatchNotificationPopup);
            int penaltyPrice = _controller.MatchBattleModel.CancelPenaltyPrice;
            PopupsManager.Instance.AddPopup(PopupType.MatchCancelPopup)
                .SetTextIconControls(_controller.MatchBattleModel.CancelPenaltyPrice)
                .LockButtons(!_team.CanBuyForCoins(penaltyPrice))
                .SetCallbacks(() =>
                {
                    _controller.CancelMatch(() => { _controller.GetMatchInfo(ChangeBattleState); });
                });
        }



        private void OnEnemyAttackHandler(string userId)
        {
            _team.UpdateEnergy(_controller.MatchSelectionModel.EnergyCost, () =>
            {
                if (_team.CanBuyForEnergy(_controller.MatchSelectionModel.EnergyCost))
                {
                    _controller.AttackTeam(userId, (success) =>
                    {
                        if (success)
                        {
                            SoundManager.Instance.PlaySFX("challenged");
                            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Matches)
                            {
                                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Matches);
                            }
                            else
                            {
                                ChangeBattleState();
                            }
                        }
                        else
                        {
                            _controller.GetMatchInfo(ChangeBattleState,true);
                        }
                    });
                }
                else
                {
                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                    .SetTitle("game.main.popup.title.notenoughenergy").HideButtons(true, false);
                }
            });
        }
        
    }
}
