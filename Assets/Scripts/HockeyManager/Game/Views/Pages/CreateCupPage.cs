﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using SCTools.Localization;
using SCTools.SoundManager;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class CreateCupPage:MonoBehaviour
    {
        [SerializeField] private InputField cupNameField;
        [SerializeField] private Slider priceFeeSlider;
        [SerializeField] private Text priceFeeValue;
        [SerializeField] private TextIconControl rewardPanel;
        [SerializeField] private Text minLevelLabel;
        [SerializeField] private Text maxLevelLabel;
        [SerializeField] private TextIconControl energyCost;
        [SerializeField] private TextIconControl timerPanel;
        [SerializeField] private Button createButton;
        [SerializeField] private Toggle goldCurrencyButton;
        [SerializeField] private Toggle moneyCurrencyButton;
        [SerializeField] private GameObject cupSelectorButtonPrefab;
        [SerializeField] private RectTransform maxPlayersContent;
        [SerializeField] private ToggleGroup maxPlayerSelectorsGroup;
        [SerializeField] private InnerPageNavigation navBar;

        private int minLevelRange = 0;
        private int maxLevelRange = 0;

        private List<Toggle> _playerSelectorToggles;

        private TournamentCupSettingsModel _cupSettingsModel;

        private TeamController _team;
        private TournamentBattleController _controller;

        private TournamentBattleModel _newCupModel = null;

        void Awake()
        {

           // intRp = new Regex(@"^["",1-9]");
            _playerSelectorToggles = new List<Toggle>();
            _controller = GameRoot.Instance.GetController<TournamentBattleController>();
            _team = GameRoot.Instance.GetController<TeamController>();
        }

        void Start()
        {
           // moneyCurrencyButton.isOn = true;

            rewardPanel.SetIcon(GameSettings.Instance.GetIcon(GameData.CurrencyCoins));
            rewardPanel.SetText(0);
        }

     

        private void OnGoldCurrencySelected(bool value)
        {
            if (value)
            {
                _newCupModel.CurrencyType = GameData.CurrencyGold;
                rewardPanel.SetIcon(GameSettings.Instance.GetIcon(_newCupModel.CurrencyType));
            }
        }

        private void OnMoneyCurrencySelected(bool value)
        {
            if (value)
            {
                _newCupModel.CurrencyType = GameData.CurrencyCoins;
                rewardPanel.SetIcon(GameSettings.Instance.GetIcon(_newCupModel.CurrencyType));
            }
        }

        public void OnShow()
        {
            cupNameField.onEndEdit.AddListener(OnEndEditCupName);
            cupNameField.onValidateInput += OnValidateCupNameInput;
            createButton.onClick.AddListener(OnCreateClickHandler);
            goldCurrencyButton.onValueChanged.AddListener(OnGoldCurrencySelected);
            moneyCurrencyButton.onValueChanged.AddListener(OnMoneyCurrencySelected);
            priceFeeSlider.onValueChanged.AddListener(OnPriceChangedHandler);
            InitializeInfo();
        }

        private char[] checkMask = "-()[]!:.".ToCharArray();

        private char OnValidateCupNameInput(string text, int charindex, char addedchar)
        {
            if (char.IsLetterOrDigit(addedchar) || char.IsWhiteSpace(addedchar) || checkMask.Contains(addedchar))
            {
                return addedchar;
            }
            return '\0';
        }


        private void OnPriceChangedHandler(float value)
        {

            _newCupModel.FeePrice = (int) value*_cupSettingsModel.PriceFeePerCount;
            priceFeeValue.text = _newCupModel.FeePrice.ToString();
            _newCupModel.CurrencyAward = _newCupModel.FeePrice * _newCupModel.MaxPlayers;
            rewardPanel.SetText(_newCupModel.CurrencyAward);
        }

        public void ChangeMinLevel(int dir)
        {
            _newCupModel.MinLevel += dir;
            _newCupModel.MinLevel = Mathf.Clamp(_newCupModel.MinLevel, minLevelRange, _team.TeamData.level);
            minLevelLabel.text = _newCupModel.MinLevel.ToString();
;       }

        public void ChangeMaxLevel(int dir)
        {
            _newCupModel.MaxLevel += dir;
            _newCupModel.MaxLevel = Mathf.Clamp(_newCupModel.MaxLevel, _team.TeamData.level+1, maxLevelRange);
            maxLevelLabel.text = _newCupModel.MaxLevel.ToString();
        }
  
        private void InitializeInfo()
        {
           
            createButton.interactable = false;
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _controller.GetCupSettings(CupSettingsLoaded);
        }

        private void OnEndEditCupName(string result)
        {
             _newCupModel.Name = result;
             createButton.interactable = !string.IsNullOrEmpty(result);
        }

        public void OnHide()
        {
            _newCupModel = null;
            cupNameField.onEndEdit.RemoveListener(OnEndEditCupName);
            cupNameField.onValidateInput -= OnValidateCupNameInput;
            createButton.onClick.RemoveListener(OnCreateClickHandler);
            goldCurrencyButton.onValueChanged.RemoveListener(OnGoldCurrencySelected);
            moneyCurrencyButton.onValueChanged.RemoveListener(OnMoneyCurrencySelected);
            priceFeeSlider.onValueChanged.RemoveListener(OnPriceChangedHandler);

            while (_playerSelectorToggles.Count > 0)
            {
                maxPlayerSelectorsGroup.UnregisterToggle(_playerSelectorToggles[0]);
                _playerSelectorToggles[0].onValueChanged.RemoveListener(OnPlayersSelectedHandler);
                Destroy(_playerSelectorToggles[0]);
                _playerSelectorToggles.RemoveAt(0);
            }
            maxPlayersContent.RemoveChildren();
        }

        private void CupSettingsLoaded()
        {
            _cupSettingsModel = _controller.CupSettingsModel;


            cupNameField.text = "";
            createButton.interactable = false;

            _newCupModel = new TournamentBattleModel();

            _newCupModel.CurrencyType = GameData.CurrencyCoins;

            _newCupModel.MinLevel = _team.TeamData.level - _cupSettingsModel.StartLevelRange;
            _newCupModel.MinLevel = Mathf.Max(1, _newCupModel.MinLevel);
            _newCupModel.MaxLevel = _team.TeamData.level + _cupSettingsModel.StartLevelRange;

            priceFeeSlider.minValue = 1;
            priceFeeSlider.maxValue = _cupSettingsModel.PriceFeeCounts;

            OnPriceChangedHandler(1f);

            minLevelLabel.text = _newCupModel.MinLevel.ToString();
            maxLevelLabel.text = _newCupModel.MaxLevel.ToString();

            minLevelRange = _team.TeamData.level - _cupSettingsModel.MaxMinLevelRange;
            minLevelRange = Mathf.Max(1, minLevelRange);

            maxLevelRange = _team.TeamData.level + _cupSettingsModel.MaxMinLevelRange;
            maxLevelRange = Mathf.Min(GameData.GSConstData.MaxTeamLevel,maxLevelRange);

            moneyCurrencyButton.isOn = true;

            rewardPanel.SetIcon(GameSettings.Instance.GetIcon(GameData.CurrencyCoins));
            rewardPanel.SetText(_newCupModel.CurrencyAward);

            cupNameField.characterLimit = _cupSettingsModel.CupNameCharactersLen;

            _newCupModel.RoundTimeSeconds = _cupSettingsModel.RondTime;

            for (int i = 0; i < _cupSettingsModel.Players.Length; i++)
            {
                Toggle child = maxPlayersContent.AddChild(cupSelectorButtonPrefab, false).GetComponent<Toggle>();
                child.gameObject.name = i.ToString();
                child.GetComponentInChildren<Text>().text = _cupSettingsModel.Players[i].ToString();
                child.onValueChanged.AddListener(OnPlayersSelectedHandler);
                child.isOn = false;
                child.group = maxPlayerSelectorsGroup;
                maxPlayerSelectorsGroup.RegisterToggle(child);
                _playerSelectorToggles.Add(child);
            }
            _playerSelectorToggles[0].isOn = true;
        }

        private void OnPlayersSelectedHandler(bool selected)
        {
            if (selected)
            {
                Toggle selectedButton = _playerSelectorToggles.Find(p => p.isOn);
                if (selectedButton != null)
                {
                    int index = int.Parse(selectedButton.gameObject.name);
                    _newCupModel.MaxPlayers = _controller.CupSettingsModel.Players[index];
                    _newCupModel.CurrencyAward = _newCupModel.FeePrice * _newCupModel.MaxPlayers;

                    _newCupModel.EnergyCost = _controller.CupSettingsModel.EnergyPrices[index];
                    _newCupModel.TotalSteps = _controller.CupSettingsModel.Rounds[index];
                    
                    energyCost.SetText(_newCupModel.EnergyCost);
                    timerPanel.SetText(MathUtils.SecondsToTimeString(_newCupModel.GetTotalPlayTimeSeconds()));
                    rewardPanel.SetText(_newCupModel.CurrencyAward);
                }
            }
        }

        private void OnCreateClickHandler()
        {
            _team.UpdateEnergy(_newCupModel.EnergyCost, () =>
            {
                if (_team.CanBuyForEnergy(_newCupModel.EnergyCost))
                {
                    if (_team.CanBuyForCurrency(_newCupModel.CurrencyType, _newCupModel.FeePrice))
                    {
                       
                        PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                        _controller.CreateNewCup(_newCupModel, () =>
                        {
                            SoundManager.Instance.PlaySFX("battlestarted");

                            //  navBar.SelectButton(1, true);
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Tournament);
                            
                        });
                    }
                    else
                    {
                        PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup).HideButtons(true, false)
                       .SetText(LocaleManager.Instance.GetText("game.main.popup.title.notenoughmoney"))
                       .SetTitle(LocaleManager.Instance.GetText("game.main.popup.title.cannotparticipate").ToUpper(),false);
                    }

                }
                else
                {
                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup).HideButtons(true, false)
                      .SetTitle(LocaleManager.Instance.GetText("game.main.popup.title.notenoughenergy").ToUpper(),false);
                }
            });
        }

    }
}
