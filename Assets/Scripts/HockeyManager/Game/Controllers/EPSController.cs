﻿using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.EPS;
using HockeyManager.HockeyManager.Game.Models.EPS;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    /// <summary>
    /// Equipment,
    /// Personal,
    /// Skills controller
    /// </summary>
    public class EPSController: IGameController, IGSResponceHandler,ICheckMessages
    {
        public const string SkillPointsAvailable = "EPSController.skillPointsAvailable";

       

        private List<EquipmentModel> _equipments = new List<EquipmentModel>();

        private List<EmployeeModel> _personal;
        
        private IGameRoot _root;

        private TrainingsController _trainings;
        private TeamController _team;
        
        public void Initialize(IGameRoot root)
        {
            _root = root;
            _personal = new List<EmployeeModel>();
            _trainings = _root.GetController<TrainingsController>();
            _team = _root.GetController<TeamController>();
        }


        public void Reset()
        {
            
        }

       
        
        #region Equipments

       
        public void GetEquipments(UnityAction<List<EquipmentModel>> callback)
        {
            if (_equipments.Count > 1)
            {
                callback(_equipments);
            }
            else
            {
                LoadEquipments(-1, () =>
                {
                    callback(_equipments);
                });
            }
        }

        public EquipmentModel GetEquipment(EquipmentType id)
        {
            return _equipments.Find(e => e.Id.Equals(id));
        }

        private EquipmentModel GetEquipment(GSData equipment)
        {
            if (equipment != null)
            {
                return _equipments.Find(e => (int) e.Id == equipment.GetInt("id").Value);
            }
            return null;
        }
        
        public EmployeeModel GetCurrentEmployeeUpgrade()
        {
            return _personal.Find(e => e.IsUpgrading);
        }

        public EquipmentModel GetCurrentEquipmentUpgrade()
        {
            return _equipments.Find(e => e.IsUpgrading);
        }

        public void EndUpgradeEquipmentProcess(UnityAction<bool> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("END_EQP_UPGRADE_PROCESS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _team.NeedUpdateTeamInfo = true;
                    callback(true);
                }
                else
                {
                    _team.NeedUpdateTeamInfo = true;
                    callback(false);
                   LoadEquipments();
                }
            });
        }

        public void SendUpgradeEquipmentRequest(EquipmentModel model, bool fast, UnityAction callback = null)
        {
            var request = new LogEventRequest();
            request.SetEventKey("UPGRADE_EQP");
            request.SetEventAttribute("ID", (int) model.Id);
            request.SetEventAttribute("FAST", fast ? 1 : 0);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {

                    GSData eqp = responce.ScriptData.GetGSData(GS_ScriptData.EquipmentsData);
                    model.Parse(eqp);
                    if (!fast)
                    {
                        // model.OnTimerLeftSecondsEvent.AddListener(OnEquipmentUpgradeCompleteHandler);
                        model.StartUpgradeTimer();
                    }

                    _team.NeedUpdateTeamInfo = true;
                    if (callback != null)
                    {
                        callback();
                    }
                }
                else
                {
                    _team.RefreshBalance();
                    LoadEquipments();
                }
            });
        }
     
        public void LoadEquipmentUpgrade(EquipmentType id, UnityAction callback)
        {
            EquipmentModel eqp = GetEquipment(id);
            if (eqp != null && eqp.Level == EquipmentModel.MaxLevel)
            {
                callback();
                return;
            }

            var request = new LogEventRequest();
            request.SetEventAttribute("ID", (int) id);
            request.SetEventKey("GET_EQP_UPGRADE");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    GSData eqpUpgrade = responce.ScriptData.GetGSData(GS_ScriptData.EquipmentUpgradeData);
                    EquipmentModel model = _equipments.Find(e => e.Id.Equals(id));
                    if (model != null)
                    {
                        if (model.UpgradeModel == null)
                        {
                            model.UpgradeModel = new EquipmentUpgradeModel();
                        }
                        model.UpgradeModel.Parse(eqpUpgrade);
                    }
                    callback();
                }
            });
        }

        private void LoadEquipments(int id = -1, UnityAction callback = null)
        {
            var request = new LogEventRequest();
            request.SetEventAttribute("ID", id);
            request.SetEventKey("GET_EQP");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }

                }
            });
        }


        //private void OnEquipmentUpgradeCompleteHandler(long seconds)
        //{
        //    if (seconds == 0)
        //    {
        //        var currentUpgrade = GetCurrentEquipmentUpgrade();
        //        if (currentUpgrade != null)
        //        {
        //            _root.CheckAndLoadJournalActions((result) =>
        //            {
        //                if (!result)
        //                {
        //                    _root.StartCoroutine(CheckEquipmentUpgrade());
        //                }
        //            }, GS_Messages.EquipmentUpgradeComplete);
        //        }
        //    }
        //}

        //IEnumerator CheckEquipmentUpgrade()
        //{
        //    yield return new WaitForSeconds(2f);
        //    OnEquipmentUpgradeCompleteHandler(0);
        //    yield return null;
        //}

        #endregion


        #region Personal

        private const int MaxEmployees = 4;

        public void GetEmployees(UnityAction<List<EmployeeModel>> callback)
        {
            if (_personal.Count == MaxEmployees)
            {
                callback(_personal);
            }
            else
            {
                LoadPersonal(-1, () =>
                {
                    callback(_personal);
                });
            }
        }


        public EmployeeModel GetEmployee(EmployeeType id)
        {
            return _personal.Find(empl => empl.Id.Equals(id));
        }

        public void GetEmployee(EmployeeType id,UnityAction<EmployeeModel> loadCallback)
        {
            EmployeeModel model = GetEmployee(id);
            if (model == null)
            {
                LoadPersonal((int) id, () =>
                {
                    model = GetEmployee(id);
                    loadCallback(model);
                });
            }
            else
            {
                loadCallback(model); 
            }
        }

        private EmployeeModel GetEmployee(GSData employee)
        {
            if (employee != null)
            {
                return _personal.Find(e => (int)e.Id == employee.GetInt("id").Value);
            }
            return null;
        }

        public void SendUpgradeEmployeeRequest(EmployeeModel model, bool fast, UnityAction callback = null)
        {
            var request = new LogEventRequest();
            request.SetEventKey("UPGRADE_EMPLOYEE");
            request.SetEventAttribute("ID", (int) model.Id);
            request.SetEventAttribute("FAST", fast ? 1 : 0);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    GSData employee = responce.ScriptData.GetGSData(GS_ScriptData.PersonalData);
                    model.Parse(employee);

                    if (!fast)
                    {
                        // model.OnTimerLeftSecondsEvent.AddListener(OnEmployeeUpgradeCompleteHandler);
                        model.StartUpgradeTimer();
                    }

                    if (callback != null)
                    {
                        callback();
                    }
                    if (model.Id == EmployeeType.SecondCouch)
                    {
                        _trainings.UpdateEffectTime();
                    }



                    _team.NeedUpdateTeamInfo = true;
                }
                else
                {
                    _team.RefreshBalance();
                }
            });
        }
      
        public void EndUpgradeEmployeeRequest(UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("END_EMPLOYEE_UPGRADE");
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    GSData employee = responce.ScriptData.GetGSData(GS_ScriptData.PersonalData);
                    EmployeeModel model = GetEmployee(employee);
                    if (model != null)
                    {
                        model.Parse(employee);
                        if (model.Id == EmployeeType.SecondCouch)
                        {
                            _trainings.UpdateEffectTime();

                        }
                        else if (model.Id == EmployeeType.Doctor || model.Id == EmployeeType.MainCouch ||
                                 model.Id == EmployeeType.Adman)
                        {
                            _team.NeedUpdateTeamInfo = true;
                        }
                    }

                    callback();
                }
                else
                {
                    _team.RefreshBalance();
                    LoadPersonal();
                }
            });
        }

        public void ChangeMainCouchTactic(MainCouchTactic tactic,EmployeeModel mainCouch)
        {
            if(mainCouch == null || mainCouch.Id != EmployeeType.MainCouch)return;
            var request = new LogEventRequest();
            request.SetEventKey("CHANGE_TACTIC_BONUS_STATE");
            request.SetEventAttribute("TYPE", (int)tactic);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    mainCouch.Parse(responce.ScriptData.GetGSData(GS_ScriptData.PersonalData));
                    _team.NeedUpdateTeamInfo = true;
                }
            });

        }

        private void LoadPersonal(int id = -1, UnityAction callback = null)
        {
            var request = new LogEventRequest();
            request.SetEventAttribute("ID", id);
            request.SetEventKey("GET_PERSONAL");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }


        //private void OnEmployeeUpgradeCompleteHandler(long seconds)
        //{
        //    if (seconds == 0)
        //    {
        //        EmployeeModel upgrade = GetCurrentEmployeeUpgrade();
        //        if (upgrade != null)
        //        {
        //          _root.CheckAndLoadJournalActions((result) =>
        //          {
        //              if (!result)
        //              {
        //                  _root.StartCoroutine(CheckEmployeeUpgrade());
        //              }
        //          });
        //        }
        //    }
        //}

        //IEnumerator CheckEmployeeUpgrade()
        //{
        //    yield return new WaitForSeconds(2f);
        //    OnEmployeeUpgradeCompleteHandler(0);
        //    yield return null;
        //}

        #endregion

        #region Skills

        private int _skillPoints = 0;
        private int _usedSkillPoints = 0;

       
        private GSRequestData _skillsRawData;
        private List<SkillModel> _skills;

        private void LocalResetSkills()
        {
            Debug.Log("Local reset skill points");
            _usedSkillPoints = 0;
            _skills.ForEach(s =>
            {
                s.Points -= s.AddedPoints;
                _skillPoints += s.AddedPoints;
                s.AddedPoints = 0;
                _usedSkillPoints += s.Points;
            });

        }

        private void GetSkillPoints()
        {

            var request = new LogEventRequest();
            request.SetEventKey("GET_SKILL_POINTS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request);
        }

        public bool SkillPointsChanged
        {
            get { return _skillPointsChanged; }
        }

        public int SkillPoints
        {
            get { return _skillPoints; }
        }

        public int UsedSkillPoints
        {
            get { return _usedSkillPoints; }
        }

        public void AddPoint(SkillModel model)
        {
            if (_skillPoints > 0 && model.Points < GameData.GSConstData.MaxSkillPoints)
            {
                _skillPointsChanged = true;
                _skillPoints--;
                _usedSkillPoints++;
                model.AddedPoints++;
                model.Points++;
            }
        }

        public void UpgradeSkills(UnityAction callback,UnityAction errorCallback)
        {
            if(_skillsRawData == null || !_skillPointsChanged)return;

           // Debug.Log("Upgrade  skills started" + _skillsRawData.JSON );

            foreach (var skillModel in _skills)
            {
                _skillsRawData.AddNumber(skillModel.Type,skillModel.AddedPoints);
                skillModel.AddedPoints = 0;
            }

          //  Debug.Log("Upgrade  skills complete" + _skillsRawData.JSON);

            var request = new LogEventRequest();
            request.SetEventKey("UPGRADE_SKILLS");
            request.SetEventAttribute("SKILLS", _skillsRawData);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _skillPointsChanged = false;
                    EventsManager.Instance.Call(SkillPointsAvailable);
                    callback();
                }
                else
                {
                    _skillPoints = responce.Errors.GetGSData(GS_ScriptData.ErrorData).GetGSData("data").GetInt("skillPoints").Value;
                    _skillPointsChanged = false;
                    errorCallback();
                }
            });
          
            //{ "faceoff":1,"stamina":2,"speed":1,"throw":0,"dribbling":2,"reaction":2,"reliability":2,"steal":1}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="local">local reset for cancel</param>
        public void ResetSkillPoints(UnityAction callback,bool local = false)
        {
            if (_skills.Count > 0)
            {
                _skillPointsChanged = false;
                if (local)
                {
                    LocalResetSkills();
                    if(callback != null)callback();
                    return;
                }
                foreach (var skillModel in _skills)
                {
                    _skillsRawData.AddNumber(skillModel.Type, skillModel.Points);
                    skillModel.AddedPoints = 0;
                }
                var request = new LogEventRequest();
                request.SetEventKey("RESET_SKILL_POINT");
                request.SetEventAttribute("SKILLS", _skillsRawData);
                request.SetEventAttribute("SKILL_POINTS_LEFT",_skillPoints);
                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        _skillsRawData = new GSRequestData(responce.ScriptData.GetGSData(GS_ScriptData.TeamSkillsData));
                        UpdateSkills();
                        EventsManager.Instance.Call(SkillPointsAvailable);
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                });
            }
        }

        public void LoadSkills(UnityAction<SkillModel[]> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_TEAM_SKILLS");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _skillsRawData = null;
                    _skillsRawData = new GSRequestData(responce.ScriptData.GetGSData(GS_ScriptData.TeamSkillsData));
                    //check skills
                    UpdateSkills();
                    callback(_skills.ToArray());
                }
            });
        }

        private bool _skillPointsChanged;

        public bool CanResetSkillPoints()
        {
            return SkillPointsChanged || (_skills != null && _skills.Exists(s => s.Points > 0));
        }

        private void UpdateSkills()
        {
            if (_skillsRawData != null)
            {
                _skills = _skills ?? new List<SkillModel>();
                if (_skills.Count > 0)
                {
                    foreach (var skillModel in _skills)
                    {
                        skillModel.Points = _skillsRawData.GetInt(skillModel.Type).Value;
                    }
                }
                else
                {
                    _skills.Add(new SkillModel(GameData.FaceoffSKill,_skillsRawData.GetInt(GameData.FaceoffSKill).Value));
                    _skills.Add(new SkillModel(GameData.StaminaSkill,_skillsRawData.GetInt(GameData.StaminaSkill).Value));
                    _skills.Add(new SkillModel(GameData.SpeedSkill,_skillsRawData.GetInt(GameData.SpeedSkill).Value));
                    _skills.Add(new SkillModel(GameData.ThrowSKill,_skillsRawData.GetInt(GameData.ThrowSKill).Value));
                    _skills.Add(new SkillModel(GameData.DribblingSkill,_skillsRawData.GetInt(GameData.DribblingSkill).Value));
                    _skills.Add(new SkillModel(GameData.ReactionSkill,_skillsRawData.GetInt(GameData.ReactionSkill).Value));
                    _skills.Add(new SkillModel(GameData.ReliabilitySKill,_skillsRawData.GetInt(GameData.ReliabilitySKill).Value));
                    _skills.Add(new SkillModel(GameData.StealSkill,_skillsRawData.GetInt(GameData.StealSkill).Value));
                }
                _usedSkillPoints = 0;
                _skills.ForEach(s =>
                {
                    _usedSkillPoints += s.Points;
                });
            }
        }

        #endregion

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.EquipmentsMaxLevelData))
            {
                EquipmentModel.MaxLevel = scriptData.GetInt(GS_ScriptData.EquipmentsMaxLevelData).Value;
            }

            if (scriptData.ContainsKey(GS_ScriptData.EquipmentsData))
            {
                var equipments = scriptData.GetGSDataList(GS_ScriptData.EquipmentsData);
                
                if (equipments != null)
                {
                    // load all equipments at start
                    foreach (var equipment in equipments)
                    {
                        EquipmentType type = (EquipmentType)equipment.GetInt("id").Value;
                        EquipmentModel model = _equipments.Find((ep) => ep.Id == type);
                        if (model == null)
                        {
                            model = new EquipmentModel(equipment);
                            if (model.IsUpgrading)
                            {
                               // model.OnTimerLeftSecondsEvent.AddListener(OnEquipmentUpgradeCompleteHandler);
                                model.StartUpgradeTimer();
                            }
                            _equipments.Add(model);
                        }
                        else
                        {
                            model.Parse(equipment);
                        }
                    }
                }
                else
                {
                    var equipment = scriptData.GetGSData(GS_ScriptData.EquipmentsData);
                    if (equipment != null)
                    {
                        var eqp = _equipments.Find(e => (int)e.Id == equipment.GetInt("id").Value);
                        if (eqp != null)
                        {
                            eqp.Parse(equipment);
                        }
                    }

                }

            }

            if (scriptData.ContainsKey(GS_ScriptData.PersonalData))
            {
                var employees = scriptData.GetGSDataList(GS_ScriptData.PersonalData);
                if (employees != null)
                {
                    foreach (var employee in employees)
                    {
                        EmployeeType type = (EmployeeType)employee.GetInt("id").Value;
                        EmployeeModel emplModel = _personal.Find((emp) => emp.Id == type);
                        if (emplModel == null)
                        {
                            emplModel = new EmployeeModel(employee);
                            if (emplModel.IsUpgrading)
                            { 
                                //emplModel.OnTimerLeftSecondsEvent.AddListener(OnEmployeeUpgradeCompleteHandler);
                                emplModel.StartUpgradeTimer();
                            }
                            _personal.Add(emplModel);
                        }
                        else
                        {
                            emplModel.Parse(employee);
                        }
                        _personal.Sort();
                    }
                }
                else
                {
                    var employment = scriptData.GetGSData(GS_ScriptData.PersonalData);
                    if (employment != null)
                    {
                        int id = employment.GetInt("id").Value;
                        var empl = GetEmployee((EmployeeType)id);
                        if (empl != null)
                        {
                            empl.Parse(employment);
                        }
                        else
                        {
                            _personal.Add(new EmployeeModel(employment));
                        }
                    }
                 
                }
            }
             
            if (scriptData.ContainsKey(GS_ScriptData.TeamData))
            {
                GSData teamData = scriptData.GetGSData(GS_ScriptData.TeamData);
                if (teamData != null)
                {
                    if (teamData.GetInt("skillPoints").HasValue)
                    {
                        _skillPoints = teamData.GetInt("skillPoints").Value;
                        Debug.LogFormat("skillPoints loaded {0}",_skillPoints);
                    }
                    
                    
                    EventsManager.Instance.Call(SkillPointsAvailable);
                }

            }
          
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.EmployeeUpgradeComplete)
            {
                var upgrade = GetCurrentEmployeeUpgrade();
                if (upgrade != null)
                {
                    LoadPersonal((int)upgrade.Id);
                }
               
            }
            if (msgCode == GS_Messages.EquipmentUpgradeComplete)
            {
                var currentEqpUpgrade = GetCurrentEquipmentUpgrade();
                if (currentEqpUpgrade != null)
                {
                    GetSkillPoints();
                    LoadEquipments((int)currentEqpUpgrade.Id);
                }
               
            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.EquipmentUpgradeComplete || 
                   msgType == GS_Messages.EmployeeUpgradeComplete;
        }

    }
}
