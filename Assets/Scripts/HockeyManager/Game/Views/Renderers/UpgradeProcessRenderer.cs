﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.Models.Trainings;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Equipments;
using HockeyManager.Game.Views.Renderers;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Renderers
{
    /// <summary>
    /// label,icon & text,yellow button
    /// </summary>
    public class UpgradeProcessRenderer :MonoBehaviour
    {
        public UnityEvent UpgradeComplete;

#pragma warning disable CS0649 // Полю "UpgradeProcessRenderer.timer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl timer;
#pragma warning restore CS0649 // Полю "UpgradeProcessRenderer.timer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "UpgradeProcessRenderer.completeButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Button completeButton;
#pragma warning restore CS0649 // Полю "UpgradeProcessRenderer.completeButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "UpgradeProcessRenderer.label" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text label;
#pragma warning restore CS0649 // Полю "UpgradeProcessRenderer.label" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private RectTransform rtransform;

        private BaseGameEntityModel _upgradeModel;

        private bool _processComplete = false;

        public bool ProcessComplete
        {
            get { return _processComplete; }
        }

        void OnDestroy()
        {
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
            }

            if (_upgradeModel != null)
            {
                _upgradeModel.OnTimerLeftSecondsEvent.RemoveListener(OnTimeChangeHandler);
                _upgradeModel.OnModelChangedEvent.RemoveListener(OnModelChangedHanler);
                _upgradeModel = null;
            }

            UpgradeComplete.RemoveAllListeners();
        }

        private void ShowUpgradeCompleteDialog()
        {
            if(_upgradeModel == null)return;

            if (_upgradeModel.EntityType == EntityType.Building)
            {
                MapManager.Instance.FinishUpgradeBuilding(_upgradeModel as IBuildingModel, false);
            }
            if (_upgradeModel.EntityType == EntityType.Equipment)
            {
                EventsManager.Instance.Call(Equipment.OnEquipmentSelectedHandler, _upgradeModel as EquipmentModel);
            }
            if (_upgradeModel.EntityType == EntityType.Personal)
            {
                EventsManager.Instance.Call(EmployeeItemRenderer.EmployeeSelectedEvent, _upgradeModel as EmployeeModel);
            }
            if (_upgradeModel.EntityType == EntityType.Training)
            {
                EventsManager.Instance.Call(TrainingItemRenderer.OnTrainingSelected, _upgradeModel as TrainingModel);
            }
        }

        void OnEnable()
        {
            UpdateTextInfo(_upgradeModel);
        }

        void Start()
        {
            rtransform = gameObject.GetComponent<RectTransform>();

            if (!GameData.IsTutorialPassed && !TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.FinishTraining,
                    TutorStepTypes.FinishMainCouch))
            {
                EventsManager.Instance.Add(TutorialManager.TutorialStepChanged,OnTutorialStepChanged);
                OnTutorialStepChanged();
            }
        }

        private void OnTutorialStepChanged()
        {
            if (TutorialManager.Instance.CurrentView != null)
            {
                 TutorialManager.Instance.CurrentView.SetShowTarget(rtransform);
            }
        }

        public void SetModel(BaseGameEntityModel model)
        {
            Debug.Log("Active upgrade process-" + model.EntityType + " Started!");
            if(_upgradeModel != null) return;
            _upgradeModel = model;

            completeButton.onClick.AddListener(ShowUpgradeCompleteDialog);

            _upgradeModel.OnModelChangedEvent.AddListener(OnModelChangedHanler);
            OnModelChangedHanler();

            UpdateTextInfo(_upgradeModel);

            gameObject.name = model.EntityType.ToString();
        }


        private void UpdateTextInfo(BaseGameEntityModel model)
        {
            if(model == null )return;
            if (model.EntityType == EntityType.Building)
            {
                var upgradeBuilding = model as IBuildingModel;
                if (upgradeBuilding != null)
                {
                    string buildName = GameSettings.Instance.GetBuildingData(upgradeBuilding.Id).GetName();
                    label.text = LocaleManager.Instance.GetText("game.main.construction") + ". " + buildName + ".";

                }
            }
            if (model.EntityType == EntityType.Equipment)
            {
                var upgradeEquipment = model as EquipmentModel;
                var eqpInfo = GameSettings.Instance.GetEquipmentData(upgradeEquipment.Id);
                label.text = LocaleManager.Instance.GetText("game.main.upgrade") + ". " + eqpInfo.GetName() + ".";
            }
            if (model.EntityType == EntityType.Personal)
            {
                var upgradeEmployee = model as EmployeeModel;
                var emplInfo = GameSettings.Instance.GetEmployeeData(upgradeEmployee.Id);
                label.text = LocaleManager.Instance.GetText("game.main.teaching") + ". " + emplInfo.GetName() + ".";
            }
            if (model.EntityType == EntityType.Training)
            {
                var upgradeTraining = model as TrainingModel;
                if (upgradeTraining != null)
                    label.text = LocaleManager.Instance.GetText("game.main.training");
                //+ "." + upgradeTraining.GetName();
            }
        }

        private void OnModelChangedHanler()
        {
            if (_upgradeModel != null && !_processComplete)
            {
                if (!_upgradeModel.IsUpgrading || _upgradeModel.EndUpgradeTimeSeconds == 0)
                {
                    PopupsManager.Instance.RemovePopup(PopupType.FinishUpgradePopup);
                    SetCompleteProgress();
                }
                else
                {
                    _upgradeModel.OnTimerLeftSecondsEvent.AddListener(OnTimeChangeHandler);
                    OnTimeChangeHandler(GameTimer.TimeLeftSeconds(_upgradeModel.EndUpgradeTimeSeconds));
                }
            }
        }

        private void OnTimeChangeHandler(long leftSeconds)
        {
            timer.SetText(GameTimer.GetGameTimeString(leftSeconds));
            if (leftSeconds == 0)
            {
                //if (upgradeTimeLeft <= 0)
                //{
                //    MapController.Instance.LoadBuildingsData((int)upgradeBuilding.Id,upgradeBuilding.Pos);
                //}

                PopupsManager.Instance.RemovePopup(PopupType.FinishUpgradePopup);
                SetCompleteProgress();
            }
        }

  
        private void SetCompleteProgress()
        {
            if(_processComplete)return;
            _processComplete = true;
            Debug.Log("Active Upgrade Process- " + _upgradeModel.EntityType + " Complete!");
            UpgradeComplete.Invoke();
            completeButton.interactable = false;
        }


    }
}
