﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.UI.Renderers;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using UnityEngine;

namespace HockeyManager.Game.Managers
{
    public class  ActiveProcessesManager:MonoBehaviour
    {

        private static ActiveProcessesManager _instance = null;

        [SerializeField] private RectTransform processesHolder;
        [SerializeField] private GameObject processRendererPrefab;


        private List<UpgradeProcessRenderer> _upgrades;

        public static ActiveProcessesManager Instance
        {
            get { return _instance; }
        }

        void Start()
        {
           InitializeUpgrades();
        }

        void Awake()
        {
            _instance = this;
            _upgrades = new List<UpgradeProcessRenderer>();
        }
        
        public void AddUpgradeProcess(BaseGameEntityModel upgradeModel)
        {
            UpgradeProcessRenderer activeProcess = AddActiveProcess();
            if (activeProcess != null)
            {
                activeProcess.SetModel(upgradeModel);
                Show();
            }
        }


        private void InitializeUpgrades()
        {
            processesHolder.RemoveChildren();
           // Debug.Log("Active processes initiate complete");
           // EventsManager.Instance.Remove(GlobalEventTypes.UserAuthenticated, InitializeUpgrades);
            CheckEPSUpgrades();
            CheckTrainingUpgrades();
        }

        private void CheckTrainingUpgrades()
        {
            TrainingsController trainings = GameRoot.Instance.GetController<TrainingsController>();
               
            trainings.RefreshTrainings(false, true, () =>
            {
                var currentUpgrade = trainings.GetCurrentUpgrade();
                if (currentUpgrade != null)
                {
                    AddUpgradeProcess(currentUpgrade);
                }
            });
    }


    private UpgradeProcessRenderer AddActiveProcess()
        {
            if (_upgrades.Count < UISettings.MaxUpgradeProcessCount)
            {
                RectTransform process = processesHolder.AddChild(processRendererPrefab,false);
                UpgradeProcessRenderer upgradeProcessRenderer = process.GetComponent<UpgradeProcessRenderer>();
                upgradeProcessRenderer.UpgradeComplete.AddListener(OnUpgradeProcessComplete);
                _upgrades.Add(upgradeProcessRenderer);
                return upgradeProcessRenderer;
            }
            return null;
            //_processRenderers.Add(renderer);
        }

        private void CheckEPSUpgrades()
        {
            EPSController epsController = GameRoot.Instance.GetController<EPSController>();
            EmployeeModel employeeUpgrade = epsController.GetCurrentEmployeeUpgrade();
            EquipmentModel equipmentUpgrade = epsController.GetCurrentEquipmentUpgrade();
            if (employeeUpgrade != null)
            {
                AddUpgradeProcess(employeeUpgrade);
            }
            if (equipmentUpgrade != null)
            {
                AddUpgradeProcess(equipmentUpgrade);
            }
        }

        private void OnUpgradeProcessComplete()
        {

            UpgradeProcessRenderer process = _upgrades.Find((p) => p.ProcessComplete);
            if (process != null)
            {
                _upgrades.Remove(process);
                Destroy(process.gameObject);
            }
           
            if (!GameData.IsTutorialPassed)
            {

                switch (TutorialManager.Instance.CurrentStep)
                {
                    case TutorStepTypes.EmptyStep:
                        TutorialManager.Instance.NextStep();
                    break;
                    case TutorStepTypes.FinishCashbox:
                    case TutorStepTypes.FinishMainCouch:
                    case TutorStepTypes.FinishShop:
                    case TutorStepTypes.FinishTraining:
                    case TutorStepTypes.FinishIceArena:
                        if (TutorialManager.Instance.IsNextStepEmpty())
                        {
                            TutorialManager.Instance.NextStep(2);
                        }
                        else
                        {
                            TutorialManager.Instance.NextStep();
                        }
                    break;
                }
              
            }
            Show();
        }
        
        public void Show()
        {
            processesHolder.gameObject.SetActive(_upgrades.Count > 0 && 
                                                 PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Home && 
                                                 !PageNavigatorManager.Instance.CancelMovePage);
        }

        public void Hide()
        {
            processesHolder.gameObject.SetActive(false);
        }

      
    }
}
