﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Base;

namespace HockeyManager.Game.Models.Map
{
    /// <summary>
    /// curren active state for building from server side
    /// </summary>
    [Serializable]
    public sealed class BuildingModel:BaseGameEntityModel, IBuildingModel
    {
        //gs current effects
        public const string ArenaAttackProp = "attackBonus";
        public const string ArenaDeffenceProp = "deffenceBonus";
        public const string ArenaMaxFansProp = "maxFans";

        public const string BankLimitProp = "limit";

        public const string HallCellsProp = "cellscount";
        public const string FansPerTimeProp = "fanspertime";
        public const string FansCollectedProp = "fans";
        public const string FansMaxqueueProp = "max";
        public const string CoinsPerTimeProp = "coinspertime";
        public const string MaxIncomeCoinsProp = "max";

        public const string BannersBonusProp = "banners";
        public const string HallItemsBonusProp = "hallItemsBonus";

        public const string EquipmentMinLevelProp = "minlevel";
        public const string EquipmentMaxLevelProp = "maxlevel";

        public const string CoinsProp = "coins";
        public const string FansBonusProp = "fansbonus";


        private MapBuildingsType _id;
        private int _pos;
        //private int _level = 0;
        private int _maxLevel = 0;

        //private bool _isUpgrading = false;
        //private int _endUpgradeTimeSeconds = 0;
        //private int _upgradeTimeSeconds = 0;

        private bool _isCollectable = false;
        private bool _isCanCollect = false;
        private int _level = 0;

        private GSData _extraGSData = null;
        private GSData _effect = null;
       // private BuildingUpgradeInfo _upgradeInfo;
        //public float GetPercentTimeProgress()
        //{
        //    return TimeLeftSeconds / (float)_upgradeModel.upgrades[0].level;
        //}

        public BuildingModel(GSData data)
        {
            _entityType = EntityType.Building;

            _id = data.GetInt("id").HasValue ? (MapBuildingsType)data.GetInt("id").Value : _id;
            _pos = data.GetInt("pos").HasValue ? data.GetInt("pos").Value : _pos;
            _maxLevel = data.GetInt("maxLevel").HasValue ? data.GetInt("maxLevel").Value : _maxLevel;
            _isCollectable = (_id == MapBuildingsType.Cashbox || _id == MapBuildingsType.Shop || _id == MapBuildingsType.Kiosk);

            Parse(data);
        }

        public MapBuildingsType Id
        {
            get { return _id; }
        }

        //public BuildingUpgradeInfo UpgradeInfo
        //{
        //    get { return _upgradeInfo; }
        //    set { _upgradeInfo = value; }

        //}

        public int Pos
        {
            get { return _pos; }
        }


        public int MaxLevel
        {
            get { return _maxLevel; }
        }
        
        public bool IsBuilded
        {
            get { return _level > 0; }
        }

        public bool IsCollectable
        {
            get { return _isCollectable; }
        }

        public bool IsCanCollect
        {
            get { return _isCanCollect; }
        }

        public int Level
        {
            get { return _level; }
        }

        public GSData Effect
        {
            get { return _effect; }
        }

        public GSData ExtraData
        {
            get { return _extraGSData; }
        }

        public override void Parse(GSData data)
        {

            if (data.ContainsKey("extraData"))
            {
                _extraGSData = data.GetGSData("extraData");
            }
           
            _effect = data.GetGSData("effect");

            _level = data.GetInt("level").HasValue ? data.GetInt("level").Value : _level;

            if (_isCollectable)
            {
                if (_effect != null)
                {
                    if (_effect.ContainsKey("fans"))
                    {
                        _isCanCollect = _effect.GetInt("fans").Value > 0;
                    }
                    else if (_effect.ContainsKey("coins"))
                    {
                        _isCanCollect = _effect.GetInt("coins").Value > 0;
                    }
                }
            }

            base.Parse(data);
        }

        public float GetEffectValue(params string[] props)
        {
            float total = 0f;
            for (int i = 0; i < props.Length; i++)
            {
                if (_effect.ContainsKey(props[i]))
                {
                    total += _effect.GetFloat(props[i]).Value;
                }
            }
            return total;
        }

        float IEffect.GetEffectValue(string prop)
        {
            if (_effect != null && _effect.ContainsKey(prop))
            {
                return _effect.GetFloat(prop).Value;
            }
            return 0f;
        }
    }
}
