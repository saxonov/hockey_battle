﻿using System;
using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Impl;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Models.Battles
{

    public enum TournamentBattleStates
    {
        Selection,
        Idle,
        Started,
        Complete,
        Expired,
        Finished
    }

    public class TournamentBattleModel : IGSParse
    {
        //public UnityEvent OnModelChanged = new UnityEvent();

        private int _maxPlayers;
        private int _feePrice;
        private int _currencyType;
        private int _currencyAward;

        private int _expiryTimeSeconds;
        private int _roundTimeSeconds;

        private string _name;
        private int _minLevel = 0;
        private int _maxLevel = 0;

        private int _energyCost;
        private int _steps;
        private int _totalSteps;
        private long _endPlayTime;
        private long _endExpiryTime;
        private long _endStepTime;

        private string _id = null;

        private string _creatorId = null;

        private List<TournamentUserModel> _players;

        private TournamentBattleStates _state = TournamentBattleStates.Selection;

        public int MaxPlayers
        {
            get { return _maxPlayers; }
            set { _maxPlayers = value; }
        }

        public int FeePrice
        {
            get { return _feePrice; }
            set { _feePrice = value; }
        }

        public int CurrencyType
        {
            get { return _currencyType; }
            set { _currencyType = value; }
        }

        public int CurrencyAward
        {
            get { return _currencyAward; }
            set { _currencyAward = value; }
        }

        public string GetName()
        {
            if (LocaleManager.Instance.HasKey(_name))
            {
                return LocaleManager.Instance.GetText(_name);
            }
            return _name;
        }
        
        public int MinLevel
        {
            get { return _minLevel; }
            set { _minLevel = value; }
        }

        public int MaxLevel
        {
            get { return _maxLevel; }
            set { _maxLevel = value; }
        }

        public int EnergyCost
        {
            get { return _energyCost; }
            set { _energyCost = value; }
        }

        public int Steps
        {
            get { return _steps; }
        }

        public int TotalSteps
        {
            get { return _totalSteps; }
            set { _totalSteps = value; }
        }

        public long EndPlayTime
        {
            get { return _endPlayTime; }
        }

        public long EndExpiryTime
        {
            get { return _endExpiryTime; }
        }

        public long EndStepTime
        {
            get { return _endStepTime; }
        }

        public TournamentBattleStates State
        {
            get { return _state; }
        }
     
        public void Finish()
        {
            _state = TournamentBattleStates.Finished;
        }

        public string CreatorId
        {
            get { return _creatorId; }
        }

        public string Id
        {
            get { return _id; }
        }

        public List<TournamentUserModel> Players
        {
            get { return _players; }
        }

        public int ExpiryTimeSeconds
        {
            get { return _expiryTimeSeconds; }
        }

        public string Name
        {
            set { _name = value; }
        }

        public int RoundTimeSeconds
        {
            set { _roundTimeSeconds = value; }
        }

        public int GetTotalPlayTimeSeconds()
        {
            return _roundTimeSeconds*_totalSteps;
        }

        public bool IsMyTeamWin()
        {

            return _players != null && _players.Exists(p => p.TeamInfo.IsMe() && p.Position == 1);
        }

        public bool IsMyPlayerInCup()
        {
           return _players != null && _players.Exists(p => p.TeamInfo.IsMe());
        }

        public int GetMyPosition()
        {
            int pos = -1;
            if (_players != null)
            {
                var player = _players.Find(p => p != null && p.TeamInfo != null && p.TeamInfo.IsMe());
                if (player != null)
                {
                   pos = player.Position;
                }
            }
            
            return pos;
        }
     
        public void Parse(GSData data)
        {
            if (data == null)
            {
                Debug.Log("Parse Tournament battle GSData not exist!");
                return;
            }

            if (data.ContainsKey("_id"))
            {
                _id = data.GetString("_id");
            }

            _players = _players ?? new List<TournamentUserModel>();

            _state = data.ContainsKey("state") ? (TournamentBattleStates) Enum.Parse(typeof (TournamentBattleStates), data.GetString("state")) : _state;

            _energyCost = data.ContainsKey("energyCost") ? data.GetInt("energyCost").Value : _energyCost;

            _steps = data.ContainsKey("steps") ? data.GetInt("steps").Value : _steps;
            _totalSteps = data.ContainsKey("totalSteps") ? data.GetInt("totalSteps").Value : _totalSteps;

            _maxPlayers = data.ContainsKey("maxPlayers") ? data.GetInt("maxPlayers").Value : _maxPlayers;

            _minLevel = data.ContainsKey("minLevel") ? data.GetInt("minLevel").Value : _minLevel;
            _maxLevel = data.ContainsKey("maxLevel") ? data.GetInt("maxLevel").Value : _maxLevel;

            _feePrice = data.ContainsKey("feePrice") ? data.GetInt("feePrice").Value : _feePrice;

            _currencyType = data.ContainsKey("currencyType") ? data.GetInt("currencyType").Value : _currencyType;
            _name = data.ContainsKey("name") ? data.GetString("name") : _name;
            _currencyAward = data.ContainsKey("currencyAward") ? data.GetInt("currencyAward").Value : _currencyAward;

            _endPlayTime = data.ContainsKey("endPlayTime") ? data.GetLong("endPlayTime").Value : _endPlayTime;
            _endStepTime = data.ContainsKey("endStepTime") ? data.GetLong("endStepTime").Value : _endStepTime;
            _endExpiryTime = data.ContainsKey("endExpiryTime") ? data.GetLong("endExpiryTime").Value : _endExpiryTime;

            _expiryTimeSeconds = data.ContainsKey("expiryTimeSeconds") ? data.GetInt("expiryTimeSeconds").Value : _expiryTimeSeconds;
            _roundTimeSeconds = data.ContainsKey("roundTimeSeconds") ? data.GetInt("roundTimeSeconds").Value : _roundTimeSeconds;

            _creatorId = data.ContainsKey("creator") ? data.GetString("creator") : _creatorId;

            var list = data.GetGSDataList("players");

            if (list != null)
            {
                _players.Clear();
                foreach (var gsData in list)
                {
                    _players.Add(new TournamentUserModel(gsData));
                }
            }

         //   OnModelChanged.Invoke();

        }
    }
}
