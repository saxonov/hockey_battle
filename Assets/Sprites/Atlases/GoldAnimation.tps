<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>E:/UNITY/HockeyManager/Assets/Sprites/Atlases/GoldAnimation.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../GoldIconSpriteSheet.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0000.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0001.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0002.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0003.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0004.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0005.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0006.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0007.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0008.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0009.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0010.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0011.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0012.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0013.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0014.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0015.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0016.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0017.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0018.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0019.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0020.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0021.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0022.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0023.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0024.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0025.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0026.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0027.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0028.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0029.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0030.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0031.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0032.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0033.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0034.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0035.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0036.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0037.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0038.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0039.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0040.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0041.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0042.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0043.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0044.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0045.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0046.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0047.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0048.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0049.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0050.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0051.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0052.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0053.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0054.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0055.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0056.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0057.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0058.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0059.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0060.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0061.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0062.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0063.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0064.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0065.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0066.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0067.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0068.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0069.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/coin/coin0070.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>D:/Dropbox/HockeyBattle/Иконки анимированные/coin</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
