﻿using UnityEngine;
using System.Collections;
using SCTools.Helpers;
using UnityEditor;

namespace SCTools.Editor
{
    [CustomEditor(typeof(ShowBounds))]
    public class ShowBoundsEditor : UnityEditor.Editor
    {

        private ShowBounds showBounds;

        public override void OnInspectorGUI()
        {

            DrawDefaultInspector();
            showBounds = (ShowBounds)target;

            //  EditorGUI.InspectorTitlebar(new Rect(0, 0, 200, 20), false, showBounds.transform, true);



            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Bounds of: " + showBounds.RendererName, GUIStyle.none);
            EditorGUILayout.LabelField("Size: " + showBounds.Bounds.size, GUIStyle.none);
            EditorGUILayout.LabelField("Center: " + showBounds.Bounds.center, GUIStyle.none);
            EditorGUILayout.LabelField("Extents: " + showBounds.Bounds.extents, GUIStyle.none);
            if (GUILayout.Button("Update"))
            {
                showBounds.UpdateBounds();
            }
            EditorGUILayout.EndVertical();
        }

    }

}
