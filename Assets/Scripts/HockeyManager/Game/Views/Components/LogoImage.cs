﻿using UnityEngine;
using UnityEngine.UI;


namespace HockeyManager.Game.UI.Components
{

    /// <summary>
    /// workaround for masked images with custom shaders
    /// http://answers.unity3d.com/questions/1130203/ui-mask-override-my-shaders-custom-property.html
    /// </summary>
    public class LogoImage : Image
    {
        private Material _modifiedMaterial;

        public Material ModifiedMaterial
        {
            get
            {
                if (_modifiedMaterial == null)
                {
                    _modifiedMaterial = material;
                }
                return _modifiedMaterial;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseMaterial"></param>
        /// <returns></returns>
        public override Material GetModifiedMaterial(Material baseMaterial)
        {
            _modifiedMaterial = base.GetModifiedMaterial(baseMaterial);
            //modifiedMat.SetVector("_HSLAAdjust", new Vector4(0.04f,0f,0f,0f));
            return _modifiedMaterial;
        }


    }


}
