﻿using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.Models.Rewards;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class MatchBattleController:IGameController,ICheckMessages,IGSResponceHandler
    {
        public const string MatchStateChanged = "MatchBattleController.matchStateChangedEvent";
        public const string MatchStartedEvent = "MatchBattleController.matchStartedEvent";
        public const string MatchYouChallengedEvent = "MatchBattleController.matchYouChallenged";
        public const string MatchYouWinnerEvent1arg = "MatchBattleController.matchYouWinnerEvent1arg";
        public const string MatchYouLooseEvent1arg = "MatchBattleController.matchYouLooseEvent";
        public const string MatchCanceledEvent = "MatchBattleController.matchCanceledEvent";

        private bool _needUpdateMatchHistory = true;
        private bool _needMatchInfoUpdate = true;

        //  private RewardModel _matchCancelPenaltyReward = null;
        //  private GSData _lastMatchData = null;
        private TeamController _team;
        private MatchBattleStates _currentMatchBattleState = MatchBattleStates.Selection;
        private IGameRoot _root;

        private MatchSelectionModel _matchSelectionModel;

        private MatchBattleModel _matchBattleModel;
        private MatchBattleHistoryModel _matchHistoryModel;
        private RewardsAchievementsController _rewards;

        public MatchBattleStates CurrentMatchBattleState
        {
            get { return _currentMatchBattleState; }
            set { _currentMatchBattleState = value; }
        }

        public MatchBattleModel MatchBattleModel
        {
            get { return _matchBattleModel; }
        }

        public MatchSelectionModel MatchSelectionModel
        {
            get { return _matchSelectionModel; }
        }

        public MatchBattleHistoryModel MatchHistoryModel
        {
            get { return _matchHistoryModel; }
        }


        public bool IsCanPlayMatchNow()
        {
            return _currentMatchBattleState == MatchBattleStates.Selection && 
                _team.CurrentActionState == GameActionState.None && 
                _team.TeamData.fans > 0;
        }

        public void GetMatchBattleHistory(UnityAction callback)
        {
            if (!_needUpdateMatchHistory)
            {
                callback();
                return;

            }
            _needUpdateMatchHistory = false;
            var request = new LogEventRequest();
            request.SetEventKey("GET_MATCHES_HISTORY");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="callback"></param>
        public void AttackTeam(string userId, UnityAction<bool> callback = null)
        {
            if (!IsCanPlayMatchNow())
            {
                if (callback != null)
                {
                    callback(false);
                }
                return;
            }

            var request = new LogEventRequest();
            request.SetEventKey("ATTACK_TEAM");
            request.SetEventAttribute("USER_ID", userId);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                //if (!responce.HasErrors)
                //{
                //    _currentMatchBattleState = MatchBattleStates.Attack;
                //}
                if (callback != null)
                {
                    callback(!responce.HasErrors);
                }

                if (responce.HasErrors)
                {
                    GSData error = responce.Errors.GetGSData(GS_ScriptData.ErrorData);
                    if (error != null)
                    {
                        if (error.GetInt("id").Value == GS_Errors.ERROR_NOT_ENOUGH_ENERGY)
                        {
                            _team.RefreshBalance();
                        }
                    }
                }
            });

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <returns>false if not enough coins</returns>
        public void CancelMatch(UnityAction callback)
        {
            if (_matchBattleModel == null)
            {
                _currentMatchBattleState = MatchBattleStates.Selection;
                _needMatchInfoUpdate = true;
                EventsManager.Instance.Call(MatchStateChanged);
                callback();
                return;
            }

            var request = new LogEventRequest();
            request.SetEventKey("CANCEL_MATCH");
            request.SetEventAttribute("BATTLE_ID", _matchBattleModel.BattleId);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                
                    _currentMatchBattleState = MatchBattleStates.Selection;
                    _needMatchInfoUpdate = true;
                    EventsManager.Instance.Call(MatchStateChanged);
                    callback();
                
            });
        }

        public void GainCancelPenaltyReward(RewardModel model)
        {
            if (model != null && model.Type == RewardType.MatchCancelPenaltyReward)
            {
                _rewards.GainReward(model, null);
            }
        }

        public void AcceptMatch(UnityAction callback)
        {
            if (_matchBattleModel == null)
            {
                _needMatchInfoUpdate = true;
                callback();
                return;
            }
            var request = new LogEventRequest();
            request.SetEventKey("ACCEPT_MATCH");
            request.SetEventAttribute("BATTLE_ID", _matchBattleModel.BattleId);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                 _needMatchInfoUpdate = true;
                 callback();
            });
        }

        /// <summary>
        /// get two results BattleModel or TeamsList to attack
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="forceUpdate"></param>
        public void GetMatchInfo(UnityAction callback = null, bool forceUpdate = false)
        {
            if (_needMatchInfoUpdate || forceUpdate)
            {
                _needMatchInfoUpdate = false;
                var request = new LogEventRequest();
                request.SetEventKey("GET_MATCH_INFO");
                request.SetDurable(true);

                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        if (callback != null)
                        {
                            callback();
                        }

                    }
                });
                return;
            }
            if (callback != null)
            {
                callback();
            }

        }

        private AttackTeamModel _attackTeamInfoModel;
        private GSRequestData attackTeamInfoRequestData;

        public void LoadAttackTeamInfo(string teamId, 
                                       UnityAction<AttackTeamModel> callback,
                                       bool loadInfo = true,
                                       bool loadStat = true,
                                       bool checkChallenged = true)
        {

            attackTeamInfoRequestData = attackTeamInfoRequestData ?? 
                                        new GSRequestData();
            attackTeamInfoRequestData.BaseData.Clear();
            attackTeamInfoRequestData.AddString("userId", teamId);
            attackTeamInfoRequestData.AddBoolean("getTeamInfo", loadInfo);
            attackTeamInfoRequestData.AddBoolean("getStat", loadStat);
            attackTeamInfoRequestData.AddBoolean("checkChallenged", checkChallenged);

            _attackTeamInfoModel = _attackTeamInfoModel ?? new AttackTeamModel();

            var request = new LogEventRequest();
            request.SetEventKey("GET_TEAM_ATTACK_INFO");
            request.SetEventAttribute("DATA", attackTeamInfoRequestData);
            _root.NetManager.RunCustomLogEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    GSData info = responce.ScriptData.GetGSData(GS_ScriptData.TeamAttackInfoData);
                    _attackTeamInfoModel.Parse(info);
                    callback(_attackTeamInfoModel);
                }
            });

        }

        private void ChallengedToMatchBattle(GSData battle)
        {
            Debug.Log("Match challenged!");
            _currentMatchBattleState = MatchBattleStates.Attacked;
            _needMatchInfoUpdate = true;
            if (battle != null)
            {
                _matchBattleModel = _matchBattleModel ?? new MatchBattleModel();
                _matchBattleModel.Parse(battle);
                EventsManager.Instance.Call(MatchYouChallengedEvent, _matchBattleModel.GetEnemyTeam());
            }

            EventsManager.Instance.Call(MatchStateChanged);
        }

        private void MatchBattleCanceled()
        {
            Debug.Log("Match canceled!");
            _currentMatchBattleState = MatchBattleStates.Selection;
            _needUpdateMatchHistory = true;
            _needMatchInfoUpdate = true;

            EventsManager.Instance.Call(MatchCanceledEvent);
            EventsManager.Instance.Call(MatchStateChanged);
        }


        private void MatchBattleWinLost(GSData data, bool win)
        {
            Debug.LogFormat("Match {0}!", win ? "win" : "lost");
            _currentMatchBattleState = MatchBattleStates.Selection;
            _needMatchInfoUpdate = true;
            _team.NeedUpdateTeamInfo = true;
            _needUpdateMatchHistory = true;
            EventsManager.Instance.Call(win ? MatchYouWinnerEvent1arg : MatchYouLooseEvent1arg, data);
            EventsManager.Instance.Call(MatchStateChanged);
        }

        public void Initialize(IGameRoot root)
        {
            _root = root;
            _team = _root.GetController<TeamController>();
            _rewards = _root.GetController<RewardsAchievementsController>();
        }

        public void Reset()
        {
            _needUpdateMatchHistory = true;
            _needMatchInfoUpdate = true;

        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            GSData data = msgData;
            switch (msgCode)
            {
                case GS_Messages.MatchStartedMsg:
                    Debug.Log("Match started!");
                    _currentMatchBattleState = MatchBattleStates.Battle;
                    _needMatchInfoUpdate = true;
                    _matchBattleModel = _matchBattleModel ?? new MatchBattleModel();
                    _matchBattleModel.Parse(data.GetGSData("battle"));
                    EventsManager.Instance.Call(MatchStartedEvent);
                    EventsManager.Instance.Call(MatchStateChanged);
                    break;
                case GS_Messages.MatchYouChallengedMsg:
                    if (data != null)
                    {
                        ChallengedToMatchBattle(data.GetGSData("battle"));
                    }
                    break;
                case GS_Messages.MatchWonMsg:
                case GS_Messages.MatchLostMsg:
                    MatchBattleWinLost(data, msgCode == GS_Messages.MatchWonMsg);
                    break;
                case GS_Messages.MatchCanceledMsg:
                    MatchBattleCanceled();
                    break;
            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.MatchWonMsg ||
                   msgType == GS_Messages.MatchLostMsg ||
                   msgType == GS_Messages.MatchYouChallengedMsg ||
                   msgType == GS_Messages.MatchCanceledMsg ||
                   msgType == GS_Messages.MatchStartedMsg;
        }

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.MatchBattleData))
            {
                GSData battleData = scriptData.GetGSData(GS_ScriptData.MatchBattleData);
                _matchBattleModel = _matchBattleModel ?? new MatchBattleModel();
                _matchBattleModel.Parse(battleData);

                if (_matchBattleModel.BattleStarted)
                {
                    _currentMatchBattleState = MatchBattleStates.Battle;

                }
                else if (_matchBattleModel.EnemyAttacked)
                {
                    _currentMatchBattleState = MatchBattleStates.Attacked;

                }
                else if (!_matchBattleModel.EnemyAttacked)
                {
                    _team.DisableShield();
                    _currentMatchBattleState = MatchBattleStates.Attack;
                }
                EventsManager.Instance.Call(MatchStateChanged);
            }

            if (scriptData.ContainsKey(GS_ScriptData.MatchTeamSelectionData))
            {
                GSData matchSelectionData = scriptData.GetGSData(GS_ScriptData.MatchTeamSelectionData);
                _matchSelectionModel = _matchSelectionModel ?? new MatchSelectionModel();
                _matchSelectionModel.Parse(matchSelectionData);
            }

            if (scriptData.ContainsKey(GS_ScriptData.MatchHistoryData))
            {
                GSData matchHistoryData = scriptData.GetGSData(GS_ScriptData.MatchHistoryData);
                _matchHistoryModel = _matchHistoryModel ?? new MatchBattleHistoryModel();
                _matchHistoryModel.Parse(matchHistoryData);
            }
        }

    }
}
