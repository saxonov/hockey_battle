﻿using System.Linq;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Popups
{
    public class ChangeTeamNamePopup:CustomBasePopup
    {

        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private TextMeshProUGUI availableText;
        [SerializeField] private TextMeshProUGUI priceTextfield;

        [SerializeField] private Button changeButton;
       
        private TeamController _team;

        private string _typedName = null;

        private int changePrice;

        void Awake()
        {
            changePrice = GameData.GSConstData.TeamNameChangeGoldPrice;
        }

       
        private char OnValidateTeamNameInputHandler(string text, int charindex, char addedchar)
        {
            if (char.IsLetterOrDigit(addedchar) || 
                char.IsWhiteSpace(addedchar) || 
                UISettings.CheckMask.Contains(addedchar))
            {
                return addedchar;
            }
            return '\0';
        }

        private void OnChangeButtonHandler()
        {

            if (_team.CanBuyForGold(changePrice))
            {
                inputField.interactable = false;
                changeButton.interactable = false;
                _team.ChangeName(_typedName,OnApply);
            }
            else
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                OnClose();
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            _team = null;
            changeButton.onClick.RemoveListener(OnChangeButtonHandler);
            inputField.onSelect.RemoveListener(OnSelectInputHandler);
            inputField.onEndEdit.RemoveListener(OnEndEditNameHandler);
            inputField.onValidateInput -= OnValidateTeamNameInputHandler;

        }

        private void OnSelectInputHandler(string str)
        {
            changeButton.interactable = false;
        }

        private void OnEndEditNameHandler(string text)
        {
            if (_typedName != text && text != "")
            {
                _typedName = text;
                _team.ChekTeamNameAvailable(text, OnTeamNameAvailableHandler);
            }
            else
            {
                changeButton.interactable = text != "";
            }
        }

        private void OnTeamNameAvailableHandler(bool available)
        {
            if (!available)
            {
                availableText.color = UISettings.BlockTextColor;
                availableText.text = this.GetLocaleText("game.common.namenotavailable");

            }
            else
            {
                availableText.color = UISettings.GreenTextColor;
                availableText.text = this.GetLocaleText("game.common.nameavailable");

            }
            changeButton.interactable = available;
        }

        protected override void SetController(IGameController controller)
        {
           _team = controller as TeamController;
            inputField.text = _team.TeamData.TeamInfo.Name;

            priceTextfield.text = this.GetLocaleText("game.common.saveforprice", string.Format("<sprite=1>{0}", changePrice));
            availableText.color = UISettings.BlockTextColor;
            
            if (_team.CurrentActionState == GameActionState.Training ||
                _team.CurrentActionState == GameActionState.None)
            {
                changeButton.interactable = false;
                changeButton.onClick.AddListener(OnChangeButtonHandler);

                availableText.text = this.GetLocaleText("game.common.namenotavailable");

                inputField.onSelect.AddListener(OnSelectInputHandler);
                inputField.onEndEdit.AddListener(OnEndEditNameHandler);
                inputField.onValidateInput += OnValidateTeamNameInputHandler;


            }
            else
            {
                availableText.text = this.GetLocaleText("game.main.popup.changeteamname.cannotchange");
                changeButton.interactable = false;
                inputField.interactable = false;
            }



        }
    }
}
