﻿using HockeyManager.Game.Dict;
using HockeyManager.TutorialSystem.Base;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.TutorialSystem
{
    [RequireComponent(typeof(Button))]
    public class TutorialButton:TutorialObject
    {
        private Button button;

        void Start()
        {
            if (GameData.IsTutorialPassed)
            {
                Destroy(this);
                return;
            }
            button = GetComponent<Button>();
        }

        public override void OnUpState()
        {
            if (button == null)
            {
                button = GetComponent<Button>();
            }
            button.OnDeselect(null);
            if (button.onClick != null)
            {
                button.onClick.Invoke();
            }
        }

        public override void OnDownState()
        {
            if (button == null)
            {
                button = GetComponent<Button>();
            }
            button.Select();
        }
    }
}
