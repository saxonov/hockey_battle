﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace SCTools.Helpers
{
    public class PointerClickHelper : MonoBehaviour, IPointerClickHandler
    {
        public IPointerClickHandler button;

        private bool _enabled = true;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (button != null && _enabled)
            {
                button.OnPointerClick(eventData);
            }
        }
    }
}
