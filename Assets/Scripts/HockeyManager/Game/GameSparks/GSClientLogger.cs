﻿using System;
using System.Text;
using GameSparks.Api.Requests;
using SCTools.SoundManager;
using UnityEngine;

namespace HockeyManager.Game.GameSparks
{
    public class GSClientLogger:MonoBehaviour
    {
        public float sendClientLogsCooldownSec = 30f;

        private float _sendLogsCoolDown;
        private bool logsSended;
        private StringBuilder _logs;

        private const string ExceptionFormat = @"<br><font color='#FF0000'>
                                                    <b>StackTrace:</b><br><p aligh='left'>{0}</p></font>
                                                    <br>";
        private const string HtmlPattern = @"<html>
                                             <meta http-equiv='Content-Type'content='text/html; charset=utf-8'>
                                              <title>{0}</title>
                                              <style>body{1}</style>
                                              <body>{2}</body>
                                             </html>";

        void Awake()
        {
            _sendLogsCoolDown = sendClientLogsCooldownSec;
        }

#if UNITY_IOS
       
        void Start()
        {
     
                                     if (CrashReport.lastReport != null)
                                     {
                                        string crashReport = CrashReport.lastReport.text;
                                        crashReport = crashReport.Replace("\n", "<br>");

                                        Debug.LogFormat("IOS Crash Details:<br><p>{0}</p><br>",crashReport);
                                        SaveClientLogs("IOS Crash logs!");
                                     }
                                     CrashReport.RemoveAll();


    }
#endif
        void OnEnable()
        {
            Application.logMessageReceived += OnLogRecievedHandler;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= OnLogRecievedHandler;
        }

        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(1))
            {
                if (_logs != null && _logs.Length > 0 && !logsSended)
                {
                    SoundManager.Instance.PlaySFX("chatmsg");
                    SaveClientLogs("Editor logs client send!");
                }
                if (logsSended)
                {
                    _sendLogsCoolDown -= Time.deltaTime;
                    if (_sendLogsCoolDown <= 0)
                    {
                        logsSended = false;
                        _sendLogsCoolDown = sendClientLogsCooldownSec;
                    }
                }
            }
#endif

            if (Input.touchSupported)
            {
                int touchCount = Input.touchCount;
                if (touchCount == 5 && !logsSended && _logs != null && _logs.Length > 0)
                {
                    Application.OpenURL("http://hockeybattle.ru/%d0%be%d0%b1%d1%80%d0%b0%d1%82%d0%bd%d0%b0%d1%8f-%d1%81%d0%b2%d1%8f%d0%b7%d1%8c/");
                    SoundManager.Instance.PlaySFX("chatmsg");
                    SaveClientLogs("5 touches client send!");
                }
                else if (logsSended && touchCount < 5)
                {
                    _sendLogsCoolDown -= Time.deltaTime;
                    if (_sendLogsCoolDown <= 0)
                    {
                        logsSended = false;
                        _sendLogsCoolDown = sendClientLogsCooldownSec;
                    }
                }
            }

        }

        public void SaveClientLogs(string condition)
        {
            if (logsSended) return;
            logsSended = true;
            _logs.Append("<br><br>====== End logs. ====");

            string htmlLog = string.Format(HtmlPattern, condition, "{margin:100px}", _logs);

            var request = new LogEventRequest();
            request.SetEventKey("SET_CLIENT_LOGS");
            request.SetEventAttribute("TYPE", condition);
            request.SetEventAttribute("LOGS", htmlLog);
            request.SetDurable(true);
            request.Send(null);
            _logs.Remove(0, _logs.Length);

        }

        private void OnLogRecievedHandler(string condition, string stacktrace, LogType type)
        {
            if (type != LogType.Assert && type != LogType.Warning)
            {
                _logs = _logs ?? new StringBuilder();

                if (_logs.Length == 0)
                {
                    _logs.Append("<font color='#3CB38A'><p>");
                    _logs.AppendFormat("Build ver.{0}<br>", Application.version);
                    _logs.AppendFormat("Device:{0} <br> Name:{1} <br> Graphics:{2} <br>", SystemInfo.deviceModel, SystemInfo.deviceName, SystemInfo.graphicsDeviceName);
                    _logs.AppendFormat("Time: {0}<br>", DateTime.Now.ToLocalTime());
                    _logs.Append("</p></font>");
                }

                try
                {
                    _logs.AppendFormat("{0}<br>",condition);
                    if (type == LogType.Exception)
                    {
                        _logs.AppendFormat(ExceptionFormat,stacktrace);
                        SaveClientLogs("Exception error!");
                    }

                }
                catch (Exception e)
                {
                    SaveClientLogs("Logs Reached memory limit!");
                }
            }
        }
    }
}
