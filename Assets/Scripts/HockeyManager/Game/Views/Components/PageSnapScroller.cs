﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum SnapScrollerOrientation
{
    Vertical,
    Horizontal
}
/// <summary>
/// Horizontal list with snap
/// </summary>
[RequireComponent(typeof(ScrollRect))]
public class PageSnapScroller : MonoBehaviour,IEndDragHandler,IDragHandler,IBeginDragHandler
{
    public UnityEvent OnScrollSnapComplete = new UnityEvent();

    [SerializeField]private float snapSpeed = 50f;
    [SerializeField]private float snapThreshoold = 0.01f;

    public SnapScrollerOrientation orientation = SnapScrollerOrientation.Horizontal;

    private ScrollRect scrollRect;

    private RectTransform content;

    private bool snapToClosest;
   

    private Vector3[] positions;
 
    private int childCount;

    private Vector3 startPosition;
    private Vector3 snapPosition;

    private int _selectedPage = 0;

    private GameObject _selectedTarget = null;

    private Image[] children;

    public GameObject SelectedTarget
    {
        get { return _selectedTarget; }
    }

    public int SelectedPage
    {
        get { return _selectedPage; }
        set
        {
            _selectedPage = value;
            _selectedPage = Mathf.Clamp(_selectedPage, 0, childCount - 1);

            ChangeScrollValue();
        }
    }

    private void ChangeScrollValue()
    {
        if (orientation == SnapScrollerOrientation.Horizontal)
        {
            scrollRect.horizontalNormalizedPosition = _selectedPage / (float)(childCount - 1);

        }else if (orientation == SnapScrollerOrientation.Vertical)
        {
            scrollRect.verticalNormalizedPosition = _selectedPage / (float)(childCount - 1);
        }

        OnScrollSnapComplete.Invoke();
    }

    // Use this for initialization
    void Start()
    {

        Mask mask = GetComponent<Mask>();
        if (mask != null)
        {
            mask.enabled = true;
        }
        scrollRect = GetComponent<ScrollRect>();
        content = scrollRect.content;
        
        RebuildPositions();
 
    }


    public void RebuildPositions()
    {
        //float nexXPostion = 0
        childCount = content.childCount;
        children = content.GetComponentsInChildren<Image>();
        positions = new Vector3[childCount];
        for (int i = 0; i < childCount; i++)
        {
            if (orientation == SnapScrollerOrientation.Horizontal)
            {
                scrollRect.horizontalNormalizedPosition = i / (float)(childCount - 1);

            }
            else if (orientation == SnapScrollerOrientation.Vertical)
            {
                scrollRect.verticalNormalizedPosition = i / (float)(childCount - 1);
            }

            positions[i] = content.localPosition;
        }

        _selectedTarget = children[0].gameObject;
        if (orientation == SnapScrollerOrientation.Horizontal)
        {
            scrollRect.horizontalNormalizedPosition = 0f;
        }
        else if (orientation == SnapScrollerOrientation.Vertical)
        {
            scrollRect.verticalNormalizedPosition = 0f;
        }

        OnScrollSnapComplete.Invoke();
    }

    void Update()
    {
        if (snapToClosest)
        {
            content.localPosition = Vector3.LerpUnclamped(content.localPosition, snapPosition, Time.deltaTime * snapSpeed);

            if (orientation == SnapScrollerOrientation.Horizontal)
            {

                if (Math.Abs(content.localPosition.x - snapPosition.x) <= snapThreshoold)
                {
                    snapToClosest = false;
                    OnScrollSnapComplete.Invoke();
                }
            }else if (orientation == SnapScrollerOrientation.Vertical)
            {
                if (Math.Abs(content.localPosition.y - snapPosition.y) <= snapThreshoold)
                {
                    snapToClosest = false;
                    OnScrollSnapComplete.Invoke();
                }
            }

        }
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        Vector3 closest = Vector3.zero;
        float distance = Mathf.Infinity;

        for (int i = 0; i < positions.Length; i++)
        {
            float dst = orientation == SnapScrollerOrientation.Horizontal ? Math.Abs(startPosition.x - positions[i].x) : Math.Abs(startPosition.y - positions[i].y);
            if (dst < distance)
            {
                distance = dst;
                closest = positions[i];
                _selectedPage = i;
                _selectedTarget = children[i].gameObject;
            }
        }
        //Debug.Log("current page :" + _selectedPage);
        snapPosition = closest;
        //content.localPosition = snapPosition;
        snapToClosest = true;
       
        //  
    }

    


    public void OnDrag(PointerEventData eventData)
    {
        snapToClosest = false;
        startPosition = content.localPosition;
    }

    public int MaxPages
    {
        get { return childCount - 1; }
    }


    public void NextPage()
    {
        snapToClosest = false;
        _selectedPage++;
        _selectedPage = Mathf.Clamp(_selectedPage, 0, childCount - 1);
        ChangeScrollValue();
    }

    public void PrevPage()
    {
        snapToClosest = false;
        _selectedPage--;
        _selectedPage = Mathf.Clamp(_selectedPage, 0, childCount - 1);
        ChangeScrollValue();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        snapToClosest = false;
    }
}
