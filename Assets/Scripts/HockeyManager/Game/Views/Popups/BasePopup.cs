﻿using System;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.UI.Components;
using HockeyManager.UI.Popups;
using SCTools.EventsSystem;
using SCTools.Helpers;
using SCTools.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Popups
{
    [Serializable]
    public struct BasePopupControls
    {
        public Button yesButton;
        public Button noButton;
        public Text title;
        public Text textField;
        public TextIconControl[] controls;
        public RectTransform[] holders;
        public TextMeshProUGUI tmpTitle;
        public TextMeshProUGUI tmpTextfield;

    }
    /// <summary>
    /// base for all popups
    /// </summary>
    public class BasePopup : MonoBehaviour,IPopup
    {
        [SerializeField] protected BasePopupControls _basePopupControls;

        private UnityAction<IPopup> _tickCallback;

        protected object _data;

        public PopupType Type
        {
            get; set;
        }

        public IPopup InjectController(IGameController controller)
        {
            SetController(controller);
            return this;
        }

        protected virtual void SetController(IGameController controller)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IPopup SetData(object data)
        {
            _data = data;
            UpdateData();
            return this;
        }

        public object GetData()
        {
            return _data;
        }

        protected virtual void UpdateData()
        {
                
        }

        public IPopup SetMinutesTickUpdate(UnityAction<IPopup> tickCallback)
        {
            if (_tickCallback == null)
            {
                _tickCallback = tickCallback;
                EventsManager.Instance.Add(GameTimer.TikTakMinuteEvent, OnTickTakHandler);
            }
            else
            {
                throw new Exception("Tickcallback already setted!");
            }
        
            return this;
        }

        private void OnTickTakHandler()
        {
            if (_tickCallback != null)
            {
                _tickCallback(this);
            }
        }

        //public BasePopupControls PopupControls
        //{
        //    get { return _basePopupControls; }
        //}

        public IPopup SetButtonLabels(string yesLocaleKey = null, string noButtonLocaleKey = null)
        {
            Text label;
            UITextLocalizator localizator;
            if (_basePopupControls.yesButton != null && yesLocaleKey != null)
            {
                localizator = _basePopupControls.yesButton.GetComponentInChildren<UITextLocalizator>();
                if (localizator != null)
                {
                    localizator.SetLocaleKeys(true,yesLocaleKey);
                    return this;
                }
                label = _basePopupControls.yesButton.GetComponentInChildren<Text>();
                if (label != null)
                {
                    label.text = this.GetLocaleText(yesLocaleKey);
                    label.text = label.text.ToUpper();
                }
            }
            if (_basePopupControls.noButton != null && noButtonLocaleKey != null)
            {
                localizator = _basePopupControls.noButton.GetComponentInChildren<UITextLocalizator>();
                if (localizator != null)
                {
                    localizator.SetLocaleKeys(true,noButtonLocaleKey);
                    return this;
                }

                label = _basePopupControls.noButton.GetComponentInChildren<Text>();
                if (label != null)
                {
                    label.text = this.GetLocaleText(noButtonLocaleKey);
                    label.text = label.text.ToUpper();
                }

            }
            return this;
        }

        public IPopup LockButtons(bool lockYesButton = false, bool lockNoButton = false)
        {
            if (_basePopupControls.yesButton)
            {
                _basePopupControls.yesButton.interactable = !lockYesButton;
            }
            if (_basePopupControls.noButton)
            {
                _basePopupControls.noButton.interactable = !lockNoButton;
            }
            return this;
        }

        public IPopup SetCallbacks(UnityAction applyCallback, UnityAction cancelCallback = null)
        {
            if (_basePopupControls.yesButton && _basePopupControls.yesButton.gameObject.activeSelf)
            {
                if (applyCallback != null)
                {
                    _basePopupControls.yesButton.onClick.AddListener(applyCallback);
                }
            }
            if (_basePopupControls.noButton && _basePopupControls.noButton.gameObject.activeSelf)
            {
                if (cancelCallback != null)
                {
                    _basePopupControls.noButton.onClick.AddListener(cancelCallback);
                }
            }

            return this;
        }

        public IPopup SetTitle(string title,bool localizable = true)
        {
            if (_basePopupControls.title != null)
            {
                _basePopupControls.title.text = localizable ? this.GetLocaleText(title) : title;
               
            }else if (_basePopupControls.tmpTitle != null)
            {
                _basePopupControls.tmpTitle.text = localizable ? this.GetLocaleText(title) : title;
            }
            return this;
        }


        public IPopup HideButtons(bool yesButton, bool noButton)
        {
            if (_basePopupControls.yesButton != null && yesButton)
            {
                _basePopupControls.yesButton.gameObject.SetActive(false);
            }
            if (_basePopupControls.noButton != null && noButton)
            {
                _basePopupControls.noButton.gameObject.SetActive(false);
            }
            return this;
        }

        public IPopup SetTextIconControls(params string[] values)
        {
            if (_basePopupControls.controls != null)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    if (i >= _basePopupControls.controls.Length)
                    {
                        break;
                        
                    }

                    _basePopupControls.controls[i].SetText(values[i]);
                }
            }
           

            return this;
        }

        public IPopup SetTextIconControls(params Sprite[] icons)
        {
            if (_basePopupControls.controls != null)
            {
                for (int i = 0; i < icons.Length; i++)
                {
                    if (i >= _basePopupControls.controls.Length)
                    {
                        break;

                    }

                    _basePopupControls.controls[i].SetIcon(icons[i]);
                }
            }
            return this;
        }

        public IPopup SetVisibilityTextIconControl(string controlName,bool visible)
        {
            if (_basePopupControls.controls != null)
            {
                for (int i = 0; i < _basePopupControls.controls.Length; i++)
                {
                    if (_basePopupControls.controls[i].gameObject.name == controlName)
                    {
                        _basePopupControls.controls[i].gameObject.SetActive(visible);
                    }
                }
            }
            return this;
        }

        public IPopup SetTextIconControls(params int[] values)
        {
            string[] svalues = new string[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                svalues[i] = values[i].ToString();
            }
            SetTextIconControls(svalues);
            return this;
        }

        public IPopup SetText(string text,bool localized = true)
        {
            if (_basePopupControls.textField != null)
            {
                if (!_basePopupControls.textField.gameObject.activeSelf)
                {
                    _basePopupControls.textField.gameObject.SetActive(true);
                }
                _basePopupControls.textField.text = localized ? this.GetLocaleText(text) : text;

            } else if (_basePopupControls.tmpTextfield != null)
            {
                if (!_basePopupControls.tmpTextfield.gameObject.activeSelf)
                {
                    _basePopupControls.tmpTextfield.gameObject.SetActive(true);
                }
                _basePopupControls.tmpTextfield.text = localized ? this.GetLocaleText(text) : text;
            }
            return this;
        }

        public IPopup SetHolderVisibility(string holderName,bool value)
        {
            if (_basePopupControls.holders != null)
            {
                for (int i = 0; i < _basePopupControls.holders.Length; i++)
                {
                    if (_basePopupControls.holders[i].name == holderName)
                    {
                        _basePopupControls.holders[i].gameObject.SetActive(value);
                    }
                }
            }
            return this;
        }

        public virtual void Dispose()
        {
            if (_basePopupControls.noButton)
            {
                _basePopupControls.noButton.onClick.RemoveAllListeners();
            }
            if (_basePopupControls.yesButton)
            {
                _basePopupControls.yesButton.onClick.RemoveAllListeners();
            }

            if (_tickCallback != null)
            {
                EventsManager.Instance.Remove(GameTimer.TikTakMinuteEvent,OnTickTakHandler);
                _tickCallback = null;
            }

            _data = null;
        }
    }
}
