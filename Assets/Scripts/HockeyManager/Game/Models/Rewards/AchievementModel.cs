﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Game.Models.Rewards
{
    public class AchievementModel:IGSParse
    {
        private string _id;
        private RewardData _reward;

        public bool _isTaked = false;

        public AchievementModel(GSData data)
        {
          Parse(data);   
        }

        public RewardData Reward
        {
            get { return _reward; }
        }

        public string Id
        {
            get { return _id; }
        }

        public bool IsTaked
        {
            get { return _isTaked; }
            set { _isTaked = value; }
        }

        public void Parse(GSData data)
        {
            _id = data.GetString("id");
            _reward = JsonUtility.FromJson<RewardData>(data.GetGSData("reward").JSON);
        }
    }
}
