﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.UI.Popups;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Popups
{
    public class TacticBonusSelectorPopup:BasePopup
    {

        [SerializeField] private Toggle[] buttons;
        [SerializeField] private Text attackLabel;
        [SerializeField] private Text deffenceLabel;

        private MainCouchTactic _selectedTactic;
        private EmployeeModel mainCouch;

        public MainCouchTactic SelectedTactic
        {
            get { return _selectedTactic; }
        }


        protected override void UpdateData()
        {
            mainCouch =_data as EmployeeModel;
            
        }

        void Start()
        {
            attackLabel.text = "";
            deffenceLabel.text = "";
            buttons[(int) mainCouch.BonusState].isOn = true;
            _selectedTactic = mainCouch.BonusState;
        }

        public void OnTacticBonusChanged(int tactic)
        {
            _selectedTactic = (MainCouchTactic) tactic;
            float percent = mainCouch.CurrentBonusEffect;
            switch (_selectedTactic)
            {
                case MainCouchTactic.TacticBalance:
                    percent = percent / 2f;
                    attackLabel.text = LocaleManager.Instance.GetText("game.common.attack") + LocaleManager.SPACE + percent.ToString("F1") + "%";
                    deffenceLabel.text = LocaleManager.Instance.GetText("game.common.deffence") + LocaleManager.SPACE + percent.ToString("F1") + "%";
                break;
                case MainCouchTactic.TacticAttack:
                    attackLabel.text = LocaleManager.Instance.GetText("game.common.attack") + LocaleManager.SPACE + percent + "%";
                    deffenceLabel.text = LocaleManager.Instance.GetText("game.common.deffence") + LocaleManager.SPACE + "0%";
                break;
                case MainCouchTactic.TacticDeffence:
                    attackLabel.text = LocaleManager.Instance.GetText("game.common.attack") + LocaleManager.SPACE + "0%";
                    deffenceLabel.text = LocaleManager.Instance.GetText("game.common.deffence") + LocaleManager.SPACE + percent + "%";
                break;
              
            }
            
        }

    }
}
