﻿using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    [ExecuteInEditMode]
    public class TextFitter:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "TextFitter.textfield" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text textfield;
#pragma warning restore CS0649 // Полю "TextFitter.textfield" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "TextFitter.fitTarget" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private RectTransform fitTarget;
#pragma warning restore CS0649 // Полю "TextFitter.fitTarget" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        [SerializeField] private float paddingLeftRight = 0f;
        [SerializeField] private float paddingTopBottom = 0f;

        [SerializeField] private bool vertical = true;
        [SerializeField] private bool horizontal = true;

        void Start()
        {
            FitToText();
        }

        void OnEnable()
        {
            LocaleManager.Instance.LanguageChanged += OnLanguageChangedHandler;
        }

        void OnDisable()
        {
            LocaleManager.Instance.LanguageChanged -= OnLanguageChangedHandler;
        }

        private void OnLanguageChangedHandler()
        {
            FitToText();
        }

        public void FitToText()
        {
            if (fitTarget != null && textfield != null)
            {
                textfield.horizontalOverflow = horizontal ? HorizontalWrapMode.Overflow : HorizontalWrapMode.Wrap;
                textfield.verticalOverflow = vertical ? VerticalWrapMode.Overflow : VerticalWrapMode.Truncate;
                textfield.resizeTextForBestFit = false;

                textfield.text = textfield.text.PadLeft(1).PadRight(1);
                Vector2 sizeDelta = fitTarget.sizeDelta;
                sizeDelta.x = horizontal ? Mathf.FloorToInt(textfield.preferredWidth + paddingLeftRight) : sizeDelta.x;
                sizeDelta.y = vertical ? Mathf.FloorToInt(textfield.preferredHeight + paddingTopBottom) : sizeDelta.y;
                fitTarget.sizeDelta = sizeDelta;
            }
            
        }

        public void SetPaddings(float leftRight = 0,float topBottom = 0)
        {
            paddingTopBottom = topBottom;
            paddingLeftRight = leftRight;
            FitToText();
        }

        public void SetFontSize(int size)
        {
            if (textfield != null)
            {
                textfield.fontSize = size;
                FitToText();
            }
        }

        public void SetText(string value)
        {
            if (textfield != null)
            {
                textfield.text = value;
                FitToText();
            }
        }
        
        void Update()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                FitToText();
            }
        }

        void OnValidate()
        {
            FitToText();
        }

    }
}
