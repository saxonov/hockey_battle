﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Components;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class AchievementItemRenderer:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.achievementLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text achievementLabel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.achievementLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.expPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl expPanel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.expPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.moneyPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl moneyPanel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.moneyPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.fanspanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl fanspanel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.fanspanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl goldPanel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "AchievementItemRenderer.hallItemLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text hallItemLabel;
#pragma warning restore CS0649 // Полю "AchievementItemRenderer.hallItemLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private AchievementModel _model;


        void Awake()
        {
            expPanel.gameObject.SetActive(false);
            moneyPanel.gameObject.SetActive(false);
            fanspanel.gameObject.SetActive(false);
            goldPanel.gameObject.SetActive(false);
            hallItemLabel.gameObject.SetActive(false);
            hallItemLabel.color = UISettings.GreenTextColor;
        }

        public void Refresh()
        {
            if (_model != null)
            {
                gameObject.SetActive(!_model.IsTaked);
            }         
        }

        public void UpdateLanguageFields()
        {
            if (_model != null)
            {
                achievementLabel.text = LocaleManager.Instance.GetText(_model.Id + "_achievement");

                if (hallItemLabel.gameObject.activeSelf)
                {
                    hallItemLabel.text = "+" + LocaleManager.Instance.GetText(_model.Reward.relics + "_hallitem");
                }
            }
        }

        public void SetModel(AchievementModel model)
        {
            if(_model != null)return;
            _model = model;
            achievementLabel.text = LocaleManager.Instance.GetText(model.Id + "_achievement");

            expPanel.gameObject.SetActive(model.Reward.exp > 0);
            moneyPanel.gameObject.SetActive(model.Reward.coins > 0);
            fanspanel.gameObject.SetActive(model.Reward.fans > 0);
            goldPanel.gameObject.SetActive(model.Reward.gold > 0);

            expPanel.SetText(model.Reward.exp.ToString());
            moneyPanel.SetText(model.Reward.coins.ToString());
            fanspanel.SetText(model.Reward.fans.ToString());
            goldPanel.SetText(model.Reward.gold.ToString());

            if (!string.IsNullOrEmpty(model.Reward.relics))
            {
                hallItemLabel.text = "+" + LocaleManager.Instance.GetText(model.Reward.relics + "_hallitem");
                hallItemLabel.gameObject.SetActive(true);
            }

            
        }

    }
}
