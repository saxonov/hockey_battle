﻿using System;

namespace HockeyManager.Game.Dict
{
    public static class GS_ScriptData
    {
        public const string TutorialStepData = "TutorialStep";

        public const string ArenaLockedLotsData = "ArenaLockedLotsData";
        public const string MatchData = "MatchData";
        public const string MatchBattleData = "MatchBattleData";
        public const string MatchTeamSelectionData = "MatchSelectionData";
        public const string MatchHistoryData = "MatchesHistory";

        public const string ChatAvaialbleData = "ChatAvailable";

        public const string TournamentsData = "Tournaments";
        public const string TournamentsHistoryData = "TournamentsHistory";
        public const string TournamentInfoData = "TournamentInfo";
        public const string TournamentSettingsData = "TournamentSettings";

        public const string LastActionsData = "LastActionsData";
        
        public const string ChatHistoryData = "ChatHistory";

        public const string CompetitionsData = "Competitions";
     //   public const string CompetitionsRewardsInfoData = "CompetitionsRewards";

        public const string LeaderBoardPlayersData = "LeaderBoardPlayersData";

        public const string TeamData = "TeamData";
        public const string TeamAttackInfoData = "TeamAttackInfoData";
        public const string GameSettingsData = "GameSettings";
        public const string TeamStatisticData = "TeamStatistic";
        public const string TrainingsData = "Trainings";
        public const string TeamSkillsData = "TeamSkillsData";
        public const string Timestamp = "timestamp";
        public const string BuildingsData = "BuildingsData";
        public const string BuildingsCollectablesData = "BuildingsToCollect";
        public const string BuildingsUpgradesData = "BuildingsUpgradeData";
        public const string BuildingUpgradeData = "BuildingUpgradeData";
        public const string EquipmentsData = "Equipments";
        public const string EquipmentsMaxLevelData = "EquipmentsMaxLevel";
        public const string EquipmentUpgradeData = "EquipmentUpgrades";
        public const string PersonalData = "Personal";
        public const string AchievementsData = "Tasks";

        public const string RewardsData = "RewardsData";
        public const string TakeRewardData = "TakeRewardData";
        public const string AllGainedRewards = "AllGainedRewards";

        public const string HallItemsData = "HallItems";
        public const string HallItemsFansBonusData = "HallItemsFansBonusData";
        public const string ErrorData = "ErrorData";
        public const string LoginData = "loginData";
        public const string NewsData = "News";


        /// <summary>
        /// 
        /// </summary>
        /// <param name="unixTimeStamp"> in milliseconds</param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0,DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp/1000);
            return dtDateTime.ToLocalTime();
        }
    }
}
