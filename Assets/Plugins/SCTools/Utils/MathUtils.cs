﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SCTools.Utils
{
    public static class MathUtils
    {

        public static Vector3 AbsVector(Vector3 source)
        {
            source.x = Math.Abs(source.x);
            source.y = Math.Abs(source.y);
            source.z = Math.Abs(source.z);
            return source;
        }

        public static int Abs(int value)
        {
           return (value ^ (value >> 31)) - (value >> 31);
        }

        public static string SecondsToTimeString(long seconds)
        {
            // int days = 0;
            long hours = 0;
            long mins = 0;
            long secs = 0;

            // check whether the time is 0
            if (seconds <= 0)
                return GetHhMmSsString(hours, mins, secs); ; ;

            // check whether the time is less than hour
            if (seconds < 3600)
            {
                mins = seconds / 60;
                secs = seconds % 60;
                return GetHhMmSsString(hours, mins, secs); ;
            }

            hours = seconds / 3600;
            mins = (seconds % 3600) / 60;
            secs = seconds % 60;

            return GetHhMmSsString(hours, mins, secs);
        }

        public static string SecondsToTimeString(int seconds)
        {
           // int days = 0;
            int hours = 0;
            int mins = 0;
            int secs = 0;
        
            // check whether the time is 0
            if (seconds <= 0 )
                return GetHhMmSsString(hours, mins, secs); ; ;

            // check whether the time is less than hour
            if (seconds < 3600)
            {
                mins = seconds / 60;
                secs = seconds % 60;
                return GetHhMmSsString(hours, mins, secs); ;
            }

            hours = seconds / 3600;
            mins = (seconds % 3600) / 60;
            secs = seconds % 60;

            return GetHhMmSsString(hours, mins, secs);
        }

        public static float SubstractPercent(float targetValue,float percent)
        {
            return targetValue - CalcPercentValue(targetValue, percent);
        }

        public static float AddPercent(float targetValue, float percent)
        {
            return targetValue + CalcPercentValue(targetValue, percent);
        }

        public static float CalcPercentValue(float currentValue, float percent)
        {
            return (currentValue * percent) * 0.01f;
        }

        public static float GetPercent(int currentValue,float maxValue)
        {
            if (maxValue == 0 && currentValue != 0) return 0f;
            return (currentValue / maxValue) * 100f;
        }

        public static float CalcPercent(float targetValue)
        {
            return targetValue/100f;
        }

        public static string GetHhMmSsString(int hours, int mins, int secs)
        {
            return string.Format("{0}:{1}:{2}", (hours < 10) ? "0" + hours : hours.ToString(),
                                                (mins < 10) ? "0" + mins : mins.ToString(),
                                                (secs < 10) ? "0" + secs : secs.ToString());
        }


        public static string GetHhMmSsString(long hours, long mins, long secs)
        {
            return string.Format("{0}:{1}:{2}", (hours < 10) ? "0" + hours : hours.ToString(),
                                                (mins < 10) ? "0" + mins : mins.ToString(),
                                                (secs < 10) ? "0" + secs : secs.ToString());
        }

        public static Vector2[] GetBezierQuadraticPoints(float x0,float y0,float x1,float y1,float x2,float y2,float step= 0.01f)
        {
            List<Vector2> positions = new List<Vector2>();
            Vector2 pos;
            for (float t = 0f; t <= 1f; t += step) 
		    {
                pos = new Vector2();
                pos.x = Mathf.Pow(1 - t, 2) * x0 + 2 * t * (1 - t) * x1 + Mathf.Pow(t, 2) * x2;
                pos.y = Mathf.Pow(1 - t, 2) * y0 + 2 * t * (1 - t) * y1 + Mathf.Pow(t, 2) * y2;

                positions.Add(pos);
            }
            return positions.ToArray();
        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
                angle += 360F;
            if (angle > 360F)
                angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

        public static int GetUniqRandomNumber(int min,int max,int previousRandom)
        {
            int random = Random.Range(min, max);
            if (previousRandom != random)
            {
                return random;
            }
            return GetUniqRandomNumber(min, max, previousRandom);
        }


        public static float GetUniqRandomNumber(float min, float max, float previousRandom)
        {
            float random = Random.Range(min, max);
            if (previousRandom.CompareTo(random) != 0)
            {
                return random;
            }
            return GetUniqRandomNumber(min, max, previousRandom);
        }
    }
}
