﻿using System;
using UnityEngine;
#if(UNITY_EDITOR)
using UnityEditor;
#endif


namespace  SCTools.Helpers
{
    [Serializable]
    public struct ArrayToolData
    {
        public Vector3 startPostion;
        public Vector3 lookTarget;
        public Vector3 direction;
    }

    [ExecuteInEditMode]
    public class ArrayClone : MonoBehaviour
    {
        public ArrayToolData[] positionsInfo;

        public int count = 0;

        public bool addToParent = true;
        public string childNameForBounds;

        public GameObject clonePrefab;
        // Use this for initialization
        void Start()
        {
            if (Application.isPlaying)
            {
                DestroyImmediate(this);
            }

        }

        public void CloneObjects()
        {
            transform.RemoveChildren();


            foreach (var info in positionsInfo)
            {
                if (addToParent)
                {
                    BuildToChild(info);
                }
                else
                {
                    BuildIn(info);
                }
            }

        }

        private void BuildIn(ArrayToolData info)
        {
            for (int i = 0; i < count; i++)
            {
                Transform child = clonePrefab != null ? transform.AddChild(clonePrefab) : transform.AddChild("Child");
                var bounds = child.GetBounds(childNameForBounds);
                var pos = Vector3.Scale(bounds.size, info.direction) * i + info.startPostion;
                child.transform.position = pos;
                child.transform.rotation = Quaternion.LookRotation(info.lookTarget);
            }
        }

        private void BuildToChild(ArrayToolData info)
        {
            for (int i = 0; i < count; i++)
            {
                Transform holder = transform.AddChild("Child");
                Transform child = holder.AddChild(clonePrefab);
                var bounds = child.GetBounds(childNameForBounds);
                var pos = Vector3.Scale(bounds.size, info.direction) * i + info.startPostion;
                holder.transform.position = pos;
                holder.transform.rotation = Quaternion.LookRotation(info.lookTarget);
            }
        }

        public void Clear()
        {
            transform.RemoveChildren();
        }

        public void ClearChilds()
        {

            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).RemoveChildren();
            }


        }

#if (UNITY_EDITOR)
        [MenuItem("GameObject/Utils/AddArrayTool", false, 0)]
        static void AddArrayTool(MenuCommand command)
        {
            GameObject go = (GameObject)command.context;
            go.AddComponent<ArrayClone>();
        }
#endif


        // Update is called once per frame
        void Update()
        {

        }
    }


}
