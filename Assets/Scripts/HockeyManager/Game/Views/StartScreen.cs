﻿using System.Linq;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.UI.Components;
using UnityEngine;
using SCTools.Localization;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HockeyManager.Game.UI
{
    public class StartScreen : MonoBehaviour
    {
        
        [SerializeField] private Text title;
        [SerializeField] private InputField teamNameField;

        [SerializeField] private Slider hueSlider;
        [SerializeField] private Slider saturationSlider;
        [SerializeField] private PageSnapScroller _logoSnapScroller;
        [SerializeField] private Button playButton;
        [SerializeField] private Button leftScrollButton;
        [SerializeField] private Button rightScrollButton;

        [SerializeField] private Animator spinnerAnimator;

        private Outline nameExistBlockerEffect;

        private HSVShaderTuner currentEditableLogo = null;

       // Use this for initialization
        void Start()
        {
            spinnerAnimator.gameObject.SetActive(false);
           // playButton.interactable = false;
            title.text = LocaleManager.Instance.GetText("game.startscreen.title", "\n" + LocaleManager.Instance["game.name"]);
            teamNameField.Select();
            nameExistBlockerEffect = teamNameField.GetComponent<Outline>();
            nameExistBlockerEffect.enabled = false;
        }

        private void OnSaturationValueChanged(float value)
        {
            if (currentEditableLogo != null)
            {
               // teamModel.LogoSaturation = value;
                currentEditableLogo.ChangeSaturation(value);
            }
        }

        private void OnHueValueChanged(float value)
        {
            if (currentEditableLogo != null)
            {
               // teamModel.LogoColor = value;
                currentEditableLogo.ChangeHUE(value);
            }
        }
        void OnEnable()
        {
            playButton.onClick.AddListener(StartGame);
           // EventsManager.Instance.Add<PopupType>(PopupsManager.PopupRemovedEvent,OnPopupRemovedHandler);
            //EventsManager.Instance.Add<GSData>(GlobalEventTypes.UserAuthenticated, OnLoginComplete);
           // teamNameField.onEndEdit.AddListener(OnEndEditHandler);
            teamNameField.onValidateInput += OnValidateTeamNameInput;
            _logoSnapScroller.OnScrollSnapComplete.AddListener(OnLogoSelectedHandler);
            hueSlider.onValueChanged.AddListener(OnHueValueChanged);
            saturationSlider.onValueChanged.AddListener(OnSaturationValueChanged);
        }

      
        void OnDisable()
        {
            playButton.onClick.RemoveListener(StartGame);
            //EventsManager.Instance.Remove<GSData>(GlobalEventTypes.UserAuthenticated, OnLoginComplete);
          //  EventsManager.Instance.Remove<PopupType>(PopupsManager.PopupRemovedEvent, OnPopupRemovedHandler);
           // teamNameField.onEndEdit.RemoveListener(OnEndEditHandler);
            teamNameField.onValidateInput -= OnValidateTeamNameInput;
            _logoSnapScroller.OnScrollSnapComplete.RemoveListener(OnLogoSelectedHandler);
            hueSlider.onValueChanged.RemoveListener(OnHueValueChanged);
            saturationSlider.onValueChanged.RemoveListener(OnSaturationValueChanged);
        }

       
        private char OnValidateTeamNameInput(string text, int charindex, char addedchar)
        {
            
            if (char.IsLetterOrDigit(addedchar) || char.IsWhiteSpace(addedchar) || UISettings.CheckMask.Contains(addedchar))
            {
                return addedchar;
            }
            return '\0';
        }

        //private void OnPopupRemovedHandler(PopupType type)
        //{
        //    if (type == PopupType.ServerErrorPopup)
        //    {
        //        teamNameField.onEndEdit.AddListener(OnEndEditHandler);
        //    }
        //}


        //private void OnEndEditHandler(string text)
        //{
        //    Debug.Log("end edit");
            
        //    if (GS.Authenticated && text != string.Empty)
        //    {
        //        CheckTeamNameExist(text);
        //    }
        //   // teamModel.Name = text;
        //}

        //private void OnTeamNameAvailable(LogEventResponse responce)
        //{
           
        //    if (responce.HasErrors)
        //    {
        //        nameExistBlockerEffect.enabled = true;
        //        teamNameField.Select();
        //        playButton.interactable = false;
        //    }
        //    else
        //    {
        //        nameExistBlockerEffect.enabled = false;
        //        playButton.interactable = true;

        //    }
        //}


        private void OnLogoSelectedHandler()
        {
            currentEditableLogo = _logoSnapScroller.SelectedTarget.GetComponent<HSVShaderTuner>();
            //teamModel.LogoId = logoScroller.SelectedPage;
            leftScrollButton.interactable = _logoSnapScroller.SelectedPage != 0;
            rightScrollButton.interactable = _logoSnapScroller.SelectedPage != _logoSnapScroller.MaxPages;
        }


        private void StartGame()
        {
            if (string.IsNullOrEmpty(teamNameField.text))
            {
                nameExistBlockerEffect.enabled = true;
                teamNameField.Select();
                return;
            }

            _logoSnapScroller.enabled = false;
            
            var logoId = _logoSnapScroller.SelectedPage;
            var teamName = teamNameField.text;
            var logoColor = hueSlider.value;
            var logoSat = saturationSlider.value;

            CreateTeam(teamName,logoId,logoColor,logoSat);
           
        }

        private void OnTeamCreatedHandler(LogEventResponse responce)
        {
            if (responce.HasErrors)
            {
                teamNameField.text = string.Empty;
               
                PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                    .SetTitle("game.common.teamnameexist")
                    .SetButtonLabels(null, "game.common.closebutton")
                    .HideButtons(true,false)
                    .SetCallbacks(() =>
                    {
                        nameExistBlockerEffect.enabled = true;
                        teamNameField.Select();
                    });
            }
            else
            {
                spinnerAnimator.gameObject.SetActive(true);
                nameExistBlockerEffect.enabled = false;
                SceneManager.LoadSceneAsync(UISettings.MainScene);
            }
        }

        private void CreateTeam(string teamName, int logoId, float logoColor, float logoSaturation)
        {
            var data = new GSRequestData();
            data.Add("id", logoId);
            data.Add("color", logoColor);
            data.Add("saturation", logoSaturation);

            LogEventRequest request = new LogEventRequest();
            request.SetEventKey("CREATE_TEAM");
            request.SetEventAttribute("NAME",teamName);
            request.SetEventAttribute("LOGO_DATA", data);
            request.SetEventAttribute("LOCALE", (int) LocaleManager.Instance.CurrentLocaleType);
            GameRoot.Instance.NetManager.RunLogEventRequest(request, OnTeamCreatedHandler,null,true);
        }


        //private void CheckTeamNameExist(string teamName)
        //{
        //    GameRoot.Instance.RunLogEventRequest(new LogEventRequest().
        //        SetEventKey("CHECK_TEAM_NAME").
        //        SetEventAttribute("NAME",teamName), OnTeamNameAvailable,"Name already exist!");
        //}
    }

}

