﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Base;

namespace HockeyManager.Game.Impl
{
    public interface IBuildingModel:IEffect,IBaseGameEntityModel
    {
        MapBuildingsType Id { get; }

        //BuildingUpgradeInfo UpgradeInfo { get; }

        GSData ExtraData { get; }

        bool IsCanCollect { get; }
        bool IsCollectable { get; }

        int Pos { get; }
      
        int Level { get; }
        int MaxLevel { get; }
        bool IsBuilded { get; }
    }
}
