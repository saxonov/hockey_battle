﻿using DG.Tweening;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class EmployeeItemRenderer : MonoBehaviour
    {
        public const string EmployeeSelectedEvent = "employeeSelectedEvent";

        [SerializeField] private Text maxLevelReachedLabel;
        [SerializeField] private Sprite fansIcon;
        [SerializeField] private Sprite energyIcon;
        [SerializeField] private Image picture;
        [SerializeField] private Text title;
        [SerializeField] private Text description;
        [SerializeField] private Text levelLabel;
        [SerializeField] private Text effectLabel;
        [SerializeField] private Text requirementLabel;
        [SerializeField] private Text upgradeLabel;
        [SerializeField] private TextIconControl upgradePrice;
        [SerializeField] private TextIconControl effectPanel;
        [SerializeField] private TextIconControl timerPanel;
        [SerializeField] private Button upgradeButton;
        [SerializeField] private Button finishUpgradeButton;
        [SerializeField] private RectTransform currentUpgradeBox;
        [SerializeField] private RectTransform upgradePriceBox;
        [SerializeField] private Slider upgradeSlider;
        [SerializeField] private Button tacticBonusButton;

        private EmployeeModel _model;
        private bool _isUpgradingNow = false;
        private static int _teamLevel;


        void Awake()
        {
            //effectPanel.gameObject.SetActive(false);
            upgradeButton.onClick.AddListener(OnUpgradeButtonClickHandler);
            finishUpgradeButton.onClick.AddListener(OnFinishButtonCLickHandler);
        }


        void OnDestroy()
        {
            if (_model != null)
            {
                _model.OnModelChangedEvent.RemoveListener(OnModelChanged);
                _model.OnTimerLeftSecondsEvent.RemoveListener(OnTikTakHandler);
                _model = null;
            }
            upgradeButton.onClick.RemoveListener(OnUpgradeButtonClickHandler);
            finishUpgradeButton.onClick.RemoveListener(OnFinishButtonCLickHandler);
        }

   


        private void OnFinishButtonCLickHandler()
        {
            if (_isUpgradingNow)
            {
               EventsManager.Instance.Call(EmployeeSelectedEvent, _model);
            }
            
        }


        private void OnUpgradeButtonClickHandler()
        {
           EventsManager.Instance.Call(EmployeeSelectedEvent, _model);
        }

        public void SetModel(EmployeeModel model,int teamLevel)
        {
            _teamLevel = teamLevel;
            if (_model == null)
            {
                _model = model;
                _model.OnModelChangedEvent.AddListener(OnModelChanged);
            }
           
            OnModelChanged();
            gameObject.SetActive(true);
        }
        
        void Start()
        {
            if (!GameData.IsTutorialPassed  && _model != null && _model.Id == EmployeeType.MainCouch)
            {
                EventsManager.Instance.Add(TutorialManager.TutorialStepChanged,OnTutorialStepChanged);
                OnTutorialStepChanged();
            }
        }

        private void OnTutorialStepChanged()
        {
                if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.HireMainCouch))
                {
                    TutorialManager.Instance.CurrentView.SetShowTarget(upgradeButton.GetComponent<RectTransform>());

                }else if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.FinishMainCouch))
                {
                    EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                    TutorialManager.Instance.CurrentView.SetShowTarget(finishUpgradeButton.GetComponent<RectTransform>());
                }
        }


        private void OnModelChanged()
        {
           
            if (_model.IsUpgrading)
            {
                SetProgress();
            }
            else
            {
                ResetProgress();
            }
            SetEffectValue();
        }

        private void SetEffectValue()
        {
            LocaleManager locales = LocaleManager.Instance;
            string effectStr = "<color='#E1DD11FF'>{0}:</color>";
            switch (_model.Id)
            {
                case EmployeeType.MainCouch:
                    effectLabel.text = string.Format(effectStr, locales.GetText("game.main.personal.effect.maincouch")) + LocaleManager.SPACE + _model.CurrentBonusEffect + "%";
                break;
                case EmployeeType.Adman:
                    effectLabel.text = string.Format(effectStr, locales.GetText("game.main.personal.effect.adman"));
                    effectPanel.gameObject.SetActive(true);
                    effectPanel.SetText("+" + _model.CurrentBonusEffect + "%");
                    effectPanel.SetIcon(null);
                break;
                case EmployeeType.Doctor:
                    int timeBonus = _model.CurrentBonusEffect == 0 ? GameData.GSConstData.EnergyStartTime : _model.CurrentBonusEffect;
                    int energyPerHour = Mathf.FloorToInt(3600f/ timeBonus);
                    effectLabel.text = string.Format(effectStr, locales.GetText("game.main.personal.effect.teamdoctor"));
                    effectPanel.gameObject.SetActive(true);
                    effectPanel.SetText(LocaleManager.Instance.GetText("game.common.perhour", energyPerHour));
                    effectPanel.SetIcon(energyIcon);
                    break;
                case EmployeeType.SecondCouch:
                    effectLabel.text = string.Format(effectStr, locales.GetText("game.main.personal.effect.secondcouch")) + " +" + _model.CurrentBonusEffect + "%";
                    break;
            }
        }

        private void SetInfo()
        {
            EmployeeSettingsInfo employeeInfo = GameSettings.Instance.GetEmployeeData(_model.Id);
            picture.sprite = employeeInfo.GetPreview();
            title.text = employeeInfo.GetName().ToUpper();
            description.text = employeeInfo.GetDescription();

            levelLabel.gameObject.SetActive(_model.Level > 0);
            if (levelLabel.enabled)
            {
                levelLabel.color = UISettings.GreenTextColor;
                levelLabel.text = LocaleManager.Instance.GetText("game.common.level2", _model.Level);

            }

            if (_model.Level < _model.MaxLevel)
            {
                maxLevelReachedLabel.gameObject.SetActive(false);
                timerPanel.SetText(GameTimer.GetGameTimeString(_model.NextUpgradeTimeSeconds));

                string nextUpgradeLevel = LocaleManager.Instance.GetText("game.common.shortlevel",
                    _model.NextUpgradeLevel);
                upgradeLabel.text = LocaleManager.Instance.GetText("game.main.personal.upgradeprice", nextUpgradeLevel);
                upgradePrice.SetText(_model.UpgradePrice.ToString());

                if (_teamLevel >= _model.UnlockLevel)
                {
                    requirementLabel.color = UISettings.GreenTextColor;
                    requirementLabel.text = LocaleManager.Instance.GetText("game.main.personal.requirementscomplete");
                    // upgradeButton.interactable = true;
                }
                else
                {
                    requirementLabel.color = UISettings.BlockTextColor;
                    requirementLabel.text = LocaleManager.Instance.GetText("game.main.personal.requirement",
                        _model.UnlockLevel);
                    // upgradeButton.interactable = false;
                }

            }
            else
            {
                upgradeButton.gameObject.SetActive(false);
                requirementLabel.gameObject.SetActive(false);
                timerPanel.gameObject.SetActive(false);
                upgradePriceBox.gameObject.SetActive(false);
                maxLevelReachedLabel.gameObject.SetActive(true);
            }

            if (tacticBonusButton != null)
            {
                tacticBonusButton.interactable = _model.Level > 0 && !_model.IsUpgrading;
            }
        }

        private void SetProgress()
        {
            if (!_isUpgradingNow)
            {

                _isUpgradingNow = true;
                upgradeLabel.gameObject.SetActive(false);
                upgradePrice.gameObject.SetActive(false);
                upgradeButton.gameObject.SetActive(false);
                requirementLabel.gameObject.SetActive(false);

                long secondsLeft = GameTimer.TimeLeftSeconds(_model.EndUpgradeTimeSeconds);
                upgradeSlider.minValue = 0;
                upgradeSlider.maxValue = _model.UpgradeTimeSeconds;
                upgradeSlider.DOValue(_model.UpgradeTimeSeconds - secondsLeft, 1f, true);

                timerPanel.SetText(GameTimer.GetGameTimeString(secondsLeft));

                finishUpgradeButton.gameObject.SetActive(true);
                currentUpgradeBox.gameObject.SetActive(true);

                _model.OnTimerLeftSecondsEvent.AddListener(OnTikTakHandler);
                OnTikTakHandler(secondsLeft);
            }
            SetInfo();
        }

        private void OnTikTakHandler(long secondsLeft)
        {
            timerPanel.SetText(GameTimer.GetGameTimeString(secondsLeft));
            upgradeSlider.DOValue(_model.UpgradeTimeSeconds - secondsLeft,2f);
            if (secondsLeft == 0)
            {
                ResetProgress();
            }
        }

        private void ResetProgress()
        {
            _isUpgradingNow = false;
            upgradeLabel.gameObject.SetActive(true);
            upgradePrice.gameObject.SetActive(true);
            upgradeButton.gameObject.SetActive(true);
            requirementLabel.gameObject.SetActive(true);

            finishUpgradeButton.gameObject.SetActive(false);
            currentUpgradeBox.gameObject.SetActive(false);
            SetInfo();
        }
    }
}
