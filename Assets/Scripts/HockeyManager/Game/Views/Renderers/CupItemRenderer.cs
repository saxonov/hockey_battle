﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class CupItemRenderer:MonoBehaviour
    {

        public const string OnParticipateClick = "CupItemRenderer.OnParticipateClick";

        [SerializeField]private Text levelsLabel;
        [SerializeField] private Text cupNameLabel;
        [SerializeField] private Text participants;
        [SerializeField] private Image selection;
        [SerializeField] private TextIconControl feePrice;
        [SerializeField] private TextIconControl rewardField;
        [SerializeField] private TextIconControl energyCostField;
        [SerializeField] private TextIconControl timerField;
        [SerializeField] private Button participateButton;

        private TournamentBattleModel _model;

        void Start()
        {
            participateButton.onClick.AddListener(OnParticipateClickHandler);
        }

        void OnDestroy()
        {
            _model = null;
            participateButton.onClick.RemoveListener(OnParticipateClickHandler);
        }

        private void OnParticipateClickHandler()
        {
            EventsManager.Instance.Call(OnParticipateClick,_model);
        }

        public void InvertSelection(bool value)
        {
            if (value)
            {
                selection.color = new Color(0f,0f,0f,1f);
            }
            else
            {
                selection.color = new Color(0f, 0f, 0f,0f);
            }
           
        }


        public void SetModel(TournamentBattleModel model)
        {
            _model = model;
            
            energyCostField.SetText(_model.EnergyCost);

            feePrice.SetIcon(GameSettings.Instance.GetIcon(_model.CurrencyType));
            feePrice.SetText(_model.FeePrice);

            rewardField.SetIcon(GameSettings.Instance.GetIcon(_model.CurrencyType));
            rewardField.SetText(_model.CurrencyAward);

            timerField.SetText(MathUtils.SecondsToTimeString(_model.GetTotalPlayTimeSeconds()));
            cupNameLabel.text = model.GetName();

            levelsLabel.color = UISettings.GreenTextColor;
            levelsLabel.text = LocaleManager.Instance.GetText("game.common.level", model.MinLevel + "-" + model.MaxLevel);

            string players = _model.Players.Count + "/" + _model.MaxPlayers;
            string participantsLabel = LocaleManager.Instance.GetText("game.main.tournament.participants2");
            participants.text = participantsLabel.GetUITextSizeFormated(50) + '\n' + players.GetUITextSizeFormated(110);
        }     
    }
}
