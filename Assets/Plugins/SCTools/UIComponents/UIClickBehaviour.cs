﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SCTools.UIComponents
{
    [RequireComponent(typeof(Graphic))]
    public class UIClickBehaviour:MonoBehaviour,IPointerClickHandler
    {
        public UnityEvent OnClick;



        void Awake()
        {
            if (OnClick == null)
            {
                OnClick = new UnityEvent();
            }
        }

        void OnDestroy()
        {
            if (OnClick != null)
            {
                OnClick.RemoveAllListeners();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(OnClick != null)
            {
                OnClick.Invoke();
            }
        }
    }
}
