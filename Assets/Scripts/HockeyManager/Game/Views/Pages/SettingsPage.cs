﻿using System;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Views.Buttons;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Helpers;
using SCTools.Localization;
using SCTools.SoundManager;
using SCTools.UI;

using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class SettingsPage:MonoBehaviour,IPageChangeHandler
    {
        [SerializeField] private RectTransform setingsContent;
        [SerializeField] private RectTransform newsContent;
        [SerializeField] private ScrollRect newsScroller;
        [SerializeField] private GameObject newsItemRendererPrefab;

        [SerializeField] private UniversalButton[] settingsButton;

        [SerializeField] private Sprite[] localeIcons;

        [SerializeField] private RectTransform settingsContent;
//#if UNITY_EDITOR
        [SerializeField] private Button resetButton;
//#endif
        [Space(5)]
        [SerializeField]private InnerPageNavigation navBar;

        [Space(5)]
        [SerializeField]private AccountButton[] accountButtons;

        [SerializeField] private UITextLocalizator accountTitle;

        private AccountButton accountButton = null;
        //[SerializeField] private InnerPageNavButtonData[] provider;
        //[SerializeField] private InnerPageNavigation navBar;

        private UniversalButton localeButton;

        private SettingsNewsController _controller;

        private string onLocaleKey = "game.common.on";
        private string offLocaleKey = "game.common.off";     

        void Awake() 
        {
            _controller = GameRoot.Instance.GetController<SettingsNewsController>();
        }

        void Start()
        {
            PageNavigatorManager.Instance.AddPageChangeHandler(this);
            int i;
            for (i = 0; i < settingsButton.Length; i++)
            {
                settingsButton[i].OnCustomClick.AddListener(OnSettingsButtonClickHandler);
                
                switch (settingsButton[i].GetId())
                {
                    case "locale":
                        localeButton = settingsButton[i];
                        localeButton.SetIcon(localeIcons[(int)LocaleManager.Instance.CurrentLocaleType]);
                        localeButton.SetLabel(LocaleManager.Instance.GetText("game.main.settingspage.language.name"));
                        break;
                    case "sound":
                        settingsButton[i].Selected = _controller.GameSettingsData.sound;
                        break;
                    case "music":
                        settingsButton[i].Selected = _controller.GameSettingsData.music;
                        break;
                    case "notification":
                        settingsButton[i].Selected = _controller.GameSettingsData.notification;
                        break;
                }
                if (settingsButton[i].GetId() != "locale")
                {
                    settingsButton[i].SetLabel(LocaleManager.Instance.GetText(settingsButton[i].Selected ? onLocaleKey : offLocaleKey));
                }
            }
            if (accountButtons != null)
            {
                for (i = 0; i < accountButtons.Length; i++)
                {
                    accountButtons[i].gameObject.SetActive(false);
                }
            }

        }

        private void OnSettingsButtonClickHandler(UniversalButton sender)
        {
            string id = sender.GetId();
            switch (id)
            {
                case "music":
                    _controller.GameSettingsData.isChanged = true;
                    _controller.GameSettingsData.music = sender.Selected;
                    SoundManager.Instance.MuteMusic = !sender.Selected;
                    break;
                case "sound":
                    _controller.GameSettingsData.isChanged = true;
                    _controller.GameSettingsData.sound = sender.Selected;
                    SoundManager.Instance.MuteSFX = !sender.Selected;
                    break;
                case "notification":
                    _controller.GameSettingsData.isChanged = true;
                    _controller.GameSettingsData.notification = sender.Selected;
                    break;
                case "locale":
                   PopupsManager.Instance.AddPopup(PopupType.LanguagePopup).SetCallbacks(() =>
                   {
                       _controller.SaveGameSettings();
                   });
                break;
            }
            for (int i = 0; i < settingsButton.Length; i++)
            {
                if (localeButton.GetId() != settingsButton[i].GetId())
                {
                    settingsButton[i].SetLabel(LocaleManager.Instance.GetText(settingsButton[i].Selected ? onLocaleKey : offLocaleKey));
                }
            }

             _controller.SaveGameSettings();
        }

        void OnEnable()
        {
//#if UNITY_EDITOR
            if (resetButton.gameObject.activeSelf)
            {
                resetButton.onClick.AddListener(OnResetGameClickHandler);
            }
//#endif
            EventsManager.Instance.Add(AccountButton.AccountButtonClickEvent,OnAccountButtonClickHandler);
            LocaleManager.Instance.LanguageChanged += OnLanguageChangedHandler;
        }

        private void OnAccountButtonClickHandler()
        {
            if (accountButton != null && !accountButton.IsBinded)
            {
                
                EventsManager.Instance.Add<bool>(GlobalEventTypes.SocialAccountConnectedResult,OnSocialAccountConnectedHandler);
                if (accountButton.Type == SocialAccountType.GameCenter)
                {
#if UNITY_EDITOR || UNITY_IOS

                    ShowNativeDialog();
#endif
                }
                else
                {
                    PopupsManager.Instance.ShowFixedPreloader();
                    GameRoot.Instance.Authtorization.LoginType = accountButton.Type;
                    GameRoot.Instance.Authtorization.Login();
                }
               
                //accountButton.Type
            }
        }

#if UNITY_IOS || UNITY_EDITOR

        private IOSDialog dialog;
        /// <summary>
        /// special case for GameCenter login
        /// </summary>
        private void ShowNativeDialog()
        {
            string title = this.GetLocaleText("game.common.ios.gamecenter.notconnected");
            string desc = this.GetLocaleText("game.common.ios.gamecenter.connectionrequirements");
            Debug.Log("Show native ios popup for GameCenter Aware Message!");

            dialog = IOSDialog.Create(title, desc, this.GetLocaleText("game.common.yesbutton"), this.GetLocaleText("game.common.nobutton"));
            dialog.OnComplete += OnCloseDialogHandler;

        }

        private void OnCloseDialogHandler(IOSDialogResult result)
        {
            dialog.OnComplete -= OnCloseDialogHandler;
            dialog = null;
            if (result == IOSDialogResult.YES)
            {
                Debug.Log("Try to Login GameCenter again!");
                PopupsManager.Instance.ShowFixedPreloader();
                GameRoot.Instance.Authtorization.LoginType = accountButton.Type;
                GameRoot.Instance.Authtorization.Login();
            }
        }
#endif

        private void OnSocialAccountConnectedHandler(bool result)
        {
            EventsManager.Instance.Remove<bool>(GlobalEventTypes.SocialAccountConnectedResult, OnSocialAccountConnectedHandler);
            PopupsManager.Instance.RemoveFixedPreloader();
            accountButton.IsBinded = result;
            if (result)
            {
                accountTitle.SetLocaleKeys(false, "game.main.accountspage.unbindtooltip");
                accountButton.IsLocked = true;
            }
        }


        void OnDisable()
        {
#if UNITY_EDITOR
            if (resetButton.gameObject.activeSelf)
            {
                resetButton.onClick.RemoveListener(OnResetGameClickHandler);
            }
#endif
            EventsManager.Instance.Remove(AccountButton.AccountButtonClickEvent, OnAccountButtonClickHandler);
            LocaleManager.Instance.LanguageChanged -= OnLanguageChangedHandler;
           
        }

        void OnDestroy()
        {
            _controller = null;
        }


        public void OnResetGameClickHandler()
        {
             _controller.ResetTeam();
        }

#if UNITY_EDITOR
        public void OnAddTestRewards()
        {
            _controller.AddTestRewards();
        }
#endif

        public void GotoWebSait()
        {
            GameRoot.Instance.NetManager.Logger.SaveClientLogs("From Settings Page Logs:");
            Application.OpenURL("http://hockeybattle.ru/forum/");
        }

        public void GotoVKGroup()
        {
            Application.OpenURL("https://vk.com/hockeybattle_game");
        }

        private void OnLanguageChangedHandler()
        {
            LocaleType type = LocaleManager.Instance.CurrentLocaleType;
            _controller.GameSettingsData.isChanged = true;
            _controller.GameSettingsData.locale = type;
            localeButton.SetIcon(localeIcons[(int)type]);
            localeButton.SetLabel(LocaleManager.Instance.GetText("game.main.settingspage.language.name"));
            for (int i = 0; i < settingsButton.Length; i++)
            {
                if (localeButton.GetId() != settingsButton[i].GetId())
                {
                    settingsButton[i].SetLabel(LocaleManager.Instance.GetText(settingsButton[i].Selected ? onLocaleKey : offLocaleKey));
                }
            }
        }
        private void BuildNewsList()
        {
            if (_controller.News != null)
            {
                newsScroller.content.RemoveChildren();
                for (int i = 0; i < _controller.News.Count; i++)
                {
                    NewsItemRenderer item = newsScroller.content.AddChild(newsItemRendererPrefab,false).GetComponent<NewsItemRenderer>();
                    item.SetData(_controller.News[i]);
                }
            }
            
        }

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (selected == NavigationPageTypes.News)
            {
                navBar.SelectButton(1, true);
                navBar.SelectButton(2, false);
                newsScroller.content.RemoveChildren();
                newsContent.gameObject.SetActive(true);
                setingsContent.gameObject.SetActive(false);
                _controller.LoadNews(BuildNewsList);
                // setingsContent.gameObject.SetActive(false);

            }
            else if (selected == NavigationPageTypes.Settings)
            {
                navBar.SelectButton(1, true);
                // navBar.SelectButton(1, true);
                // navBar.SelectButton(2, false);
                setingsContent.gameObject.SetActive(true);
                newsContent.gameObject.SetActive(false);
                //setingsContent.gameObject.SetActive(true);
            }else if (selected == NavigationPageTypes.Accounts)
            {
               // navBar.SelectButton(1, false);
                navBar.SelectButton(2, true);
                newsContent.gameObject.SetActive(false);
                setingsContent.gameObject.SetActive(false);
                if (accountButtons != null)
                {
                    for (int i = 0; i < accountButtons.Length; i++)
                    {
                        #if UNITY_ANDROID || UNITY_EDITOR

                        if (accountButtons[i].Type == SocialAccountType.GooglePlayGames)
                        {
                            accountButton = accountButtons[i];
                            accountButton.gameObject.SetActive(true);
                            accountButton.IsBinded = GameRoot.Instance.Authtorization.LoginType == accountButton.Type;
                            if (accountButton.IsBinded)
                            {
                                accountTitle.SetLocaleKeys(false, "game.main.accountspage.unbindtooltip");
                            }
                            
                        }
                        else
                        {
                            Destroy(accountButtons[i].gameObject);
                        }
#elif UNITY_IOS
                                        if (accountButtons[i].Type == SocialAccountType.GameCenter)
                                        {
                                            accountButton = accountButtons[i];
                                            accountButton.gameObject.SetActive(true);
                                            accountButton.IsBinded = GameRoot.Instance.Authtorization.LoginType == accountButton.Type;
                                              if (accountButton.IsBinded)
                                                {
                                                    accountTitle.SetLocaleKeys(false, "game.main.accountspage.unbindtooltip");
                                                }
                                        }else
                                        {
                                            Destroy(accountButtons[i].gameObject);
                                        }
#endif
                    }
                    accountButtons = null;
                }
            }
           
        }
    }
}
