﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace HockeyManager.TutorialSystem.Base
{
    public abstract class TutorialObject:MonoBehaviour
    {

        public PointerEventData pointerEventData;

        public abstract void OnUpState();
        
        public abstract void OnDownState();
    }
}
