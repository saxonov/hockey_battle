﻿using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Popups;

namespace HockeyManager.Game.Views.Popups
{


    public class RewardAdsPopup : BasePopup
    {

        private RewardModel _reward;

        private long _endTime = 0;

        void OnEnable()
        {
            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
        }

        void OnDisable()
        {
            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
        }

        void Start()
        {
            _basePopupControls.yesButton.onClick.AddListener(LockAdsButton);
        }

        private void LockAdsButton()
        {
            _basePopupControls.yesButton.interactable = false;
            _basePopupControls.noButton.interactable = false;
        }

        protected override void UpdateData()
        {
            _reward = (RewardModel) _data;
            _endTime = _reward.CustomData.GetLong("endShowTime").Value;

            int count = _reward.CustomData.GetInt("count").Value;
            if (count > 0)
            {
                _basePopupControls.controls[0].gameObject.SetActive(true);
                _basePopupControls.controls[1].gameObject.SetActive(false);
                SetTitle("game.main.popup.rewardad");
                _basePopupControls.controls[0].SetText(_reward.RewardData.gold);
            }
            else
            {
                _basePopupControls.controls[0].gameObject.SetActive(false);
                _basePopupControls.controls[1].gameObject.SetActive(true);
                SetTitle("game.main.popup.rewardadtimelimit");
                _basePopupControls.yesButton.gameObject.SetActive(false);
                OnTikTakHandler();
               
                
            }
        }


        private void OnTikTakHandler()
        {
            if (_basePopupControls.controls[1].gameObject.activeSelf)
            {
                long timeLeft = GameTimer.TimeLeftSeconds(_endTime);
                if (timeLeft > 0)
                {
                    _basePopupControls.controls[1].SetText(GameTimer.GetGameTimeString(timeLeft));

                }
                else
                {
                    _basePopupControls.noButton.onClick.Invoke();
                }
            }       
        }
    }
}
