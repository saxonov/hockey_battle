﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Views.Panels;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    public class FooterNavButton:MonoBehaviour,IPointerClickHandler
    {
        
        [HideInInspector]
        public FooterPanel footerPanel;

#pragma warning disable CS0649 // Полю "FooterNavButton.pageTypesAffect" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private NavigationPageTypes[] pageTypesAffect;
#pragma warning restore CS0649 // Полю "FooterNavButton.pageTypesAffect" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Toggle toggle;

        void Start()
        {
            if (toggle == null)
            {
                toggle = GetComponent<Toggle>();
            }
        }

        

        public bool CheckPage(NavigationPageTypes page)
        {
            for (int i = 0; i < pageTypesAffect.Length; i++)
            {
                if (pageTypesAffect[i].Equals(page))
                {
                    return true;
                }
            }
            return false;
        }

        public void Lock()
        {
            toggle.interactable = false;
        }

        public void Unlock()
        {
            toggle.interactable = true;
        }

        public void Select()
        {
            toggle.isOn = true;
        }

        public void Deselect()
        {
            toggle.isOn = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (footerPanel != null)
            {
                footerPanel.OnFooterButtonClickHandler(pageTypesAffect[0]);
            }
        }
    }
}
