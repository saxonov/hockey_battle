﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.UI.Popups;
using SCTools.EventsSystem;
using SCTools.SoundManager;

namespace HockeyManager.Game.Managers
{
    public class PopupsManager : MonoBehaviour
    {
        public const string PopupAddedEvent = "PopupAddedEvent";
        public const string PopupRemovedEvent = "PopupRemovedEvent";
        public const string LastPopupRemoved = "LastPopupRemoved";

       
        private bool _hasActivePopups = false;

        private List<int> _requestHashCodes;

        private static PopupsManager _instance;

        [SerializeField]
        private float preloadDelaySeconds = 1f;

        [SerializeField]
        private RectTransform content;

        [SerializeField]
        private GameObject preloaderPopup;
 
        private Dictionary<PopupType, GameObject> _prefabsList;

        private List<IPopup> _popupsList; 

        private float _originDelaySeconds = 0f;
        private bool _isFixedPreloader = false;

        //safe remove preloader, if deadlock case,5 minutes wait
        private WaitForSeconds removePreloaderDelay = new WaitForSeconds(35);

        public static PopupsManager Instance
        {
            get { return _instance; }
        }

        public void ShowFixedPreloader()
        {
            if (preloaderPopup != null && !_isFixedPreloader)
            {
                Debug.Log("fixed preloader show!");
                _isFixedPreloader = true;
                preloaderPopup.gameObject.SetActive(true);
            }
        }

        public void RemoveFixedPreloader()
        {
            if (preloaderPopup != null && _isFixedPreloader)
            {
                Debug.Log("fixed preloader removed!");
                _isFixedPreloader = false;
                preloaderPopup.gameObject.SetActive(false);
            }
        }

        public bool HasActivePopups
        {
            get { return _hasActivePopups; }
        }

        public void SetPreloadDelaySeconds(float seconds)
        {
            preloadDelaySeconds = seconds;
        }

        void Awake()
        {
            _requestHashCodes = new List<int>();
            _originDelaySeconds = preloadDelaySeconds;
            _prefabsList = new Dictionary<PopupType, GameObject>();
            _popupsList = new List<IPopup>();
            _instance = this;
        }

        void Start()
        {
            content = GetComponent<RectTransform>();
            if (preloaderPopup != null)
            {
                content.RemoveChildren(preloaderPopup.gameObject.name);
                RemovePreload();
            }
            else
            {
                content.RemoveChildren();
            }

          //  StartTest();

        }

        //private void StartTest()
        //{
        //    for (int i = 0; i < 10; i++)
        //    {
        //        AddPopup(PopupType.AlertPopup).SetTitle("Test:" + (i + 1));
        //    }
        //}

        void OnDestroy()
        {
            _instance = null;
            while (_popupsList != null && _popupsList.Count > 0)
            {
                Destroy(_popupsList[0].gameObject);
                _popupsList.RemoveAt(0);
            }
        }

        void OnEnable()
        {
            //EventsManager.Instance.Add(GlobalEventTypes.ShowLockPreloader, OnShowPreloader);
           // EventsManager.Instance.Add(GlobalEventTypes.HideLockPreloader, OnHidePreloader);
            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionLost,ShowFixedPreloader);
            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionExist,RemoveFixedPreloader);
            EventsManager.Instance.Add<int>(GlobalEventTypes.ServerRequestSended, OnServerRequestSendHandler);
            EventsManager.Instance.Add<GSData>(GlobalEventTypes.ServerResponceError, ShowServerErrorPopup);
            EventsManager.Instance.Add<int>(GlobalEventTypes.ServerResponceRecieved, OnServerResponceHandler);
        }

        void OnDisable()
        {
          //  EventsManager.Instance.Remove(GlobalEventTypes.ShowLockPreloader, OnShowPreloader);
           // EventsManager.Instance.Remove(GlobalEventTypes.HideLockPreloader, OnHidePreloader);
            EventsManager.Instance.Remove<GSData>(GlobalEventTypes.ServerResponceError, ShowServerErrorPopup);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionLost, OnInternetConnectionLost);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionExist, OnInternetConnectionExist);
            EventsManager.Instance.Remove<int>(GlobalEventTypes.ServerRequestSended, OnServerRequestSendHandler);
            EventsManager.Instance.Remove<int>(GlobalEventTypes.ServerResponceRecieved, OnServerResponceHandler);
        }

        private void OnServerResponceHandler(int hash)
        {
            if (_requestHashCodes.Count > 0)
            {
                if (_requestHashCodes.Contains(hash))
                {
                    _requestHashCodes.Remove(hash);
                    if (_requestHashCodes.Count == 0)
                    {
                        RemovePreload();
                    }
                   
                }
            }
            else
            {
                RemovePreload();
            }
        }

        private void OnServerRequestSendHandler(int hash)
        {
            if (!_requestHashCodes.Contains(hash))
            {
                _requestHashCodes.Add(hash);
            }
            AddPreload();
        }

        private void OnInternetConnectionExist()
        {
            Debug.Log("Internet exist!");
           RemoveFixedPreloader();
        }

        private void OnInternetConnectionLost()
        {
            Debug.Log("Internet connection lost!");
            ShowFixedPreloader();
        }


        private void ShowServerErrorPopup(GSData data)
        {
            _requestHashCodes.Clear();
            RemovePreload();
            //if (data.ContainsKey(GS_ScriptData.ErrorData))
            //{
              
                
                
            //    //GSData errorData = data.GetGSData(GS_ScriptData.ErrorData);
            //    //string msg = errorData.GetString("msg");
            //    //int id = errorData.GetInt("id").Value;
            //    //if (!GS_Errors.NeedProcessError(id))
            //    //{
            //    //    AddPopup(PopupType.ServerErrorPopup).SetText(msg,false).SetTitle("Error_" + id,false);
            //    //}

            //    //Debug.LogWarning(msg);
            //}else
            //{
            //   RemovePreload();
            //}
        }

     

        private GameObject GetPopupPrefab(PopupType type)
        {
            if (!_prefabsList.ContainsKey(type))
            {
                _prefabsList.Add(type, Resources.Load<GameObject>(UISettings.PopupResourcesPath + type));
            }
            return _prefabsList[type];
        }

        private void RemovePreload()
        {
            if (preloaderPopup != null && !_isFixedPreloader)
            {
                preloadDelaySeconds = _originDelaySeconds;
                StopAllCoroutines();
                preloaderPopup.SetActive(false);
                if (_popupsList.Count == 0)
                {
                    EventsManager.Instance.Call(LastPopupRemoved);
                }
               
            }
        }

        IEnumerator ShowPreloader()
        {
            yield return new WaitForSeconds(preloadDelaySeconds);
            preloaderPopup.SetActive(true);
            preloaderPopup.transform.SetAsLastSibling();
            EventsManager.Instance.Call(PopupAddedEvent);
            yield return null;
        }   

        private void AddPreload()
        {
            if (preloaderPopup != null && !preloaderPopup.activeSelf && !_isFixedPreloader)
            {
                StopCoroutine(ShowPreloader());
                StartCoroutine(ShowPreloader());
                StartCoroutine(RemovePreloadRoutine());
            }
        }

        private IEnumerator RemovePreloadRoutine()
        {
            yield return removePreloaderDelay;
            RemovePreload();
            _requestHashCodes.Clear();
            yield return null;
        }

        public GameObject GetCurrentPopup()
        {
            if (_popupsList.Count == 0)
            {
                return null;
            }
            return _popupsList[_popupsList.Count - 1].gameObject;
        }

        public IPopup AddPopup(PopupType type,bool closeOnApply = true)
        {
            IPopup popBase = null;

            GameObject prefab = GetPopupPrefab(type);

            if (prefab != null)
            {
                

                _hasActivePopups = true;
                RectTransform child = content.AddChild(prefab, false);

                popBase = child.GetComponent<IPopup>();
                popBase.gameObject.SetActive(false);
                popBase.Type = type;
                _popupsList.Add(popBase);

                if (closeOnApply)
                {
                    popBase.SetCallbacks(OnClosePopupHandler, OnClosePopupHandler);

                }
                else
                {
                    popBase.SetCallbacks(null, OnClosePopupHandler);

                }

                if (_popupsList.Count == 1)
                {
                    PlayPopupOpenSound(type);
                    popBase.gameObject.SetActive(true);
                    EventsManager.Instance.Call(PopupAddedEvent);
                    Debug.Log("Opened popup :" + type);
                }
                else
                {
                    Debug.Log("popup added to stack :" + type);
                }
            }
           
            return popBase;
        }

        private void PlayPopupOpenSound(PopupType type)
        {
            switch (type)
            {
                    case PopupType.LockedToChangePopup:
                    case PopupType.NeedLevelPopup:
                    case PopupType.NotEnoughFansCoinsPopup:
                    case PopupType.NotEnoughMoneyPopup:
                    SoundManager.Instance.PlaySFXOnGameObject("notenoughpopup", gameObject);
                    break;
                default:
                    SoundManager.Instance.PlaySFXOnGameObject("popup", gameObject);
                    break;
            }
        }

        private void OnClosePopupHandler()
        {
            RemovePopup();

        }

        public void RemovePopup(PopupType type)
        {
            IPopup popup = _popupsList.Find(p => p.Type == type);
            if (popup != null)
            {
                RemovePopup(popup);
            }
        }

        public void RemovePopup(IPopup popup)
        {
            if (_popupsList.Count == 0) return;
            if (_popupsList.Contains(popup))
            {
                _popupsList.Remove(popup);
                EventsManager.Instance.Call(PopupRemovedEvent, popup.Type);

                GameObject child = popup.gameObject;
                popup.Dispose();
                Destroy(child.gameObject);
            }
            if (_popupsList.Count == 0)
            {
                _popupsList.Clear();
                _hasActivePopups = false;
                EventsManager.Instance.Call(LastPopupRemoved);
            }
            else
            {
                _popupsList[0].gameObject.SetActive(true);
                EventsManager.Instance.Call(PopupAddedEvent);
                Debug.Log("Opened popup :" + _popupsList[_popupsList.Count - 1].Type);
            }
        }

        private void RemovePopup()
        {
            
            if (_popupsList.Count == 0) return;
            IPopup popBase = _popupsList[0];

            if (popBase != null)
            {
                
                _popupsList.Remove(popBase);

                EventsManager.Instance.Call(PopupRemovedEvent, popBase.Type);

                GameObject child = popBase.gameObject;
                //if (EventsManager.Instance.HasEventType(UIEventTypes.PopupClosed))
                //{
                //    EventsManager.Instance.Call(UIEventTypes.PopupClosed, popBase);
                //}
                popBase.Dispose();
                Destroy(child.gameObject);
            }
            if (_popupsList.Count == 0)
            {
                _hasActivePopups = false;
                EventsManager.Instance.Call(LastPopupRemoved);
            }
            else
            {
                PlayPopupOpenSound(_popupsList[0].Type);
                _popupsList[0].gameObject.SetActive(true);
                EventsManager.Instance.Call(PopupAddedEvent);
                Debug.Log("Opened popup :" + _popupsList[0].Type);
            }

        }

    }


}

