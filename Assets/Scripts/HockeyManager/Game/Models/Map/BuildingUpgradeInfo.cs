﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Models.Map
{
 
    public class BuildingUpgradeInfo:IEffect,IGSParse
    {
        //gs upgrade effects
        public const string ArenaAttackProp = "attack%";
        public const string ArenaDeffenceProp = "deffence%";
        public const string ArenaMaxFansProp = "fanslimit";

        public const string BankLimitProp = "coinsmax";

        public const string HallCellsProp = "cellsmax";
        public const string FansPerTimeProp = "fanspertime";
        public const string FansMaxqueueProp = "maxqueue";
        public const string CoinsPerTimeProp = "coinspertime";
        public const string MaxIncomeCoinsProp = "maxincome";

        public const string MaxExchangeGold = "maxExchangeGold";
        public const string MaxExchageMoney = "maxExchangeMoney";

        public const string FansBonusProp = "fansbonus";

        public const string EquipmentMinLevelProp = "eqpminlvl";
        public const string EquipmentMaxLevelProp = "eqpmaxlvl";

        private int _pos = -1;

        private MapBuildingsType _id;
        private int _level;
        private int _maxLevel;
        private int _price;
        private int _unlockLevel = 0;
        //in minutes
        private int _timeSeconds;
        private int _upgradeGoldPrice;
        private RewardData _reward;

        private Dictionary<string, Int32> _effects = new Dictionary<string, Int32 >();

        string[] baseProps = { "id", "level","maxLevel", "price", "time", "unlockLevel", "reward" };
        
        public void Parse(GSData data)
        {
            var upgradeData = data.GetGSData("upgrades");
            _level = upgradeData.GetInt("level").Value;
            _price = upgradeData.GetInt("price").Value;
            _timeSeconds = upgradeData.GetInt("time").Value*60;
            _upgradeGoldPrice = upgradeData.GetInt("time").Value*2;
            if (upgradeData.ContainsKey("unlockLevel"))
            {
                _unlockLevel = upgradeData.GetInt("unlockLevel").Value;
            }

            if (upgradeData.ContainsKey("reward"))
            {
                _reward = JsonUtility.FromJson<RewardData>(upgradeData.GetGSData("reward").JSON);
            }
            
            foreach (string prop in upgradeData.BaseData.Keys)
            {
                if (!baseProps.Contains(prop))
                {
                    if (!_effects.ContainsKey(prop))
                    {
                        _effects.Add(prop,Convert.ToInt32(upgradeData.BaseData[prop]));
                    }
                    else
                    {
                        _effects[prop] = Convert.ToInt32(upgradeData.BaseData[prop]);
                    }
                 
                }
            }
        }

        public float GetEffectValue(string prop)
        {
            if (_effects.ContainsKey(prop))
            {
                return _effects[prop];
            }
            return 0f;
        }

        public float GetEffectValue(params string[] props)
        {
            float total = 0f;
            for (int i = 0; i < props.Length; i++)
            {
                if (_effects.ContainsKey(props[i]))
                {
                    total += _effects[props[i]];
                }
            }
            return total;
        }

        public BuildingUpgradeInfo(GSData data)
        {
            _pos = data.GetInt("pos").Value;
            _id = (MapBuildingsType)data.GetInt("id").Value;
            _maxLevel = data.GetInt("maxLevel").Value;
            Parse(data);
        }

        public RewardData Reward
        {
            get { return _reward; }
        }

        public int TimeSeconds
        {
            get { return _timeSeconds; }
        }

        public int Price
        {
            get { return _price; }
        }

        public int Level
        {
            get { return _level; }
        }

        public MapBuildingsType Id
        {
            get { return _id; }
        }

        public int UnlockLevel
        {
            get { return _unlockLevel; }
        }

        public int MaxLevel
        {
            get { return _maxLevel; }
        }

        public int UpgradeGoldPrice
        {
            get { return _upgradeGoldPrice; }
        }

        public int Pos
        {
            get { return _pos; }
        }
    }
}
