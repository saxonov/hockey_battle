﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Models.Map;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    public class BankInfoExchangePanel:MonoBehaviour
    {
        private const string CurrencyIndicatorFormat = "<sprite={0}>{1}";

        public UnityEvent OnBuyClickEvent;

        [SerializeField] private Toggle BuyGoldToggle;
        [SerializeField] private Toggle BuyMoneyToggle;
        [SerializeField] private TextMeshProUGUI exchangeLabel;
        [SerializeField] private Slider exchangeSlider;
        [SerializeField] private TextMeshProUGUI leftValueText;
        [SerializeField] private TextMeshProUGUI rightValueText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private Button buyButton;

        private IBuildingModel _buildingModel;

        private int goldToMoneyRate = 0;
        private int moneyToGoldRate = 0;

        private int _goldMaxToBuy;
        private int _moneyMaxToBuy;

        private int _exchangeCurrencyType;
        private int _currencyBuyType;

        private int _moneyiconIndex;
        private int _goldIconIndex;

        private long _unlockTimeStamp = 0;

        private int _currentPrice = 0;

        public int SelectedAmountToBuy
        {
            get { return (int)exchangeSlider.value; }
        }

        public int CurrencyBuyType
        {
            get { return _currencyBuyType; }
        }

        public int CurrentPrice
        {
            get { return _currentPrice; }
        }

        public int ExchangeCurrencyType
        {
            get { return _exchangeCurrencyType; }
        }

        void Awake()
        {
            if (OnBuyClickEvent != null)
            {
                OnBuyClickEvent.RemoveAllListeners();
            }


            _moneyiconIndex = (int)IconsEnum.Money;
            _goldIconIndex = (int)IconsEnum.Gold;

            goldToMoneyRate = GameData.GSConstData.BankExchangeGoldToMoney;
            moneyToGoldRate = GameData.GSConstData.BankExchangeMoneyToGold;

            _currencyBuyType = GameData.CurrencyCoins;
            _exchangeCurrencyType = GameData.CurrencyGold;

            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
        }

        void Start()
        {
            BuyGoldToggle.onValueChanged.AddListener(OnGoldToMoneySelected);
            BuyMoneyToggle.onValueChanged.AddListener(OnMoneyToGoldSelected);
            buyButton.onClick.AddListener(OnBuyClickHandler);
            exchangeSlider.onValueChanged.AddListener(OnBuyAmountChanged);

            exchangeLabel.text = this.GetLocaleText("game.main.bank.exchange.title",
            string.Format(CurrencyIndicatorFormat, _goldIconIndex, _goldMaxToBuy));


        }

        private void UpdateCurrencyIndicators()
        {
            
            int amount = (int)exchangeSlider.value;

            buyButton.interactable = amount > 0;

            if (_exchangeCurrencyType == GameData.CurrencyGold)
            {
                _currentPrice = amount * moneyToGoldRate;
                leftValueText.text = string.Format(CurrencyIndicatorFormat, _moneyiconIndex, _currentPrice);
                rightValueText.text = string.Format(CurrencyIndicatorFormat,_goldIconIndex, amount);
            }
            else if (_exchangeCurrencyType == GameData.CurrencyCoins)
            {
                _currentPrice = amount;
                leftValueText.text = string.Format(CurrencyIndicatorFormat, _goldIconIndex, amount);
                rightValueText.text = string.Format(CurrencyIndicatorFormat, _moneyiconIndex, amount * goldToMoneyRate);

            }
        }


        private void OnTikTakHandler()
        {
            if (timerText.gameObject.activeSelf)
            {
                UpdatTimer();
            }
        }


        private void OnBuyAmountChanged(float value)
        {
            UpdateCurrencyIndicators();
        }

        void OnDestroy()
        {
            if (_buildingModel != null)
            {
                _buildingModel.OnModelChangedEvent.RemoveListener(OnModeChangedHandler);
            }
            _buildingModel = null;

            BuyGoldToggle.onValueChanged.RemoveListener(OnGoldToMoneySelected);
            BuyMoneyToggle.onValueChanged.RemoveListener(OnMoneyToGoldSelected);
            buyButton.onClick.RemoveListener(OnBuyClickHandler);
            exchangeSlider.onValueChanged.RemoveListener(OnBuyAmountChanged);
            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
        }

        private void OnModeChangedHandler()
        {
            if (_buildingModel.ExtraData != null)
            {
                if (_buildingModel.ExtraData.ContainsKey("unlockTime"))
                {
                    _unlockTimeStamp = _buildingModel.ExtraData.GetLong("unlockTime").Value;
                    
                    UpdatTimer();

                }
                
            }
            else
            {
                timerText.gameObject.SetActive(false);
                buyButton.gameObject.SetActive(true);
            }
        }

        private void UpdatTimer()
        {
            long timeLeft = GameTimer.TimeLeftSeconds(_unlockTimeStamp);
            if (timeLeft > 0)
            {
                buyButton.gameObject.SetActive(false);
                timerText.gameObject.SetActive(true);
                timerText.text = string.Format("<sprite name=time>{0}", GameTimer.GetGameTimeString(timeLeft));

            }
            else
            {
                timerText.gameObject.SetActive(false);
                buyButton.gameObject.SetActive(true);
            }
        }

        private void OnBuyClickHandler()
        {
            if (OnBuyClickEvent != null)
            {
                OnBuyClickEvent.Invoke();
            }
        }

        /// <summary>
        /// buy gold for money
        /// </summary>
        /// <param name="selected"></param>
        private void OnGoldToMoneySelected(bool selected)
        {
            if (selected)
            {
                exchangeLabel.text = this.GetLocaleText("game.main.bank.exchange.title",
                    string.Format(CurrencyIndicatorFormat,_goldIconIndex, _goldMaxToBuy));
                _exchangeCurrencyType = GameData.CurrencyGold;
                _currencyBuyType = GameData.CurrencyCoins;
                SetExchangeSlider();
                UpdateCurrencyIndicators();
            }
        }

        private void OnMoneyToGoldSelected(bool selected)
        {
            if (selected)
            {
                exchangeLabel.text = this.GetLocaleText("game.main.bank.exchange.title",
                    string.Format(CurrencyIndicatorFormat,_moneyiconIndex,_moneyMaxToBuy));
                _exchangeCurrencyType = GameData.CurrencyCoins;
                _currencyBuyType = GameData.CurrencyGold;
                SetExchangeSlider();
                UpdateCurrencyIndicators();
            }
        }


        private void SetExchangeSlider()
        {
            exchangeSlider.minValue = 0;
            if (_exchangeCurrencyType == GameData.CurrencyCoins)
            {
                exchangeSlider.maxValue = _moneyMaxToBuy/goldToMoneyRate;
            }
            else if(_exchangeCurrencyType == GameData.CurrencyGold)
            {
                exchangeSlider.maxValue = _goldMaxToBuy;
            }

            exchangeSlider.value = 1;
           // UpdateCurrencyIndicators();

        }

       
        public void SetMaxExchangeRates(IBuildingModel model)
        {
            if (_buildingModel == null)
            {
                _buildingModel = model;
                _buildingModel.OnModelChangedEvent.AddListener(OnModeChangedHandler);

                _goldMaxToBuy = (int)model.GetEffectValue(BuildingUpgradeInfo.MaxExchangeGold);
                _moneyMaxToBuy = (int)model.GetEffectValue(BuildingUpgradeInfo.MaxExchageMoney);

                //int canBuyCoins = balance.gold > 0 ? balance.gold / moneyToGoldRate : 1;
                //int canBuyGold = balance.coins > 0 ? balance.coins / goldToMoneyRate : 1;

                //_goldMaxToBuy = Mathf.Min(canBuyGold, _goldMaxToBuy);
                //_moneyMaxToBuy = Mathf.Min(canBuyCoins, _moneyMaxToBuy);

                SetExchangeSlider();
                UpdateCurrencyIndicators();
                OnModeChangedHandler();

            }

           
            
           
        }

    }
}