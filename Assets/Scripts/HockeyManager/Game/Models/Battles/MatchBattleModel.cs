﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{

    public enum MatchBattleStates
    {
        Selection,
        Attacked,
        Attack,
        Battle,
        Empty
    }


    public class MatchBattleModel:IGSParse
    {
        private string _battleId;
        //private string _type;
        private MatchPlayerModel _attacker;
        private MatchPlayerModel _attacked;
        private int _startTime;
        private long _endAcceptTime = 0;
        private long _endBattleTime = 0;
     //   private int _maxPlayers = 0;
        private int _acceptDeffenceBonusBefore;
        private int _acceptDeffenceBonusAfter;
        private int _cancelPenaltyPrice;
        private bool _battleStarted = false;

        private int _acceptTimeSeconds;
        private int _matchTimeSeconds;

        private bool _enemyAttacked = false;


        public string BattleId
        {
            get { return _battleId; }
        }

        //public string Type
        //{
        //    get { return _type; }
        //}

        public int StartTime
        {
            get { return _startTime; }
        }

        public long EndAcceptTime
        {
            get { return _endAcceptTime; }
        }

        public long EndBattleTime
        {
            get { return _endBattleTime; }
        }

        public int AcceptDeffenceBonusBefore
        {
            get { return _acceptDeffenceBonusBefore; }
        }

        public int AcceptDeffenceBonusAfter
        {
            get { return _acceptDeffenceBonusAfter; }
        }

        public int CancelPenaltyPrice
        {
            get { return _cancelPenaltyPrice; }
        }

        public bool BattleStarted
        {
            get { return _battleStarted; }
        }

        public MatchPlayerModel GetMyTeam()
        {
            if (_attacker.TeamInfo.IsMe())
            {
                return _attacker;
            }
            return _attacked;
        }

        public MatchPlayerModel GetEnemyTeam()
        {
            if (_attacker.TeamInfo.IsMe())
            {
                return _attacked;
            }
            return _attacker;
        }
        
        public int AcceptTimeSeconds
        {
            get { return _acceptTimeSeconds; }
        }

        public bool EnemyAttacked
        {
            get { return _enemyAttacked; }
        }

        public int MatchTimeSeconds
        {
            get { return _matchTimeSeconds; }
        }


        public void Parse(GSData data)
        {
            _battleId = data.GetString("_id");
          //  _type = data.GetString("type");
            _startTime = data.GetInt("startTime").Value;
            _endAcceptTime = data.GetLong("endAcceptTime").Value;
            _endBattleTime = data.GetLong("endBattleTime").Value;

            _acceptDeffenceBonusBefore = data.GetInt("acceptDeffenceBonusBefore1min").Value;
            _acceptDeffenceBonusAfter = data.GetInt("acceptDeffenceBonusAfter1min").Value;

            _cancelPenaltyPrice = data.GetInt("cancelPenaltyPrice").Value;
            _battleStarted = data.GetBoolean("battleStarted").Value;

            if (data.ContainsKey("acceptTimeSeconds"))
            {
                _acceptTimeSeconds = data.GetInt("acceptTimeSeconds").Value;
            }

            if (data.ContainsKey("battleTimeSeconds"))
            {
                _matchTimeSeconds = data.GetInt("battleTimeSeconds").Value;
            }

            _attacker = new MatchPlayerModel(data.GetGSData("attacker"));

            _attacked = new MatchPlayerModel(data.GetGSData("attacked"));

            _enemyAttacked = !_attacker.TeamInfo.IsMe();

        }
    }
}
