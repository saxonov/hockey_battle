﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace HockeyManager.Game.Views.Components
{
    public class TMP_TextLinkEvent : UnityEvent<TMP_TextLinkProcessor>{}

    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMP_TextLinkProcessor:MonoBehaviour,IPointerClickHandler
    {
        //public bool lockLinkClick;

        public readonly TMP_TextLinkEvent OnLinkClickEvent = new TMP_TextLinkEvent();

        [SerializeField]private Camera cam;
        [SerializeField] private RectTransform holder;
        private TextMeshProUGUI textField;

        private string _linkText;
        private string _linkId;
        private Vector2 _linkPosition;

        public Vector2 LinkPosition
        {
            get { return _linkPosition; }
        }

        public string LinkText
        {
            get { return _linkText; }
        }

        public string LinkId
        {
            get { return _linkId; }
        }

        void Start()
        {
            if (cam == null)
            {
                cam = Camera.main;
            }
            if (holder == null)
            {
                holder = GetComponent<RectTransform>();
            }

            textField = GetComponent<TextMeshProUGUI>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(textField, eventData.position,cam);
            if (linkIndex != -1)
            {
                TMP_LinkInfo info = textField.textInfo.linkInfo[linkIndex];
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(holder, eventData.position,
                    eventData.pressEventCamera, out _linkPosition))
                {
                    _linkText = info.GetLinkText();
                    _linkId = info.GetLinkID();

                    if (OnLinkClickEvent != null)
                    {
                        OnLinkClickEvent.Invoke(this);
                    }
                }
                else
                {
                    OnLinkClickEvent.Invoke(null);
                }
            }
            else
            {
                OnLinkClickEvent.Invoke(null);
            }
        }
    }
}
