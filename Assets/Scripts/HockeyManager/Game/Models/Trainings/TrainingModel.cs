﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Base;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Models.Trainings
{
    public struct TrainingEffect
    {
        public int attack;
        public int deffence;

        public TrainingEffect(int a=0,int d = 0)
        {
            attack = a;
            deffence = d;
        }
    }

    public sealed class TrainingModel:BaseGameEntityModel
    {
        //for lock state between start upgrade request and responce
        public static bool UpgradeStarted = false;

       // public const string EaseLevel = "easy";
       // public const string HardLevel = "hard";

        private int _bonusEffectTime = 0;

        private int _attack;
        private int _deffence;

        private string _type;
        private string _level;
        private int _energyCost = 0;

        private int _endEffectTime = 0;
        private int _effectTimeSeconds = 0;
        private int _id;
        private string _uid;
        private RewardData _reward;

       

        public TrainingModel(GSData data)
        {
            _entityType = EntityType.Training;
            Parse(data);
        }

        public int Id
        {
            get { return _id; }
        }
        
        public int UpgradeGoldPrice
        {
            get { return (_upgradeTimeSeconds/60)*2; }
        }

        public int EffectTimeSeconds
        {
            get { return _effectTimeSeconds; }
        }

        public int EnergyCost
        {
            get { return _energyCost; }
        }

        public string Level
        {
            get { return _level; }
        }

        public string Type
        {
            get { return _type; }
        }

        public RewardData Reward
        {
            get { return _reward; }
        }
        
        public int EndEffectTime
        {
            get { return _endEffectTime; }
        }
        
        public int Deffence
        {
            get { return _deffence; }
        }

        public int Attack
        {
            get { return _attack; }
        }

        public string UID
        {
            get { return _uid; }
        }

        public int BonusEffectTime
        {
            get { return _bonusEffectTime; }
            set
            {
                _bonusEffectTime = value;
                OnModelChangedEvent.Invoke();
            }
        }

        public string GetName()
        {
            return LocaleManager.Instance.GetText("game.main.trainings.title.training" + (_id + 1));
        }

        public override void Parse(GSData data)
        {
            if (data == null)
            {
                _isUpgrading = false;
                _endUpgradeTimeSeconds = 0;
                base.Parse(null);
                return;
            }
            _uid = data.GetString("uid");

            _id = data.GetInt("id").Value;

            _type = data.GetString("type");

            _attack = data.GetInt("attack%").Value;

            _deffence = data.GetInt("deffence%").Value;

            _level = data.GetString("level");

            _energyCost = data.ContainsKey("energyCost") ? data.GetInt("energyCost").Value : _energyCost;

            _effectTimeSeconds = data.ContainsKey("effectTime") ? data.GetInt("effectTime").Value*60 : _effectTimeSeconds;

            _endEffectTime = data.GetInt("endEffectTime").HasValue ? data.GetInt("endEffectTime").Value : 0;

            _reward = data.ContainsKey("reward") ? JsonUtility.FromJson<RewardData>(data.GetGSData("reward").JSON) : _reward;
            
            base.Parse(data);

        }

    }
}
