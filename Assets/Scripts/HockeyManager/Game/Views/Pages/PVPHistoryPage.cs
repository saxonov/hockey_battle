﻿using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.Views.Renderers;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class PVPHistoryPage:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "PVPHistoryPage.historyScroller" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private ScrollRect historyScroller;
#pragma warning restore CS0649 // Полю "PVPHistoryPage.historyScroller" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PVPHistoryPage.matchBattleResultRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject matchBattleResultRendererPrefab;
#pragma warning restore CS0649 // Полю "PVPHistoryPage.matchBattleResultRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PVPHistoryPage.descriptionTitle" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text descriptionTitle;
#pragma warning restore CS0649 // Полю "PVPHistoryPage.descriptionTitle" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private MatchBattleController _controller;

        void Awake()
        {
            _controller = GameRoot.Instance.GetController<MatchBattleController>();
        }

        public void OnShow()
        {
            historyScroller.content.RemoveChildren();
            _controller.GetMatchBattleHistory(BuildMatchesHistory);
        }

        public void OnHide()
        {
            historyScroller.content.RemoveChildren();
        }



        private void BuildMatchesHistory()
        {
            MatchBattleHistoryModel model = _controller.MatchHistoryModel;

            if(model == null)return;

            string lostStr = model.Lost > model.Wins ? model.Lost.ToString().GetUITextColorFormated(UISettings.BlockTextColor) : model.Lost.ToString();
            string winStr = model.Wins.ToString().GetUITextColorFormated(UISettings.GreenTextColor);
            
            string title = LocaleManager.Instance.GetText("game.main.matchhistorypage.totalmatches.title", model.Total, winStr, lostStr);
            descriptionTitle.text = title.GetUITextSizeFormated(70);


            for (int i = 0; i < model.Results.Count; i++)
            {
                MatchBattleHistoryItemRenderer itemRenderer = historyScroller.content.AddChild(matchBattleResultRendererPrefab, false).GetComponent<MatchBattleHistoryItemRenderer>();
                itemRenderer.InvertColors(i);
                itemRenderer.SetData(model.Results[i]);
            }
        }
    }
}
