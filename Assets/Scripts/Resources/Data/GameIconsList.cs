﻿using System.Collections.Generic;
using HockeyManager.Game.Dict;
using UnityEngine;

public class GameIconsList:ScriptableObject {

	public List<IconData> icons;

    public List<RewardIconData> rewardIcons;
}
