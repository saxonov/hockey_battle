﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Trainings;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class TrainingItemRenderer:MonoBehaviour
    {
        public const string OnTrainingSelected = "OnTrainingSelectedEvent";

        [SerializeField] private Text title;
        [SerializeField] private Text effectTimerLabel;
        [SerializeField] private TextIconControl attackBonus;
        [SerializeField] private TextIconControl deffenceBonus;
        [SerializeField] private TextIconControl expAwardPanel;
       [SerializeField] private TextIconControl moneyAwardPanel;
        [SerializeField] private TextIconControl energyCostPanel;
        [SerializeField] private TextIconControl upgradeTimerPanel;
        [SerializeField] private Animator upgradeProgress;
        [SerializeField] private Button upgradeButton;
        [SerializeField] private Button finishUpgradeButton;
        [SerializeField] private RectTransform tutorialHelper;

        private int _index = -1;
        private bool _isUpgradeSetted;
        private TrainingModel _model;

        private void ResetView()
        {
            finishUpgradeButton.gameObject.SetActive(false);
            effectTimerLabel.color = UISettings.GreenTextColor;
            upgradeProgress.Stop();
            upgradeProgress.gameObject.SetActive(false);
            upgradeButton.onClick.AddListener(OnButtonClickHandler);
            finishUpgradeButton.onClick.AddListener(OnButtonClickHandler);
        }

        void OnDisable()
        {
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
            }
        }

        void Start()
        {

            if (!GameData.IsTutorialPassed && _index == 0)
            {
                EventsManager.Instance.Add(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                OnTutorialStepChanged();
            }
        }
       
        private void OnTutorialStepChanged()
        {
            if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.StartTraining))
            {
                TutorialManager.Instance.CurrentView.SetShowTarget(tutorialHelper);
            }
            if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.FinishTraining))
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                TutorialManager.Instance.CurrentView.SetShowTarget(tutorialHelper);
            }
        }

        void OnDestroy()
        {
            //if (_model != null)
            //{
            //    _model.OnTimerLeftSecondsEvent.RemoveListener(OnTikTakHandler);
            //    _model.OnModelChangedEvent.RemoveListener(OnModelChanged);
            //    _model = null;

            //}
           
            upgradeButton.onClick.RemoveListener(OnButtonClickHandler);
            finishUpgradeButton.onClick.RemoveListener(OnButtonClickHandler);
        }

        public void SetModel(TrainingModel model,int index)
        {
            _index = index;

            if (_model != null)
            {
                _model.OnModelChangedEvent.RemoveListener(OnModelChanged);
                _model = null;
            }
            else
            {
                ResetView();
            }


            _model = model;
            _model.OnModelChangedEvent.AddListener(OnModelChanged);
            OnModelChanged();
            gameObject.SetActive(true);
        }

        private void OnModelChanged()
        {
            SetUpgradeProcess();
            SetUpgradeInfo();
        }

        private void OnButtonClickHandler()
        {
            if (!TrainingModel.UpgradeStarted)
            {
                EventsManager.Instance.Call(OnTrainingSelected, _model);
            }
         
        }

        private void SetUpgradeProcess()
        {
            if (_model.IsUpgrading)
            {
                if (!_isUpgradeSetted)
                {
                    _isUpgradeSetted = true;
                    upgradeProgress.gameObject.SetActive(true);
                    energyCostPanel.gameObject.SetActive(false);
                    upgradeButton.gameObject.SetActive(false);
                    finishUpgradeButton.gameObject.SetActive(true);
                    _model.OnTimerLeftSecondsEvent.AddListener(OnTikTakHandler);
                    upgradeTimerPanel.SetText(MathUtils.SecondsToTimeString(GameTimer.TimeLeftSeconds(_model.EndUpgradeTimeSeconds)));

                }

            }
            else
            {
                ResetUpgradeProcess();
            }
              
        }

        private void ResetUpgradeProcess()
        {
            upgradeProgress.gameObject.SetActive(false);
            energyCostPanel.gameObject.SetActive(true);
            upgradeButton.gameObject.SetActive(true);
            finishUpgradeButton.gameObject.SetActive(false);
            _isUpgradeSetted = false;
        }

        private void OnTikTakHandler(long secondsLeft)
        {
            if (secondsLeft > 0)
            {
                upgradeTimerPanel.SetText(MathUtils.SecondsToTimeString(secondsLeft));
            }
            else
            {
                ResetUpgradeProcess();

            }
        }

        private void SetUpgradeInfo()
        {
            title.text = _model.GetName();
            attackBonus.gameObject.SetActive(_model.Attack > 0);
            deffenceBonus.gameObject.SetActive(_model.Deffence > 0);
            attackBonus.SetText(_model.Attack + "%");
            deffenceBonus.SetText(_model.Deffence+"%");
            energyCostPanel.SetText(_model.EnergyCost.ToString());

            int effectTime = (_model.EffectTimeSeconds + _model.BonusEffectTime);
            effectTimerLabel.text = LocaleManager.Instance.GetText("game.common.upto", GameTimer.GetGameTimeString(effectTime));

            if (!_model.IsUpgrading)
            {
                upgradeTimerPanel.SetText(MathUtils.SecondsToTimeString(_model.UpgradeTimeSeconds));
            }
           

            moneyAwardPanel.SetText(_model.Reward.coins.ToString());
            expAwardPanel.SetText(_model.Reward.exp.ToString());
        }
    }
}
