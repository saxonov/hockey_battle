﻿using System;
using System.Collections.Generic;
using System.Text;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using SCTools.EventsSystem;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class ChatController:IGameController,ICheckMessages
    {
        public const int MaxLastMessagesLines = 3;

        public const string NewChatMessageEvent = "ChatController.newChatMessage";
        public const string UpdateChatHistory = "ChatController.updateChatHistory";

        private const string MessageFormat = "<color={0}>{1}</color> {2}";

        private const string UIDLinkFormat = "<link=\"{0}_{1}_{2}\">{3} {4}:</link>";

        private static readonly DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private string _hexNameColor;
        private string _hexModerNameColor;

        private IGameRoot _root;

        private List<string> _lastMeesagesFlow = new List<string>();

        private StringBuilder chatBuffer;

        private bool _needUpdateChatHistory = true;



        public string HexModerNameColor
        {
            set { _hexModerNameColor = value; }
        }

        public string HexNameColor
        {
            set { _hexNameColor = value; }
        }


        public void Initialize(IGameRoot root)
        {
            _root = root;
            chatBuffer = new StringBuilder();
        }

        public void Reset()
        {
            _needUpdateChatHistory = true;
        }

        public List<string> LastChatMessages
        {
            get { return _lastMeesagesFlow; }
        }

        public StringBuilder ChatBuffer
        {
            get { return chatBuffer; }
        }
        
        public void SendChatMessage(string message, UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventAttribute("MSG", message);
            request.SetEventKey("SEND_MESSAGE");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                callback();
            });
        }

        public void LoadChatHistory(UnityAction callback, int limit)
        {
            if (!_needUpdateChatHistory)
            {
                callback();
                return;
            }
            _needUpdateChatHistory = false;
            var request = new LogEventRequest();
            request.SetEventKey("GET_CHAT_HISTORY");
            request.SetEventAttribute("LIMIT", limit);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _lastMeesagesFlow.Clear();
                    chatBuffer.Remove(0, chatBuffer.Length);
                    List<GSData> history = responce.ScriptData.GetGSDataList(GS_ScriptData.ChatHistoryData);
                    foreach (var gsData in history)
                    {
                        InsertMessage(gsData);
                    }
                    callback();
                }
            });
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.ChatMsg)
            {
                if (!_root.IsGameScreen())
                {
                    _needUpdateChatHistory = true;
                }
                InsertMessage(msgData);
                EventsManager.Instance.Call(NewChatMessageEvent);
            }
            if (msgCode == GS_Messages.ChatMsgRemoved)
            {
                _needUpdateChatHistory = true;
                EventsManager.Instance.Call(UpdateChatHistory);
            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return (data != null && msgType == GS_Messages.ChatMsg) || 
                                    msgType == GS_Messages.ChatMsgRemoved;
        }




        private void InsertMessage(GSData msgData)
        {
            string id = msgData.ContainsKey("id") ? msgData.GetString("id") : null;
            //string uid = msgData.ContainsKey("uid") ? msgData.GetString("uid") : msgData.GetString("name");
            long msgTime = msgData.GetLong("time").Value;
            string time = date.AddMilliseconds(msgTime).ToLocalTime().ToString("HH:mm");

            string msg = msgData.GetString("msg");

            bool isModerator = msgData.ContainsKey("isModer") && msgData.GetBoolean("isModer").Value;
            string color = isModerator ? _hexModerNameColor : _hexNameColor;
            string teamName = msgData.GetString("name");
            string uidName;

            if (GameData.IsMyUserId(id))
            {
                uidName = string.Format("{0} {1}:", time, teamName);
            }
            else
            {
                uidName = string.Format(UIDLinkFormat, id, msgTime, teamName, time, teamName);
            }

            chatBuffer.AppendLine(string.Format(MessageFormat, color,uidName, msg));

            if (_lastMeesagesFlow.Count == MaxLastMessagesLines)
            {
                _lastMeesagesFlow.RemoveAt(0);
            }
            _lastMeesagesFlow.Add(string.Format(MessageFormat, color,uidName, msg));
        }
    }
}
