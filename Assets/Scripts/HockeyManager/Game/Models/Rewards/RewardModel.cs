﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Game.Models.Rewards
{
    
    public class RewardModel:IGSParse,IComparable<RewardModel>
    {
        public bool isInList = false;
        public bool newAdded = false;

        private bool _showInList = true;

        private RewardType _type;

        private long _dateTimestamp;

        private GSData _customData;
        
        private RewardData _rewardData;

        private int _uid = -1;

        private bool _isGained = false;
        private DateTime _dateTIme;

        public RewardModel(GSData data)
        {
            Parse(data);
        }
       
        public RewardData RewardData
        {
            get { return _rewardData; }
        }

        public RewardType Type
        {
            get { return _type; }
        }

        public bool IsGained
        {
            get { return _isGained; }
            set { _isGained = value; }
        }

        public long DateTimestamp
        {
            get { return _dateTimestamp; }
        }

        public GSData CustomData
        {
            get { return _customData; }
        }

        public bool ShowInList
        {
            get { return _showInList; }
        }

        public int Uid
        {
            get { return _uid; }
        }

        public void Parse(GSData data)
        {
            _type = (RewardType) data.GetInt("type").Value;
            _uid = data.GetInt("uid").Value;
            _rewardData = JsonUtility.FromJson<RewardData>(data.GetGSData("rewardData").JSON);
            _rewardData.exp = GameData.HasVip ? _rewardData.exp*2 : _rewardData.exp;

            _customData = data.GetGSData("data");
            _dateTimestamp = data.GetLong("date").Value;
            _dateTIme = GS_ScriptData.UnixTimeStampToDateTime(_dateTimestamp);

           _showInList = _type != RewardType.DailyBonusReward && _type != RewardType.AdsReward;

        }

        public override string ToString()
        {
            return _type + ":" + _dateTimestamp + " uid:" + _uid;
        }

        public int CompareTo(RewardModel other)
        {
            if (other == null) return 0;
            if (DateTimestamp == other.DateTimestamp)
            {
                return 0;
            }
            if (DateTimestamp < other.DateTimestamp)
            {
                return -1;
            }
            return 1;
        }
    }
}
