﻿using UnityEngine;
using System.Collections;
using SCTools.Helpers;
using UnityEditor;

namespace SCTools.Editor
{
    [CustomEditor(typeof(ArrayClone))]
    public class ArrayCloneEditor : UnityEditor.Editor
    {

        private ArrayClone arrayTool;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            arrayTool = (ArrayClone)target;


            if (GUILayout.Button("Clone"))
            {
                arrayTool.CloneObjects();
            }

            if (GUILayout.Button("Clear"))
            {
                arrayTool.Clear();
            }
            if (GUILayout.Button("ClearChilds"))
            {
                arrayTool.ClearChilds();
            }

        }
    }


}
