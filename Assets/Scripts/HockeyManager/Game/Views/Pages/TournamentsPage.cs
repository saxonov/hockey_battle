﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class TournamentsPage:MonoBehaviour,IPageChangeHandler
    {
        private IGameSettings settings;

        [SerializeField]private GameObject cupEmptyItemRendererPrefab;

        [Header("Tournaments list")]
        [SerializeField] private ScrollRect cupsScroller;
        [SerializeField] private GameObject cupItemRendererPrefab;
        [SerializeField] private Text levelsField;

        [Header("Idle state")]
        [SerializeField] private GameObject idleBox;
        [SerializeField] private Text cupNameLabel;
        [SerializeField] private Text participantsLabel;
        [SerializeField] private Text waitTimerLabel;
        [SerializeField] private TextIconControl priceFeePanel;
        [SerializeField] private TextIconControl rewardPanel;
        [SerializeField] private Button exitButton;

        [Header("Started state / HistoryInfo")]
        [SerializeField] private Text cupNameTitle;
        [SerializeField] private Text endCupTimerStateLabel;
        [SerializeField] private Text totalInfoLabel;
        [SerializeField] private Text currentMatchTimerWinPostitionLabel;
        [SerializeField] private Button closeGridButton;
        [SerializeField] private GameObject cupTeamResultItemRendererPrefab;
        [SerializeField] private ScrollRect resultsScroller;

        [Header("Battle History Page")]
        [SerializeField] private ScrollRect cupsHistoryScroller;
        [SerializeField] private GameObject cupBattleHistoryItemRendererPrefab;
        [SerializeField] private Text historyPageTitle;

        [Space(10f)]
        [SerializeField] private GameObject tournamentGridBox;

        [SerializeField] private GameObject tournamentCups;

        [Space(5)]
        [SerializeField]private InnerPageNavigation navBar;

        private TeamController _team;
        private TournamentBattleController _controller;
        private MatchBattleController _matchBattleController;

        void Awake()
        {
            settings = GameSettings.Instance;

            _matchBattleController = GameRoot.Instance.GetController<MatchBattleController>();
            _controller = GameRoot.Instance.GetController<TournamentBattleController>();
            _team = GameRoot.Instance.GetController<TeamController>();

          

            closeGridButton.onClick.AddListener(OnCloseCupResultGrid);
            exitButton.onClick.AddListener(OnExitCupClickHandler);

            EventsManager.Instance.Add<TournamentUserModel>(CupTeamItemRenderer.CupTeamItemSelected1ArgEvent,OnCupTeamItemSelectedHandler);
            EventsManager.Instance.Add<TournamentResultData>(TournamentBattleHistoryRenderer.OnTournamentHistoryRecordSelected,OnCupItemSelectedHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupWinnerEvent, OnCupWinnerHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupLooseEvent, OnCupLooseHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupStartedEvent,OnCupTournamentStartedHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupRoundCompleteEvent,OnCupRoundCompleteHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupExpiredEvent,OnCupExpiredHandler);
            EventsManager.Instance.Add(TournamentBattleController.CupPlayersChangedEvent,OnPlayersChangedHandler);
        //  EventsManager.Instance.Add(HockeyBattlesController.CupCanceledEvent,OnCupCancelHandler);

            EventsManager.Instance.Add<TournamentBattleModel>(CupItemRenderer.OnParticipateClick,OnParticipateClickHandler);
            EventsManager.Instance.Add(GameTimer.TikTakSecondEvent, OnTikTakHandler);

            PageNavigatorManager.Instance.AddPageChangeHandler(this);
        }

        void Start()
        {
            levelsField.color = UISettings.GreenTextColor;

            tournamentGridBox.SetActive(false);
            tournamentCups.SetActive(false);
            cupsScroller.gameObject.SetActive(false);
            idleBox.gameObject.SetActive(false);

            if (_team.CurrentActionState == GameActionState.PlayTournament)
            {
                _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
            }

        }

        void OnDestroy()
        {
            _controller = null;
            _team = null;
            exitButton.onClick.RemoveListener(OnExitCupClickHandler);
            closeGridButton.onClick.RemoveListener(OnCloseCupResultGrid);
            EventsManager.Instance.Remove<TournamentUserModel>(CupTeamItemRenderer.CupTeamItemSelected1ArgEvent, OnCupTeamItemSelectedHandler);
            EventsManager.Instance.Remove<TournamentResultData>(TournamentBattleHistoryRenderer.OnTournamentHistoryRecordSelected, OnCupItemSelectedHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupWinnerEvent, OnCupWinnerHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupLooseEvent, OnCupLooseHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupStartedEvent, OnCupTournamentStartedHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupRoundCompleteEvent, OnCupRoundCompleteHandler);
           // EventsManager.Instance.Remove(HockeyBattlesController.CupCanceledEvent, OnCupCancelHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupPlayersChangedEvent, OnPlayersChangedHandler);
            EventsManager.Instance.Remove(TournamentBattleController.CupExpiredEvent, OnCupExpiredHandler);
            EventsManager.Instance.Remove(GameTimer.TikTakSecondEvent, OnTikTakHandler);
            EventsManager.Instance.Remove<TournamentBattleModel>(CupItemRenderer.OnParticipateClick, OnParticipateClickHandler);
        }


        private void OnCupTeamItemSelectedHandler(TournamentUserModel model)
        {

            _matchBattleController.LoadAttackTeamInfo(model.TeamInfo.UserId, infoModel =>
            {
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                PopupsManager.Instance.AddPopup(PopupType.AttackTeamPopup)
                    .SetData(infoModel)
                    .HideButtons(true, false);
            });
        }


        private void OnCupItemSelectedHandler(TournamentResultData resultData)
        {
            tournamentGridBox.gameObject.SetActive(true);
            closeGridButton.gameObject.SetActive(true);

            cupNameTitle.text = resultData.GetCupName();

            totalInfoLabel.text = this.GetLocaleText("game.main.tournament.playedmatches.2args", resultData.TotalSteps,resultData.TotalSteps);

            int position = resultData.Players.Find(p => p.TeamInfo.IsMe()).Position;
            endCupTimerStateLabel.text = this.GetLocaleText("game.main.tournament.cupfinished").ToUpper().GetUITextColorFormated(UISettings.GreenTextColor);
            currentMatchTimerWinPostitionLabel.text = this.GetLocaleText("game.main.tournament.cupwinposition.1arg", position).ToUpper();

            List<TournamentUserModel> players = resultData.Players;
            resultsScroller.content.RemoveChildren();
            for (int i = 0; i < players.Count; i++)
            {
                CupTeamItemRenderer child = resultsScroller.content.AddChild(cupTeamResultItemRendererPrefab, false).GetComponent<CupTeamItemRenderer>();
                child.InvertSelection(i % 2 == 0);
                child.SetModel(players[i]);
            }
        }

        private void OnPlayersChangedHandler()
        {
            if (_controller.CurrentPlayCup != null && 
                (_controller.CurrentPlayCup.State == TournamentBattleStates.Finished || 
                _controller.CurrentPlayCup.Players.Count == 0))
            {
                ShowNotitficationPopup("game.common.canceled", UISettings.BlockTextColor,_controller.CurrentPlayCup.GetName());
            }
            _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
        }

        private void OnCupRoundCompleteHandler()
        {
            if(_controller.CurrentPlayCup == null || _controller.CurrentPlayCup.State == TournamentBattleStates.Finished) return;
            
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Tournament)
            {
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            }
            _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
        }

        private void OnCupLooseHandler()
        {
            if (!ShowNotitficationPopup("game.common.finished", UISettings.GreenTextColor,
                    _controller.CurrentPlayCup.GetName()))
            {
                ChangeState();
            }
           
        }

        private void OnCloseCupResultGrid()
        {
            tournamentGridBox.gameObject.SetActive(false);
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Tournament)
            {
                if (_controller.CurrentPlayCup != null)
                {
                    _controller.CurrentPlayCup.Finish();
                 }
                _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
            }
        }

        private void OnCupWinnerHandler()
        {
            SoundManager.Instance.PlaySFXOnGameObject("battlewinner", gameObject);
            if (!ShowNotitficationPopup("game.common.finished", UISettings.GreenTextColor,
                _controller.CurrentPlayCup.GetName()))
            {
                ChangeState();
            }
        }

        private void OnCupExpiredHandler()
        {
            if(_controller.CurrentPlayCup == null || !_controller.CurrentPlayCup.IsMyPlayerInCup())return;

            if (!ShowNotitficationPopup("game.common.canceled",UISettings.BlockTextColor,_controller.CurrentPlayCup.GetName(), "game.main.tournament.navbutton.cups"))
            {
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            }
            if (_controller.CurrentPlayCup != null)
            {
                _controller.CurrentPlayCup.Finish();
            }
            _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
        }

        private bool ShowNotitficationPopup(string localeContent,Color color,string cupName,string applyButton = "game.common.detailbutton")
        {
            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Tournament)
            {
                PopupsManager.Instance.RemovePopup(PopupType.TournamentNotificationPopup);
                string contentText = LocaleManager.Instance.GetText(localeContent);
                PopupsManager.Instance.AddPopup(PopupType.TournamentNotificationPopup)
                    .SetTitle(cupName.ToUpper())
                    .SetText(contentText.GetUITextColorFormated(color))
                    .SetButtonLabels(applyButton)
                    .SetCallbacks(() =>
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Tournament);
                    });
                return true;
            }
            return false;
        }

        private void OnCupTournamentStartedHandler()
        {
            if(_controller.CurrentPlayCup == null || _controller.CurrentPlayCup.State == TournamentBattleStates.Finished)return;
            SoundManager.Instance.PlaySFXOnGameObject("battlestarted", gameObject);
            if (!ShowNotitficationPopup("game.common.started",UISettings.GreenTextColor,_controller.CurrentPlayCup.GetName()))
            {
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            }
            _controller.GetTournamentsInfo(OnTournamentsChangeHandler);
        }

      
        private void OnExitCupClickHandler()
        {
            exitButton.interactable = false;
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _controller.CancelCupTournament(OnTournamentsChangeHandler);
        }

        private void UpdateTimers(TournamentBattleModel model = null)
        {
            if (model != null)
            {
                if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Tournament) return;

                long timeLeft = 0;
                if (model.State == TournamentBattleStates.Idle || model.State == TournamentBattleStates.Selection)
                {
                    timeLeft = GameTimer.TimeLeftSeconds(model.EndExpiryTime);
                    string cancelTitle = LocaleManager.Instance.GetText("game.main.tournament.cupcanceltime");
                    string timeStr = MathUtils.SecondsToTimeString(timeLeft);
                    waitTimerLabel.text = cancelTitle.ToUpper().GetUITextSizeFormated(50) + "\n" + timeStr.GetUITextSizeFormated(120);

                }
                else if (model.State == TournamentBattleStates.Started ||
                         model.State == TournamentBattleStates.Complete ||
                         model.State == TournamentBattleStates.Expired)
                {
                    timeLeft = GameTimer.TimeLeftSeconds(model.EndPlayTime);
                    if (timeLeft <= 0 || model.State == TournamentBattleStates.Complete)
                    {
                        endCupTimerStateLabel.text = LocaleManager.Instance.GetText("game.main.tournament.cupfinished").ToUpper().GetUITextColorFormated(UISettings.GreenTextColor);

                        currentMatchTimerWinPostitionLabel.text = LocaleManager.Instance.GetText("game.main.tournament.cupwinposition.1arg", model.GetMyPosition());

                    }
                    else
                    {
                        string endTimeStr = LocaleManager.Instance.GetText("game.main.tournament.cupendtime");
                        endCupTimerStateLabel.text = endTimeStr.ToUpper().GetUITextSizeFormated(40) + "\n" + MathUtils.SecondsToTimeString(timeLeft).GetUITextSizeFormated(70);

                        long matchRoundTimeLeft = GameTimer.TimeLeftSeconds(model.EndStepTime);
                        string currentRoundStr = LocaleManager.Instance.GetText("game.main.tournament.currentmatch");
                        currentMatchTimerWinPostitionLabel.text = currentRoundStr.ToUpper().GetUITextSizeFormated(40) + "\n" + MathUtils.SecondsToTimeString(matchRoundTimeLeft).GetUITextSizeFormated(70);

                    }

                }
            }
        }

        private void OnTikTakHandler()
        {
            UpdateTimers(_controller.CurrentPlayCup);
        }

        private void OnParticipateClickHandler(TournamentBattleModel model)
        {
            _team.UpdateEnergy(model.EnergyCost, () =>
            {
                if (_team.CanBuyForEnergy(model.EnergyCost))
                {
                    if (_team.CanBuyForCurrency(model.CurrencyType, model.FeePrice))
                    {
                        PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                        _controller.ParticipateCup(model.Id, OnTournamentsChangeHandler);
                    }
                    else
                    {
                        PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup).HideButtons(true,false)
                       .SetText(LocaleManager.Instance.GetText("game.main.popup.title.notenoughmoney"))
                       .SetTitle(LocaleManager.Instance.GetText("game.main.popup.title.cannotparticipate").ToUpper());
                    }

                }
                else
                {
                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup).HideButtons(true,false)
                      .SetText(LocaleManager.Instance.GetText("game.main.popup.title.notenoughenergy"))
                      .SetTitle(LocaleManager.Instance.GetText("game.main.popup.title.cannotparticipate").ToUpper());
                }
            });
           
           
        }


        private void ChangeState(TournamentBattleModel model = null)
        {
            List<TournamentBattleModel> cupList = _controller.AvailableCups;
            TournamentBattleModel currentPlayCup = model ?? _controller.CurrentPlayCup;
            if (cupList != null && (currentPlayCup == null || currentPlayCup.State == TournamentBattleStates.Finished))
            {
                    idleBox.gameObject.SetActive(false);
                    cupsScroller.gameObject.SetActive(true);
                    cupsScroller.content.RemoveChildren();
                    for (int i = 0; i < cupList.Count; i++)
                    {
                        CupItemRenderer child = cupsScroller.content.AddChild(cupItemRendererPrefab, false).GetComponent<CupItemRenderer>();
                        child.InvertSelection(i % 2 != 0);
                        child.SetModel(cupList[i]);
                    }

                if (cupList.Count == 0)
                {
                    Text emptyRendererLabel = cupsScroller.content.AddChild(cupEmptyItemRendererPrefab, false).GetComponent<Text>();
                    emptyRendererLabel.text = LocaleManager.Instance.GetText("game.main.tournament.emptyrenderer.noavailblecups");
                }

                cupsScroller.verticalNormalizedPosition = 1f;
                navBar.ShowButton(2,true);

                Debug.Log("Tournaments list length - " + cupList.Count);

                return;
            }

            if (currentPlayCup != null)
            {

                Debug.Log("Tournament state " + currentPlayCup.State);
                switch (currentPlayCup.State)
                {
                    case TournamentBattleStates.Selection:
                    case TournamentBattleStates.Idle:

                        navBar.ShowButton(2,false);
                        cupNameLabel.text = currentPlayCup.GetName();
                        exitButton.interactable = true;
                        cupsScroller.gameObject.SetActive(false);
                        idleBox.SetActive(true);
                        string players = (currentPlayCup.Players.Count + "/" + currentPlayCup.MaxPlayers);
                        string participantsStr = LocaleManager.Instance.GetText("game.main.tournament.participants2");
                        participantsLabel.text = participantsStr.GetUITextSizeFormated(50) + '\n' + players.GetUITextSizeFormated(110);

                        levelsField.text = this.GetLocaleText("game.common.level", currentPlayCup.MinLevel + "-" + currentPlayCup.MaxLevel);

                        priceFeePanel.SetIcon(settings.GetIcon(currentPlayCup.CurrencyType));
                        priceFeePanel.SetText(currentPlayCup.FeePrice);

                        rewardPanel.SetIcon(settings.GetIcon(currentPlayCup.CurrencyType));
                        rewardPanel.SetText(currentPlayCup.CurrencyAward);

                        UpdateTimers(currentPlayCup);

                    break;
                    case TournamentBattleStates.Complete:
                    case TournamentBattleStates.Started:
                    case TournamentBattleStates.Expired:
                        navBar.ShowButton(2,false);
                        if (currentPlayCup.State != TournamentBattleStates.Expired)
                        {
                            UpdateTournamentResults(currentPlayCup);
                            UpdateTimers(currentPlayCup);
                        }
                        else
                        {
                            currentPlayCup.Finish();
                        }
                   break;
                   
                }
            }
        }

        private void UpdateTournamentResults(TournamentBattleModel model)
        {
            Debug.Log("Updates tournament results,");
            idleBox.SetActive(false);
            cupsScroller.gameObject.SetActive(false);
            tournamentGridBox.SetActive(true);

            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Tournament)return;

            string cupName = model.GetName();
            cupNameTitle.text = model.State == TournamentBattleStates.Started ? 
            LocaleManager.Instance.GetText("game.common.battleinprocess.1arg", cupName) : cupName;

            totalInfoLabel.text = LocaleManager.Instance.GetText("game.main.tournament.playedmatches.2args",model.Steps,model.TotalSteps);

            List<TournamentUserModel> players = model.Players;
            resultsScroller.content.RemoveChildren();
            for (int i = 0; i < players.Count; i++)
            {
                CupTeamItemRenderer child = resultsScroller.content.AddChild(cupTeamResultItemRendererPrefab,false).GetComponent<CupTeamItemRenderer>();
                child.InvertSelection(i%2==0);
                child.SetModel(players[i]);
            }

            closeGridButton.gameObject.SetActive(model.State == TournamentBattleStates.Complete);
           
        }


        private void BuildHistoryPage()
        {
            TournamentHistoryModel model = _controller.CupsHistoryModel;

            if(model == null)return;

            string winsStr = model.Wins > 0 ? model.Wins.ToString().GetUITextColorFormated(UISettings.GreenTextColor) : model.Wins.ToString();
            historyPageTitle.text = LocaleManager.Instance.GetText("game.main.tournament.history.title.3args",model.Total, winsStr,model.Lost);
         
            for (int i = 0; i < model.Results.Count; i++)
            {
                TournamentBattleHistoryRenderer item = cupsHistoryScroller.content.AddChild(cupBattleHistoryItemRendererPrefab,false).GetComponent<TournamentBattleHistoryRenderer>();
                item.SetData(model.Results[i]);
                item.InvertColors(i);
            }
            
        }

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (selected == NavigationPageTypes.Tournament)
            {
                navBar.SelectButton(1,true);
                tournamentCups.SetActive(true);
                tournamentGridBox.SetActive(false);
                cupsHistoryScroller.content.RemoveChildren();
                _controller.GetTournamentsInfo(OnTournamentsChangeHandler); 
            }
            else if (selected == NavigationPageTypes.TournamentHistory)
            {
                resultsScroller.content.RemoveChildren();
                cupsScroller.content.RemoveChildren();
                tournamentCups.SetActive(false);
                tournamentGridBox.SetActive(false);
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                _controller.GetCupsHistoryData(BuildHistoryPage);
            }
            else if (selected == NavigationPageTypes.СreateTournament)
            {
                cupsHistoryScroller.content.RemoveChildren();
                resultsScroller.content.RemoveChildren();
                cupsScroller.content.RemoveChildren();
                cupsHistoryScroller.content.RemoveChildren();
                tournamentCups.SetActive(false);
                tournamentGridBox.SetActive(false);
            }

            if (selected != NavigationPageTypes.Tournament && prev == NavigationPageTypes.Tournament)
            {
                if (_controller.CurrentPlayCup != null &&
                    _controller.CurrentPlayCup.State == TournamentBattleStates.Complete)
                {
                    _controller.CurrentPlayCup.Finish();
                }
            }
        }

        private void OnTournamentsChangeHandler()
        {
            ChangeState();
        }
    }
}
