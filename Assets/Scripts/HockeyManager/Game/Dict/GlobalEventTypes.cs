﻿namespace HockeyManager.Game.Dict
{
    
    public class GlobalEventTypes
    {
        public const string InternetConnectionLost = "noInternetConnection";
        public const string InternetConnectionExist = "internetConnectionEstablished";

        public const string SocialAccountConnectedResult = "socialAccountConnectedResult";


        public const string ServerRequestSended = "serverRequestSended";
        public const string ServerResponceRecieved = "serverResponceRecieved";
        public const string ServerResponceError = "serverResponceError";

        public const string GameUnpaused = "gameUnpaused";
        public const string GamePaused = "gamePaused";
    }
}
