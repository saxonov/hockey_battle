﻿using GameSparks.Core;

namespace HockeyManager.Game.Impl
{
    public interface ICheckMessages
    {
        void CheckMessage(GSData msgData,string msgCode);

        bool CanCheckMessage(string msgType,GSData data = null);
    }
}
