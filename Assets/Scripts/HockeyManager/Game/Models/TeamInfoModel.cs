﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Game.Models
{
    public struct TeamInfoData
    {
        public string name;
        public int logoId;
        public float logoColor;
        public float logoSaturation;
    }

    public sealed class TeamInfoModel : IGSParse
    {
        private string _userId;

        private TeamInfoData _viewInfo;

        
        public TeamInfoModel(string userId)
        {
            _userId = userId;
        }

        public TeamInfoModel(GSData data)
        {
            if (data.ContainsKey("userId"))
            {
                _userId = data.GetString("userId");
            }
            if (data.ContainsKey("_id"))
            {
                _userId = data.GetString("_id");
            }
            Parse(data);
        }

        public string Name
        {
            get { return _viewInfo.name; }
            set { _viewInfo.name = value; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public bool IsMe()
        {
            return GameData.IsMyUserId(_userId);
        }

        public float LogoSaturation
        {
            get { return _viewInfo.logoSaturation; }
            set { _viewInfo.logoSaturation = value; }
        }

        public float LogoColor
        {
            get { return _viewInfo.logoColor; }
            set { _viewInfo.logoColor = value; }
        }

        public int Logo
        {
            get { return _viewInfo.logoId; }
            set { _viewInfo.logoId = value; }
        }

        public void Parse(string json)
        {
            _viewInfo = JsonUtility.FromJson<TeamInfoData>(json);
        }

        public void Parse(GSData data)
        {
           

            if (data.ContainsKey("NAME"))
            {
                _viewInfo.name = data.GetString("NAME");
            }
            if (data.ContainsKey("name"))
            {
                _viewInfo.name = data.GetString("name");
            }

            if (data.ContainsKey("logoId") || data.ContainsKey("LI"))
            {
                _viewInfo.logoId = data.GetInt("logoId").HasValue ? data.GetInt("logoId").Value : data.GetInt("LI").Value;
            }

            if (data.ContainsKey("logoSaturation") || data.ContainsKey("LS"))
            {
                _viewInfo.logoSaturation = data.GetFloat("logoSaturation").HasValue ? data.GetFloat("logoSaturation").Value : data.GetFloat("LS").Value;
            }

            if (data.ContainsKey("logoColor") || data.ContainsKey("LC"))
            {
                _viewInfo.logoColor = data.GetFloat("logoColor").HasValue ? data.GetFloat("logoColor").Value : data.GetFloat("LC").Value;
            }
        }

        

        public override string ToString()
        {
            return "Name: " + _viewInfo.name + " logoId:" + _viewInfo.logoId + " logoColor: " + _viewInfo.logoColor + " logoSaturation: " + _viewInfo.logoSaturation;
        }
    }

}
