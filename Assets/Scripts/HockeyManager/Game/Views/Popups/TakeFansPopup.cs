﻿using HockeyManager.Game.Controllers;
using HockeyManager.Game.Impl;
using UnityEngine;
using HockeyManager.Game.UI.Components;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Popups
{
    public class TakeFansPopup :BasePopup
    {
        [SerializeField]private Slider _fansSLider;
        [SerializeField] private TextIconControl _fansCounter;

        private TextIconControl goldPricePanel;

        private int goldPrice = 0;
        private int fansCount = 0;

        private TeamController _controller;

        protected override void SetController(IGameController controller)
        {
            if (_controller == null)
            {
                _controller = controller as TeamController;
            }
        }

        public Slider FansSLider
        {
            get { return _fansSLider; }
        }

        public int FansCount
        {
            get { return fansCount; }
        }

        public int GoldPrice
        {
            get { return goldPrice; }
        }

        void OnDestroy()
        {
            _controller = null;
            _fansSLider.onValueChanged.RemoveListener(OnSliderValueChanged);
        }
        // Use this for initialization
        private void Start()
        {
            goldPricePanel = _basePopupControls.controls[0];
           
        
            _fansSLider.minValue = 0;
            _fansSLider.maxValue = _controller.MaxFansToBuy;
            _fansSLider.onValueChanged.AddListener(OnSliderValueChanged);
            OnSliderValueChanged(0);
        }

        private void OnSliderValueChanged(float value)
        {
            fansCount = Mathf.RoundToInt(value);
            goldPrice = _controller.GetGoldPricePerFan(fansCount);
            goldPricePanel.SetText(goldPrice.ToString());
            _fansCounter.SetText(fansCount.ToString());
            _basePopupControls.yesButton.interactable = goldPrice > 0;
        }

      

    }
}