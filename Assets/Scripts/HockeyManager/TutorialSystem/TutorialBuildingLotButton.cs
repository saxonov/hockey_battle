﻿using HockeyManager.Game;
using HockeyManager.Game.Dict;
using HockeyManager.Game.UI.Map;
using HockeyManager.TutorialSystem.Base;
using UnityEngine;

namespace HockeyManager.TutorialSystem
{
    public class TutorialBuildingLotButton:TutorialObject
    {
#pragma warning disable CS0649 // Полю "TutorialBuildingLotButton.mapLot" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private MapBuildingLot mapLot;
#pragma warning restore CS0649 // Полю "TutorialBuildingLotButton.mapLot" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


        void Start()
        {
            if (GameData.IsTutorialPassed)
            {
                Destroy(gameObject);
            }
            
        }

        public override void OnUpState()
        {
            mapLot.OnPointerClick(pointerEventData);
        }

        public override void OnDownState()
        {
          
        }
    }
}
