﻿using System;
using UnityEngine.Purchasing;

namespace HockeyManager.Game.Models.Shop
{
    public class PurchaseItemModel:IComparable<PurchaseItemModel>
    {

        private int _goldToBuy = 0;

        private Product _product;

        public PurchaseItemModel(int goldToBuy, Product p)
        {
            _goldToBuy = goldToBuy;
            _product = p;

        }
        public Product Product
        {
            get { return _product; }
        }

        public int GoldToBuy
        {
            get { return _goldToBuy; }
        }


        public int CompareTo(PurchaseItemModel other)
        {
            return _goldToBuy - other.GoldToBuy;
        }
    }
}
