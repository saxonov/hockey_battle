﻿using System.Collections;
using HockeyManager.Game.GameSparks.Impl;
using UnityEngine;

namespace HockeyManager.Game.Impl
{

    public interface IGameRoot
    {
        bool IsPaused { get; }

        T GetController<T>() where T : IGameController;

        bool IsGameScreen();
        bool IsGameStarted { get; }

        IGSManager NetManager { get; }
        IGSAuthentication Authtorization { get; }

        Coroutine StartCoroutine(IEnumerator routine);
    }
}
