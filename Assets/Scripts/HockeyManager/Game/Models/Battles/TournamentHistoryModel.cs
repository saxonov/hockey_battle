﻿using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{
    public class TournamentHistoryModel:IGSParse
    {

        private int _total = 0;
        private int _wins = 0;
        private int _lost = 0;

        private List<TournamentResultData> _results = new List<TournamentResultData>();

        public int Total
        {
            get { return _total; }
        }

        public int Wins
        {
            get { return _wins; }
        }

        public int Lost
        {
            get { return _lost; }
        }

        public List<TournamentResultData> Results
        {
            get { return _results; }
        }


        public void Parse(GSData data)
        {
            _total = data.GetInt("total").Value;
            _wins = data.GetInt("wins").Value;
            _lost = data.GetInt("lost").Value;

            var list = data.GetGSDataList("history");

            _results.Clear();

            foreach (var gsData in list)
            {
                _results.Add(new TournamentResultData(gsData));
            }

        }
    }
}
