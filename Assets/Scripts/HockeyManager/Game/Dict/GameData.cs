﻿using System;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Dict
{
    //public struct NextLevelExp
    //{
    //    public int level;
    //    public int needExp;
    //}

    public enum GameActionState
    {
        Training,
        PlayMatch,
        PlayTournament,
        None = -1
    }

    public struct NewsData
    {
        public string title;
        public string desc;
        public long timestamp;
    }

    public class GameSettingsData
    {
        public bool music = true;
        public bool sound = true;
        public bool notification = true;
        public LocaleType locale = LocaleType.None;

        public bool tutorialPassed = false;
        
        public bool isChanged = false;


    }

    public struct BalanceInfoData
    {
        public int fans;
        public int maxFans;

        public int maxEnergy;
        public int energy;

        public int coins;
        public int gold;
    }

    public struct RewardData
    {
        public int exp;
        public int coins;
        public int gold;
        public int fans;
        public string relics;

        public void Add(RewardData data)
        {
            exp += data.exp;
            coins += data.coins;
            fans += data.fans;
            gold += data.gold;
        }

        //public int this[int i]
        //{
        //    get
        //    {
        //        int outValue = 0;
        //        switch (i)
        //        {
        //            case 0:
        //                outValue = exp;
        //            break;
        //            case 1:
        //                outValue = fans;
        //            break;
        //            case 2:
        //                outValue = coins;
        //             break;
        //            case 3:
        //                outValue = gold;
        //            break;
        //        }

        //        return outValue;
        //    }
        //}

        //public void SetNewExp(int exp)
        //{
        //    this.exp = exp;
        //}

        //public void SetGSData(GSData data)
        //{
        //    exp = data.GetInt("exp").HasValue ? data.GetInt("exp").Value : 0;
        //    coins = data.GetInt("coins").HasValue ? data.GetInt("coins").Value : 0;
        //    gold = data.GetInt("gold").HasValue ? data.GetInt("gold").Value : 0;
        //    fans = data.GetInt("fans").HasValue ? data.GetInt("fans").Value : 0;

        //}
    }

    public enum RankType
    {
        Beginner = 0,
        Amateur,
        Professional,
        Master,
        TopStar
    }
    
    public enum EntityType
    {
        Building,
        Equipment,
        Personal,
        Training
    }

    public enum SocialAccountType
    {
        GooglePlayGames,
        GameCenter,
        Facebook,
        None
    }
    //public enum SkillType
    //{
    //    Faceoff,
    //    Stamina,
    //    Speed,
    //    Throw,
    //    Dribbling,
    //    Reaction,
    //    Reliability,
    //    Steal,
    //    None
    //}

    public enum RewardType
    {
        BuildigUpgradedReward = 0,
        TeamLevelUpReward = 1,
        EquipmnentUpgradedReward = 4,
        PersonalUpgradedReward = 5,
        AchievementReward = 6,
        TrainingUpgradedReward = 7,
        MatchWinnerReward = 8,
        MatchLooseReward = 9,
        TournamentWinnerReward = 10,
        TournamentLostReward = 11,
        CompetitionWinnerReward = 12,
        DailyBonusReward = 13,
        MatchCancelPenaltyReward = 14,
        AdsReward = 15
    }

    public enum EquipmentType
    {
        Pullover,//свитер
        Shorts,
        Gloves,
        Helmet,
        Skates,
        HockeyStick
    }

    public enum EmployeeType
    {
        MainCouch = 0,
        SecondCouch,
        Doctor,
        Adman //рекламный агент
    }

    public enum MainCouchTactic
    {
        TacticBalance = 0,
        TacticAttack,
        TacticDeffence

    }

    public enum MapBuildingsType
    {
        IceArena = 0,
        Shop,
        Cashbox,
        Base,
        Hall,//hall of fame
        Bank,
        Kiosk,
        AdBanner,
        None // not setted

    }



    //public enum UpgradeProcessCategory
    //{
    //    UpgradeEquipmentProcess,
    //    UpgradeBuildingProcess,
    //    TrainingProcess,
    //    UpgradePersonalProcess
    //}

    public static class GameData
    {
        public const string ExpTypeCompetition = "exp";
        public const string CoinsTypeCompetition = "coins";
        public const string GoalsTypeCompetition = "goals";

        //battles
        public const string MatchBattle = "match";
        public const string TournamentBattle = "cup";
            
        //trainngs
        public const string AttackTraining = "attack";
        public const string CommonTraining = "common";
        public const string DeffenceTraining = "deffence";
        public const string GatherTraining = "gather";
        
        //skills
        public const string FaceoffSKill = "faceoff";
        public const string StaminaSkill = "stamina";
        public const string DribblingSkill = "dribbling";
        public const string ThrowSKill = "throw";
        public const string SpeedSkill = "speed";
        public const string ReactionSkill = "reaction";
        public const string ReliabilitySKill = "reliability";
        public const string StealSkill = "steal";

        private static readonly DateTime Jan1St1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static bool _isTutorialPassed = false;
        /// <summary>Get extra long current timestamp</summary>
        public static long Seconds
        {
            get { return (long)((DateTime.UtcNow - Jan1St1970).TotalSeconds); }
        }

        public const int MaxHallInventorySlots = 10;

        public const int BaseBuildingPosition = 4;
        public const int ShopBuildingPosition = 2;
        public const int ArenaBuildingPosition = 0;
        public const int HallBuildingPosition = 1;
        public const int CashboxTutorialPosition = 5;


        public const int CurrencyGold = 1;
        public const int CurrencyCoins = 2;

        public const int MaxBuildingTypes = 8;

        private static GS_Constants _gsConstData;

        private static bool _hasVip = false;

        public static GS_Constants GSConstData
        {
            get
            {
                _gsConstData = _gsConstData ?? new GS_Constants();
                return _gsConstData;
            }
        }

        public static bool HasVip
        {
            get { return _hasVip; }
            set { _hasVip = value; }
        }

        public static bool IsTutorialPassed
        {
            get { return _isTutorialPassed; }
            set
            {
                if (_isTutorialPassed == false)
                {
                    _isTutorialPassed = value;
                }
               
            }
        }

        public static bool IsNewTeam
        {
            get { return _isNewTeam; }
        }

        private static string _userID = "";

        private static bool _isNewTeam;

        public static void SetUserID(string userId,bool isNew)
        {
            _isNewTeam = isNew;
            if (string.IsNullOrEmpty(_userID))
            {
                _userID = userId;
            }
        }

        public static bool IsMyUserId(string userId)
        {
            if (string.IsNullOrEmpty(_userID)) throw new Exception("GameData::IsMyTeam - userID not setted!");
            return _userID == userId;
        }

        public static int GetTimerGoldPrice(long endSecondsTime)
        {
            const int GoldPricePerTime = 1;
            long diff = GameTimer.TimeLeftSeconds(endSecondsTime);
            float minutesLeft = Mathf.Floor(diff / 60f);
            float golPrice = minutesLeft > 0 ? minutesLeft * GoldPricePerTime : GoldPricePerTime;
            return Mathf.FloorToInt(golPrice);
        }

        //public const int IceArenaPosition = 1;
    }
}
