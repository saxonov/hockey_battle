<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>E:/UNITY/HockeyBattle/Assets/Sprites/Atlases/BaseMainUI.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Fast</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <true/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../BaseUIAtlas.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Fast</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kl1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kl2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kl3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kn1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kn2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_kn3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_p1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_p2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_p3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sh1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sh2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sh3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sl1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sl2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sl3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sv1.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sv2.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображения экипировки/а_sv3.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/ecipirovka.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/kubki.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/matches.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/nagrada.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/obuchenie.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/prize.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/stroyka.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Иконки для наград/trenirovka.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/кубки.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/матчи.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/награды.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/тренировки.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,75</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Apps-Google-Play-Games-icon.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Game-Center-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Attack.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,26,64,52</rect>
                <key>scale9Paddings</key>
                <rect>32,26,64,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Defense.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,65</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,65</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Energy-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,16,38,32</rect>
                <key>scale9Paddings</key>
                <rect>19,16,38,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Lose.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Wins.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,36,64,72</rect>
                <key>scale9Paddings</key>
                <rect>32,36,64,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Points.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,33,64,66</rect>
                <key>scale9Paddings</key>
                <rect>32,33,64,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Scored.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,26,64,51</rect>
                <key>scale9Paddings</key>
                <rect>32,26,64,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/ataka_liderbord.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/effektivnost.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/soisok.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/ban.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/otp.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,31,82,61</rect>
                <key>scale9Paddings</key>
                <rect>41,31,82,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/bolel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,14,32,27</rect>
                <key>scale9Paddings</key>
                <rect>16,14,32,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/coin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,36,32</rect>
                <key>scale9Paddings</key>
                <rect>18,16,36,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/delet_msg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,21,47,42</rect>
                <key>scale9Paddings</key>
                <rect>24,21,47,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/dlyaknopki.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,10,96,21</rect>
                <key>scale9Paddings</key>
                <rect>48,10,96,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/effekt.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/time.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,7,19,14</rect>
                <key>scale9Paddings</key>
                <rect>9,7,19,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/error.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,24,55,48</rect>
                <key>scale9Paddings</key>
                <rect>27,24,55,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/fon_valyti.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,9,110,18</rect>
                <key>scale9Paddings</key>
                <rect>55,9,110,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/gold.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/icon-edit.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/ind.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Магазин/sh_s.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Магазин/vip_s.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/энергия.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/knopka_onaje_fon_progress.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/zalivka_progress.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,10,55,21</rect>
                <key>scale9Paddings</key>
                <rect>27,10,55,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/knopka_zarabotat.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,24,170,47</rect>
                <key>scale9Paddings</key>
                <rect>85,24,170,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/magaz.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,14,39,28</rect>
                <key>scale9Paddings</key>
                <rect>19,14,39,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/nehvatka.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,43,85,85</rect>
                <key>scale9Paddings</key>
                <rect>43,43,85,85</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/obnovit.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,51,128,103</rect>
                <key>scale9Paddings</key>
                <rect>64,51,128,103</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/obvodka.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,36,76,71</rect>
                <key>scale9Paddings</key>
                <rect>38,36,76,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/ok.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/strelka1.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/vopros.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/zamok.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/popap.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,48,94,95</rect>
                <key>scale9Paddings</key>
                <rect>47,48,94,95</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/ruki.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,129</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,129</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/st.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/vk_gr.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,35,75,70</rect>
                <key>scale9Paddings</key>
                <rect>37,35,75,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/strelka.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,53,128,106</rect>
                <key>scale9Paddings</key>
                <rect>64,53,128,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/strelkakub.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,13,32,26</rect>
                <key>scale9Paddings</key>
                <rect>16,13,32,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/video.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,28,71,55</rect>
                <key>scale9Paddings</key>
                <rect>35,28,71,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/zalivka.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>0,8,1,16</rect>
                <key>scale9Paddings</key>
                <rect>0,8,1,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/zamok_ malenkii.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/zk.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,31</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,21,64,43</rect>
                <key>scale9Paddings</key>
                <rect>32,21,64,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/1.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/10.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/11.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/12.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/13.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/14.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/15.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/16.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/17.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/2.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/3.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/4.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/5.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/6.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/7.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/8.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Зал Славы/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,56,112,112</rect>
                <key>scale9Paddings</key>
                <rect>56,56,112,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Магазин/Untitled-1-11.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Магазин/Untitled-1-12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,100,200,200</rect>
                <key>scale9Paddings</key>
                <rect>100,100,200,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Улучшение/progres_niz.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Улучшение/progres_verh.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,8,64,16</rect>
                <key>scale9Paddings</key>
                <rect>32,8,64,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/cha.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/che.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/deu.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/eng.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/fin.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/fra.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/rus.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/Флаги/sve.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,20,64,41</rect>
                <key>scale9Paddings</key>
                <rect>32,20,64,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/дом.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,36,36</rect>
                <key>scale9Paddings</key>
                <rect>18,18,36,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/другое.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,23,50,46</rect>
                <key>scale9Paddings</key>
                <rect>25,23,50,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/конкурсы.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,21,51,43</rect>
                <key>scale9Paddings</key>
                <rect>25,21,51,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/обводка.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,30,59,60</rect>
                <key>scale9Paddings</key>
                <rect>29,30,59,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/опыт.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,15,34,30</rect>
                <key>scale9Paddings</key>
                <rect>17,15,34,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/плюс.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,21,42,41</rect>
                <key>scale9Paddings</key>
                <rect>21,21,42,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/раздевалка.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,19,50,38</rect>
                <key>scale9Paddings</key>
                <rect>25,19,50,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки/чат.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,20,50,41</rect>
                <key>scale9Paddings</key>
                <rect>25,20,50,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Персонал/adman.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Персонал/doctor.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Персонал/main_couch.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Персонал/second_couch.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,114,175,228</rect>
                <key>scale9Paddings</key>
                <rect>88,114,175,228</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>D:/Dropbox/HockeyBattle/Иконки</filename>
            <filename>D:/Dropbox/HockeyBattle/Персонал</filename>
            <filename>D:/Dropbox/HockeyBattle/Изображения экипировки</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
