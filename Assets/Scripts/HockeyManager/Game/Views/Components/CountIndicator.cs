﻿using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    public class CountIndicator:MonoBehaviour
    {

#pragma warning disable CS0649 // Полю "CountIndicator.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text counterLabel;
#pragma warning restore CS0649 // Полю "CountIndicator.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private int _count = 0;

        void Awake()
        {
            SetValue(_count);
        }

        void OnEnable()
        {
            if (_count > 0)
            {
                UpdateText();
            }
        }

        public void SetValue(int count)
        {
            _count = count;
            UpdateText();
        }


        private void UpdateText()
        {
            if(!gameObject.activeSelf)return;
            counterLabel.text = _count.ToString();
        }
    }
}
