﻿namespace HockeyManager.TutorialSystem.Data
{

    public static class TutorStepTypes
    {
        public const string EmptyStep = "empty";

        public const string Intro = "intro";

        public const string SelectIceArena = "select_ice_arena";
        public const string SelectCashbox = "select_cashbox";
        public const string SelectShop = "select_shop";
        public const string BuildShop = "build_shop";
        public const string FinishShop = "finish_shop";
        public const string CollectShop = "collect_shop";
        public const string BuildCashbox = "build_cashbox";
        public const string FinishCashbox = "finish_cashbox";
        public const string CollectCashbox = "collect_cashbox";
        public const string BuildIceArena = "build_ice_arena";
        public const string FinishIceArena = "finish_ice_arena";

        public const string ShowRewardsPage = "rewards_page_start";
        public const string ShowMatchesPage = "match_page_start";
        public const string ShowTrainingsPage = "trainings_page_start";
        public const string StartTraining = "start_training";
        public const string FinishTraining = "finish_training";
        public const string GotoHomeFromTrainings = "goto_home_page_from_trainings";
        public const string TakeReward = "take_reward";
        public const string GotoHomePageFromRewards = "goto_home_page_from_rewards";
        public const string GotoHomePageFromSkills = "goto_home_page_from_skills";
        public const string GotoCloakroomPage = "goto_cloakroom_page";
        public const string GotoPersonalPage = "goto_personal_page";
        public const string GotoSkillsPage = "goto_skills_page";
        public const string DistributeSkillPoints = "distribute_skill_points";
        public const string ApplySkillPoints = "apply_skill_points";
        public const string HireMainCouch = "hire_main_couch";
        public const string FinishMainCouch = "finish_main_couch";


        public const string EndTutorial = "end_tutorial";

    }
}
