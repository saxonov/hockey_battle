﻿using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Views.Components
{
    public class VipShieldsIndicator:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "VipShieldsIndicator.vipIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image vipIndicator;
#pragma warning restore CS0649 // Полю "VipShieldsIndicator.vipIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VipShieldsIndicator.shieldIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image shieldIndicator;
#pragma warning restore CS0649 // Полю "VipShieldsIndicator.shieldIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


#pragma warning disable CS0169 // Поле "VipShieldsIndicator.firstPosition" никогда не используется.
        [SerializeField]private Vector2 firstPosition;
#pragma warning restore CS0169 // Поле "VipShieldsIndicator.firstPosition" никогда не используется.
#pragma warning disable CS0169 // Поле "VipShieldsIndicator.secondPosition" никогда не используется.
        [SerializeField]private Vector2 secondPosition;
#pragma warning restore CS0169 // Поле "VipShieldsIndicator.secondPosition" никогда не используется.

        void Awake()
        {
            vipIndicator.gameObject.SetActive(false); 
            shieldIndicator.gameObject.SetActive(false);
        }

        public void EnableVipIndicator(bool value)
        {
            vipIndicator.gameObject.SetActive(value);
        }

        public void EnableShieldIndicator(bool value)
        {
            shieldIndicator.gameObject.SetActive(value);
        }
    }
}
