﻿using UnityEngine.Events;

namespace HockeyManager.Game.Models.HallItems
{
    public class HallItemSlotModel
    {
        public UnityEvent OnItemChangeEvent = new UnityEvent();

        private bool _isLocked = true;

        private int _slotId = 0;

        private int _itemId = -1;

        public bool IsLocked
        {
            get { return _isLocked; }
            set
            {
                if (_isLocked != value)
                {
                    _isLocked = value;
                    OnItemChangeEvent.Invoke();
                }
            }
        }

        public int SlotId
        {
            get { return _slotId; }
        }

        public int ItemId
        {
            get { return _itemId; }
        }

        public HallItemSlotModel(int id)
        {
            _slotId = id;
        }

        public void SetItem(int id)
        {
            _itemId = id;
            OnItemChangeEvent.Invoke();
        }


    }
}
