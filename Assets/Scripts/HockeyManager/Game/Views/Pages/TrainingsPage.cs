﻿using System.Collections;
using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Trainings;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class TrainingsPage:MonoBehaviour
    {

        [SerializeField] private TextIconControl currentAttackBonus = null;
        [SerializeField] private TextIconControl currentDeffenceBonus = null;
        [SerializeField] private Button upgradeTrainings = null;
        [SerializeField] private Text upgradeTimerPanel = null;
        [SerializeField] private ScrollRect trainingsList = null;
        [SerializeField] private Text trainingsNotExistLabel = null;
        //[SerializeField] private GameObject trainingItemRendererPrefab = null;
       // [SerializeField] private GameObject emptyItemRendererPrefab = null;

       // [SerializeField] private InnerPageNavigation navBar = null;

        [SerializeField]private TrainingItemRenderer[] trainingRenderers = new TrainingItemRenderer[5];
        
        private TeamController _teamController;
        private TrainingsController _controller;

        void Awake()
        {
            _controller = GameRoot.Instance.GetController<TrainingsController>();
            _teamController = GameRoot.Instance.GetController<TeamController>();
        }

        //void Start()
        //{
        //    StartCoroutine(RunTestNotifyNewTrainings());
        //}

        //private IEnumerator RunTestNotifyNewTrainings()
        //{
        //    yield return new WaitForSeconds(4f);
        //    OnNewTrainingsNotifyHandler();
        //}

        void OnDestroy()
        {
            _teamController = null;
            _controller = null;
           
        }

        void OnEnable()
        {
            EventsManager.Instance.Add(TrainingsController.UpdateTrainingsEvent, OnNewTrainingsAvailable);
            EventsManager.Instance.Add(TrainingsController.NewTrainingsNotifyEvent, OnNewTrainingsNotifyHandler);
            EventsManager.Instance.Add<TrainingModel>(TrainingItemRenderer.OnTrainingSelected, OnItemSelectedHandler);

        }

        void OnDisable()
        {
            EventsManager.Instance.Remove<TrainingModel>(TrainingItemRenderer.OnTrainingSelected, OnItemSelectedHandler);
            EventsManager.Instance.Remove(TrainingsController.UpdateTrainingsEvent, OnNewTrainingsAvailable);
            EventsManager.Instance.Remove(TrainingsController.NewTrainingsNotifyEvent, OnNewTrainingsNotifyHandler);
        }

        public void OnShow()
        {
            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
            upgradeTrainings.onClick.AddListener(OnUpgradeNowClick);
            OnTikTakHandler();
            _controller.RefreshTrainings(false, true, UpdateTrainingsList);
        }

        public void OnHide()
        {
            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
            upgradeTrainings.onClick.RemoveListener(OnUpgradeNowClick);
        }

        private void OnNewTrainingsNotifyHandler()
        {
            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Training && 
                _teamController.CurrentActionState != GameActionState.PlayMatch  && 
                _teamController.CurrentActionState != GameActionState.PlayTournament &&
                GameData.IsTutorialPassed)
            {
                PopupsManager.Instance.AddPopup(PopupType.BaseNotificationPopup)
                    .SetText(LocaleManager.Instance.GetText("game.main.trainings.notifications.newtrainingslist"))
                    .SetCallbacks(() =>
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Training);
                    });
            }
        }

        private void OnItemSelectedHandler(TrainingModel model)
        {
            
            TrainingModel currentUpgrade = _controller.GetCurrentUpgrade();

            if (currentUpgrade != null && currentUpgrade.Id != model.Id)
            {
                string title = "game.main.popup.title.cannotcontinue";
                string upgradeName = currentUpgrade.GetName();
                PopupsManager.Instance.AddPopup(PopupType.LockedToChangePopup)
                    .SetTitle(title)
                    .SetText(LocaleManager.Instance.GetText("game.main.training", upgradeName))
                    .SetTextIconControls(GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds))
                    .SetCallbacks(() =>
                    {
                        if (_teamController.CanBuyForGold(GameData.GSConstData.TrainingsRefreshGoldPrice))
                        {
                          
                            _controller.EndTrainingsUpgrade((result) =>
                            {
                                if (result)
                                {
                                    SoundManager.Instance.PlaySFX("finishforgold");
                                    UpdateTrainingsList();
                                }
                                else
                                {
                                    _controller.RefreshTrainings(false, true, UpdateTrainingsList);
                                }
                            });
                        }
                        else
                        {
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                        }
                       

                    }).SetMinutesTickUpdate((popup) =>
                    {
                        popup.SetTextIconControls(GameData.GetTimerGoldPrice(currentUpgrade.EndUpgradeTimeSeconds));
                    });
                return;
            }

            if (!model.IsUpgrading)
            {

                _teamController.UpdateEnergy(model.EnergyCost,() =>
                {
                    if (_teamController.CanBuyForEnergy(model.EnergyCost))
                    {
                        _controller.StartTraining(model.UID, false, (result) =>
                        {
                            if (result)
                            {
                                SoundManager.Instance.PlaySFX("starttraining");
                                ActiveProcessesManager.Instance.AddUpgradeProcess(_controller.GetCurrentUpgrade());
                                UpdateTrainingsList();

                            }
                        });
                    }
                    else
                    {
                        PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                            .SetTitle("game.main.popup.title.notenoughenergy")
                            .HideButtons(true, false);
                    }
                });
              
            }
            else
            {
                string time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(model.EndUpgradeTimeSeconds));
                int goldPrice = GameData.GetTimerGoldPrice(model.EndUpgradeTimeSeconds);
                string title = LocaleManager.Instance.GetText("game.main.training");
                PopupsManager.Instance.AddPopup(PopupType.FinishUpgradePopup) 
                    .SetTitle(title)
                    .LockButtons(false,!GameData.IsTutorialPassed)
                    .SetTextIconControls(goldPrice.ToString(), time)
                    .SetCallbacks(() =>
                    {
                        if (_teamController.CanBuyForGold(goldPrice))
                        {
                            
                            _controller.EndTrainingsUpgrade((result) =>
                            {
                                if (result)
                                {
                                    SoundManager.Instance.PlaySFX("finishforgold");
                                    UpdateTrainingsList();
                                }
                                else
                                {
                                    _controller.RefreshTrainings(false, true, UpdateTrainingsList);
                                }
                            });
                        }
                        else
                        {
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                        }
                    })
                    .SetMinutesTickUpdate((popup) =>
                    {
                        goldPrice = GameData.GetTimerGoldPrice(model.EndUpgradeTimeSeconds);
                        time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(model.EndUpgradeTimeSeconds));
                        popup.SetTextIconControls(goldPrice.ToString(), time);
                    });
            }
            
        }


        private void OnTikTakHandler()
        {
            if (_controller != null)
            {
                long timeLeft = GameTimer.TimeLeftSeconds(_controller.NextGenerateTrainingsTime);
                if (timeLeft >= 0)
                {
                    if (upgradeTimerPanel != null)
                    {
                        upgradeTimerPanel.text = GameTimer.GetGameTimeString(timeLeft);
                    }
                }
            }
        }

        private void OnNewTrainingsAvailable()
        {
            _controller.RefreshTrainings(false, true, UpdateTrainingsList);
        }


        private void OnUpgradeNowClick()
        {
            PopupsManager.Instance.AddPopup(PopupType.BuyForPricePopup)
                .SetTitle("game.main.popup.refreshtrainingslist")
                .SetButtonLabels("game.common.yesbutton")
                .SetTextIconControls(GameData.GSConstData.TrainingsRefreshGoldPrice)
                .SetCallbacks(() =>
                {
                    if (_teamController.CanBuyForGold(GameData.GSConstData.TrainingsRefreshGoldPrice))
                    {

                        _controller.RefreshTrainings(true, true, UpdateTrainingsList);
                    }
                    else
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    }
                });


        }

        private void UpdateTrainingsList()
        {
            trainingsList.content.HideChildren();


            TrainingEffect totalEffect = _controller.GetTotalEffect();

            string attackLabel = totalEffect.attack == 0 ? LocaleManager.Instance.GetText("game.common.nobutton").ToLower() : totalEffect.attack+"%";
            string deffenceLabel = totalEffect.deffence == 0 ? LocaleManager.Instance.GetText("game.common.nobutton").ToLower() : totalEffect.deffence+"%";

            currentAttackBonus.SetText(LocaleManager.Instance.GetText("game.common.attack1",attackLabel));
            currentDeffenceBonus.SetText(LocaleManager.Instance.GetText("game.common.deffence1",deffenceLabel));

           // RectTransform child = null;
            TrainingModel currentUpgrade = _controller.GetCurrentUpgrade();
            int index = 0;
            if (currentUpgrade != null)
            {
                trainingRenderers[index].SetModel(currentUpgrade, index);
                //child = trainingsList.content.AddChild(trainingItemRendererPrefab, false);
                //child.GetComponent<TrainingItemRenderer>().SetModel();
                index++;
            }

            List<TrainingModel> list = _controller.AvailableTrainings;
            foreach (var trainingModel in list)
            {
                trainingRenderers[index].SetModel(trainingModel,index);
               // child = trainingsList.content.AddChild(trainingItemRendererPrefab, false);
               // child.GetComponent<TrainingItemRenderer>().SetModel(trainingModel,index);
                index++;
            }
            trainingsNotExistLabel.gameObject.SetActive(list.Count == 0 && currentUpgrade == null);
            upgradeTrainings.interactable = _controller.RefreshCount > 0;

            _controller.UpdateEffectTime();
        }
    }
}
