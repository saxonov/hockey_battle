﻿using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Battles;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class TournamentBattleController : IGameController, ICheckMessages, IGSResponceHandler
    {
        private IGameRoot _root;
        private TeamController _team;

        public const string CupStateChangedEvent = "TournamentBattleController.cupStateChangedEvent";
        public const string CupPlayersChangedEvent = "TournamentBattleController.cupPlayersChangedEvent";
        public const string CupStartedEvent = "TournamentBattleController.cupStartedEvent";
        public const string CupWinnerEvent = "TournamentBattleController.cupWinnerEvent";
        public const string CupLooseEvent = "TournamentBattleController.cupLooseEvent";
        public const string CupExpiredEvent = "TournamentBattleController.cupExpiredEvent";
        public const string CupRoundCompleteEvent = "TournamentBattleController.cupRoundCompleteEvent";
        //   public const string MatchCanceledEvent = "HockeyBattlesController.matchCanceledEvent";

        private bool _needUpdateCupsHistory = true;
        private List<TournamentBattleModel> _availableCups;
        private TournamentBattleModel _currentPlayCup;

        private TournamentCupSettingsModel _cupSettingsModel;

        private TournamentHistoryModel _cupsHistoryModel;

        public List<TournamentBattleModel> AvailableCups
        {
            get { return _availableCups; }
        }

        public TournamentBattleModel CurrentPlayCup
        {
            get { return _currentPlayCup; }
        }

        public TournamentHistoryModel CupsHistoryModel
        {
            get { return _cupsHistoryModel; }
        }

        public TournamentCupSettingsModel CupSettingsModel
        {
            get { return _cupSettingsModel; }
        }

        public bool IsCanPlayOrCreateCupNow()
        {
            return _team.CurrentActionState == GameActionState.None;
        }

        public void GetCupSettings(UnityAction callback)
        {
            if (_cupSettingsModel != null)
            {
                callback();
                return;
            }

            var request = new LogEventRequest();
            request.SetEventKey("GET_CUPS_SETTINGS");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });
        }

        public void GetTournamentsInfo(UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_CUPS_INFO");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (_currentPlayCup != null)
                    {
                        EventsManager.Instance.Call(CupStateChangedEvent);
                    }
                    callback();
                }
            });
        }

        public void GetCupsHistoryData(UnityAction callback)
        {
            if (!_needUpdateCupsHistory)
            {
                callback();
                return;
            }
            _needUpdateCupsHistory = false;
            var request = new LogEventRequest();
            request.SetEventKey("GET_CUPS_HISTORY");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });
        }

        public void CreateNewCup(TournamentBattleModel model,UnityAction callback)
        {
            if (model != null && IsCanPlayOrCreateCupNow())
            {
                _currentPlayCup = model;
                
               

                var request = new LogEventRequest();
                request.SetEventKey("CREATE_CUP");
                request.SetEventAttribute("NAME", model.GetName());
                request.SetEventAttribute("MIN_LVL", model.MinLevel);
                request.SetEventAttribute("MAX_LVL", model.MaxLevel);
                request.SetEventAttribute("MAX_PLAYERS", model.MaxPlayers);
                request.SetEventAttribute("CURRENCY", model.CurrencyType);
                request.SetEventAttribute("FEE_PRICE", model.FeePrice);
                request.SetDurable(true);

                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        if (_availableCups != null)
                        {
                            _availableCups.Clear();
                        }
                        EventsManager.Instance.Call(CupStateChangedEvent);
                        callback();
                    }
                    else
                    {
                        _team.RefreshBalance();
                    }
                });
            }
        }

        public void ParticipateCup(string cupId,UnityAction callback)
        {
            if (!IsCanPlayOrCreateCupNow())
            {
                Debug.LogWarning("Cannot participate in cup toiurnament!");
                return;
            }
          
            var request = new LogEventRequest();
            request.SetEventKey("PARTICIPATE_CUP");
            request.SetEventAttribute("CUP_ID", cupId);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (_availableCups != null) _availableCups.Clear();
                    EventsManager.Instance.Call(CupStateChangedEvent);
                    callback();
                }
                else
                {
                    GSData errorData = responce.Errors.GetGSData(GS_ScriptData.ErrorData);
                  
                    if (errorData != null)
                    {
                        int id = errorData.GetInt("id").Value;
                        if (id == GS_Errors.TOURNAMENT_NOT_EXIST)
                        {
                            //fix bug when error participation
                            EventsManager.Instance.Call(CupPlayersChangedEvent);
                        }
                        else
                        {
                            _team.RefreshBalance();
                        }
                    }
                  
                }
               
            });
        }

        public void CancelCupTournament(UnityAction callback)
        {
            if (_currentPlayCup != null)
            {
                var request = new LogEventRequest();
                request.SetEventKey("CANCEL_CUP");
                request.SetEventAttribute("CUP_ID", _currentPlayCup.Id);
                request.SetDurable(true);

                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        _currentPlayCup.Finish();
                        EventsManager.Instance.Call(CupStateChangedEvent);
                        callback();
                    }
                    else
                    {
                        _currentPlayCup.Finish();
                        GetTournamentsInfo(callback);
                    }
                });
            }
          
        }

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.TournamentsData))
            {
                _availableCups = _availableCups ?? new List<TournamentBattleModel>();
                _availableCups.Clear();

                TournamentBattleModel cup;
                var list = scriptData.GetGSDataList(GS_ScriptData.TournamentsData);
                if (list != null)
                {
                    foreach (var gsData in list)
                    {
                        cup = new TournamentBattleModel();
                        cup.Parse(gsData);
                        _availableCups.Add(cup);
                    }
                }
                else
                {
                    var cupData = scriptData.GetGSData(GS_ScriptData.TournamentsData);
                    if(cupData != null)
                    {
                        cup = new TournamentBattleModel();
                        cup.Parse(cupData);
                        _availableCups.Add(cup);
                    }
                }
               

            }else if (scriptData.ContainsKey(GS_ScriptData.TournamentInfoData))
            {
                _currentPlayCup = _currentPlayCup ?? new TournamentBattleModel();
                _currentPlayCup.Parse(scriptData.GetGSData(GS_ScriptData.TournamentInfoData));
            }

            if (scriptData.ContainsKey(GS_ScriptData.TournamentsHistoryData))
            {
                _cupsHistoryModel = _cupsHistoryModel ?? new TournamentHistoryModel();
                _cupsHistoryModel.Parse(scriptData.GetGSData(GS_ScriptData.TournamentsHistoryData));
            }

            if (scriptData.ContainsKey(GS_ScriptData.TournamentSettingsData))
            {
                _cupSettingsModel = _cupSettingsModel ?? new TournamentCupSettingsModel();
                _cupSettingsModel.Parse(scriptData.GetGSData(GS_ScriptData.TournamentSettingsData));
            }
        }

      

        public void Initialize(IGameRoot root)
        {
            _root = root;
            _team = _root.GetController<TeamController>();
        }

        public void Reset()
        {
            _needUpdateCupsHistory = true;
        }

      

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if(!GameData.IsTutorialPassed)return;
            GSData data = msgData;
            GSData battleResult = null;
            switch (msgCode)
            {
                case GS_Messages.TournamentStartMsg:
                    Debug.Log("Tournament started!");
                    // _team.IsPlayTournamentNow = true;
                    EventsManager.Instance.Call(CupStartedEvent);
                    break;
                case GS_Messages.TournamentCupCompleteMsg:
                    Debug.Log("Tournament complete!");
                    _needUpdateCupsHistory = true;
                    battleResult = data.GetGSData("battle");
                    _currentPlayCup = _currentPlayCup ?? new TournamentBattleModel();
                    _currentPlayCup.Parse(battleResult);
                    EventsManager.Instance.Call(_currentPlayCup.IsMyTeamWin() ? CupWinnerEvent : CupLooseEvent);
                    EventsManager.Instance.Call(CupStateChangedEvent);

                break;
                case GS_Messages.TournamentCupExpiredMsg:
                     Debug.Log("Tournament expired!");
                    battleResult = data.GetGSData("battle");
                    _currentPlayCup = _currentPlayCup ?? new TournamentBattleModel();
                    _currentPlayCup.Parse(battleResult);
                    EventsManager.Instance.Call(CupExpiredEvent);
                    EventsManager.Instance.Call(CupStateChangedEvent);
                break;
                case GS_Messages.TournamentPlayerChangedMsg:
                    EventsManager.Instance.Call(CupPlayersChangedEvent);
                break;
                case GS_Messages.TournamentRoundCompleteMsg:
                    Debug.Log("Tournament round complete!");
                    EventsManager.Instance.Call(CupRoundCompleteEvent);
                break;
            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.TournamentStartMsg ||
                   msgType == GS_Messages.TournamentCupCompleteMsg ||
                   msgType == GS_Messages.TournamentCupExpiredMsg ||
                   msgType == GS_Messages.TournamentPlayerChangedMsg ||
                   msgType == GS_Messages.TournamentRoundCompleteMsg;

        }

    }
}
