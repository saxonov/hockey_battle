﻿using HockeyManager.Game.UI.Components;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    [RequireComponent(typeof(Image))]
    public class ButtonBlocker : MonoBehaviour,IPointerClickHandler
    {
        public UnityEvent ClickEvent = new UnityEvent();

        [SerializeField] private Image buttonImage;

#pragma warning disable CS0649 // Полю "ButtonBlocker.blockerIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image blockerIcon;
#pragma warning restore CS0649 // Полю "ButtonBlocker.blockerIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        [SerializeField] private NavigateToPage navToPageComponent;

        private bool _isLocked = false;

        void OnDestroy()
        {
            ClickEvent.RemoveAllListeners();
            ClickEvent = null;
        }

        private void Awake()
        {
            if (navToPageComponent == null)
            {
                navToPageComponent = GetComponent<NavigateToPage>();
            }
            if (buttonImage == null)
            {
                buttonImage = GetComponent<Image>();
            }
       

            blockerIcon.enabled = false;
            buttonImage.color = Color.white;

        }

        public void LockButton()
        {
            if(_isLocked)return;
            _isLocked = true;
            navToPageComponent.enabled = false;
         //   buttonImage.raycastTarget = false;
            buttonImage.color = Color.gray;
            blockerIcon.enabled = true;
        }

        public void UnlockButton()
        {
            if(!_isLocked)return;
            _isLocked = false;
            navToPageComponent.enabled = true;
           // buttonImage.raycastTarget = true;
            buttonImage.color = Color.white;
            blockerIcon.enabled = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isLocked)
            {
              //  Debug.Log(gameObject.name + "_events registered:" + ClickEvent.GetPersistentEventCount());
                ClickEvent.Invoke();
            }
        }
    }
}
