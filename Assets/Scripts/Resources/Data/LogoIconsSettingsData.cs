﻿using UnityEngine;

public class LogoIconsSettingsData:ScriptableObject
{
#pragma warning disable CS0649 // Полю "LogoIconsSettingsData._logo64" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
    [SerializeField] private Sprite[] _logo64;
#pragma warning restore CS0649 // Полю "LogoIconsSettingsData._logo64" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "LogoIconsSettingsData._logos128" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
    [SerializeField] private Sprite[] _logos128;
#pragma warning restore CS0649 // Полю "LogoIconsSettingsData._logos128" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "LogoIconsSettingsData._logos256" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
    [SerializeField] private Sprite[] _logos256;
#pragma warning restore CS0649 // Полю "LogoIconsSettingsData._logos256" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


    public Sprite[] Logo64
    {
        get { return _logo64; }
    }

    public Sprite[] Logos128
    {
        get { return _logos128; }
    }

    public Sprite[] Logos256
    {
        get { return _logos256; }
    }
}
