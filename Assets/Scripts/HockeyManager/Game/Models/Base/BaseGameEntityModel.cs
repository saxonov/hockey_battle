﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Models.Base
{

  
    public abstract class BaseGameEntityModel : IBaseGameEntityModel
    {
        protected EntityType _entityType;

        private bool _upgradeTimerComplete = false;
       
        protected bool _isUpgrading;
        protected long _endUpgradeTimeSeconds = 0;
        protected int _upgradeTimeSeconds = 0;
        
        private UnityEvent _onModelChangedEvent;
        
        private bool _upgradeStarted = false;

        private TimerLeftSecondsEvent _OnTimerLeftSecondsEvent = null;

        //~BaseGameEntityModel()
        //{
        //    _onModelChangedEvent.RemoveAllListeners();
        //}

        public bool StartUpgradeTimer()
        {
           
            if (!_upgradeStarted && _endUpgradeTimeSeconds > 0)
            {
                Debug.Log(_entityType + "-Upgrade timer started!");
                _upgradeTimerComplete = false;
                _upgradeStarted = true;
                GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
                OnTikTakHandler();
                return true;
            }
          return false;
        }


        protected void RemoveUpgradeTimer()
        {
            if (_upgradeStarted)
            {
                Debug.Log(_entityType + "-Upgrade timer complete!");
                _upgradeStarted = false;
                GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
                _upgradeTimerComplete = true;

                _OnTimerLeftSecondsEvent.RemoveAllListeners();
                _OnTimerLeftSecondsEvent = null;
            }
        }

        private void OnTikTakHandler()
        {
            if (_OnTimerLeftSecondsEvent != null)
            {
                long timeLeftSeconds = GameTimer.TimeLeftSeconds(_endUpgradeTimeSeconds);

                if (timeLeftSeconds <= 0)
                {
                   

                    _OnTimerLeftSecondsEvent.Invoke(0);
                    RemoveUpgradeTimer();
                }
                else
                {
                    _OnTimerLeftSecondsEvent.Invoke(timeLeftSeconds);
                }
                

            }
        }

        public int UpgradeTimeSeconds
        {
            get { return _upgradeTimeSeconds; }
        }

        public EntityType EntityType
        {
            get { return _entityType; }
        }

        public UnityEvent OnModelChangedEvent
        {
            
            get
            {
                _onModelChangedEvent = _onModelChangedEvent ??  new UnityEvent();
                return _onModelChangedEvent;
            }
        }


        public long EndUpgradeTimeSeconds
        {
            get { return _endUpgradeTimeSeconds; }
        }

        public bool IsUpgrading
        {
            get { return _isUpgrading; }
        }

        public TimerLeftSecondsEvent OnTimerLeftSecondsEvent
        {
            get
            {
                _OnTimerLeftSecondsEvent = _OnTimerLeftSecondsEvent ?? new TimerLeftSecondsEvent();
                return _OnTimerLeftSecondsEvent;
            }
        }

        public bool UpgradeTimerComplete
        {
            get { return _upgradeTimerComplete; }
        }

        public virtual void Parse(GSData data)
        {
            if (data != null)
            {
                _isUpgrading = data.GetBoolean("isUpgrading").HasValue ? data.GetBoolean("isUpgrading").Value : _isUpgrading;

                _upgradeTimeSeconds = data.GetInt("upgradeTime").HasValue ? data.GetInt("upgradeTime").Value : _upgradeTimeSeconds;

                _endUpgradeTimeSeconds = data.GetInt("endUpgradeTime").HasValue ? data.GetInt("endUpgradeTime").Value : _endUpgradeTimeSeconds;

            }

            if (data == null || (!_isUpgrading && _upgradeStarted))
            {
                _upgradeTimerComplete = true;

                RemoveUpgradeTimer();
            }

            OnModelChangedEvent.Invoke();

        }
    }
}
