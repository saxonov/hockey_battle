﻿using HockeyManager.Game.Dict;
using SCTools.Localization;
using UnityEngine.Events;

namespace HockeyManager.HockeyManager.Game.Models.EPS
{
    public class SkillModel
    {

        private string _type;
        private int _points;
        private int _addedPoints = 0;
        private string _localeKeyName;
        
        public UnityEvent OnModelChanged = new UnityEvent();

        //private SkillType ParseSkillType(string type)
        //{
        //   type = type.Replace(type[0].ToString(),type[0].ToString().ToUpper());
        //    return (SkillType)Enum.Parse(typeof(SkillType), type);
        //}
        public SkillModel(string type,int points)
        {
            _localeKeyName = GetSkillName(type);
            _type = type;// ParseSkillType(type);
            Points = points;
        }


        public int Points
        {
            get { return _points; }
            set
            {
                _points = value;
                OnModelChanged.Invoke();
            }
        }

        public string Type
        {
            get { return _type; }
        }

        public string LocaleKeyName
        {
            get { return _localeKeyName; }
        }

        public int AddedPoints
        {
            set { _addedPoints = value; }
            get { return _addedPoints; }
        }

        private string GetSkillName(string type)
        {
            string localeKey = "";
            switch (type)
            {
                case GameData.FaceoffSKill:
                    localeKey = "game.main.skills.faceoff";
                    break;
                case GameData.DribblingSkill:
                    localeKey = "game.main.skills.dribbling";
                    break;
                case GameData.SpeedSkill:
                    localeKey = "game.main.skills.speed";
                    break;
                case GameData.ReactionSkill:
                    localeKey = "game.main.skills.reaction";
                    break;
                case GameData.ReliabilitySKill:
                    localeKey ="game.main.skills.reliability";
                    break;
                case GameData.StaminaSkill:
                    localeKey = "game.main.skills.stamina";
                    break;
                case GameData.StealSkill:
                    localeKey = "game.main.skills.steal";
                    break;
                case GameData.ThrowSKill:
                    localeKey = "game.main.skills.throw";
                    break;
            }
            return localeKey;
        }
    }
}
