﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{
    public class TournamentUserModel:IGSParse
    {
       // public UnityEvent OnModelChangeEvent;

        private int _totalGoals = 0;
        private int _wins = 0;
        private int _exp = 0;
        private int _loss = 0;
        private int _points = 0;

        private TeamInfoModel _teamInfo;

        private int _position;
        private int _level;


        public TournamentUserModel(GSData data)
        {
            _teamInfo = new TeamInfoModel(data);
           // OnModelChangeEvent = new UnityEvent();
            Parse(data);
        }

        public int TotalGoals
        {
            get { return _totalGoals; }
        }

        public int Wins
        {
            get { return _wins; }
        }

        public int Loss
        {
            get { return _loss; }
        }

        public int Points
        {
            get { return _points; }
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public TeamInfoModel TeamInfo
        {
            get { return _teamInfo; }
        }

        public int Exp
        {
            get { return _exp; }
        }

        public int Level
        {
            get { return _level; }
        }

        public void Parse(GSData data)
        {
            _points = data.GetInt("points").Value;
            _wins = data.GetInt("wins").Value;
            _loss = data.GetInt("lost").Value;

            if (data.ContainsKey("level"))
            {
                _level = data.GetInt("level").Value;
            }


            if (data.ContainsKey("exp"))
            {
                _exp = data.GetInt("exp").Value;
            }
            if (data.ContainsKey("totalGoals"))
            {
                _totalGoals = data.GetInt("totalGoals").Value;
            }
            else if (data.ContainsKey("goals"))
            {
                _totalGoals = data.GetInt("goals").Value;
            }
            if (data.ContainsKey("number"))
            {
                _position = data.GetInt("number").Value;

            }else if (data.ContainsKey("position"))
            {
                _position = data.GetInt("position").Value;
            }
            
            //OnModelChangeEvent.Invoke();
        }
    }
}
