﻿using UnityEngine;
using System.Collections;
using SCTools.EventsSystem;
using UnityEngine.Events;

namespace SCTools.Controllers 
{
    public class SwipeController : MonoBehaviour
    {

        public const string SWIPE_DIRECTION_EVENT = "swipeDirectionEvent";

        private Vector3 pressPosition;
        private Vector3 unPressPosition;

        public GameObject hitTarget;
        public float swipeDetectDistance = 0.5f;

        private bool isHitted = true;
        // Use this for initialization
        void Start()
        {

        }

        void FixedUpdate()
        {
            if (hitTarget != null)
            {
                isHitted = false;
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    isHitted = hit.collider.gameObject == hitTarget;
                }
            }

        }

        // Update is called once per frame
        void Update()
        {

#if (UNITY_EDITOR_WIN)
            if (Input.GetMouseButtonDown(0))
            {
                pressPosition = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                unPressPosition = Input.mousePosition;

                float detect = Vector3.Distance(pressPosition, unPressPosition);

                if (detect >= swipeDetectDistance && isHitted)
                {
                    Vector3 direction = (pressPosition - unPressPosition).normalized;

                    EventsManager.Instance.Call(SWIPE_DIRECTION_EVENT, direction);
                }

            }
#endif

        }
    }


}
