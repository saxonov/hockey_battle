﻿using HockeyManager.Game.UI.Map;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Map
{
    public class MapCollectableBuilding:MapBuilding
    {

#pragma warning disable CS0649 // Полю "MapCollectableBuilding.collectableIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image collectableIcon;
#pragma warning restore CS0649 // Полю "MapCollectableBuilding.collectableIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MapCollectableBuilding.collectPostions" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Vector2[] collectPostions;
#pragma warning restore CS0649 // Полю "MapCollectableBuilding.collectPostions" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        
        protected override void Awake()
        {
            base.Awake();
            collectableIcon.gameObject.SetActive(false);
        }

        protected override void OnModelChanged()
        {
            base.OnModelChanged();
            ShowCollectIcon();
        }

        public void HideCollectIcon()
        {
            collectableIcon.GetComponent<Animator>().Stop();
            collectableIcon.gameObject.SetActive(false);
        }

        private void ShowCollectIcon()
        {
            if (_model != null && !_model.IsUpgrading)
            {
                collectableIcon.rectTransform.anchoredPosition = collectPostions[_currentState];
                collectableIcon.gameObject.SetActive(_model.IsCanCollect);
            }
        }

    }
}
