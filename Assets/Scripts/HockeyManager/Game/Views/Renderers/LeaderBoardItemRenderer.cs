﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Leaderboard;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class LeaderBoardItemRenderer:MonoBehaviour,IPointerClickHandler
    {
        public const string OnLeaderItemSelectedEvent = "LeaderBoardItemRenderer.OnLeaderItemSelectedEvent";

        [SerializeField] private Image attackIndicator;
        [SerializeField] private Text rankLabel;
        [SerializeField] private Text teamNameLabel;
        [SerializeField] private Text levelLabel;
        [SerializeField] private LogoImage teamLogo;
        [SerializeField] private TextIconControl expPanel;
        [SerializeField] private Image selection;

        private static Color32 selectionColor = new Color32(29,36,18,0);

        private int _lastLogo = -1;
        private bool _matSetted = false;
        private LeaderTeamModel _model;
        private HSVShaderTuner tuner;
        void Start()
        {
             tuner = teamLogo.GetComponent<HSVShaderTuner>();
        }

        public void SetModel(LeaderTeamModel model)
        {
           
            _model = model;
           
           // Debug.Log("Leaderboard model: " + model.TeamInfo);
            rankLabel.text = _model.Position.ToString();
            levelLabel.text = LocaleManager.Instance.GetText("game.common.shortlevel2", _model.Level);
            
            expPanel.SetText(_model.TotalExp);

            selection.color = selectionColor;

            teamNameLabel.text = _model.TeamInfo.Name;

            if (_lastLogo == -1 || _lastLogo != _model.TeamInfo.Logo)
            {
                if (tuner == null)
                {
                    tuner = teamLogo.GetComponent<HSVShaderTuner>();
                }
                if (!_matSetted)
                {
                    teamLogo.material = new Material(GameSettings.Instance.HsvShader);
                    _matSetted = true;
                }
                teamLogo.sprite = GameSettings.Instance.GetLogo128(_model.TeamInfo.Logo);

                tuner.ChangeHUE(_model.TeamInfo.LogoColor);
                tuner.ChangeSaturation(_model.TeamInfo.LogoSaturation);
                _lastLogo = _model.TeamInfo.Logo;
            }
           

            if (_model.TeamInfo.IsMe())
            {
                selection.color = UISettings.LeaderboardSelectionItemColor;
            }
            else if(_model.Index % 2 == 0)
            {
                selection.color = UISettings.LeaderboardNextItemColor;
                
            }

            attackIndicator.enabled = model.IsCanChallenged && !_model.TeamInfo.IsMe();
           
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_model.TeamInfo.IsMe() && !eventData.dragging)
            {
                SoundManager.Instance.PlaySFX("click");
                EventsManager.Instance.Call(OnLeaderItemSelectedEvent, _model);
            }
        }
    }
}
