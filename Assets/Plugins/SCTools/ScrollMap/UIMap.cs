﻿using UnityEngine;

namespace SCTools.ScrollMap
{
    [RequireComponent(typeof(RectTransform))]
    public class UIMap:MonoBehaviour
    {
        private const float SmoothZoomSpeed = 10f;

        [SerializeField] private RectTransform scrollableContent;
        [SerializeField] private Canvas canvasHolder;

        
        [SerializeField]private float zoomSpeed = 5f;
        [SerializeField] private float maxZoomScale = 1f;

        [SerializeField]private float panSpeed = 2f;
        //[SerializeField]private float inertia = 0.02f;

        public bool enableZoom = false;
        public bool enablePan = false;



        private Vector3 startDragPos;
        private Vector2 lastScrollPos = Vector2.zero;
        private RectTransform rtransform;
        private float minZoomScale = 1f;

        private bool startZoom = false;
        private bool startDrag = false;
        private bool zoomPointFixed = false;
      
        private Vector2 zoomScale = Vector2.one;

       
        //private Vector3 velocity = Vector3.zero;

        private Rect fitRect;
        private Vector2 contentPivot;
        private Vector2 zoomPoint;

        private bool isPinchZoom = false;
       

      //  private Vector2 prevZoomScale = Vector2.one;
      

        void Awake()
        {
          
            contentPivot = scrollableContent.pivot;

            Debug.Log(fitRect);
        }

        void Start()
        {
            rtransform = GetComponent<RectTransform>();
            fitRect = rtransform.rect;
            CalcMinZoom();

        }

        public void GotoPosition(Vector2 pos,bool smooth = false)
        {
           // Vector2 diff = pos-scrollableContent.anchoredPosition;
            scrollableContent.anchoredPosition = pos;
        }

        private void CalcMinZoom()
        {
            RectTransform parent = rtransform;

            float w = parent.rect.width;
            float h = parent.rect.height;
            float scalex = Mathf.Abs(w / (scrollableContent.rect.width));
            float scaley = Mathf.Abs(h / (scrollableContent.rect.height));

            minZoomScale = Mathf.Max(scalex, scaley);
           // minZoomScale = Mathf.Min(minScale, minZoomScale);
        }

        void Update()
        {
            
            if (Input.touchSupported)
            {
                if (Input.touchCount == 2 )
                {
                    startDrag = false;

                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);


                    if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
                    {
                        isPinchZoom = true;
                        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                        // Find the magnitude of the vector (the distance) between the touches in each frame.
                        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                        // Find the difference in the distances between each frame.
                        float deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag) * 1f;

                        
                            Vector2 pos;
                            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(scrollableContent,
                                (touchZero.position + touchOne.position) * 0.5f, null, out pos))
                            {
                                return;
                            }
                            zoomPoint = pos;
                            zoomPointFixed = true;

                        


                        zoomScale.x -= deltaMagnitudeDiff; //Mathf.Min(scaleVec.x, minZoom);
                        zoomScale.y -= deltaMagnitudeDiff; //Mathf.Max(scaleVec.y, maxZoom);
                        zoomScale.x = Mathf.Clamp(zoomScale.x, minZoomScale, maxZoomScale);
                        zoomScale.y = Mathf.Clamp(zoomScale.y, minZoomScale, maxZoomScale);
                        startZoom = true;
                       
                    }
                }
                else
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began && !startDrag && !isPinchZoom)
                    {
                        startDragPos = Input.GetTouch(0).position;
                        startDrag = true;
                       // startZoom = false;

                    }
                    if (touch.phase == TouchPhase.Ended && startDrag && !isPinchZoom)
                    {
                        startDrag = false;
                     //   startZoom = false;
                        lastScrollPos = scrollableContent.anchoredPosition;
                    }

                }
            }
            else
            {
                if (Input.GetMouseButton(0) && !startDrag)
                {
                   // RectTransformUtility.ScreenPointToLocalPointInRectangle(rtransform, Input.mousePosition,null,out startDragPos);
                    startDragPos = Input.mousePosition;
                    startDrag = true;
                    startZoom = false;
                }
                if (Input.GetMouseButtonUp(0) && startDrag)
                {
                    startDrag = false;
                //    velocity = (Input.mousePosition-startDragPos).normalized * inertia;
                    lastScrollPos = scrollableContent.anchoredPosition;
                   //lastScrollPos = FitTobounds(lastScrollPos);
                }


                //}
                float zoom = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
                if (zoom != 0)
                {
                    if (!zoomPointFixed)
                    {
                        Vector2 pos;

                        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(scrollableContent,
                            Input.mousePosition, null, out pos))
                        {
                            return;
                        }

                        zoomPoint = pos;
                        zoomPointFixed = true;
                    }

            
                   
                  //  Debug.Log("zoomPoint " + zoomPoint + " " + scrollableContent.sizeDelta);
                    zoomScale.x += zoom; //Mathf.Min(scaleVec.x, minZoom);
                    zoomScale.y += zoom; //Mathf.Max(scaleVec.y, maxZoom);
                    zoomScale.x = Mathf.Clamp(zoomScale.x, minZoomScale, maxZoomScale);
                    zoomScale.y = Mathf.Clamp(zoomScale.y, minZoomScale, maxZoomScale);

                  
                    startZoom = true;
                   
                }
               
            }


            Zoom();
            Drag();
        }

        void Drag()
        {
            //if (isMoving)
            //{
            //    float vel = 0f;
            //    velocity.x = Mathf.SmoothDamp(velocity.x, 0f, ref vel, 0.5f);
            //    velocity.y = Mathf.SmoothDamp(velocity.y, 0f, ref vel, 0.5f);
            //    scrollableContent.Translate(velocity);
            //    scrollableContent.anchoredPosition = FitTobounds(scrollableContent.anchoredPosition,scrollableContent.localScale.x);
            //}

            if (startDrag)
            {
                Vector3 currentPos;
               
                if (Input.touchSupported)
                {
                    currentPos = Input.GetTouch(0).position;
                }
                else
                {
                    currentPos = Input.mousePosition;
                }
                Vector2 scrollPos = (currentPos - startDragPos) * panSpeed;
                //Vector2 currentPos = ;
                //RectTransformUtility.ScreenPointToLocalPointInRectangle(rtransform, Input.mousePosition, null, out currentPos);


                //velocity = scrollPos.normalized;

                scrollPos = FitTobounds(lastScrollPos+scrollPos,scrollableContent.localScale.x);
;               scrollableContent.anchoredPosition = scrollPos;
                
            }

           
           
        }

        private void Zoom()
        {
            if (startZoom)
            {

                Vector2 scale;
                if (zoomScale.x > minZoomScale && zoomScale.x < maxZoomScale)
                {
                    Vector2 scrollPos = -1f * zoomPoint * zoomScale.x;
                    scrollPos = FitTobounds(scrollPos, zoomScale.x);
                    scrollableContent.localScale = Vector2.LerpUnclamped(scrollableContent.localScale, zoomScale,Time.unscaledDeltaTime * SmoothZoomSpeed);
                    scrollableContent.anchoredPosition = Vector2.LerpUnclamped(scrollableContent.anchoredPosition, scrollPos, Time.unscaledDeltaTime * SmoothZoomSpeed);
                    scale = scrollableContent.localScale;
                }
                else
                {
                    startZoom = false;
                    scrollableContent.anchoredPosition = FitTobounds(scrollableContent.anchoredPosition, scrollableContent.localScale.x);
                    lastScrollPos = scrollableContent.anchoredPosition;
                    zoomPointFixed = false;
                    isPinchZoom = false;
                    return;
                }





                //float xDelta = ((scrollableContent.rect.width * zoomScale.x) * contentPivot.x) - fitRect.width * contentPivot.x;
                //float yDelta = ((scrollableContent.rect.height * zoomScale.y) * contentPivot.y) - fitRect.height *contentPivot.y;





                //   Vector2 diff = scrollPos - scrollableContent.anchoredPosition;
                ////   diff = FitTobounds(diff);
                //   diff.x = Mathf.Clamp(diff.x, -xDelta, xDelta);
                //   diff.y = Mathf.Clamp(diff.y, -yDelta, yDelta);

                //  scrollableContent.anchoredPosition =  scrollPos;

                //   lastScrollPos = scrollableContent.anchoredPosition;




                float check = (zoomScale - scale).magnitude;
                startZoom = check > 0.01f;
                // Debug.Log("check " + check);
                if (!startZoom)
                {
                    lastScrollPos = scrollableContent.anchoredPosition;
                    zoomPointFixed = false;
                    isPinchZoom = false;
                }
            }
        }

        private Vector2 FitTobounds(Vector2 pos,float scale = 1f)
        {
            float xsize = (scrollableContent.sizeDelta.x * scale)* contentPivot.x - fitRect.width*contentPivot.x;
            float ysize = (scrollableContent.sizeDelta.y * scale) * contentPivot.y - fitRect.height*contentPivot.y;
            //Debug.Log(scrollableContent.anchoredPosition);
            if (pos.x > 0 && pos.x >= xsize)
            {
                pos.x = xsize;
            }
            if (pos.x < 0 && pos.x <= -xsize)
            {
                pos.x = -xsize;
            }
            if (pos.y > 0 && pos.y >= ysize)
            {
                pos.y = ysize;
            }
            if (pos.y < 0 && pos.y <= -ysize)
            {
                pos.y = -ysize;
            }
            //Debug.Log(pos.x + "  " + xsize + " " + fitRect);
            return pos;
        }
    }
}
