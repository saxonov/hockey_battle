﻿using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.UI.Popups;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Popups
{
    public class MatchNotificationPopup:BasePopup
    {


        [SerializeField] private LogoImage logo;
        [SerializeField] private Text teamInfoLabel;

        private MatchPlayerModel model;

        protected override void UpdateData()
        {
            model = (MatchPlayerModel) _data;

            logo.sprite = GameSettings.Instance.GetLogo256(model.TeamInfo.Logo);

            var tuner = logo.GetComponent<HSVShaderTuner>();
            tuner.ChangeHUE(model.TeamInfo.LogoColor);
            tuner.ChangeSaturation(model.TeamInfo.LogoSaturation);

            teamInfoLabel.text = model.TeamInfo.Name.GetUITextSizeFormated(70) + "\n" +
                                 LocaleManager.Instance.GetText("game.common.shortlevel2", model.Level);
        }
    }
}
