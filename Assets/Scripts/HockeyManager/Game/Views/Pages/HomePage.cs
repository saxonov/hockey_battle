﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Renderers;
using HockeyManager.Game.Views.Renderers;
using HockeyManager.Models.Map;
using HockeyManager.UI.Popups;
using SCTools.Ads;
using SCTools.Helpers;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Pages
{
    public class HomePage : MonoBehaviour
    {
        private IGameSettings settings;

#pragma warning disable CS0649 // Полю "HomePage.mapLotInfoVBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private RectTransform mapLotInfoVBox;
#pragma warning restore CS0649 // Полю "HomePage.mapLotInfoVBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HomePage.mapBuildingInfoBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private RectTransform mapBuildingInfoBox;
#pragma warning restore CS0649 // Полю "HomePage.mapBuildingInfoBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HomePage.buildingInfoBoxName" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text buildingInfoBoxName;
#pragma warning restore CS0649 // Полю "HomePage.buildingInfoBoxName" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HomePage.lotInfoPrefabRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject lotInfoPrefabRenderer;
#pragma warning restore CS0649 // Полю "HomePage.lotInfoPrefabRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HomePage.buildingInfoPrefabRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject buildingInfoPrefabRenderer;
#pragma warning restore CS0649 // Полю "HomePage.buildingInfoPrefabRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        
        private UpgradeInfoRenderer _buildingInfoRenderer;
        private Stack<MapLotInfoRenderer> _mapLotInfoRenderers;

        private RewardsAchievementsController _rewards;
        private IPopup _adRewardPopup;

        void Awake()
        {
            settings = GameSettings.Instance;
            _rewards = GameRoot.Instance.GetController<RewardsAchievementsController>();
        }

        void Start()
        {
            if (MapManager.Instance.isHallExist())
            {
                HallItemsController hallItems = GameRoot.Instance.GetController<HallItemsController>();
                hallItems.LoadItems(null, true);
            }
        }

        public void OnShowRewardPopup()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _rewards.GetAdsReward(() =>
            {
                RewardModel reward = _rewards.GetReward(RewardType.AdsReward);

                _adRewardPopup = PopupsManager.Instance.AddPopup(PopupType.RewardAdsPopup, false)
                                    .SetData(reward)
                                    .SetCallbacks(OnShowAds, OnCancelAdsReward);

            });

        }

        private void OnShowAds()
        {
            if (AdverstimentManager.Instance.ShowRewardAd())
            {
                AdverstimentManager.Instance.OnAdsShowResultCallback += OnRewardAdShowResult;
            }
            else
            {
                
                PopupsManager.Instance.RemovePopup(_adRewardPopup);
                _adRewardPopup = null;
                PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                    .SetTitle("game.common.rewardadsnotavailable")
                    .HideButtons(true,false)
                    .SetButtonLabels(null,"game.common.closebutton");
            }
        }

        private void OnRewardAdShowResult(ShowResult res)
        {
           
            AdverstimentManager.Instance.OnAdsShowResultCallback -= OnRewardAdShowResult;
            if (res != ShowResult.Failed)
            {
                RewardModel model = _rewards.GetReward(RewardType.AdsReward);
                Debug.Log("Gain ads rewards " + model);
                _rewards.GainReward(model, (result) =>
                {
                    if (result)
                    {
                        VisualEffectsManager.Instance.OnFlyRewardCompleteEvent.AddListener(OnRewardFlyTweensComplete);
                        VisualEffectsManager.Instance.FlyRewards(_adRewardPopup.gameObject.transform.position,
                            model.RewardData);
                    }
                    else
                    {
                        _rewards.RemoveAdReward();
                        OnRewardFlyTweensComplete();
                    }
                });
            }
            else
            {
                _rewards.RemoveAdReward();
                PopupsManager.Instance.RemovePopup(_adRewardPopup);
                _adRewardPopup = null;
            }
        }

        private void OnRewardFlyTweensComplete()
        {
            if (_adRewardPopup != null)
            {
                PopupsManager.Instance.RemovePopup(_adRewardPopup);
                _adRewardPopup = null;
            }
        }

        private void OnCancelAdsReward()
        {
            _rewards.RemoveAdReward();
        }

        public void OnShow()
        {
            ActiveProcessesManager.Instance.Show();
            MapManager.Instance.Show();
            SoundManager.Instance.PlayMusicOnGameobject("maptheme", gameObject, 0.6f);
        }

        //hide map page
        public void OnHide()
        {
            ActiveProcessesManager.Instance.Hide();
            MapManager.Instance.Hide();
            SoundManager.Instance.StopMusic("maptheme");
        }

        #region Map

        private NavigationPageTypes _fromToMapBuildingPage = NavigationPageTypes.None;

        public void OnShowMapLotInfoPage()
        {
            _fromToMapBuildingPage = PageNavigatorManager.Instance.LastVisitedPage;
            BuildingUpgradeInfo[] upgrades = MapManager.Instance.GetConstructionUpgrades();
            if (upgrades != null)
            {
                for (int i = 0; i < upgrades.Length; i++)
                {
                    AddLotInfoRenderer(upgrades[i]);
                }
            }
        }

        public void OnShowBuildingInfoPage()
        {
            _fromToMapBuildingPage = PageNavigatorManager.Instance.LastVisitedPage;

            AddBuildingInfoRenderer(MapManager.Instance.GetSelectedBuilding());
        }

        public void OnHideMapLotUpgradeInfoPage()
        {
            ClearLotInfoRenderers();
            if (_buildingInfoRenderer != null)
            {
                Destroy(_buildingInfoRenderer.gameObject);
                _buildingInfoRenderer = null;
            }
        }



        public void OnCloseMapInfoLotPage()
        {
            if (_fromToMapBuildingPage == NavigationPageTypes.Buildings)
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Buildings);
            }
            else
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);
            }
        }

        private void AddBuildingInfoRenderer(IBuildingModel buildingModel)
        {
            if (buildingModel != null)
            {
                buildingInfoBoxName.GetComponent<UITextLocalizator>().enabled = false;
                buildingInfoBoxName.text = settings.GetBuildingData(buildingModel.Id).GetName();
                _buildingInfoRenderer = mapBuildingInfoBox.AddChild(buildingInfoPrefabRenderer, false).GetComponent<UpgradeInfoRenderer>();
                _buildingInfoRenderer.gameObject.SetActive(true);
                _buildingInfoRenderer.SetModel(buildingModel as BaseGameEntityModel);

            }
        }

        private void AddLotInfoRenderer(BuildingUpgradeInfo upgradeInfo)
        {
            _mapLotInfoRenderers = _mapLotInfoRenderers ?? new Stack<MapLotInfoRenderer>();
            RectTransform child = mapLotInfoVBox.AddChild(lotInfoPrefabRenderer, false);
            MapLotInfoRenderer lotInfoRenderer = child.GetComponent<MapLotInfoRenderer>();
            lotInfoRenderer.SetData(upgradeInfo);
            _mapLotInfoRenderers.Push(lotInfoRenderer);
            // renderer.SetData();
        }

        private void ClearLotInfoRenderers()
        {
            if (_mapLotInfoRenderers != null)
            {
                while (_mapLotInfoRenderers.Count > 0)
                {
                    Destroy(_mapLotInfoRenderers.Pop().gameObject);
                }
            }
        }
        #endregion

    }


}

