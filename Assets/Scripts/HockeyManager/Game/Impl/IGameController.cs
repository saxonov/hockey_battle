﻿namespace HockeyManager.Game.Impl
{
    public interface IGameController
    {
        void Initialize(IGameRoot root);

        void Reset();

        /// <summary>
        ///Must  Run exactly when Main scene loaded
        /// </summary>
        //void Start();
    }
}
