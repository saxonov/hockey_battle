﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.HallItems;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class HallIItemRenderer:MonoBehaviour,IPointerClickHandler
    {
        public const string HallItemBuyClick = "hallItemBuyClick";
        public const string HallItemSelect = "hallItemSelect";

#pragma warning disable CS0649 // Полю "HallIItemRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image icon;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.title" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text title;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.title" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.timelimitLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text timelimitLabel;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.timelimitLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.effect1Panel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl effect1Panel;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.effect1Panel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.effect2Panel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl effect2Panel;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.effect2Panel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.buyBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private RectTransform buyBox;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.buyBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.buyButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button buyButton;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.buyButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "HallIItemRenderer.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl pricePanel;
#pragma warning restore CS0649 // Полю "HallIItemRenderer.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
      //  [SerializeField] private Text title;


        private HallItemModel _model;

        void Awake()
        {
            effect1Panel.gameObject.SetActive(false);
            effect2Panel.gameObject.SetActive(false);
            buyBox.gameObject.SetActive(false);
        }

        void OnEnable()
        {
            EventsManager.Instance.Add<int>(GlobalEventTypes.ServerRequestSended, OnRequestSended);
            EventsManager.Instance.Add<int>(GlobalEventTypes.ServerResponceRecieved, OnResponceRecieved);
        }


        void OnDisable()
        {
            EventsManager.Instance.Remove<int>(GlobalEventTypes.ServerRequestSended, OnRequestSended);
            EventsManager.Instance.Remove<int>(GlobalEventTypes.ServerResponceRecieved, OnResponceRecieved);
        }

        private void OnResponceRecieved(int hash)
        {
            buyButton.interactable = true;
        }

        private void OnRequestSended(int hash)
        {
            buyButton.interactable = false;
        }


        void OnDestroy()
        {
            if (_model != null)
            {
                _model.OnModelChanged.RemoveListener(OnModelChanged);
                _model = null;
            }
            buyButton.onClick.RemoveListener(OnBuyClickHandler);
        }

   

        public void SetModel(HallItemModel model)
        {
            if (_model == null)
            {
                _model = model;

                if (_model.TimeLimit == 0)
                {
                    timelimitLabel.text = LocaleManager.Instance.GetText("game.main.hallpage.timeunlimited");
                }

                HallItemInfo hallItemSettingInfo = GameSettings.Instance.GetHallItemInfo(_model.Id);

                title.text = hallItemSettingInfo.GetName();
                icon.sprite = hallItemSettingInfo.GetPreview();

                SetEffectInfo();

                _model.OnModelChanged.AddListener(OnModelChanged);
                OnModelChanged();
            }
        }

        private void SetEffectInfo()
        {
            float attackBonus = _model.GetEffectValue(HallItemModel.AttackBoosterEffect);
            float deffenceBonus = _model.GetEffectValue(HallItemModel.DeffenceBoosterEffect);
            float energyBonus = _model.GetEffectValue(HallItemModel.EnergyBoosterEffect);
            float fansBonus = _model.GetEffectValue(HallItemModel.FansBoosterEffect);

            if (attackBonus != 0)
            {
               effect1Panel.gameObject.SetActive(true);
                effect1Panel.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Attack));
                effect1Panel.SetText("+" + attackBonus + "%");
            }
            if (deffenceBonus != 0)
            {
              
                effect2Panel.gameObject.SetActive(true);
                effect2Panel.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Deffence));
                effect2Panel.SetText("+" + deffenceBonus + "%");
            }
            if (energyBonus != 0)
            {
               
                effect1Panel.gameObject.SetActive(true);
                effect1Panel.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Energy));
                effect1Panel.SetText("+" + energyBonus + "%");
            }
            if (fansBonus != 0)
            {
               
                effect1Panel.gameObject.SetActive(true);
                effect1Panel.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Fans));
                effect1Panel.SetText("+" + fansBonus + "%");
            }
        }

        private void OnModelChanged()
        {
           // buyBox.gameObject.SetActive(false);
            if (_model.IsOwned)
            {
                buyBox.gameObject.SetActive(false);
            }
            else
            {
                if (_model.CurrencyType > 0)
                {
                    buyBox.gameObject.SetActive(true);
                    pricePanel.SetIcon(GameSettings.Instance.GetIcon(_model.CurrencyType));
                    pricePanel.SetText(_model.CurrencyType == GameData.CurrencyCoins ? _model.MoneyPrice : _model.GoldPrice);
                    buyButton.onClick.AddListener(OnBuyClickHandler);
                }
            }
        }

        private void OnBuyClickHandler()
        {
            EventsManager.Instance.Call(HallItemBuyClick, _model);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_model.IsOwned)
            {
                EventsManager.Instance.Call(HallItemSelect, _model);
            }
        }
    }
}
