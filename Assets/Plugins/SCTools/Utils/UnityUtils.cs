﻿

using UnityEngine;

using System.IO;
using System.Net;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace SCTools.Utils
{

    public static class UnityUtils
    {

        private static Ray ray;

        public static GameObject GetClickTarget()
        {
            RaycastHit hit;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray,out hit))
            {
                return hit.collider.gameObject;
            }
            return null;
        }

      
       

        public static bool GetMouseHit(out RaycastHit hit)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            return Physics.Raycast(ray, out hit);
        }

        public static GameObject GetClickTarget(Camera cam)
        {
            RaycastHit hit;
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                return hit.collider.gameObject;
            }
            return null;
        }


        public static Camera GetCamera(string cameraName = null)
        {
            if (cameraName == null)
            {
                return Camera.main;
            }
            
            return GameObject.Find(cameraName).GetComponent<Camera>();
        }


        
        /// <summary>
        /// Taken from C# 3.0 Cookbook
        /// </summary>
        /// <param name="resource">uri check resource,default google.com</param>
        /// <returns>html string</returns>
        public static string GetHtmlFromUri(string resource = "http://www.gamesparks.com/")
        {
            string html = string.Empty;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        html = reader.ReadToEnd();
                    }
                }
            }
            return html;
        }

        /// <summary>
        /// check internet connection
        /// </summary>
        /// <returns></returns>
        public static bool HasInternetConnection()
        {
            string html = GetHtmlFromUri();
            return !string.IsNullOrEmpty(html);
        }

        public static void SendEmail(string email,string subject,string body)
        {
            subject = WWW.EscapeURL(subject).Replace("+", "%20");
            body = WWW.EscapeURL(body).Replace("+", "%20");
            Application.OpenURL(string.Format("mailto:{0}?subject={1}&body={2}",email,subject,body) );
        }



#if UNITY_EDITOR

        [MenuItem("GameObject/Utils/RemoveChildren", false, 0)]
        static void RemoveChildren(MenuCommand command)
        {
            GameObject go = (GameObject)command.context;
            go.transform.RemoveChildren();
        }

        [MenuItem("SCTools/Clear PlayerPrefs")]
        public static void ClearAllPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
        [MenuItem("SCTools/CreateScriptableObject")]
        public static void CreateAsset()
        {
            CreateAsset<ScriptableObject>();
        }

        public static void CreateAsset<T>() where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "")
            {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
#endif
    }

}
