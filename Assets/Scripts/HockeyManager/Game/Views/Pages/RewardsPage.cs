﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using HockeyManager.TutorialSystem;
using SCTools.Ads;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class RewardsPage : MonoBehaviour,IPageChangeHandler
    {
        private IGameSettings settings;

        [Header("Rewards")]
#pragma warning disable CS0649 // Полю "RewardsPage.slotsBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private HorizontalLayoutGroup slotsBox;
#pragma warning restore CS0649 // Полю "RewardsPage.slotsBox" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.rewardsHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private RectTransform rewardsHolder;
#pragma warning restore CS0649 // Полю "RewardsPage.rewardsHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.rewardItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject rewardItemRendererPrefab;
#pragma warning restore CS0649 // Полю "RewardsPage.rewardItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.firstRrewardSlotRt" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private  RectTransform firstRrewardSlotRt;
#pragma warning restore CS0649 // Полю "RewardsPage.firstRrewardSlotRt" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.rewardsEmptyRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text rewardsEmptyRenderer;
#pragma warning restore CS0649 // Полю "RewardsPage.rewardsEmptyRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text rewardsCountLabel = null;
#pragma warning disable CS0649 // Полю "RewardsPage.takeAllRewardsButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Button takeAllRewardsButton ;
#pragma warning restore CS0649 // Полю "RewardsPage.takeAllRewardsButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private float rewardsShiftSpeed = 5f;

        [Space(10)] [Header("Achievements")]
#pragma warning disable CS0649 // Полю "RewardsPage.achievementsScroller" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private ScrollRect achievementsScroller;
#pragma warning restore CS0649 // Полю "RewardsPage.achievementsScroller" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.achievementItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject achievementItemRendererPrefab;
#pragma warning restore CS0649 // Полю "RewardsPage.achievementItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardsPage.achievementsEmptyRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text achievementsEmptyRenderer;
#pragma warning restore CS0649 // Полю "RewardsPage.achievementsEmptyRenderer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private bool _achievementsChanged = true;

        [Space(10)]
#pragma warning disable CS0649 // Полю "RewardsPage.navBar" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private InnerPageNavigation navBar;
#pragma warning restore CS0649 // Полю "RewardsPage.navBar" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private Vector2 startSlotPosition;
        private Vector2[] slots;
        //  private NavigationPageTypes _lastSelectedPage = NavigationPageTypes.Rewards;

        private List<AchievementItemRenderer> _achievementRenderers;

        private bool _takeAllrewardsStarted = false;
        private RewardsAchievementsController _controller;
       // private TeamController _teamController;

#pragma warning disable CS0414 // Полю "RewardsPage._emptyRewardItemRenderer" присвоено значение, но оно ни разу не использовано.
        private Text _emptyRewardItemRenderer = null;
#pragma warning restore CS0414 // Полю "RewardsPage._emptyRewardItemRenderer" присвоено значение, но оно ни разу не использовано.

        private Stack<RewardItemRenderer> renderersPool;
        private List<RewardItemRenderer> _currentRewardsBuffer; 

         
        private Queue<RewardItemRenderer> _gainRewardsQueue;
        private bool _updateShiftingFlag = false;
      
        

        private void Awake()
        {
            settings = GameSettings.Instance;
           // _teamController = GameRoot.Instance.GetController<TeamController>();
            _controller = GameRoot.Instance.GetController<RewardsAchievementsController>();
            _gainRewardsQueue = new Queue<RewardItemRenderer>();
            renderersPool = new Stack<RewardItemRenderer>();
            _achievementRenderers = new List<AchievementItemRenderer>();
            //init test 6 positions
            slots = new Vector2[3];

            Vector2 startPos = firstRrewardSlotRt.anchoredPosition;
            Vector2 size = firstRrewardSlotRt.sizeDelta;

            for (int i = 0; i < slots.Length; i++)
            {
                slots[i] = startPos;
                startPos.x += size.x + slotsBox.spacing;

            }

            startSlotPosition = startPos;
            _currentRewardsBuffer = new List<RewardItemRenderer>();

            LocaleManager.Instance.LanguageChanged += OnLanguageChangedHandler;

            PageNavigatorManager.Instance.AddPageChangeHandler(this);

            EventsManager.Instance.Add(RewardsAchievementsController.RewardAdded, OnRewardsAddedHandler);

        }

        void OnDestroy()
        {
            LocaleManager.Instance.LanguageChanged -= OnLanguageChangedHandler;
            EventsManager.Instance.Remove(RewardsAchievementsController.RewardAdded, OnRewardsAddedHandler);
        }

       

        void Start()
        {
            takeAllRewardsButton.interactable = false;

            achievementsEmptyRenderer.gameObject.SetActive(false);
            rewardsEmptyRenderer.gameObject.SetActive(false);

            
        }


        public void OnShow()
        {
            takeAllRewardsButton.onClick.AddListener(OnTakeAllRewardsHandler);
            EventsManager.Instance.Add(RewardsAchievementsController.RewardsCountChanged, OnRewardsCountChanged);
            EventsManager.Instance.Add<RewardItemRenderer>(RewardItemRenderer.OnGainRewardClicked, OnGainRewardHandler);


        }

        public void OnHide()
        {
            takeAllRewardsButton.onClick.RemoveListener(OnTakeAllRewardsHandler);
            EventsManager.Instance.Remove<RewardItemRenderer>(RewardItemRenderer.OnGainRewardClicked, OnGainRewardHandler);
            EventsManager.Instance.Remove(RewardsAchievementsController.RewardsCountChanged, OnRewardsCountChanged);
        }


        private void OnRewardsAddedHandler()
        {
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Rewards && !_takeAllrewardsStarted)
            {
                BuildRewardsList(false);
            }

        }

        private void OnLanguageChangedHandler()
        {
            foreach (var rewardItemRenderer in _currentRewardsBuffer)
            {
                rewardItemRenderer.SetDescription(GetRewardDescription(rewardItemRenderer.Model));
            }
        }


        private void OnGainRewardHandler(RewardItemRenderer itemRenderer)
        {
            navBar.LockNavButtons(true);
            PageNavigatorManager.Instance.LockControlls();

            if (itemRenderer.Model.Type == RewardType.AchievementReward)
            {
                _achievementsChanged = true;
            }

            if (_gainRewardsQueue.Count == 0)
            {
                _gainRewardsQueue.Enqueue(itemRenderer);
                _controller.GainReward(itemRenderer.Model,OnRewardGained);
            }
            else
            {
                _gainRewardsQueue.Enqueue(itemRenderer);
            }

            takeAllRewardsButton.interactable = _controller.RewardsCount > 0 && _gainRewardsQueue.Count == 0;
        }


        private void ShiftElements(bool animateShifting)
        {
            //navBar.LockNavButtons(true);
           // PageNavigatorManager.Instance.LockControlls();
            for (int i = 0; i < slots.Length; i++)
            {
               // _currentRewardsBuffer[i].index = i;
                if (i < _currentRewardsBuffer.Count)
                {
                    if (!animateShifting)
                    {
                        _currentRewardsBuffer[i].isShifting = false;
                        _currentRewardsBuffer[i].Rtransform.anchoredPosition = slots[i];
                    }
                    else
                    {
                        _currentRewardsBuffer[i].isShifting = true;
                    }
                    _currentRewardsBuffer[i].gameObject.SetActive(true);
                }
            }
            _updateShiftingFlag = animateShifting;
        }

        private void OnRewardGained(bool result)
        {
            if (result)
            {
                RewardItemRenderer item = _gainRewardsQueue.Peek();

                VisualEffectsManager.Instance.OnFlyRewardCompleteEvent.AddListener(OnRewardFlyTweensComplete);
                VisualEffectsManager.Instance.FlyRewards(item.Rtransform.position, item.Model.RewardData, false);

            }
            else
            {
                OnRewardFlyTweensComplete();
            }

        }

        private void ResetAndAddToPool(RewardItemRenderer item)
        {
            item.gameObject.SetActive(false);
            item.Rtransform.anchoredPosition = startSlotPosition;
            renderersPool.Push(item);
        }

        private void OnRewardFlyTweensComplete()
        {
            VisualEffectsManager.Instance.OnFlyRewardCompleteEvent.RemoveListener(OnRewardFlyTweensComplete);
            RewardItemRenderer item = _gainRewardsQueue.Dequeue();
            ResetAndAddToPool(item);
            _currentRewardsBuffer.Remove(item);

            if (_gainRewardsQueue.Count > 0)
            {
                item = _gainRewardsQueue.Peek();
                _controller.GainReward(item.Model, OnRewardGained);
            }
            else
            {
                navBar.LockNavButtons(false);
                PageNavigatorManager.Instance.UnLockControlls();

            }

            bool hasRewards = _controller.RewardsCount > 0;
            if (hasRewards)
            {
                BuildRewardsList(true);
            }
            else
            {
                rewardsEmptyRenderer.gameObject.SetActive(true);
            }
            
            if (!GameData.IsTutorialPassed)
            {
                TutorialManager.Instance.NextStep();
            }
            else
            {
                if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Missions)
                {
                    PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Missions);
                }
            }
        }

      
        private void BuildAchievements()
        {

            achievementsScroller.verticalNormalizedPosition = 1f;

            _achievementRenderers.ForEach(a =>
            {
                a.UpdateLanguageFields();
            });

            if (!_achievementsChanged)
            {
                return;
            }
            _achievementsChanged = false;

            if (_controller.Achievements.Count > 0)
            {
                if (_achievementRenderers.Count == 0)
                {
                    for (int i = 0; i < _controller.Achievements.Count; i++)
                    {
                        RectTransform itemR = achievementsScroller.content.AddChild(achievementItemRendererPrefab, false);
                        AchievementItemRenderer achievment = itemR.GetComponent<AchievementItemRenderer>();
                        achievment.SetModel(_controller.Achievements[i]);
                        _achievementRenderers.Add(achievment);
                    }
                }
                else if(_achievementRenderers.Count != _controller.Achievements.Count)
                {
                    _achievementRenderers.ForEach(f=>f.Refresh());
                    _achievementRenderers.RemoveAll(f => !f.gameObject.activeSelf);
                    achievementsEmptyRenderer.gameObject.SetActive(_achievementRenderers.Count == 0);
                }
            }
            else
            {
                achievementsEmptyRenderer.gameObject.SetActive(true);
            }
        }

      
        private void BuildRewardsList(bool animateShifting)
        {
                if(slots == null)return;
                rewardsEmptyRenderer.gameObject.SetActive(false);

                List<RewardModel> rewards = _controller.RewardsList();
                if (rewards.Count > 0)
                {
                    rewards.Sort();
                    for (int i = 0; i < rewards.Count; i++)
                    {
                        AddRendererToBuffer(rewards[i]);
                    }
                   
                }
                ShiftElements(animateShifting);
                rewardsEmptyRenderer.gameObject.SetActive(_currentRewardsBuffer.Count == 0);
                OnRewardsCountChanged();
                
            //  Debug.LogFormat("Rewards pool size {0}",renderersPool.Count);
        }

        private void AddRendererToBuffer(RewardModel model)
        {
              //  Debug.Log("Reward added to list " + model);
                model.isInList = true;
                RewardItemRenderer item = GetRendererFromPool();
                item.SetModel(model);
                item.SetDescription(GetRewardDescription(model));
                item.Rtransform.anchoredPosition = startSlotPosition;
                _currentRewardsBuffer.Add(item);
        }

        private string GetRewardDescription(RewardModel model)
        { 
            LocaleManager locales = LocaleManager.Instance;
            string res = "";
            int level = 0;
            switch (model.Type)
            {
                case RewardType.BuildigUpgradedReward:

                    MapBuildingsType btype = (MapBuildingsType)model.CustomData.GetInt("id").Value;
                    string buildingName = settings.GetBuildingData(btype).GetName();
                    level = model.CustomData.GetInt("level").Value;
                    res = locales.GetText("game.main.construction") + ". " + buildingName + ". " + locales.GetText("game.common.shortlevel2", level);
                    break;
                case RewardType.TeamLevelUpReward:
                    level = model.CustomData.GetInt("upgradedLevel").Value;
                    res = locales.GetText("game.main.rewards.teamlevelupreward", level);
                    break;
                case RewardType.EquipmnentUpgradedReward:
                    EquipmentType eqpType = (EquipmentType)model.CustomData.GetInt("id").Value;
                    level = model.CustomData.GetInt("level").Value;
                    EquipmentsSettingsInfo eqpInfo = settings.GetEquipmentData(eqpType);
                    res = locales.GetText("game.main.upgrade") + ". " + eqpInfo.GetName() + ". " + locales.GetText("game.common.shortlevel2", level);
                    break;
                case RewardType.PersonalUpgradedReward:
                    EmployeeType emplType = (EmployeeType)model.CustomData.GetInt("id").Value;
                    level = model.CustomData.GetInt("level").Value;
                    EmployeeSettingsInfo emplInfo = settings.GetEmployeeData(emplType);
                    res = locales.GetText("game.main.teaching") + ". " + emplInfo.GetName() + ". " + locales.GetText("game.common.shortlevel2", level);
                    break;
                case RewardType.AchievementReward:
                    string achievementId = model.CustomData.GetString("id");
                    res = locales.GetText(achievementId + "_achievement");
                    break;
                case RewardType.TrainingUpgradedReward:
                    res = locales.GetText("game.main.rewards.trainingupdatereward");
                    break;
                case RewardType.MatchWinnerReward:
                    res = locales.GetText("game.main.rewards.matchwinnerreward", model.CustomData.GetString("enemyTeam"));
                    break;
                case RewardType.MatchLooseReward:
                    res = locales.GetText("game.main.rewards.matchloosereward", model.CustomData.GetString("enemyTeam"));
                    break;
                case RewardType.TournamentWinnerReward:
                    res = locales.GetText("game.main.tournament.reward.winner", locales.GetText(model.CustomData.GetString("cupName")));
                    break;
                case RewardType.TournamentLostReward:
                    res = locales.GetText("game.main.tournament.reward.loose", locales.GetText(model.CustomData.GetString("cupName")));
                    break;
                case RewardType.CompetitionWinnerReward:

                    string competitionName = "";
                    int position = model.CustomData.GetInt("position").Value;
                    string posStr = locales.GetText("game.common.positionplace.1arg", position).AddPrefix('\n');
                    settings.GetIconTextCompetitionType(model.CustomData.GetString("type"), out competitionName);
                    res = competitionName.AddPostfix(' ') + posStr;
                    break;
                case RewardType.MatchCancelPenaltyReward:
                    res = locales.GetText("game.main.matchpage.reward.cancelpenaltyreward.1arg",model.CustomData.GetString("teamName"));
                    break;
            }
            return res;
        }

        private RewardItemRenderer GetRendererFromPool()
        {
            RewardItemRenderer item;
            if (renderersPool.Count > 0)
            {
                item = renderersPool.Pop();
                return item;
            }
            item = AddRenderer();
            return item;
        }

        private RewardItemRenderer AddRenderer()
        {
            RectTransform child = Instantiate(rewardItemRendererPrefab, rewardsHolder, false).GetComponent<RectTransform>();
            child.sizeDelta = firstRrewardSlotRt.sizeDelta;
            child.gameObject.SetActive(false);
            return  child.GetComponent<RewardItemRenderer>();
        }

       

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            switch (selected)
            {
                case NavigationPageTypes.Rewards:
                    navBar.SelectButton(1,true);
                    _controller.LoadRewards(OnRewardsLoaded, false);
                    break;
                case NavigationPageTypes.Missions:
                    _controller.LoadAchievements(BuildAchievements);
                    break;

            }
        }

        private void OnRewardsLoaded()
        {
           BuildRewardsList(false);
        }


        void Update()
        {
            if (_updateShiftingFlag)
            {
                bool hasItemToShift = false;
                for (int i = 0; i < _currentRewardsBuffer.Count; i++)
                {
                    if (i < slots.Length && _currentRewardsBuffer[i].isShifting)
                    {
                        Vector2 slotPos = slots[i];
                        Vector2 currentPos = _currentRewardsBuffer[i].Rtransform.anchoredPosition;
                        _currentRewardsBuffer[i].Rtransform.anchoredPosition = Vector2.Lerp(currentPos,slotPos,Time.deltaTime*rewardsShiftSpeed);

                        Vector2 diff = currentPos - slotPos;

                        if (Mathf.Abs(diff.x) <= 0.0001f)
                        {
                            _currentRewardsBuffer[i].isShifting = false;
                        }
                        else
                        {
                            hasItemToShift = true;
                        }
                      
                    }
                }

                if (!hasItemToShift)
                {
                    _updateShiftingFlag = false;
                    OnShiftingComplete();
                }
            }
        }

        private void OnShiftingComplete()
        {

        }

        private void OnTakeAllRewardsHandler()
        {
            _takeAllrewardsStarted = true;
            takeAllRewardsButton.interactable = false;
            if (AdverstimentManager.Instance.ShowRewardAd())
            {
                AdverstimentManager.Instance.OnAdsShowResultCallback += OnRewardAdShowResult;
            }
            else
            {
               // #if UNITY_EDITOR
               // DOVirtual.DelayedCall(1f, () =>
                  //  {
                        OnRewardAdShowResult(ShowResult.Finished);
                   // });
               // #endif
            }
        }

        private void OnRewardAdShowResult(ShowResult result)
        {
            AdverstimentManager.Instance.OnAdsShowResultCallback -= OnRewardAdShowResult;
            if (result != ShowResult.Failed)
            {
                navBar.LockNavButtons(true);
                PageNavigatorManager.Instance.LockControlls();
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                _controller.GainAllRewards(total =>
                {
                    for (int i = 0; i < _currentRewardsBuffer.Count; i++)
                    {
                        ResetAndAddToPool(_currentRewardsBuffer[i]);
                    }
                    _currentRewardsBuffer.Clear();
                  
                    VisualEffectsManager.Instance.OnFlyRewardCompleteEvent.AddListener(OnTotalRewardsFlyCompleteHandler);
                    VisualEffectsManager.Instance.FlyRewards(takeAllRewardsButton.transform.position, total);
                });
            }
            else
            {
                _takeAllrewardsStarted = false;
                OnRewardsCountChanged();
            }
        }

        private void OnTotalRewardsFlyCompleteHandler()
        {
            navBar.LockNavButtons(false);
            PageNavigatorManager.Instance.UnLockControlls();
            VisualEffectsManager.Instance.OnFlyRewardCompleteEvent.RemoveListener(OnTotalRewardsFlyCompleteHandler);
            BuildRewardsList(false);
            _takeAllrewardsStarted = false;
        }

        private void OnRewardsCountChanged()
        {
            takeAllRewardsButton.interactable = _controller.RewardsCount > 0 && _gainRewardsQueue.Count == 0;
            rewardsCountLabel.text = LocaleManager.Instance.GetText("game.common.rewardsavailable", _controller.RewardsCount);
        }
    }
}
