﻿using DG.Tweening;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Views.Components;
using SCTools.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Map
{
    /// <summary>
    /// base view for all buildings types
    /// </summary>
    public class MapBuilding : MonoBehaviour
    {
        protected TextFitter caption;
        protected Slider progressBar;

        protected UITextLocalizator progressLabel;

        protected IBuildingModel _model;

        protected Vector3 hudScale = Vector3.one;

        protected int _currentState = -1;

        protected Image[] states;
        protected bool _isInProgress = false;

        private void OnMapZoomEventHandler(Vector3 scale)
        {
            if (scale.x < 0.7f)
            {
                hudScale = Vector3.one * 2f;

            }
            else
            {
                hudScale = Vector3.one;
            }

            if (caption != null)
            {
                caption.transform.localScale = hudScale;
            }
            if (progressBar != null)
            {
                progressBar.transform.localScale = hudScale;
            }
        }

        private void SetStates()
        {
            if (states == null)
            {
                states = new Image[UISettings.MaxBuildingEquipmentViewStates];

                for (int i = 0; i < states.Length; i++)
                {
                    Transform child = transform.FindChild("State" + (i + 1));
                    if (child)
                    {
                        states[i] = child.GetComponent<Image>();
                        states[i].gameObject.SetActive(true);
                        states[i].enabled = false;
                    }
                }
            }
        }

        protected virtual void Awake()
        {
            SetStates();

            //caption.gameObject.SetActive(false);
        }

        protected virtual void OnDestroy()
        {
            if (_model != null)
            {
                _model.OnModelChangedEvent.RemoveListener(OnModelChanged);
                _model.OnTimerLeftSecondsEvent.RemoveListener(OnTimerUpdateHandler);
            }
        }

        public void SetModel(IBuildingModel model)
        {
            if (_model == null)
            {
                _model = model;
                _model.OnModelChangedEvent.AddListener(OnModelChanged);
                OnModelChanged();
            }
        }

        protected virtual void OnModelChanged()
        {
            ChangeState();
            if (_model.IsUpgrading)
            {
                SetUpgradeProcess();
            }
            else
            {
                ResetUpgradeProcess();
            }
           
           
        }

        private void OnTimerUpdateHandler(long leftSeconds)
        {
            if (progressBar != null)
            {
                progressBar.DOValue(_model.UpgradeTimeSeconds - leftSeconds, 2f);
            }
        }

        private void SetUpgradeProcess()
        {
            if (!_isInProgress)
            {
                if (progressBar == null)
                {
                    GameObject progressPrefab = GameSettings.Instance.BuildingProgressPrefab;
                    progressBar = states[_currentState].transform.AddChild(progressPrefab).GetComponent<Slider>();
                    progressBar.GetComponent<RectTransform>().anchoredPosition = Vector3.up *2f;

                    progressLabel = progressBar.GetComponentInChildren<UITextLocalizator>();
                }

              

                long timeLeft = GameTimer.TimeLeftSeconds(_model.EndUpgradeTimeSeconds);
                _isInProgress = true;
                states[_currentState].color = UISettings.BuldingUpgradeColor;
                progressBar.maxValue = _model.UpgradeTimeSeconds;
                progressBar.minValue = 0;
                progressBar.DOValue(_model.UpgradeTimeSeconds- timeLeft, 1f,true);
                progressBar.gameObject.SetActive(true);

                _model.OnTimerLeftSecondsEvent.AddListener(OnTimerUpdateHandler);
                //upgradeLabel.transform.parent.gameObject.SetActive(true);
                progressLabel.SetLocaleKeys(true,"game.main.building.buildprocess");
                
              
               // progressLabel.transform.localScale = hudScale;
             }
            //SetCaption();
        }

        private void ResetUpgradeProcess()
        {
            _isInProgress = false;
            states[_currentState].color = Color.white;
            if (progressBar != null)
            {
                Destroy(progressBar.gameObject);
                progressBar = null;
            }
           // SetCaption();
        }

        private void ChangeState()
        {
            SetStates();
            _currentState = UISettings.GetBuildingEquipmentState(_model.Level,_model.MaxLevel);
            for (int i = 0; i < states.Length; i++)
            {
                states[i].enabled = false;
            }

            states[_currentState].enabled = true;
           // SetCaption();
        }

        //private void SetCaption()
        //{
        //    //captionLabel.text = title.title + " " + 
        //    if (caption == null)
        //    {
        //        caption = states[_currentState].transform.AddChild(captionPrefab).GetComponent<TextFitter>();
        //        caption.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        //    }
        //    else if (!caption.transform.IsChildOf(states[_currentState].transform))
        //    {
        //        states[_currentState].transform.AddChild(caption.transform);
        //        caption.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

        //    }
        //    caption.transform.localScale = hudScale;
        //    caption.gameObject.SetActive(true);
        //    caption.SetText(_model.Name + LocaleManager.SPACE + LocaleManager.Instance.GetText("game.common.shortlevel2", _model.Level));
        //}
      
    }
}
