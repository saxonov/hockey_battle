﻿using SCTools.EventsSystem;
using SCTools.UI;
using UnityEngine;

namespace SCTools
{

    public class PinchScale2D: MonoBehaviour
    {

        public const string OnZoomingEvent = "PinchScale2D.OnZoomingEvent";

        private Vector3 scaleVec = Vector2.one;

        public float zoomSpeed = 0.01f;
 
        public float minZoom = 1.5f;
        public float maxZoom = 15f;

        public bool enableZoom = true;

        public CanvasGroup[] canvGroups;
        public float pixelPerUnit = 100f;
        public CustomScrollRect scrollRect;
        public RectTransform zoomTarget;
        public Canvas canvas;

        [HideInInspector]
        public bool isZooming = false;

        private bool startZoom = false;

      //  private float delayTime = -0f;

        private Vector2? zoomPoint = null;

        private Rect srect;
        private RectTransform srecttransform;
        private float checkPinchZoomThreshould = 0.01f;

        //   private PositionMarker[,] markers;

        public void SetStartZoom(float startScale)
        {
           // scrollRect.enabled = false;
           // scrollRect.disableDrag = true;
          //  zoomPoint = Vector2.zero;
            scaleVec = Vector3.one*startScale;
           // scaleVec.x = Mathf.Clamp(scaleVec.x, minZoom, maxZoom);
            //scaleVec.y = Mathf.Clamp(scaleVec.y, minZoom, maxZoom);

            zoomTarget.localScale = scaleVec;
            zoomTarget.anchoredPosition = FitTobounds(zoomTarget.anchoredPosition, scaleVec.x);
            //delayTime = 0.5f;
            // startZoom = true;
        }


        void Start()
        {


            if (canvas == null)
            {
                canvas = GetComponent<Canvas>();
            }
            srecttransform = scrollRect.GetComponent<RectTransform>();
            srect = srecttransform.rect;

            zoomTarget = zoomTarget ?? scrollRect.content;
            CalcMinZoom();

        }

        private void CalcMinZoom()
        {
            RectTransform parent = srecttransform;

            float w = parent.rect.width;
            float h = parent.rect.height;
            float scalex = Mathf.Abs(w / (zoomTarget.rect.width));
            float scaley = Mathf.Abs(h / (zoomTarget.rect.height));

            float scale = Mathf.Max(scalex, scaley);
            minZoom = Mathf.Max(minZoom, scale);
            // minZoomScale = Mathf.Min(minScale, minZoomScale);
        }

        void Update()
        {
           if(!enableZoom)return;
            if (zoomTarget != null)
            {
                if (Input.touchSupported)
                {

                    if (Input.touchCount > 1)
                    {
                        scrollRect.disableDrag = true;
                        scrollRect.enabled = false;
                        if (canvGroups != null)
                        {
                            for (int i = 0; i < canvGroups.Length; i++)
                            {
                                canvGroups[i].interactable = false;
                                canvGroups[i].blocksRaycasts = false;
                            }
                           
                        }
                       
                        Touch touchZero = Input.GetTouch(0);
                        Touch touchOne = Input.GetTouch(1);
                        zoomPoint = touchOne.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began ? null : zoomPoint;
                        if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
                        {

                            isZooming = true;

                            // Find the position in the previous frame of each touch.
                            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                            // Find the magnitude of the vector (the distance) between the touches in each frame.
                            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                            // Find the difference in the distances between each frame.
                            float deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag)*zoomSpeed;
                            // Debug.Log("DeltaMagnitudeDiff: " + deltaMagnitudeDiff);
                            if (deltaMagnitudeDiff >= checkPinchZoomThreshould ||
                                deltaMagnitudeDiff <= -checkPinchZoomThreshould)
                            {
                                if (zoomPoint == null)
                                {
                                    Vector2 pos;
                                    if (RectTransformUtility.ScreenPointToLocalPointInRectangle(zoomTarget,
                                        (touchZero.position + touchOne.position) * 0.5f, Camera.main, out pos))
                                    {
                                        zoomPoint = pos;
                                    }

                                }

                                scaleVec.x -= deltaMagnitudeDiff; //Mathf.Min(scaleVec.x, minZoom);
                                scaleVec.y -= deltaMagnitudeDiff; //Mathf.Max(scaleVec.y, maxZoom);
                                scaleVec.x = Mathf.Clamp(scaleVec.x, minZoom, maxZoom);
                                scaleVec.y = Mathf.Clamp(scaleVec.y, minZoom, maxZoom);

                              //  delayTime = 0.4f;
                                startZoom = true;
                            }
                            else
                            {
                                isZooming = false;
                                startZoom = false;
                            }

                        }
                        else
                        {
                            isZooming = false;
                            startZoom = false;
                        }

                    }
                    else
                    {
                        scrollRect.disableDrag = false;
                        scrollRect.enabled = true;
                        if (canvGroups != null)
                        {
                            for (int i = 0; i < canvGroups.Length; i++)
                            {
                                canvGroups[i].interactable = true;
                                canvGroups[i].blocksRaycasts = true;
                            }
                        }
                    }
                    
                }
                else
                {
                    float zoom = Input.GetAxis("Mouse ScrollWheel");
                    if (zoom != 0)
                    {
                        //   scrollRect.StopMovement();
                        if (canvGroups != null)
                        {
                            for (int i = 0; i < canvGroups.Length; i++)
                            {
                                canvGroups[i].interactable = false;
                                canvGroups[i].blocksRaycasts = false;
                            }
                        }
                        startZoom = false;
                        zoomPoint = null;
                        if (zoomPoint == null && zoomTarget != null)
                        {
                            Vector2 pos;

                            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(zoomTarget,
                                Input.mousePosition,Camera.main, out pos))
                            {
                                return;
                            }

                            zoomPoint = pos;
                           // Debug.Log("zoom point " + zoomPoint);
                        }



                        scaleVec.x += zoom; //Mathf.Min(scaleVec.x, minZoom);
                        scaleVec.y += zoom; //Mathf.Max(scaleVec.y, maxZoom);
                        scaleVec.x = Mathf.Clamp(scaleVec.x, minZoom, maxZoom);
                        scaleVec.y = Mathf.Clamp(scaleVec.y, minZoom, maxZoom);

                       // delayTime = 0.2f;
                        startZoom = true;
                    }
                    
                }
            }
            else
            {
                isZooming = false;
            }

            if (startZoom && zoomTarget != null)
            {

                if (zoomPoint != null)
                {

                    scrollRect.disableDrag = true;
                    scrollRect.enabled = false;

                    // Debug.Log(lastScrollPos + "::" + zoomTarget.anchoredPosition);
                    if (canvGroups != null)
                    {
                        for (int i = 0; i < canvGroups.Length; i++)
                        {
                            canvGroups[i].interactable = false;
                            canvGroups[i].blocksRaycasts = false;
                        }
                    }

                    zoomTarget.localScale = Vector3.LerpUnclamped(zoomTarget.localScale, scaleVec, Time.unscaledDeltaTime*10f);

                    Vector3 scale = zoomTarget.localScale;
                    float check = (scaleVec - scale).magnitude;

                    if (check > 0.001f)
                    {
                        //Debug.Log("zoom point" + zoomPoint);

                        //float xDelta = ((zoomTarget.rect.width*scaleVec.x)/2f) - srect.width/2f;
                        //float yDelta = ((zoomTarget.rect.height * scaleVec.y)/2f) - srect.height/2f;

                        Vector2 scrollPos = -1f*zoomPoint.Value*scaleVec.x;

                        //Vector2 diff = scrollPos - zoomTarget.anchoredPosition;

                        //diff.x = Mathf.Clamp(diff.x, -xDelta, xDelta);
                        //diff.y = Mathf.Clamp(diff.y, -yDelta, yDelta);

                        scrollPos = FitTobounds(scrollPos, scaleVec.x);

                        zoomTarget.anchoredPosition = Vector3.LerpUnclamped(zoomTarget.anchoredPosition, scrollPos,
                            Time.unscaledDeltaTime*10f);
                    }
                    else
                    {
                        zoomPoint = null;
                    }
                  


                    EventsManager.Instance.Call(OnZoomingEvent, scaleVec);

                    //    Time.deltaTime*10f);
                    //  diff.x = xDelta - diff.x;

                    //diff.x = Mathf.Clamp(diff.x, -xDelta, xDelta);
                    //diff.y = Mathf.Clamp(diff.y, -yDelta, yDelta);
                    //   Debug.Log(diff.x + " " + xDelta + " " + yDelta);


                    // Vector2 lastSrollPos = scrollRect.normalizedPosition;
                    // zoomTarget.anchoredPosition = diff;

                    //;
                    //scrollPosNormalized = diff.normalized;


                    //Vector3.LerpUnclamped(zoomTarget.localScale, scaleVec, Time.deltaTime * 5f);

                }
                else
                {
                    scrollRect.disableDrag = false;
                    scrollRect.enabled = true;
                    zoomPoint = null;
                   // delayTime = -0f;
                    if (canvGroups != null)
                    {
                        for (int i = 0; i < canvGroups.Length; i++)
                        {
                            canvGroups[i].interactable = true;
                            canvGroups[i].blocksRaycasts = true;
                        }
                    }
                    startZoom = false;

                }
              //  delayTime -= Time.deltaTime;
            }
           
        }

        private Vector2 FitTobounds(Vector2 pos, float scale = 1f)
        {
            float xsize = (zoomTarget.sizeDelta.x * scale) * zoomTarget.pivot.x - srect.width * zoomTarget.pivot.x;
            float ysize = (zoomTarget.sizeDelta.y * scale) * zoomTarget.pivot.y - srect.height * zoomTarget.pivot.y;
            //Debug.Log(scrollableContent.anchoredPosition);
            if (pos.x > 0 && pos.x >= xsize)
            {
                pos.x = xsize;
            }
            if (pos.x < 0 && pos.x <= -xsize)
            {
                pos.x = -xsize;
            }
            if (pos.y > 0 && pos.y >= ysize)
            {
                pos.y = ysize;
            }
            if (pos.y < 0 && pos.y <= -ysize)
            {
                pos.y = -ysize;
            }
           // Debug.Log(pos.x + "  " + xsize + " " + srect);
            return pos;
        }

    }
}
