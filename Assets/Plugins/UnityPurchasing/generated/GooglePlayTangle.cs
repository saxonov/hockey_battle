#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("+FV7UVLLm6Q7gMDQuHf9y3zNltTmBAxwaWUYZ3wqu9Vpadv0lyVk40gMgr9yBXNgL3Pl+t2AvAjhtgd6L9/q4L+BS2y3I3R2OLggJtI0mHM5hkkcIBxA6tY6MGSh85RRjYYB2FDT3dLiUNPY0FDT09JajHgUjL4b9gniVQCjReVjOJw+Lul5snIfOE08gITzV17pL1ZEzc3lp24GZ/VxleJQ0/Di39Tb+FSaVCXf09PT19LRyVupZ4IkUzoNiSqzt9J7Rn4BIDNnUhRLlJ0qhyErtMwck/o169a9Dc15RZNAtOxWr9OdUA3BlUtdnSIP0FYEVU7IgLGVzWEejRGfRHKpkK9847apoADvqw1Bz1etBNcKw0b6McA+CIoJtGwHudDR09LT");
        private static int[] order = new int[] { 8,12,13,10,6,12,11,11,8,12,10,12,12,13,14 };
        private static int key = 210;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
