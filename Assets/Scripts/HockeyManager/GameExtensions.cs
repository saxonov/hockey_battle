﻿using SCTools.Localization;
using UnityEngine;

public static class GameExtensions
{

    public static string GetLocaleText(this MonoBehaviour holder,string localeKey)
    {
        return LocaleManager.Instance.GetText(localeKey);
    }

    public static string GetLocaleText(this MonoBehaviour holder, string localeKey, params object[] args)
    {
        return LocaleManager.Instance.GetText(localeKey,args);
    }

    //public static IGSManager GetNetManager(this MonoBehaviour from)
    //{
    //    return GameRoot.Instance.NetManager;
    //}

    //public static T GetController<T>(this MonoBehaviour from) where T : IGameController
    //{
    //    return GameRoot.Instance.GetController<T>();
    //}

}

