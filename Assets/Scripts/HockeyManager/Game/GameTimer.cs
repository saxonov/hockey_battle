﻿using System.Globalization;
using HockeyManager.Game.Dict;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game
{

    public class TimerLeftSecondsEvent : UnityEvent<long>
    {

    }

    public static class GameTimer
    {
        public const string TikTakSecondEvent = "tikTakEventSecond";
        public const string TikTakMinuteEvent = "tikTakEventMinute";

        public static UnityEvent OnSecondsChanged = new UnityEvent();
        public static UnityEvent OnMinutesChanged = new UnityEvent();

       // private static DateTime _date;
        private static float _tick = 1f;
        private static int _minutesTick = 0;
        private static long _timeStamp = 0;

        public static long TimeStamp
        {
            set
            {
               
               _timeStamp = value;
               // _date = GS_ScriptData.UnixTimeStampToDateTime(_timeStamp);
            }
            get { return _timeStamp; }
        }

        public static string GetServerTimeString()
        {
            return GS_ScriptData.UnixTimeStampToDateTime(_timeStamp*1000).ToLocalTime().ToString(CultureInfo.InvariantCulture);
        }

        //public static DateTime Date
        //{
        //    get { return _date; }
        //}

        public static long TimeLeftSeconds(long endTime)
        {
            if (endTime > 0)
            {
                return endTime - _timeStamp;
            }
            return 0;
        }

        
        public static bool IsBiggerThanDay(int seconds)
        {
            return seconds >= 86400;
        }

        //public const string BuldingUpgradeColor = "#80808000";

        public static string GetGameTimeString(long seconds)
        {

            long hours = 0;
            long mins = 0;
            long secs = 0;


            string minStr = "";
            string secStr = "";
            string hrsStr = "";

            string space = LocaleManager.SPACE.ToString();

            // check whether the time is 0
            if (seconds < 60)
            {
                secStr = seconds + LocaleManager.Instance.GetText("game.common.sec");

            }
            else if (seconds < 3600)
            {
                mins = seconds / 60;
                secs = seconds % 60;

                minStr = mins + LocaleManager.Instance.GetText("game.common.min");
                secStr = secs == 0 ? "" : secs + LocaleManager.Instance.GetText("game.common.sec");
            }
            else if (seconds < 86400)
            {
                hours = seconds / 3600;
                mins = (seconds % 3600) / 60;
                secs = seconds % 60;
                hrsStr = hours + LocaleManager.Instance.GetText("game.common.hour");
                minStr = mins + LocaleManager.Instance.GetText("game.common.min");
                secStr = secs != 0 ? secs + LocaleManager.Instance.GetText("game.common.sec") : "";

            }
            else if (seconds < 604800)
            {
                //check days when is less 1 week
                long days = seconds / 86400;
                hours = (seconds / 3600) % 24;
                //mins = (seconds % 3600) / 60;
                //secs = seconds % 60;

                string daysLocale = LocaleManager.Numeral((int)days, "game.common.1day", "game.common.2days", "game.common.5days");

                return LocaleManager.Instance.GetText(daysLocale, days) + space + hours + LocaleManager.Instance.GetText("game.common.hour"); ;
                //hrsStr = hours + LocaleManager.Instance.GetText("game.common.hour");
                //minStr = mins + LocaleManager.Instance.GetText("game.common.min");
                //secStr = secs != 0 ? secs + LocaleManager.Instance.GetText("game.common.sec") : "";
            }
            else if (seconds < 2419200)
            {
                //check weeks when is less 1 mounth
                long weeks = seconds / 604800;
                return LocaleManager.Instance.GetText("game.common.1week", weeks);

            }

            if (hrsStr != "")
            {
                return hrsStr + space + minStr + space + secStr;

            }
            if (minStr != "")
            {
                return minStr + space + secStr;
            }

            return secStr;
        }

        public static void Update()
        {
            _tick -= Time.deltaTime;
            if (_tick <= 0)
            {
                _timeStamp += 1;
                _tick = 1f;
                if (EventsManager.Instance.HasEventType(TikTakSecondEvent))
                {
                    EventsManager.Instance.Call(TikTakSecondEvent);
                }

                if (OnSecondsChanged != null)
                {
                    OnSecondsChanged.Invoke();
                }

                _minutesTick++;
                if (_minutesTick % 60 == 0)
                {
                    if (EventsManager.Instance.HasEventType(TikTakMinuteEvent))
                    {
                        EventsManager.Instance.Call(TikTakMinuteEvent);
                    }
                    _minutesTick = 0;
                    if (OnMinutesChanged != null)
                    {
                        OnMinutesChanged.Invoke();
                    }
                }
            }
        }
    }
}
