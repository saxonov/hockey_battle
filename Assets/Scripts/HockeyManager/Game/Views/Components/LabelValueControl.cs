﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    [ExecuteInEditMode]
    public class LabelValueControl:MonoBehaviour
    {
        [SerializeField] private Text textLabel;
        [SerializeField] private Text valueLabel;

        public int fontSize = 0;

        void Start()
        {
            textLabel.resizeTextMaxSize = fontSize;
            valueLabel.resizeTextMaxSize = fontSize;
        }

        public void SetLabel(string text)
        {
            textLabel.text = text;
        }

      
        void OnValidate()
        {
            textLabel.resizeTextMaxSize = fontSize;
            valueLabel.resizeTextMaxSize = fontSize;

        }

        public void SetValue(int value)
        {
            valueLabel.text = value.ToString();
        }

        public void SetValue(float value)
        {
            valueLabel.text = value.ToString("f1");
        }

    }
}
