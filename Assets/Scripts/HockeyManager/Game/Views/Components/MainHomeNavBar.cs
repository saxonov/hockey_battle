﻿using System;
using HockeyManager.Gam.UI.Components;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Battles;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    public class MainHomeNavBar:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "MainHomeNavBar.matchHintIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image matchHintIndicator;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.matchHintIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.tournamentHintIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image tournamentHintIndicator;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.tournamentHintIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.tournamentTimeIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TimerIndicator tournamentTimeIndicator;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.tournamentTimeIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.matchTimeIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TimerIndicator matchTimeIndicator;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.matchTimeIndicator" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.rewardsCounter" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private CountIndicator rewardsCounter;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.rewardsCounter" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.trainingsCounter" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private CountIndicator trainingsCounter;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.trainingsCounter" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


#pragma warning disable CS0649 // Полю "MainHomeNavBar.trainingsBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private ButtonBlocker trainingsBlocker;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.trainingsBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.matchesBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private ButtonBlocker matchesBlocker;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.matchesBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MainHomeNavBar.tournamentsBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private ButtonBlocker tournamentsBlocker;
#pragma warning restore CS0649 // Полю "MainHomeNavBar.tournamentsBlocker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private Vector2 scale = Vector2.one;

        private TeamController _team;
        private TournamentBattleController _cupBattlesController;
        private MatchBattleController _matchBattleController;
        private TrainingsController _trainings;
        private RewardsAchievementsController _rewards;

        public void OnShow()
        {
            scale.x = 1f;
            gameObject.transform.localScale = scale;
        }

        public void OnHide()
        {
            scale.x = 0f;
            gameObject.transform.localScale = scale;
        } 

        void Awake()
        {
            _rewards = GameRoot.Instance.GetController<RewardsAchievementsController>();
            _cupBattlesController = GameRoot.Instance.GetController<TournamentBattleController>();
            _matchBattleController = GameRoot.Instance.GetController<MatchBattleController>();
            _trainings = GameRoot.Instance.GetController<TrainingsController>();
            _team = GameRoot.Instance.GetController<TeamController>();

            EventsManager.Instance.Add(TournamentBattleController.CupStateChangedEvent,OnCupStateChangedChanged);
            EventsManager.Instance.Add(MatchBattleController.MatchStateChanged,OnMatchBattleStateChanged);
            EventsManager.Instance.Add(GlobalEventTypes.GameUnpaused, OnGameUnpausedHandler);
            EventsManager.Instance.Add(TeamController.CurrentActionStateChanged,OnGameActionsStateChanged);
            EventsManager.Instance.Add(RewardsAchievementsController.RewardsCountChanged, OnRewardsChangedHanlder);
            EventsManager.Instance.Add(TrainingsController.OnTrainingsListRefreshComplete, OnTrainingsRefreshed);

            tournamentsBlocker.ClickEvent.AddListener(OnTournamentBlockerClicked);
            matchesBlocker.ClickEvent.AddListener(OnMatchBlockerClicked);
            trainingsBlocker.ClickEvent.AddListener(OnTrainingsBlockerClicked);

        }

        void OnDestroy()
        {

            EventsManager.Instance.Remove(TournamentBattleController.CupStateChangedEvent, OnCupStateChangedChanged);
            EventsManager.Instance.Remove(MatchBattleController.MatchStateChanged, OnMatchBattleStateChanged);
            EventsManager.Instance.Remove(GlobalEventTypes.GameUnpaused, OnGameUnpausedHandler);
            EventsManager.Instance.Remove(TrainingsController.OnTrainingsListRefreshComplete, OnTrainingsRefreshed);
            EventsManager.Instance.Remove(RewardsAchievementsController.RewardsCountChanged, OnRewardsChangedHanlder);
            EventsManager.Instance.Remove(TeamController.CurrentActionStateChanged, OnGameActionsStateChanged);

            matchesBlocker.ClickEvent.RemoveListener(OnMatchBlockerClicked);
            tournamentsBlocker.ClickEvent.RemoveListener(OnTournamentBlockerClicked);
            trainingsBlocker.ClickEvent.RemoveListener(OnTrainingsBlockerClicked);
        }

        void Start()
        {
            matchHintIndicator.enabled = false;
            tournamentHintIndicator.enabled = false;
            trainingsCounter.gameObject.SetActive(false);

            OnRewardsChangedHanlder();
            OnGameActionsStateChanged();
            OnTrainingsRefreshed();
        }

        private void OnGameUnpausedHandler()
        {
            //  PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            //OnTrainingsBatllesChangedHandler();
            OnTrainingsRefreshed();

          //  Debug.Log("MainNavBar: unpaused current page " + PageNavigatorManager.Instance.SelectedPage);
        }

        private string GetBlockerPopupTitle()
        {
            string text = "";
            switch (_team.CurrentActionState)
            {
                case GameActionState.Training:
                    text = LocaleManager.Instance.GetText("game.common.whiletrainings").ToUpper();
                    break;
                case GameActionState.PlayMatch:
                    text = LocaleManager.Instance.GetText("game.common.whilematches").ToUpper();
                    break;
                case GameActionState.PlayTournament:
                    text = LocaleManager.Instance.GetText("game.common.whiletournaments").ToUpper();
                    break;
            }

            return text;
        }

        private void OnTournamentBlockerClicked()
        {
            PopupsManager.Instance.AddPopup(PopupType.BlockerPopup).
                SetTitle(LocaleManager.Instance.GetText("game.common.tournaments.popup.blocks").ToUpper())
                .SetText(GetBlockerPopupTitle());
        }

        private void OnTrainingsBlockerClicked()
        {
            PopupsManager.Instance.AddPopup(PopupType.BlockerPopup)
                .SetTitle(LocaleManager.Instance.GetText("game.common.trainings.popup.blocks").ToUpper())
                .SetText(GetBlockerPopupTitle());
        }

        private void OnMatchBlockerClicked()
        {
            PopupsManager.Instance.AddPopup(PopupType.BlockerPopup)
                .SetTitle(LocaleManager.Instance.GetText("game.common.matches.popup.blocks").ToUpper())
                .SetText(GetBlockerPopupTitle());
        }


        private void OnMatchBattleStateChanged()
        {
           switch (_matchBattleController.CurrentMatchBattleState)
            {
                case MatchBattleStates.Selection:
                    matchTimeIndicator.StopTimer();
                    matchHintIndicator.enabled = false;
                    // Debug.Log("MainNavBar: Match timer indicator stopped");
                    break;
                case MatchBattleStates.Attack:
                case MatchBattleStates.Attacked:
                    tournamentTimeIndicator.StopTimer();
                    tournamentHintIndicator.enabled = false;
                    matchHintIndicator.enabled = true;
                    matchHintIndicator.color = Color.green;
                    matchTimeIndicator.StopTimer();
                    matchTimeIndicator.StartTimer(_matchBattleController.MatchBattleModel.EndAcceptTime, _matchBattleController.MatchBattleModel.AcceptTimeSeconds);
                    // Debug.Log("MainNavBar: Match timer indicator started! " + _matchBattleController.CurrentMatchBattleState);
                    break;
                case MatchBattleStates.Battle:
                    tournamentTimeIndicator.StopTimer();
                    tournamentHintIndicator.enabled = false;
                    matchHintIndicator.enabled = true;
                    matchHintIndicator.color = Color.red;
                    matchTimeIndicator.StopTimer();
                    matchTimeIndicator.StartTimer(_matchBattleController.MatchBattleModel.EndBattleTime, _matchBattleController.MatchBattleModel.MatchTimeSeconds);
                    //Debug.Log("MainNavBar: Match timer indicator started! " + _matchBattleController.CurrentMatchBattleState);
                    break;
            }
        }

        private void OnCupStateChangedChanged()
        {
            if (_cupBattlesController.CurrentPlayCup != null)
            {
                if (_cupBattlesController.CurrentPlayCup.State == TournamentBattleStates.Selection || 
                    _cupBattlesController.CurrentPlayCup.State == TournamentBattleStates.Idle)
                {
                    matchTimeIndicator.StopTimer();
                    matchHintIndicator.enabled = false;
                    tournamentHintIndicator.enabled = true;
                    tournamentTimeIndicator.StopTimer();
                    tournamentHintIndicator.color = Color.green;
                    tournamentTimeIndicator.StartTimer(_cupBattlesController.CurrentPlayCup.EndExpiryTime, _cupBattlesController.CurrentPlayCup.ExpiryTimeSeconds);
                    //Debug.Log("MainNavBar: Tournament timer started " + _cupBattlesController.CurrentPlayCup.State);
                }
                else if (_cupBattlesController.CurrentPlayCup.State == TournamentBattleStates.Started)
                {
                    matchTimeIndicator.StopTimer();
                    matchHintIndicator.enabled = false;
                    tournamentHintIndicator.enabled = true;
                    tournamentTimeIndicator.StopTimer();
                    tournamentHintIndicator.color = Color.red;
                    tournamentTimeIndicator.StartTimer(_cupBattlesController.CurrentPlayCup.EndPlayTime, _cupBattlesController.CurrentPlayCup.GetTotalPlayTimeSeconds());
                    //Debug.Log("MainNavBar: Tournament timer started " + _cupBattlesController.CurrentPlayCup.State);
                }
                else
                {
                    tournamentTimeIndicator.StopTimer();
                    tournamentHintIndicator.enabled = false;
                    // Debug.Log("MainNavBar: Tournament timer stopped " + _cupBattlesController.CurrentPlayCup.State);
                }
            }
            else
            {
                tournamentTimeIndicator.StopTimer();
                tournamentHintIndicator.enabled = false;
            }
        }

        private void ResetAllLockedButtons()
        {
            matchesBlocker.UnlockButton();
            trainingsBlocker.UnlockButton();
            tournamentsBlocker.UnlockButton();
            Debug.Log("MainNavBar: reset all locked buttons!");
        }

        private void OnTrainingsRefreshed()
        {
            trainingsCounter.gameObject.SetActive(_trainings.TrainingsCount > 0 && 
                                                  _team.CurrentActionState != GameActionState.PlayMatch && 
                                                  _team.CurrentActionState != GameActionState.PlayTournament);
            trainingsCounter.SetValue(_trainings.TrainingsCount);
        }


        private void OnRewardsChangedHanlder()
        {
            rewardsCounter.gameObject.SetActive(_rewards.RewardsCount > 0);
            rewardsCounter.SetValue(_rewards.RewardsCount);
        }

        private void OnGameActionsStateChanged()
        {
            switch (_team.CurrentActionState)
            {
                case GameActionState.Training:
                    matchesBlocker.LockButton();
                    tournamentsBlocker.LockButton();
                    break;
                case GameActionState.PlayMatch:
                    trainingsCounter.gameObject.SetActive(false);
                    trainingsBlocker.LockButton();
                    tournamentsBlocker.LockButton();
                    OnMatchBattleStateChanged();
                    break;
                case GameActionState.PlayTournament:
                    trainingsBlocker.LockButton();
                    matchesBlocker.LockButton();
                    trainingsCounter.gameObject.SetActive(false);
                    OnCupStateChangedChanged();
                    break;
                default:
                    ResetAllLockedButtons();
                    trainingsCounter.gameObject.SetActive(_trainings.TrainingsCount > 0);
                    break;
            }
        }
    }
}
