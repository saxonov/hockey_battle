﻿using HockeyManager.Game.UI.Components;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Equipments
{
    public class PulloverColorTuner:MonoBehaviour
    {
        private const string PulloverColorPerfs = "pulloverColorPrefs";

        
#pragma warning disable CS0649 // Полю "PulloverColorTuner.colorTunerHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject colorTunerHolder;
#pragma warning restore CS0649 // Полю "PulloverColorTuner.colorTunerHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PulloverColorTuner.hueProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Slider hueProgress;
#pragma warning restore CS0649 // Полю "PulloverColorTuner.hueProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PulloverColorTuner.tunner" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private HSVShaderTuner tunner;
#pragma warning restore CS0649 // Полю "PulloverColorTuner.tunner" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PulloverColorTuner.applyButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button applyButton;
#pragma warning restore CS0649 // Полю "PulloverColorTuner.applyButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0169 // Поле "PulloverColorTuner.openButton" никогда не используется.
        [SerializeField] private Button openButton;
#pragma warning restore CS0169 // Поле "PulloverColorTuner.openButton" никогда не используется.


        private bool changed = false;

        private bool _isOpened = true;

        void Start()
        {

            if (PlayerPrefs.HasKey(PulloverColorPerfs))
            {
                hueProgress.value = PlayerPrefs.GetFloat(PulloverColorPerfs);
                
            }
            OnCloseColorTunner();
            //  picture.GetModifiedMaterial()
        }

        void OnEnable()
        {
            tunner.ChangeHUE(hueProgress.value);
            hueProgress.onValueChanged.AddListener(ChangeHUEColor);
        }

        

        void OnDisable()
        {
            if (changed)
            {
                PlayerPrefs.Save();
                changed = false;
            }
            tunner.ChangeHUE(0);
            hueProgress.onValueChanged.RemoveListener(ChangeHUEColor);
           
        }

        public void OnCloseColorTunner()
        {
            if (_isOpened)
            {
                _isOpened = false;
                applyButton.gameObject.SetActive(false);
                hueProgress.gameObject.SetActive(false);
            }   
        }


        public void OnOpenColorTunner()
        {
            if (!_isOpened)
            {
                _isOpened = true;
                applyButton.gameObject.SetActive(true);
                hueProgress.gameObject.SetActive(true);
            }
        }
        
        private void ChangeHUEColor(float value)
        {
            changed = true;
           tunner.ChangeHUE(value);
            PlayerPrefs.SetFloat(PulloverColorPerfs,value);
        }

        public void ShowTunner()
        {
            if (colorTunerHolder != null)
            {
                colorTunerHolder.SetActive(true);

            }
        }

        public void HideTunner()
        {
            if (colorTunerHolder != null)
            {
                colorTunerHolder.SetActive(false);
            }
        }
    }
}
