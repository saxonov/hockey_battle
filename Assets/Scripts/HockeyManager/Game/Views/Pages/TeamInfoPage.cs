﻿using GameSparks.Core;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Leaderboard;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class TeamInfoPage:MonoBehaviour,IPageChangeHandler
    {
        private const int TotalCountUpdatePeriodMin = 5;

        [Header("TeamInfo")]
        [SerializeField] private TMP_TextLinkProcessor teamNameLinkProcessor;
        [SerializeField] private TextMeshProUGUI teamInfoLabel;
        [SerializeField] private TextIconControl totalExpPanel;
        [SerializeField] private TextIconControl energyPerTimePanel;
        [SerializeField] private Text energyPerTimeLabel;
        [SerializeField] private TextIconControl moneyPerTimePanel;
        [SerializeField] private Text moneyPerTimeLabel;
        [SerializeField] private TextIconControl fansPerTimePanel;
        [SerializeField] private Text fansPerTimeLabel;
        [SerializeField] private Text totalTrainingsLabel;
        [SerializeField] private Text totalCupsLabel;
        [SerializeField] private Text totalMatchesLabel;
        [SerializeField] private Text totalGoalsLabel;
        [SerializeField] private Text competitionsLabel;
        [SerializeField] private Text totalmoneyIncomeLabel;
        [SerializeField] private TextIconControl totalMoneyPanel;
        [SerializeField] private Text totalFansLabel;
        [SerializeField] private TextIconControl totalFansPanel;

        [SerializeField] private ScrollRect teamInfoScroller;

        [Header("Teaminfo.Attack")]
        [SerializeField] private LabelValueControl attackBox;
        [SerializeField] private LabelValueControl baseAttackBox;
        [SerializeField] private LabelValueControl arenaAttackBox;
        [SerializeField] private LabelValueControl equipmentAttackBox;
        [SerializeField] private LabelValueControl tacticAttackBox;
        [SerializeField] private LabelValueControl hallItemsAttackBox;
        [SerializeField] private LabelValueControl trainingsAttackBox;

        [Header("Teaminfo.Deffence")]
        [SerializeField] private LabelValueControl deffenceBox;
        [SerializeField] private LabelValueControl baseDeffenceBox;
        [SerializeField] private LabelValueControl arenaDdeffenceBox;
        [SerializeField] private LabelValueControl equipmentDdeffenceBox;
        [SerializeField] private LabelValueControl tacticDdeffenceBox;
        [SerializeField] private LabelValueControl hallItemsDdeffenceBox;
        [SerializeField] private LabelValueControl trainingsDdeffenceBox;

        [Header("Leaderboard")]
        [SerializeField] private Text totalTeamsLabel;
        [SerializeField] private ScrollRect leaderboardScroller;
        [SerializeField] private GameObject leaderBoardItemRendererPrefab;

        [Space(5)] [SerializeField] private InnerPageNavigation navBar;

        private LeaderBoardItemRenderer[] leaderRenderersPool;

        private MatchBattleController _battleController;
        private TeamController _controller;

        private int rankPosition = 0;
        private int _totalCountUpdateMinutes = 0;

        private byte _currentMaxLeaderboard = UISettings.MaxLeaderboardRenderersPageCount;
        private bool _leaderBoardLoadComplete = false;

        void Awake()
        {
            teamInfoScroller.transform.parent.gameObject.SetActive(false);
            _battleController = GameRoot.Instance.GetController<MatchBattleController>();
            _controller = GameRoot.Instance.GetController<TeamController>();

            leaderRenderersPool = new LeaderBoardItemRenderer[UISettings.MaxLeaderboardRenderersCount+1];

            leaderboardScroller.content.RemoveChildren();

            leaderboardScroller.onValueChanged.AddListener(OnLeaderBoardScrollChanged);

            totalTeamsLabel.text = LocaleManager.Instance.GetText("game.common.teamtotal",
                        _controller.TotalTeamsCount);

            GameTimer.OnMinutesChanged.AddListener(OnMinuteTikTakHandler);

            teamNameLinkProcessor.OnLinkClickEvent.AddListener(OnEditTeamNameClickHandler);

            PageNavigatorManager.Instance.AddPageChangeHandler(this);
        }



        private void OnMinuteTikTakHandler()
        {
            _totalCountUpdateMinutes--;
        }

        private void LoadTotalTeamCount()
        {

            if (_totalCountUpdateMinutes <= 0)
            {
                _totalCountUpdateMinutes = TotalCountUpdatePeriodMin;
                _controller.GetTotalTeamsCount(() =>
                {
                    totalTeamsLabel.text = LocaleManager.Instance.GetText("game.common.teamtotal",
                        _controller.TotalTeamsCount);

                });
            }
            else
            {
                totalTeamsLabel.text = LocaleManager.Instance.GetText("game.common.teamtotal",
                       _controller.TotalTeamsCount);
            }
        }

        void OnEnable()
        {
            EventsManager.Instance.Add<GSData>(GlobalEventTypes.ServerResponceError,OnServerResponceErrorHandler);
            EventsManager.Instance.Add<LeaderTeamModel>(LeaderBoardItemRenderer.OnLeaderItemSelectedEvent,OnLeaderItemSelected);
            EventsManager.Instance.Add(TeamController.LeaderBoardUpdateEvent,OnLeaderboardChangedHandler);
        }

    

        void OnDisable()
        {
            EventsManager.Instance.Remove<GSData>(GlobalEventTypes.ServerResponceError, OnServerResponceErrorHandler);
            EventsManager.Instance.Remove<LeaderTeamModel>(LeaderBoardItemRenderer.OnLeaderItemSelectedEvent, OnLeaderItemSelected);
            EventsManager.Instance.Remove(TeamController.LeaderBoardUpdateEvent, OnLeaderboardChangedHandler);
        }


        void OnDestroy()
        {
            _battleController = null;
            _controller = null;
            teamNameLinkProcessor.OnLinkClickEvent.RemoveListener(OnEditTeamNameClickHandler);
            GameTimer.OnMinutesChanged.RemoveListener(OnMinuteTikTakHandler);
        }

        private void OnServerResponceErrorHandler(GSData data)
        {
            if (data.ContainsKey(GS_ScriptData.ErrorData))
            {
                GSData errorData = data.GetGSData(GS_ScriptData.ErrorData);
                int id = errorData.GetInt("id").Value;

                if (id == GS_Errors.TEAM_CANNOT_BE_ATTACKED)
                {
                    PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                    OnLeaderboardChangedHandler();
                }

            }
        }

        private void OnLeaderboardChangedHandler()
        {
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Leaderboard)
            {
                _leaderBoardLoadComplete = false;
                _controller.LoadLeaderBoard(BuildLeaderBoard,_currentMaxLeaderboard);
            }

        }

        private void OnLeaderBoardScrollChanged(Vector2 position)
        {
            if (Mathf.Abs(position.y) <= 0.3f && _leaderBoardLoadComplete && _currentMaxLeaderboard < UISettings.MaxLeaderboardRenderersCount)
            {
                _currentMaxLeaderboard += UISettings.MaxLeaderboardRenderersPageCount;
                _leaderBoardLoadComplete = false;
                _controller.LoadLeaderBoard(BuildLeaderBoard, _currentMaxLeaderboard);
            }
        }

        private void OnLeaderItemSelected(LeaderTeamModel model)
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _battleController.LoadAttackTeamInfo(model.TeamInfo.UserId,(attackInfo) =>
            {
                model.Parse(attackInfo.RawData);

                PopupsManager.Instance.AddPopup(PopupType.AttackTeamPopup)
                   .SetData(model)
                   .HideButtons(!model.IsCanChallenged, false)
                   .SetCallbacks(() =>
                   {
                       _battleController.GetMatchInfo(() =>
                       {
                           EventsManager.Instance.Call(EnemyItemRenderer.EnemyTeamSelectedToAttack,
                               model.TeamInfo.UserId);

                       }, true);
                   });
            },false,true,false);
                
               

            
        }

        private void BuildLeaderBoard()
        {
            
            int len = _controller.LeadersBoard.Count;
            //safe check
            if (len > leaderRenderersPool.Length)
            {
                len = leaderRenderersPool.Length;
            }
            for (int i = 0; i < len; i++)
            {
                if (leaderRenderersPool[i] == null)
                {
                    RectTransform child = leaderboardScroller.content.AddChild(leaderBoardItemRendererPrefab, false);

                    leaderRenderersPool[i] = child.GetComponent<LeaderBoardItemRenderer>();
                    leaderRenderersPool[i].SetModel(_controller.LeadersBoard[i]);
                }
                else
                {
                    leaderRenderersPool[i].SetModel(_controller.LeadersBoard[i]);
                }
            }

            _leaderBoardLoadComplete = true;

        }

        private void BuildTeamInfo(GSData info)
        {

                if(info == null)return;

                teamInfoScroller.transform.parent.gameObject.SetActive(true);
            
                LocaleManager locales = LocaleManager.Instance;

                rankPosition = info.GetInt("rankPosition").Value;


                SetTeamName();

                totalExpPanel.SetText(info.GetInt("totalExp").Value);

                attackBox.SetLabel(locales.GetText("game.common.attack") + ":");
                attackBox.SetValue(info.GetFloat("totalAttack").Value);

                arenaAttackBox.SetLabel("+"+locales.GetText("game.common.arena") + ":");
                baseAttackBox.SetLabel(locales.GetText("game.main.teaminfo.baselevel",locales.GetText("game.common.shortlevel",_controller.TeamData.level)));
                equipmentAttackBox.SetLabel("+"+locales.GetText("game.common.equipment") + ":");
                tacticAttackBox.SetLabel("+"+locales.GetText("game.common.tactics") + ":");
                hallItemsAttackBox.SetLabel("+"+locales.GetText("game.main.building.halloffame") + ":");
                trainingsAttackBox.SetLabel("+"+locales.GetText("game.common.trainings") + ":");

                arenaAttackBox.SetValue(info.GetFloat("arenaAttack").Value);
                baseAttackBox.SetValue(info.GetFloat("attack").Value);
                equipmentAttackBox.SetValue(info.GetFloat("eqpAttack").Value);
                tacticAttackBox.SetValue(info.GetFloat("tacticAttack").Value);
                hallItemsAttackBox.SetValue(info.GetFloat("hallAttackItems").Value);
                trainingsAttackBox.SetValue(info.GetFloat("trainingAttack").Value);

                deffenceBox.SetLabel(locales.GetText("game.common.deffence") + ":");
                deffenceBox.SetValue(info.GetFloat("totalDeffence").Value);
                
                arenaDdeffenceBox.SetLabel("+" + locales.GetText("game.common.arena") + ":");
                baseDeffenceBox.SetLabel(locales.GetText("game.main.teaminfo.baselevel", locales.GetText("game.common.shortlevel", _controller.TeamData.level)));
                equipmentDdeffenceBox.SetLabel("+" + locales.GetText("game.common.equipment") + ":");
                tacticDdeffenceBox.SetLabel("+" + locales.GetText("game.common.tactics") + ":");
                hallItemsDdeffenceBox.SetLabel("+" + locales.GetText("game.main.building.halloffame") + ":");
                trainingsDdeffenceBox.SetLabel("+" + locales.GetText("game.common.trainings") + ":");

                arenaDdeffenceBox.SetValue(info.GetFloat("arenaDeffence").Value);
                baseDeffenceBox.SetValue(info.GetFloat("deffence").Value);
                equipmentDdeffenceBox.SetValue(info.GetFloat("eqpDeffence").Value);
                tacticDdeffenceBox.SetValue(info.GetFloat("tacticDeffence").Value);
                hallItemsDdeffenceBox.SetValue(info.GetFloat("hallDeffenceItems").Value);
                trainingsDdeffenceBox.SetValue(info.GetFloat("trainingDeffence").Value);

                int energyPerHour = Mathf.FloorToInt(3600f/info.GetInt("energyRestorePerTime").Value);
                energyPerHour = Mathf.Max(GameData.GSConstData.EnergyStartTime, energyPerHour);
                energyPerTimeLabel.text = locales.GetText("game.main.personal.effect.teamdoctor") + ":";
                energyPerTimePanel.SetText(locales.GetText("game.common.perhour", energyPerHour));

                moneyPerTimeLabel.text = locales.GetText("game.main.teaminfo.coinsincome") + ":";
                moneyPerTimePanel.SetText(locales.GetText("game.common.perhour",info.GetFloat("totalCoinsPerHour").Value));

                fansPerTimeLabel.text = locales.GetText("game.main.building.upgrades.fansbonus") + ":";
                fansPerTimePanel.SetText(locales.GetText("game.common.perhour",info.GetFloat("totalFansPerHour").Value));

                totalTrainingsLabel.text = locales.GetText("game.main.teaminfo.totaltrainings", info.GetInt("totalTrainings").Value);

                string lostColor = ColorUtility.ToHtmlStringRGB(UISettings.BlockTextColor);
                string winColor = ColorUtility.ToHtmlStringRGB(UISettings.GreenTextColor);
                
                string matchesWins = string.Format("<color='#{0}'>{1}</color>",winColor,info.GetInt("matchesWins").Value); 
                string matchesLost = string.Format("<color='#{0}'>{1}</color>", lostColor, info.GetInt("matchesLosses").Value);

                string matches = info.GetInt("totalMatches").Value + "(" + matchesWins + "/" + matchesLost + ")";

                totalMatchesLabel.text = locales.GetText("game.main.teaminfo.totalmatches",matches);

                string cupsWins = string.Format("<color='#{0}'>{1}</color>", winColor, info.GetInt("cupsWins").Value);
                string cupsLost = string.Format("<color='#{0}'>{1}</color>", lostColor, info.GetInt("cupsLosses").Value);
                string cups = info.GetInt("totalCups").Value + "(" + cupsWins + "/" + cupsLost + ")";

                totalCupsLabel.text = locales.GetText("game.main.teaminfo.totalcups",cups);
                totalGoalsLabel.text = locales.GetText("game.main.teaminfo.totalgoals",info.GetInt("totalGoals").Value);

                string winner = string.Format("<color='#{0}'>{1}</color>", winColor, info.GetInt("competitionWins").Value);
                string awardee = string.Format("<color='#{0}'>{1}</color>", lostColor, info.GetInt("competitionLosses").Value);

                competitionsLabel.text = locales.GetText("game.main.teaminfo.competitions",winner,awardee);

                totalmoneyIncomeLabel.text = locales.GetText("game.main.teaminfo.totalmoney");
                totalMoneyPanel.SetText(info.GetInt("totalTakedCoins").Value);

                totalFansLabel.text = locales.GetText("game.main.teaminfo.totalfans");
                totalFansPanel.SetText(info.GetInt("totalTakedFans").Value);

                
            
        }

        private void SetTeamName()
        {
            string rankPostion = string.Format("<size=35>{0}", this.GetLocaleText("game.common.rankposition", rankPosition));
            string formattedName = string.Format("{0} <size=60><link=team_name_edit><sprite name=icon-edit></link>", _controller.TeamData.TeamInfo.Name);
            teamInfoLabel.text = formattedName + "\n" + rankPostion;
        }

        private void OnEditTeamNameClickHandler(TMP_TextLinkProcessor linkProcess)
        {
            if (linkProcess != null)
            {
                SoundManager.Instance.PlaySFX("click");
                PopupsManager.Instance.AddPopup(PopupType.ChangeTeamNamePopup)
                    .InjectController(_controller).SetCallbacks(SetTeamName);
            }
           
        }

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (selected == NavigationPageTypes.TeamInfo)
            {
               // Resources.UnloadUnusedAssets();
                navBar.SelectButton(1,true);
                _controller.LoadTeamInfo(BuildTeamInfo);

            }
            else if (selected == NavigationPageTypes.Leaderboard)
            {
                navBar.SelectButton(2, true);
                teamInfoScroller.transform.parent.gameObject.SetActive(false);
                _leaderBoardLoadComplete = false;
                _controller.LoadLeaderBoard(BuildLeaderBoard,_currentMaxLeaderboard);
                LoadTotalTeamCount();
            }
           
        }
    }
}
