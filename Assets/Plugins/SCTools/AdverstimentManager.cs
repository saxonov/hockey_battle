﻿//#define UNITY_ADS
//#undef UNITY_ADS
namespace SCTools.Ads
{




    using System.Collections;
    using System.Collections.Generic;
    using SCTools.Utils;
    using UnityEngine;
#if UNITY_ADS
    using UnityEngine.Advertisements;
#endif
    using UnityEngine.Analytics;
    using UnityEngine.Events;

    public class AdverstimentManager : MonoBehaviour
    {
#if UNITY_ADS
        public UnityAction<ShowResult> OnAdsShowResultCallback;
#endif
        [SerializeField] private string androidId, iosId;
        public bool testMode = true;
        private string gameId = "";

        private static AdverstimentManager _instance;

        public static AdverstimentManager Instance
        {
            get { return _instance; }
        }

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);

#if UNITY_ANDROID
                gameId = androidId;
#elif UNITY_IOS
            gameId = iosId;
#endif

#if UNITY_ADS // If the Ads service is not enabled...
                if (Advertisement.isSupported)
                { // If runtime platform is supported...
                    Advertisement.Initialize(gameId, testMode); // ...initialize.
                    Debug.Log("Unity ads inited");
                }
#endif

            }
            else if (_instance != this)
            {
                DestroyImmediate(this.gameObject);
            }
        }


#if UNITY_ADS
        public void ShowAd(UnityAction showedCallback = null)
        {
            if (showedCallback != null && UnityUtils.HasInternetConnection())
            {
                StartCoroutine(StartShowingAds(showedCallback));
            }
            else
            {
                if (Advertisement.IsReady())
                {
                    Debug.Log("Show Unity Ads");
                    Advertisement.Show();
                }
                else
                {
                    Debug.Log("Ads not ready");
                }
            }

        }

        private IEnumerator StartShowingAds(UnityAction callback)
        {
            yield return new WaitUntil(Advertisement.IsReady);
            Debug.Log("Show Unity Ads");
            Advertisement.Show();
            if (callback != null)
            {
                callback();
            }

        }

        public bool isShowing
        {
            get
            {
                return Advertisement.isSupported && Advertisement.isShowing;
            }
        }

        public bool ShowRewardAd()
        {
            if (Advertisement.IsReady("rewardedVideo"))
            {
                Debug.Log("Show Unity Reward video Ads");
                ShowOptions options = new ShowOptions() { resultCallback = HandleRewardVideoResultCallback };
                Advertisement.Show("rewardedVideo", options);
                return true;
            }
            Debug.Log("ADs not ready");
            return false;
        }

        private void HandleRewardVideoResultCallback(ShowResult res)
        {
            Analytics.CustomEvent("rewardVideoResult", new Dictionary<string, object>()
                {
                    { "result",res.ToString()}

                });

            switch (res)
            {
                case ShowResult.Finished:
                    Debug.Log("Reward ads video complete!");
                    if (OnAdsShowResultCallback != null)
                    {
                        OnAdsShowResultCallback.Invoke(res);
                    }

                    break;
                default:

                    if (OnAdsShowResultCallback != null)
                    {
                        Debug.Log("Reward Ads video fail!");
                        OnAdsShowResultCallback.Invoke(res);
                    }
                    break;

            }
        }
#endif
    }

}
