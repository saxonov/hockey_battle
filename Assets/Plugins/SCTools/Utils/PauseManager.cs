﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace SCTools.Utils
{

    public static class PauseManager
    {

        public static UnityAction<bool> OnPauseCallback;

        private static bool _isPaused = false;

        public static bool IsPaused
        {
            get { return _isPaused; }
            set
            {
                _isPaused = value;
                if (_isPaused)
                {
                    Pause(true);
                }
                else
                {
                    UnPause(true);
                }
            }
        }

        public static void Pause(bool raizeEvent)
        {
           
            Time.timeScale = 0f;
            _isPaused = true;
            Debug.Log("PauseManager: paused");
            if (OnPauseCallback != null && raizeEvent)
            {
                OnPauseCallback.Invoke(_isPaused);
            }
        }

        public static void UnPause(bool raizeEvent)
        {
            Time.timeScale = 1f;
            _isPaused = false;
            Debug.Log("PauseManager: unpaused");
            if (OnPauseCallback != null && raizeEvent)
            {
                OnPauseCallback.Invoke(_isPaused);
            }
        }
    }

}
