<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>E:/UNITY/HockeyManager/Assets/Sprites/Atlases/Map.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename>../Resources/Map/MapAtlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>2</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Resources/Map/MapAtlas.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">PremultiplyAlpha</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.528634,0.903226</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/np_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,24,50,47</rect>
                <key>scale9Paddings</key>
                <rect>25,24,50,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/np_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,22,50,45</rect>
                <key>scale9Paddings</key>
                <rect>25,22,50,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/AdBanner1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,36,32</rect>
                <key>scale9Paddings</key>
                <rect>18,16,36,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/AdBanner1_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,36,33</rect>
                <key>scale9Paddings</key>
                <rect>18,16,36,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Bank1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,22,58,44</rect>
                <key>scale9Paddings</key>
                <rect>29,22,58,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Base1.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Kiosk2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,19,44,38</rect>
                <key>scale9Paddings</key>
                <rect>22,19,44,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Cachbox1_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,21,50,41</rect>
                <key>scale9Paddings</key>
                <rect>25,21,50,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Cashbox1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,21,50,42</rect>
                <key>scale9Paddings</key>
                <rect>25,21,50,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Hall1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,27,55,53</rect>
                <key>scale9Paddings</key>
                <rect>27,27,55,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/IceArena1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,49,136,99</rect>
                <key>scale9Paddings</key>
                <rect>68,49,136,99</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Kiosk1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,18,45,37</rect>
                <key>scale9Paddings</key>
                <rect>23,18,45,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Kiosk1_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,18,43,37</rect>
                <key>scale9Paddings</key>
                <rect>21,18,43,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 1/Shop1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,27,67,53</rect>
                <key>scale9Paddings</key>
                <rect>33,27,67,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/AdBanner2.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/AdBanner2_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,22,39,45</rect>
                <key>scale9Paddings</key>
                <rect>20,22,39,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Bank2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,28,58,57</rect>
                <key>scale9Paddings</key>
                <rect>29,28,58,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Base2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,23,57,46</rect>
                <key>scale9Paddings</key>
                <rect>29,23,57,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Cashbox2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,23,54,47</rect>
                <key>scale9Paddings</key>
                <rect>27,23,54,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Cashbox2_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,23,53,45</rect>
                <key>scale9Paddings</key>
                <rect>27,23,53,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Hall2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,31,62,62</rect>
                <key>scale9Paddings</key>
                <rect>31,31,62,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/IceArena2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,66,202,133</rect>
                <key>scale9Paddings</key>
                <rect>101,66,202,133</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Kiosk2_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,18,46,37</rect>
                <key>scale9Paddings</key>
                <rect>23,18,46,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 2/Shop2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,27,70,54</rect>
                <key>scale9Paddings</key>
                <rect>35,27,70,54</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/AdBanner3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,32,49,64</rect>
                <key>scale9Paddings</key>
                <rect>25,32,49,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/AdBanner3_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,34,52,67</rect>
                <key>scale9Paddings</key>
                <rect>26,34,52,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Bank3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,43,82,87</rect>
                <key>scale9Paddings</key>
                <rect>41,43,82,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Base3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,27,61,55</rect>
                <key>scale9Paddings</key>
                <rect>31,27,61,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Cashbox3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,35,84,69</rect>
                <key>scale9Paddings</key>
                <rect>42,35,84,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Cashbox3_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,34,86,68</rect>
                <key>scale9Paddings</key>
                <rect>43,34,86,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Hall3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,54,114,109</rect>
                <key>scale9Paddings</key>
                <rect>57,54,114,109</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/IceArena3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>99,66,199,132</rect>
                <key>scale9Paddings</key>
                <rect>99,66,199,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Kiosk3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,20,66,39</rect>
                <key>scale9Paddings</key>
                <rect>33,20,66,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Kiosk3_fliped.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,20,56,39</rect>
                <key>scale9Paddings</key>
                <rect>28,20,56,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/ Карта 1024х790/Уровень 3/Shop3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,40,87,81</rect>
                <key>scale9Paddings</key>
                <rect>44,40,87,81</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/img_banner.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,47,103,94</rect>
                <key>scale9Paddings</key>
                <rect>51,47,103,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/AdBanner1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Bank1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Base1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Cashbox1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Hall1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/IceArena1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Kiosk1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 1/Shop1_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/AdBanner2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Bank2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Base2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Cashbox2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Hall2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/IceArena2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Kiosk2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 2/Shop2_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/AdBanner3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Bank3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Base3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Cashbox3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Hall3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/IceArena3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Kiosk3_preview.jpg</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Изображение построек/Уровень 3/Shop3_preview.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.528634,0.903226</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>D:/Dropbox/HockeyBattle/Изображение построек</filename>
            <filename>D:/Dropbox/HockeyBattle/ Карта 1024х790</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
