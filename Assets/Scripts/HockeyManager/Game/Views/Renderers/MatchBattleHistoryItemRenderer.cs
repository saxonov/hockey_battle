﻿using System;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class MatchBattleHistoryItemRenderer:MonoBehaviour
    {

#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.dateLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text dateLabel;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.dateLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.timeLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text timeLabel;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.timeLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.teamNameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text teamNameLabel;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.teamNameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.scoreLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text scoreLabel;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.scoreLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.penaltyPricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl penaltyPricePanel;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.penaltyPricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.dateField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image dateField;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.dateField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.timeField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image timeField;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.timeField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.teamNameField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image teamNameField;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.teamNameField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.scoreField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image scoreField;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.scoreField" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.firstColumnColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
        [SerializeField]private Color firstColumnColor;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.firstColumnColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.firstColumnInvertedColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
        [SerializeField]private Color firstColumnInvertedColor;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.firstColumnInvertedColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.secondColumnColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
        [SerializeField]private Color secondColumnColor;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.secondColumnColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
#pragma warning disable CS0649 // Полю "MatchBattleHistoryItemRenderer.secondColumnInvertedColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .
        [SerializeField]private Color secondColumnInvertedColor;
#pragma warning restore CS0649 // Полю "MatchBattleHistoryItemRenderer.secondColumnInvertedColor" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию .

        //[SerializeField]private Color firstColumn2RowColor;
        //[SerializeField]private Color firstColumn2RowInvertedColor;
        //[SerializeField]private Color secondColumn2RowColor;
        //[SerializeField]private Color secondColumnInverted2RowColor;

        void Awake()
        {
            scoreLabel.gameObject.SetActive(false);
            penaltyPricePanel.gameObject.SetActive(false);
        }

        private MatchBattleResultData _resultData;

        public void SetData(MatchBattleResultData resultData)
        {
            _resultData = resultData;


            DateTime date = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(_resultData.StartTimeStamp);

            dateLabel.text = date.ToString("dd/MM/yy");
            timeLabel.text = date.ToLocalTime().ToString("HH:mm");
            teamNameLabel.text = _resultData.EnemyTeam.Name;

            if (_resultData.IsCanceled)
            {
                scoreLabel.gameObject.SetActive(false);
                penaltyPricePanel.gameObject.SetActive(true);
                penaltyPricePanel.SetText(_resultData.CancelPenaltyPrice.ToString().AddPrefix('-')
                                        .GetUITextColorFormated(UISettings.BlockTextColor));
            }
            else
            {
                scoreLabel.gameObject.SetActive(true);
                penaltyPricePanel.gameObject.SetActive(false);
                scoreLabel.text = _resultData.MyTeamWinner
               ? _resultData.ResultString.GetUITextColorFormated(UISettings.GreenTextColor)
               : _resultData.ResultString.GetUITextColorFormated(UISettings.BlockTextColor);
            }
          
        }

        public void InvertColors(int index)
        {
            if (index%2 == 0)
            {
                dateField.enabled = true;
                teamNameField.enabled = true;
                dateField.color = firstColumnInvertedColor;
                timeField.color = secondColumnInvertedColor;
                teamNameField.color = firstColumnInvertedColor;
                scoreField.color = secondColumnInvertedColor;
            }
            else
            {
                dateField.color = firstColumnColor;
                timeField.color = secondColumnColor;
                teamNameField.color = firstColumnColor;
                scoreField.color = secondColumnColor;
            }
        }

    }
}
