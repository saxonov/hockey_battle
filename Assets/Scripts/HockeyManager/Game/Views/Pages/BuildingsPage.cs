﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Map;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class BuildingsPage:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "BuildingsPage.iceArenaPreview" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Image iceArenaPreview;
#pragma warning restore CS0649 // Полю "BuildingsPage.iceArenaPreview" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.fansPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl fansPanel;
#pragma warning restore CS0649 // Полю "BuildingsPage.fansPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.timerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl timerPanel;
#pragma warning restore CS0649 // Полю "BuildingsPage.timerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.arenaRequirementsText" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text arenaRequirementsText;
#pragma warning restore CS0649 // Полю "BuildingsPage.arenaRequirementsText" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.arenaLevelLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Text arenaLevelLabel;
#pragma warning restore CS0649 // Полю "BuildingsPage.arenaLevelLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.upgradeProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Slider upgradeProgress; 
#pragma warning restore CS0649 // Полю "BuildingsPage.upgradeProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "BuildingsPage.buildingsHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private RectTransform buildingsHolder;
#pragma warning restore CS0649 // Полю "BuildingsPage.buildingsHolder" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.buildingItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject buildingItemRendererPrefab;
#pragma warning restore CS0649 // Полю "BuildingsPage.buildingItemRendererPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingsPage.horizontalSeparatorPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private GameObject horizontalSeparatorPrefab;
#pragma warning restore CS0649 // Полю "BuildingsPage.horizontalSeparatorPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private List<BuildingLotModel> _lots;

        private MapController _map;
        private bool _canGotoArena = false;
        private BuildingLotModel arenaLotModel;
        private TeamController _team;

        private string fansFormatStr = "{0}: {1}/{2} ({3}%)";

        void Awake()
        {
            _map = GameRoot.Instance.GetController<MapController>();
            _team = GameRoot.Instance.GetController<TeamController>();
            _lots = MapManager.Instance.GetLots();

            arenaLotModel = _lots.Find(b => b.Id == GameData.ArenaBuildingPosition);

            _lots.Remove(arenaLotModel);

            fansPanel.gameObject.SetActive(false);
            timerPanel.gameObject.SetActive(false);
            arenaLevelLabel.gameObject.SetActive(false);
            upgradeProgress.gameObject.SetActive(false);
            arenaLevelLabel.gameObject.SetActive(false);
            arenaRequirementsText.gameObject.SetActive(false);
            iceArenaPreview.gameObject.SetActive(true);

            MapManager.Instance.ArenaUpgradeCompleteEvent.AddListener(SetArenaInfo);
           
        }

        void OnEnable()
        {
            SetArenaInfo();
        }
        
        void OnDisable()
        {
            if (arenaLotModel != null && arenaLotModel.IsBuildingExist())
            {
                arenaLotModel.Building.OnModelChangedEvent.RemoveListener(SetArenaInfo);
            }

            buildingsHolder.RemoveChildren();
        }

      
        private void OnUpgradeComplete(IBuildingModel building)
        {
            if (building.Id == MapBuildingsType.IceArena)
            {
                SetArenaInfo();
            }
            else
            {
                BuildBuildingsTree();
            }
        }

        private void BuildBuildingsTree()
        {
            buildingsHolder.RemoveChildren();

            _map.LoadUpgrades(() =>
            {
                _lots.Sort();
                _lots =_lots.OrderByDescending((l) => (l.ArenaUnlockLevel == 0)).ToList();
                //_lots =_lots.OrderByDescending((l) => (l.IsBuildingExist() && l.Building.IsBuilded)).ToList();
                
                BuildingItemRenderer startConstructionLot = null;
                int count = 0;
                for (int i = 0; i < _lots.Count; i++)
                {
                    BuildingItemRenderer item =
                        buildingsHolder.AddChild(buildingItemRendererPrefab, false).GetComponent<BuildingItemRenderer>();


                    item.SetData(_lots[i]);
                    item.SetSelection(count % 2 == 0);
                    item.gameObject.name = "slot_" + _lots[i].Id;

                    if (_lots[i].IsBuildingExist() || _lots[i].ArenaUnlockLevel == 0)
                    {
                        startConstructionLot = item;
                    }

                    count++;
                }

                if (startConstructionLot != null)
                {
                    RectTransform child =
                        buildingsHolder.AddChild(horizontalSeparatorPrefab, false).GetComponent<RectTransform>();
                    child.SetSiblingIndex(startConstructionLot.transform.GetSiblingIndex()+1);
                }

            });
        }

       
        private void SetArenaInfo()
        {
            if (arenaLotModel.IsBuildingExist())
            {
                iceArenaPreview.color = Color.white;
                fansPanel.gameObject.SetActive(true);
                IBuildingModel arenaBuilding = arenaLotModel.Building;
                arenaBuilding.OnModelChangedEvent.RemoveListener(SetArenaInfo);
                if (arenaBuilding.Level < arenaBuilding.MaxLevel)
                {
                    arenaRequirementsText.gameObject.SetActive(arenaBuilding.IsBuilded);
                }

                float fansLimit;
                if (_team.NeedFansToUpgrade(out fansLimit))
                {
                    string fillFansLabel = LocaleManager.Instance.GetText("game.common.full") +
                                           LocaleManager.SPACE +
                                           GameData.GSConstData.ArenaUpgradeFansLimitPercent +
                                           LocaleManager.PERCENT;

                    arenaRequirementsText.text =
                        LocaleManager.Instance.GetText("game.main.building.upgrades.requirements", fillFansLabel);
                    arenaRequirementsText.color = UISettings.BlockTextColor;
                    _canGotoArena = true;

                }
                else
                {
                    _canGotoArena = true;
                    arenaRequirementsText.text =
                        LocaleManager.Instance.GetText("game.main.building.upgrades.requirementscomplete");
                    arenaRequirementsText.color = UISettings.GreenTextColor;

                }

                string fansStr = LocaleManager.Instance.GetText("game.common.suppoorters");
                fansPanel.SetText(string.Format(fansFormatStr, fansStr, _team.TeamData.fans,
                    _team.TeamData.maxFans,
                    Mathf.Floor(fansLimit)));

                if (arenaBuilding.IsUpgrading)
                {
                    _canGotoArena = false;
                    arenaBuilding.OnTimerLeftSecondsEvent.AddListener(OnTikTakHandler);
                    upgradeProgress.gameObject.SetActive(true);
                    timerPanel.gameObject.SetActive(true);
                    arenaRequirementsText.gameObject.SetActive(false);
                    arenaLevelLabel.gameObject.SetActive(false);
                    upgradeProgress.minValue = 0;
                    upgradeProgress.maxValue = arenaBuilding.UpgradeTimeSeconds;
                    OnTikTakHandler(GameTimer.TimeLeftSeconds(arenaBuilding.EndUpgradeTimeSeconds));
                }
                else
                {
                    _canGotoArena = true;
                    upgradeProgress.gameObject.SetActive(false);
                    timerPanel.gameObject.SetActive(false);
                    arenaRequirementsText.gameObject.SetActive(arenaBuilding.Level < arenaBuilding.MaxLevel);
                    arenaLevelLabel.gameObject.SetActive(true);
                    arenaLevelLabel.text = LocaleManager.Instance.GetText("game.common.level2", arenaBuilding.Level);
                }

                MapBuildingSettingsInfo buildingSettingsInfo = GameSettings.Instance.GetBuildingData(arenaBuilding.Id);
                
                iceArenaPreview.sprite = buildingSettingsInfo.GetPreview(arenaBuilding.Level, arenaBuilding.MaxLevel);

               

            }
            else
            {
                //Tutorial must skip this state
                iceArenaPreview.color = UISettings.BlockPreviewColor;
                arenaRequirementsText.gameObject.SetActive(true);
                arenaRequirementsText.text =LocaleManager.Instance.GetText("game.main.building.upgrades.requirementscomplete");
                arenaRequirementsText.color = UISettings.GreenTextColor;
            }

            BuildBuildingsTree();
        }
        
        public void OnArenaRegionClickHandler()
        {
            SoundManager.Instance.PlaySFX("click");
            if (arenaLotModel.IsBuildingExist())
            {
                if (arenaLotModel.Building.IsUpgrading)
                {
                    MapManager.Instance.FinishUpgradeBuilding(arenaLotModel.Building,false);
                }
                else if(_canGotoArena)
                {
                     MapManager.Instance.SelectMapBuilding(arenaLotModel.Id);
                    
                }
               
            }
        }

        private void OnTikTakHandler(long secondsLeft)
        {
            if (secondsLeft > 0)
            {
                timerPanel.SetText(GameTimer.GetGameTimeString(secondsLeft));
                upgradeProgress.DOValue(arenaLotModel.Building.UpgradeTimeSeconds - secondsLeft, 1f);
            }
        }
    }
}
