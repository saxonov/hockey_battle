﻿using UnityEngine;
using HockeyManager.Game.Dict;
using UnityEngine.Events;

namespace HockeyManager.Game.UI.Pages
{

    public class BasePage : MonoBehaviour
    {
        [SerializeField]private NavigationPageTypes _navigationID = NavigationPageTypes.None;

        public UnityEvent OnShow;
        public UnityEvent OnHide;

        //[SerializeField]private List<BasePage> _innerPages;

        [SerializeField] private GameObject parentPage;
   
        //public BasePage GetInnerPage(NavigationPageTypes pageTypes)
        //{
        //    if (_innerPages != null && _innerPages.Count > 0)
        //    {
        //        return _innerPages.Find(p => p.NavigationId == pageTypes);
        //    }
        //    return null;
        //}

        public NavigationPageTypes NavigationId
        {
            get { return _navigationID; }
        }

        public void Show()
        {
            if (!gameObject.activeInHierarchy)
            {
                if (parentPage != null)
                {
                    parentPage.gameObject.SetActive(true);
                }
                gameObject.SetActive(true);

                if (OnShow != null)
                {
                    OnShow.Invoke();
                }
            }
        }

        public void Hide()
        {
            if (gameObject.activeSelf)
            {
                //if (_innerPages != null)
                //{
                //    _innerPages.ForEach(p => p.Hide());
                //}
              

                if (OnHide != null)
                {
                    OnHide.Invoke();
                }
                gameObject.SetActive(false);
            }
           
        }
       
    }


}
