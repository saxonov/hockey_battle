﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.Map;
using HockeyManager.Game.UI.Map;
using HockeyManager.Game.UI.Popups;
using HockeyManager.Game.Views.Map;
using HockeyManager.Models.Map;
using HockeyManager.TutorialSystem;
using SCTools;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using SCTools.UI;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Managers
{
    [RequireComponent(typeof (PinchScale2D))]
    public class MapManager : MonoBehaviour
    {

       // public const string MapFinishUpgradeComplete = "mapFinishUpgradeComplete";
        //    public const string MapBuildingUpgradeComplete = "MapBuildingUpgradeComplete";

        public UnityEvent ArenaUpgradeCompleteEvent = new UnityEvent();

        public void Hide()
        {
            _pinchScale2D.enableZoom = false;
            //scrollRect.enabled = false;
            scrollRect.gameObject.SetActive(false);
        }


        public void Show()
        {
            if (!PopupsManager.Instance.HasActivePopups)
            {
                _pinchScale2D.enableZoom = GameData.IsTutorialPassed && GameRoot.Instance.NetManager.InternetExist;
            }
           //scrollRect.enabled = true;
           scrollRect.gameObject.SetActive(true);
        }


        private static MapManager _instance;

        private IGameSettings settings;

        [SerializeField] private CustomScrollRect scrollRect;
        [SerializeField] private RectTransform mapTransform;
        [SerializeField] private RectTransform constructionSet;

        private PinchScale2D _pinchScale2D;
        
        private MapBuildingLot[] _lots;

        private TeamController _teamController;
        private MapController _mapController;
        private MapBuildingLot _selectedLot = null;

        public static MapManager Instance
        {
            get { return _instance; }
        }

        

        private void Awake()
        {

           // MapBuilding.captionPrefab = GameRoot.Instance.Settings.BuildingSettings.buildingCaptionPrefab;
            _lots = constructionSet.GetComponentsInChildren<MapBuildingLot>();
            _pinchScale2D = GetComponent<PinchScale2D>();
            _instance = this;

            settings = GameSettings.Instance;

            _mapController = GameRoot.Instance.GetController<MapController>();
            _teamController = GameRoot.Instance.GetController<TeamController>();

            EventsManager.Instance.Add(GameTimer.TikTakMinuteEvent,OnMinuteHandler);

            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionLost,OnInternetConnectionLost);
            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionExist,OnInternetConnectionExist);
            EventsManager.Instance.Add<IBuildingModel>(MapController.BuildingUpgradeComplete,OnUpgradeCompleteHandler);
            EventsManager.Instance.Add(PopupsManager.PopupAddedEvent, OnLockScrollMap);
            EventsManager.Instance.Add(PopupsManager.LastPopupRemoved, OnUnlockScrollMap);
        }


        private void OnDestroy()
        {
            _mapController = null;
            _teamController = null;
            _instance = null;

            EventsManager.Instance.Remove(GameTimer.TikTakMinuteEvent, OnMinuteHandler);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionLost, OnInternetConnectionLost);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionExist, OnInternetConnectionExist);
            EventsManager.Instance.Remove<IBuildingModel>(MapController.BuildingUpgradeComplete, OnUpgradeCompleteHandler);
            EventsManager.Instance.Remove(PopupsManager.PopupAddedEvent, OnLockScrollMap);
            EventsManager.Instance.Remove(PopupsManager.LastPopupRemoved, OnUnlockScrollMap);
        }



        private void OnMinuteHandler()
        {
            if (_mapController != null)
            {
                _mapController.LoadCollectables();
            }
        }


        private void OnInternetConnectionLost()
        {
            if (_pinchScale2D != null)
            {
                _pinchScale2D.enableZoom = false;
            }
        }

        private void OnInternetConnectionExist()
        {
            if (_pinchScale2D != null)
            {
                _pinchScale2D.enableZoom = true;
            }
        }


        private void OnUpgradeCompleteHandler(IBuildingModel model)
        {
            if (model.Id == MapBuildingsType.IceArena)
            {
                UnlockLotsTouch();
                SetLotsArenaUnlockLevels();
                ArenaUpgradeCompleteEvent.Invoke();
            }
            else
            {
                if (!_isCollectStarted)
                {
                    UnlockLotsTouch();
                }
              
            }
        }

        private void OnUnlockScrollMap()
        {
            _pinchScale2D.enableZoom = GameData.IsTutorialPassed && GameRoot.Instance.NetManager.InternetExist;
            scrollRect.enabled = true;
        }

        private void OnLockScrollMap()
        {
            _pinchScale2D.enableZoom = false;
            scrollRect.enabled = false;
        }
        
        private void Start()
        {
            _pinchScale2D.zoomTarget = scrollRect.content;
            _pinchScale2D.enableZoom = true;

            LockLotsTouch();

            SetLotsArenaUnlockLevels();
            BuildMap();
        }
         
        private void OpenUpgradePage(MapBuildingsType id,int pos,NavigationPageTypes page)
        {
            if (_mapController.UpgradeExist(_selectedLot.LotData))
            {
                if (_selectedLot != null)
                {
                    _selectedLot.ClickLocked = false;
                }
                if (PageNavigatorManager.Instance.SelectedPage == page)
                {
                    PageNavigatorManager.Instance.ReloadSelectedPage();
                }
                else
                {
                    PageNavigatorManager.Instance.GotoPage(page);
                }
               
                return;
            }

            _mapController.LoadUpgradeData((int)id,pos, () =>
            {
                if (_selectedLot != null)
                {
                    _selectedLot.ClickLocked = false;
                }
               
                if (!GameData.IsTutorialPassed)
                {
                   TutorialManager.Instance.NextStep(); 
                }
                if (PageNavigatorManager.Instance.SelectedPage == page)
                {
                    PageNavigatorManager.Instance.ReloadSelectedPage();
                }
                else
                {
                    PageNavigatorManager.Instance.GotoPage(page);
                }
            });
        }

        public void SelectMapBuilding(int lotId)
        {
            _selectedLot = GetLot(lotId);
            if(_selectedLot == null)throw  new UnityException("Error SelectMapBuilding: wrong lot id!");
            int arenaLevel = _mapController.GetArenaLevel();
            if (_selectedLot.LotData.ArenaUnlockLevel > arenaLevel && arenaLevel >= 0)
            {
                _selectedLot.ClickLocked = false;

                string arenaTitle = LocaleManager.Instance.GetText("game.common.arena") + LocaleManager.SPACE + 
                                    LocaleManager.Instance.GetText("game.common.shortlevel2",_selectedLot.LotData.ArenaUnlockLevel);

                PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                      .SetTitle("game.main.popup.title.cannotbuild")
                      .SetButtonLabels("game.common.toicearenabutton")
                      .SetText(LocaleManager.Instance.GetText("game.main.popup.title.needlevelitem",arenaTitle),false)
                      .SetCallbacks(() =>
                      {
                          _selectedLot = GetLot(GameData.ArenaBuildingPosition);
                          
                          if (_selectedLot != null)
                          {
                              OpenUpgradePage(_selectedLot.LotData.Building.Id, _selectedLot.LotData.Id, NavigationPageTypes.MapBuildingInfo);
                              GotoBuilding(GameData.ArenaBuildingPosition);
                          }
                         

                      });
                return;
            }

            //check if building constructed
            if (!_selectedLot.LotData.IsBuildingExist())
            {
                OpenUpgradePage(_selectedLot.LotData.Sites[0].type, _selectedLot.LotData.Id,NavigationPageTypes.MapLotInfo);
            }
            else
            {
                //building is builded, show upgrade info
                IBuildingModel model = _selectedLot.LotData.Building;
                if (model != null)
                {
                    if (model.IsCollectable && model.IsCanCollect && !model.IsUpgrading)
                    {
                        CollectBuildingIncome(_selectedLot);
                    }
                    else if (model.IsBuilded && !model.IsUpgrading)
                    {
                        //_selectedId = lot.CurrentBuilding.Model.Id;
                        OpenUpgradePage(model.Id,model.Pos,NavigationPageTypes.MapBuildingInfo);
                    }
                    else if(!model.UpgradeTimerComplete)
                    {
                        FinishUpgradeBuilding(model, false);
                    }
                }
            }
           

        }

        //private void LockNonCollectables()
        //{
        //    for (int i = 0; i < _lots.Length; i++)
        //    {
        //        if (_lots[i].LotData.IsBuildingExist())
        //        {
        //            if (!_lots[i].LotData.Building.IsCanCollect)
        //            {
        //                _lots[i].ClickLocked = true;
        //            }
        //        }
        //    }
        //}

        private bool _isCollectStarted = false;

        private void  CollectBuildingIncome(MapBuildingLot buildingLot)
        {
            MapCollectableBuilding building = buildingLot.CurrentBuilding as MapCollectableBuilding;
            if(building == null)return;
            building.HideCollectIcon();

            _isCollectStarted = true;

            int coinsToAdd = 0;
            int fansToAdd = 0;

            //lock building from selection while is collecting
            //LockBuildingLot(building.Model.Pos, true);
            buildingLot.ClickLocked = true;
            LockNonCollectables();

            string collectSound = "collectshop";
            if (buildingLot.LotData.Building.Id == MapBuildingsType.Cashbox)
            {
                collectSound = "collectcashbox";
            }
            
            SoundManager.Instance.PlaySFXOnGameObject(collectSound, building.gameObject);

            _mapController.Collect(buildingLot.LotData.Building, out coinsToAdd, out fansToAdd, (() =>
            {
                 //GetLot(buildingLot.LotData.Id).ClickLocked = false;
                //LockBuildingLot(building.Model.Pos, false);
            }));

            if (!GameData.IsTutorialPassed)
            {
               TutorialManager.Instance.NextStep();
            }   
            VisualEffectsManager.Instance.OnAllTweensComplete.AddListener(OnFlyTweensCompleteHandler);
            VisualEffectsManager.Instance.FlyBuildingAward(building.transform, coinsToAdd, fansToAdd);
        }

        private void OnFlyTweensCompleteHandler()
        {
            _isCollectStarted = false;
            VisualEffectsManager.Instance.OnAllTweensComplete.RemoveListener(OnFlyTweensCompleteHandler);
            UnlockLotsTouch();
        }

       
        private void LockLotsTouch()
        {
            for (int i = 0; i < _lots.Length; i++)
            {
                _lots[i].ClickLocked = true;
            }
        }

        private void LockNonCollectables()
        {
            for (int i = 0; i < _lots.Length; i++)
            {
                if (_lots[i].LotData.IsBuildingExist() && 
                    (!_lots[i].LotData.Building.IsCollectable || 
                    !_lots[i].LotData.Building.IsCanCollect))
                {
                    _lots[i].ClickLocked = true;

                }
            }
        }

        private void UnlockLotsTouch()
        {
            for (int i = 0; i < _lots.Length; i++)
            {
                _lots[i].ClickLocked = false;
            }
        }

        //private void LockBuildingLot(int pos, bool locked)
        //{
        //    for (int i = 0; i < _lots.Length; i++)
        //    {
        //        if (_lots[i].CurrentBuilding != null && _lots[i].ID == pos)
        //        {
        //            _lots[i].locked = locked;
        //            break;
        //        }
        //    }
        //}


        //private List<MapBuilding> GetBuildings(MapBuildingsType type)
        //{
        //    List<MapBuilding> buildings = new List<MapBuilding>();
        //    for (int i = 0; i < _lots.Length; i++)
        //    {
        //        if (_lots[i].LotData.IsBuildingExist() && _lots[i].LotData.Building.Id == type)
        //        {
        //            buildings.Add(_lots[i].CurrentBuilding);
        //        }
        //    }
        //    return buildings;
        //}


        /// <summary>
        /// for Buildings page view hierarchy
        /// </summary> 
        /// <returns></returns>
        public List<BuildingLotModel> GetLots()
        {
            List<BuildingLotModel> lotsDataList = new List<BuildingLotModel>();
            for (int i = 0; i < _lots.Length; i++)
            {
                lotsDataList.Add(_lots[i].LotData);
            }
            return lotsDataList;
        } 

        private MapBuildingLot GetLot(int positionID)
        {
            for (int i = 0; i < _lots.Length; i++)
            {
                if (_lots[i].LotData.Id == positionID)
                {
                    return _lots[i];
                }
            }
            return null;
        }




        private void SetLotsArenaUnlockLevels()
        {
            int arenaLevel = _mapController.GetArenaLevel();
            for (int i = 0; i < _lots.Length; i++)
            {
                int lotUnlockLevel = _mapController.GetArenaUnlockLevel(_lots[i].LotData.Id);
                if (lotUnlockLevel > arenaLevel || arenaLevel == -1)
                {
                    _lots[i].LotData.ArenaUnlockLevel = lotUnlockLevel;
                    _lots[i].ChangeArenaLockStates();
                }
                else
                {
                    _lots[i].LotData.ArenaUnlockLevel = 0;
                    _lots[i].ChangeArenaLockStates();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildMap()
        {
            _pinchScale2D.SetStartZoom(0.5f);

           // EventsManager.Instance.Remove(GlobalEventTypes.UserAuthenticated, BuildMap);
            List<IBuildingModel> buildings = _mapController.Buildings;
            if (buildings.Count == 0)
            {
                //first build arena
                GetLot(GameData.ArenaBuildingPosition).ClickLocked = false;
            }
            else
            {
                UnlockLotsTouch();
                foreach (var b in buildings)
                {
                    GetLot(b.Pos).Build(b);
                    if (b.IsUpgrading)
                    {
                        ActiveProcessesManager.Instance.AddUpgradeProcess(b as BaseGameEntityModel);
                    }
                }
            }
        }
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pos"></param>
        public void GotoBuilding(int pos)
        {
            scrollRect.enabled = false;
            MapBuildingLot lot = GetLot(pos);
            Vector2 mapPos = mapTransform.InverseTransformPoint(lot.transform.position);
            Vector2 scrollPos = (Vector2.zero - mapPos);
            scrollPos = Vector2.Scale(scrollPos, mapTransform.localScale);
            mapTransform.anchoredPosition = scrollPos;
           
            scrollRect.verticalNormalizedPosition = Mathf.Clamp(scrollRect.verticalNormalizedPosition, 0f, 1f);
            scrollRect.horizontalNormalizedPosition = Mathf.Clamp(scrollRect.horizontalNormalizedPosition, 0f, 1f);

            scrollRect.enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="zoom"></param>
        public void GotoBuilding(int pos, float zoom)
        {
            _pinchScale2D.SetStartZoom(zoom);
            GotoBuilding(pos);
        }
        
        public void SelectHall()
        {
            MapBuildingLot arenaLot = GetLot(GameData.ArenaBuildingPosition);
            if (!arenaLot.LotData.IsBuildingExist())
            {
                _selectedLot = arenaLot;
                GotoBuilding(GameData.ArenaBuildingPosition);
                OpenUpgradePage(_selectedLot.LotData.Sites[0].type, _selectedLot.LotData.Id, NavigationPageTypes.MapLotInfo);
                return;
            }
            if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Home)
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);
            }

            _selectedLot = GetLot(GameData.HallBuildingPosition);
            if (_selectedLot.LotData.IsBuildingExist())
            {
                if (!arenaLot.LotData.Building.IsUpgrading && !_selectedLot.LotData.Building.IsUpgrading)
                {
                    OpenUpgradePage(MapBuildingsType.Hall, _selectedLot.LotData.Id, NavigationPageTypes.MapLotInfo);
                }

            }

            _pinchScale2D.SetStartZoom(0.8f);
            GotoBuilding(GameData.HallBuildingPosition);
           
        }

        public bool isHallExist()
        {
            MapBuildingLot lot = GetLot(GameData.HallBuildingPosition);
            if (lot != null && lot.LotData.IsBuildingExist())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// select base building and open 
        /// </summary>
        public void SelectBase()
        {
            MapBuildingLot arenaLot = GetLot(GameData.ArenaBuildingPosition);
            if (arenaLot.CurrentBuilding == null)
            {
                _selectedLot = GetLot(0);
                GotoBuilding(GameData.ArenaBuildingPosition);
                OpenUpgradePage(_selectedLot.LotData.Sites[0].type, _selectedLot.LotData.Id, NavigationPageTypes.MapLotInfo);
                return;
            }

            //
            _selectedLot = GetLot(GameData.BaseBuildingPosition);
            GotoBuilding(GameData.BaseBuildingPosition);
            if (!_selectedLot.LotData.IsBuildingExist())
            {
                OpenUpgradePage(_selectedLot.LotData.Sites[0].type, _selectedLot.LotData.Id, NavigationPageTypes.MapLotInfo);
            }
            else
            {
                if (_selectedLot.LotData.Building.IsUpgrading)
                {
                    PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);
                }
                else
                {
                    OpenUpgradePage(_selectedLot.LotData.Sites[0].type, _selectedLot.LotData.Id, NavigationPageTypes.MapBuildingInfo);
                }
                
            }
        }
        
        


        public void FinishUpgradeBuilding(IBuildingModel bmodel,bool upgradeImmidiate)
        {
            if (bmodel != null && bmodel.IsUpgrading)
            {
                string title = "";
                string time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(bmodel.EndUpgradeTimeSeconds));
                int goldPrice = GameData.GetTimerGoldPrice(bmodel.EndUpgradeTimeSeconds);

                string bname = settings.GetBuildingData(bmodel.Id).GetName();
                title = LocaleManager.Instance.GetText("game.main.construction") + ". " + bname + ".";

                UnityAction finishAcceptCallback = () =>
                {
                    if (_teamController.CanBuyForGold(goldPrice))
                    {
                        _mapController.EndUpgradeProcess((result) =>
                        {

                            UnlockLotsTouch();
                            if (result)
                            {
                                SoundManager.Instance.PlaySFX("finishforgold");
                            }
                           
                            //if (fromUpgradeWindow)
                            //{
                            //    SelectMapBuilding(bmodel.Pos);
                            //}
                         //   EventsManager.Instance.Call(MapFinishUpgradeComplete);
                            //
                        });
                    }
                    else
                    {
                        if (_selectedLot != null)
                        {
                            _selectedLot.ClickLocked = false;
                        }
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    }
                };

                if (upgradeImmidiate)
                {
                    finishAcceptCallback();
                }
                else
                {
                    if (_selectedLot != null)
                    {
                        _selectedLot.ClickLocked = false;
                    }
                    PopupsManager.Instance.AddPopup(PopupType.FinishUpgradePopup)
                    .SetTitle(title)
                    .SetTextIconControls(goldPrice.ToString(), time)
                    .SetCallbacks(finishAcceptCallback)
                    .LockButtons(false,!GameData.IsTutorialPassed)
                    .SetMinutesTickUpdate((popup) =>
                    {
                        goldPrice = GameData.GetTimerGoldPrice(bmodel.EndUpgradeTimeSeconds);
                        time = GameTimer.GetGameTimeString(GameTimer.TimeLeftSeconds(bmodel.EndUpgradeTimeSeconds));
                        popup.SetTextIconControls(goldPrice.ToString(), time);
                    });
                }
               
            }
        }
        /// <summary>
        /// Global Upgrade or Build building method, checks all conditions
        /// </summary>
        /// <param name="upgradeData"></param>
        public void UpgradeBuilding(BuildingUpgradeInfo upgradeData)
        {
            string title;
            //check for current upgrade first
            IBuildingModel bmodel = _mapController.GetCurrentUpgrade();
            if (bmodel != null)
            {
                title = upgradeData.Level == 1 ? "game.main.popup.title.cannotbuild" : "game.main.popup.title.cannotupgrade";
                string processTitle = upgradeData.Level > 1 ? "game.main.popup.textfield.upgradeprocess" : "game.main.popup.textfield.buildprocess";
                string upgradeName = settings.GetBuildingData(bmodel.Id).GetName();
                string lvl = LocaleManager.Instance.GetText("game.common.shortlevel2", bmodel.Level + 1);
                PopupsManager.Instance.AddPopup(PopupType.LockedToChangePopup)
                    .SetTitle(title)
                    .SetText(LocaleManager.Instance.GetText(processTitle, upgradeName + LocaleManager.SPACE + lvl))
                    .SetTextIconControls(GameData.GetTimerGoldPrice(bmodel.EndUpgradeTimeSeconds))
                    .SetCallbacks(() =>
                    {
                       FinishUpgradeBuilding(bmodel,true);
                      
                    }).SetMinutesTickUpdate((popup) =>
                    {
                        popup.SetTextIconControls(GameData.GetTimerGoldPrice(bmodel.EndUpgradeTimeSeconds));
                    });
                return;
            }

            if (upgradeData.Id != MapBuildingsType.IceArena)
            {
                int arenaLevel = _mapController.GetArenaLevel();
                //if bigger than 0 then arena exists
                if (arenaLevel > 0 && upgradeData.UnlockLevel > arenaLevel)
                {
                    
                    string arenaName = settings.GetBuildingData(MapBuildingsType.IceArena).GetName();
                    title = LocaleManager.Instance.GetText("game.main.popup.title.needlevelitem", arenaName);
                    title += LocaleManager.SPACE+LocaleManager.Instance.GetText("game.common.shortlevel2", upgradeData.UnlockLevel);
                    PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                        .SetTitle(title)
                        .SetButtonLabels("game.common.toicearenabutton")
                        .SetCallbacks(() =>
                        {
                            _selectedLot = GetLot(GameData.ArenaBuildingPosition);
                            GotoBuilding(GameData.ArenaBuildingPosition);
                            OpenUpgradePage(_selectedLot.LotData.Building.Id,_selectedLot.LotData.Id,NavigationPageTypes.MapBuildingInfo);
                            
                        });
                    return;
                }
            }
            else
            {
                if (upgradeData.Level > 0 && _teamController.NeedFansToUpgrade())
                {
                    PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                    _mapController.LoadBuyArenaFansInfo(OnArenaBuyInfoLoaded);
                    
                    return;
                    
                }
            }


            title = LocaleManager.Instance.GetText("game.main.popup.title.notenoughmoney");
            if (_teamController.CanBuyForCoins(upgradeData.Price))
            {
               _mapController.SendUpgradeRequest(upgradeData, false,((b) =>
               {
                   _selectedLot.ClickLocked = false;
                   _selectedLot.Build(b);
                   SoundManager.Instance.PlaySFX("construction");
                   ActiveProcessesManager.Instance.AddUpgradeProcess(b as BaseGameEntityModel);
                   PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);

               }));
            }
            else if (_teamController.CanBuyForGold(upgradeData.UpgradeGoldPrice))
            {
                string completeButton = upgradeData.Level > 1 ? "game.common.upgradebutton" : "game.common.buildbutton";
                PopupsManager.Instance.AddPopup(PopupType.NotEnoughFansCoinsPopup).
                    SetTitle(title).
                    SetTextIconControls(upgradeData.UpgradeGoldPrice).
                    SetButtonLabels(completeButton, "game.common.closebutton").SetCallbacks(() =>
                    {
                        _mapController.SendUpgradeRequest(upgradeData,true, (b) =>
                        {
                            UnlockLotsTouch();
                            _selectedLot.Build(b);
                            SoundManager.Instance.PlaySFX("finishforgold");
                            SoundManager.Instance.PlaySFX("construction");
                            //ActiveProcessesManager.Instance.AddUpgradeProcess(b as BaseGameEntityModel);
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);
                        });
                      
                    });
            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.NotEnoughMoneyPopup)
                    .SetTitle("game.main.popup.title.notenoughmoney")
                    .SetButtonLabels(LocaleManager.Instance.GetText("game.main.building.shop").ToUpper())
                    .SetCallbacks(() =>
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    });
               
            }
        }

        private void OnArenaBuyInfoLoaded()
        {
            int count = _teamController.MaxFansToBuy;
            int goldPrice = _teamController.GetGoldPricePerFan(count);
            PopupsManager.Instance.AddPopup(PopupType.NotEnoughFansCoinsPopup)
                .SetTitle("game.main.popup.title.minimumfans")
                .SetButtonLabels("game.common.overtakebutton")
                .SetTextIconControls(goldPrice)
                .SetCallbacks(() =>
                {

                    ShowTakeFansPopup(() =>
                    {
                        //reload mapbuilding upgrade info page
                        PageNavigatorManager.Instance.ReloadSelectedPage();
                    });

                });

        }


        public void ShowTakeFansPopup(UnityAction OnFansBuyComplete)
        {
            if (_teamController.MaxFansToBuy > 0)
            {
                TakeFansPopup buyArenaFansPopup = PopupsManager.Instance.AddPopup(PopupType.TakeFansPopup) as TakeFansPopup;
                buyArenaFansPopup.InjectController(_teamController).SetCallbacks(() =>
                {
                    if (_teamController.CanBuyForGold(buyArenaFansPopup.GoldPrice))
                    {
                        SoundManager.Instance.PlaySFX("flytofans");
                        _mapController.BuyArenaFans(buyArenaFansPopup.FansCount, OnFansBuyComplete);
                    }
                    else
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    }
                   
                });
            }
        }
      


        public int GetArenaLevel()
        {
            return _mapController.GetArenaLevel();
        }

        public BuildingUpgradeInfo[] GetConstructionUpgrades(BuildingLotModel lotModel = null)
        {
            if (lotModel == null && _selectedLot != null)
            {
                lotModel = _selectedLot.LotData;
            }
            BuildingUpgradeInfo[] upgrades = null;
            if (lotModel != null && !lotModel.IsBuildingExist())
            {
                if (lotModel.Sites != null)
                {
                    upgrades = new BuildingUpgradeInfo[lotModel.Sites.Length];
                    for (int i = 0; i < lotModel.Sites.Length; i++)
                    {
                        upgrades[i] = _mapController.GetUpgrade(lotModel.Sites[i].type, lotModel.Id);
                    }
                }
            }
            return upgrades;
        }

        public IBuildingModel GetSelectedBuilding()
        {
            if (_selectedLot != null && _selectedLot.LotData.IsBuildingExist())
            {
                return _selectedLot.LotData.Building;
            }
            return null;
        }



        public void LoadBuildingUpgrade(BuildingLotModel lot, UnityAction callback)
        {

            if (!_mapController.UpgradeExist(lot))
            {
                if (lot.IsBuildingExist())
                {
                    _mapController.LoadUpgradeData((int) lot.Building.Id, lot.Building.Pos, callback);
                }
                else
                {
                    callback();
                }
            }
        }
        /// <summary>
        /// get current selected building upgrade
        /// </summary>
        /// <returns></returns>
        public BuildingUpgradeInfo GetUpgrade(BuildingLotModel lotModel)
        {
            if (lotModel != null && lotModel.IsBuildingExist())
            {
                return _mapController.GetUpgrade(lotModel.Building.Id, lotModel.Id);
            }
            return null;
        }

        public BuildingUpgradeInfo GetUpgrade(IBuildingModel building)
        {
            if (building != null)
            {
                return _mapController.GetUpgrade(building.Id, building.Pos);
            }
            return null;
        }
    }
}
