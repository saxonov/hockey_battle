﻿using UnityEngine;
using SCTools.Localization;

namespace SCTools.Helpers
{
    [ExecuteInEditMode]
    public class LanguageSwitcher : MonoBehaviour
    {

        public LocaleType locale;

        void Start()
        {
            if (Application.isPlaying)
            {
                Destroy(this);
            }
        }
       
        void OnEnable()
        {
            LocaleManager.Instance.LanguageChanged += OnLocaleChanged;
        }


        void OnDisable()
        {
            LocaleManager.Instance.LanguageChanged -= OnLocaleChanged;
        }

        void OnValidate()
        {
            LocaleManager.Instance.CurrentLocaleType = locale;
        }

        private void OnLocaleChanged()
        {
            locale = LocaleManager.Instance.CurrentLocaleType;
        }
    }
}

