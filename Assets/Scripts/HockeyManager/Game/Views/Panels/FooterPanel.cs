﻿using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Views.Components;
using JetBrains.Annotations;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
#pragma warning disable 649

namespace HockeyManager.Game.Views.Panels
{
    public class FooterPanel:MonoBehaviour,IPageChangeHandler
    {

        [SerializeField]private FooterNavButton[] _navButtons;
        [SerializeField]private ToggleGroup _navButtonsToggleGroup;
        [SerializeField]private Image skillPointsIndicator;

        public UnityEvent OnChangeEvent;

        private bool _footerNavButtonSelected = false;

        private EPSController _epsController;
        private MapController _map;

        void Awake()
        {
            _epsController = GameRoot.Instance.GetController<EPSController>();
            _map = GameRoot.Instance.GetController<MapController>();
        }

        void Start()
        {
            PageNavigatorManager.Instance.AddPageChangeHandler(this);
            skillPointsIndicator.enabled = false;
            InitializeFooterBar();
            OnSkillPointsAvailble();
        }

        void OnEnable()
        {
            EventsManager.Instance.Add(EPSController.SkillPointsAvailable, OnSkillPointsAvailble);
            //FooterNavButton.OnFooterClickHandler -= OnFooterButtonClickHandler;

        }

        void OnDisable()
        {
            EventsManager.Instance.Remove(EPSController.SkillPointsAvailable, OnSkillPointsAvailble);
            EventsManager.Instance.Remove<IBuildingModel>(MapController.BuildingUpgradeComplete, OnHallBuildingUpgradeHandler);
        }


        private void InitializeFooterBar()
        {

            for (int i = 0; i < _navButtons.Length; i++)
            {
                _navButtons[i].footerPanel = this;

                if (_navButtons[i].CheckPage(NavigationPageTypes.HallOfFame))
                {
                    if (_map.isHallOfFameExist())
                    {
                        _navButtons[i].Unlock();
                    }
                    else
                    {
                        EventsManager.Instance.Add<IBuildingModel>(MapController.BuildingUpgradeComplete, OnHallBuildingUpgradeHandler);
                        _navButtons[i].Lock();

                    }
                }
            }
        }


        private void OnHallBuildingUpgradeHandler(IBuildingModel building)
        {
            if (_map.isHallOfFameExist())
            {
                EventsManager.Instance.Remove<IBuildingModel>(MapController.BuildingUpgradeComplete, OnHallBuildingUpgradeHandler);
                for (int i = 0; i < _navButtons.Length; i++)
                {
                    if (_navButtons[i].CheckPage(NavigationPageTypes.HallOfFame))
                    {
                        _navButtons[i].Unlock();
                        break;
                    }
                }
            }
        }

        public void OnFooterButtonClickHandler(NavigationPageTypes page)
        {
            SoundManager.Instance.PlaySFX("click");

            if (page == NavigationPageTypes.HallOfFame && !_map.isHallOfFameExist())
            {
                MapManager.Instance.SelectHall();

            }
            else
            {
                _footerNavButtonSelected = true;
                _navButtonsToggleGroup.allowSwitchOff = false;
                PageNavigatorManager.Instance.GotoPage(page);
            }
            if (OnChangeEvent != null)
            {
                OnChangeEvent.Invoke();
            }
        }

        private void OnSkillPointsAvailble()
        {
            skillPointsIndicator.enabled = _epsController.SkillPoints > 0;
        }

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (!_footerNavButtonSelected)
            {
                _navButtonsToggleGroup.SetAllTogglesOff();
                for (int i = 0; i < _navButtons.Length; i++)
                {
                    if (_navButtons[i].CheckPage(selected))
                    {
                        _navButtons[i].Select();

                        break;
                    }
                    _navButtonsToggleGroup.allowSwitchOff = true;
                }
            }
            _footerNavButtonSelected = false;
        }
    }
}
