﻿using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SCTools.UI
{
    public class CustomScrollRect : ScrollRect,IPointerEnterHandler,IPointerExitHandler
    {
        public PointerEventData pointerData;
        public bool disableDrag = false;


        protected override void LateUpdate()
        {
            if (disableDrag) return;
            base.LateUpdate();
        }

        public override bool IsActive()
        {
            return base.IsActive() && !disableDrag;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            pointerData = eventData;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
          
        }
    }
}
