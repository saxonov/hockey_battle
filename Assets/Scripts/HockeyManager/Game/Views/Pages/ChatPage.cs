﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Pages
{
    public class ChatPage:MonoBehaviour
    {
        [SerializeField] private TMP_TextLinkProcessor linkProcessor;
        [SerializeField]private TextMeshProUGUI lastMessages;
        [SerializeField] private TMP_InputField editorInputField;
        [SerializeField]private ScrollRect messageScroller;

        [SerializeField]private TextMeshProUGUI messagesField;
        [SerializeField] private GameObject sendMsgChatBox;
        [SerializeField] private GameObject lockChatBox;
        [SerializeField] private TextIconControl lockTimerPanel;
        [SerializeField]private GameObject chatWindow;
        [SerializeField]private GameObject compactChat;
        [SerializeField] private Button showKeyboardButton;

        [SerializeField] private RectTransform chatMenu; 

        [Header("ChatMenu")]
        [Space(5)]
        [SerializeField]private Text teamNameTitle;
        [SerializeField] private GameObject moderatorPanel;
       
        //public UnityEvent OnChatWindowOpened;
        //public UnityEvent OnChatWindowClosed;

        private bool _chatWindowActive = false;
        
        private bool _chatMsgSended = false;
      //  private List<string> _lines;
        private string[] _selectedLinkData = null;

        private GSAdminController _adminController;
        private ChatController _chatController;

        private MatchBattleController _battleController;
        private TouchScreenKeyboard keyboard;
       
        void Awake()
        {
            _battleController = GameRoot.Instance.GetController<MatchBattleController>();
            _chatController = GameRoot.Instance.GetController<ChatController>();
            _adminController = GameRoot.Instance.GetController<GSAdminController>();

            _chatController.HexModerNameColor = "#" + ColorUtility.ToHtmlStringRGB(UISettings.ModerChatNameColor);
            _chatController.HexNameColor = "#" + ColorUtility.ToHtmlStringRGB(UISettings.SimpleChatNameColor);

           // _lines = _chatController.ChatHistory;            
        }



        private void OnChatAvailableHandler()
        {
            if (_adminController.IsChatAvailable)
            {
                sendMsgChatBox.SetActive(true);
                lockChatBox.SetActive(false);
            }
            else
            {
                long timeLeft = GameTimer.TimeLeftSeconds(_adminController.EndChatLockedTime);
                lockTimerPanel.SetText(GameTimer.GetGameTimeString(timeLeft));
                sendMsgChatBox.SetActive(false);
                lockChatBox.SetActive(true);
            }
            
        }

        void OnEnable()
        {
            showKeyboardButton.onClick.AddListener(OnShowKeyboardButtonClickHandler);
            EventsManager.Instance.Add(GSAdminController.ModeratorValueChangeEvent,OnModeratorValueChanged);
            EventsManager.Instance.Add(GSAdminController.ChatAvailableEvent, OnChatAvailableHandler);
            EventsManager.Instance.Add(GlobalEventTypes.GameUnpaused, OnGameUnpaused);
            EventsManager.Instance.Add(ChatController.NewChatMessageEvent, OnChatMessageRecieved);
            EventsManager.Instance.Add(ChatController.UpdateChatHistory,OnUpdateAllChatHandler);
        }
      
        void OnDisable()
        {
            showKeyboardButton.onClick.RemoveListener(OnShowKeyboardButtonClickHandler);
            EventsManager.Instance.Remove(ChatController.NewChatMessageEvent, OnChatMessageRecieved);
            EventsManager.Instance.Remove(GSAdminController.ChatAvailableEvent, OnChatAvailableHandler);
            EventsManager.Instance.Remove(GSAdminController.ModeratorValueChangeEvent, OnModeratorValueChanged);
            EventsManager.Instance.Remove(GlobalEventTypes.GameUnpaused, OnGameUnpaused);
            EventsManager.Instance.Remove(ChatController.UpdateChatHistory, OnUpdateAllChatHandler);
        }

        private void OnUpdateAllChatHandler()
        {
            _chatController.LoadChatHistory(() =>
            {
                FillLastMessages();
                FillMessages();

            }, UISettings.MaxChatHistoryLimit);
        }


        private void OnModeratorValueChanged()
        {
            moderatorPanel.gameObject.SetActive(_adminController.IsModerator);
        }

        private void OnGameUnpaused()
        {
            if(!GameRoot.Instance.IsGameScreen())return;
            _chatController.LoadChatHistory(() =>
            {
                FillLastMessages();
                FillMessages();

            }, UISettings.MaxChatHistoryLimit);
            _adminController.CheckIsChatAvailable();
        }

        private void OnShowKeyboardButtonClickHandler()
        {
            ResetSelectedTeamMenu();
            if (Application.isEditor)
            {
                SendChatMessage(editorInputField.text);
                editorInputField.text = "";
            }
            else
            {
                keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default,true, false, false);
            }
        }

        void Update()
        {
            if (keyboard != null && keyboard.done)
            {
                try
                {
                    keyboard.text = UISettings.RemoveEmoji(keyboard.text);
                    if (!string.IsNullOrEmpty(keyboard.text))
                    {
                        SendChatMessage(keyboard.text);
                        keyboard = null;
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex.Message);
                    keyboard = null;
                }
            }
        }


        private void OnTikTakHandler()
        {
            if (!_adminController.IsChatAvailable)
            {
                long timeLeft = GameTimer.TimeLeftSeconds(_adminController.EndChatLockedTime);
                if (timeLeft >= 0)
                {
                    lockTimerPanel.SetText(GameTimer.GetGameTimeString(timeLeft));
                }
                else
                {
                    _adminController.CheckIsChatAvailable(true);
                }
            }
        }

        void Start()
        {

            editorInputField.gameObject.SetActive(Application.isEditor);
           
            chatWindow.gameObject.SetActive(false);
            compactChat.gameObject.SetActive(true);

            _chatController.LoadChatHistory(FillLastMessages, UISettings.MaxChatHistoryLimit);
            _adminController.CheckIsChatAvailable();

            OnModeratorValueChanged();

        }

        private void OnChatMessageRecieved()
        {
            if (_chatWindowActive)
            {
                SoundManager.Instance.PlaySFXOnGameObject("chatmsg", chatWindow.gameObject);
                FillMessages(true);
            }
            FillLastMessages();
           
        }

        private void SendChatMessage(string msg)
        {
            if (!_chatMsgSended)
            {
                    
                    _chatMsgSended = true;
                    if (string.IsNullOrEmpty(msg))
                    {
                        _chatMsgSended = false;
                        return;
                    }
                    _chatController.SendChatMessage(msg, () =>
                    {
                        _chatMsgSended = false;
                      
                    });
            }
        }

      
        /// <summary>
        /// show chat window
        /// </summary>
        public void OnShow()
        {
            ResetSelectedTeamMenu();

            lastMessages.text = "";
            compactChat.gameObject.SetActive(false);
            FillMessages();
            _chatWindowActive = true;
            linkProcessor.OnLinkClickEvent.AddListener(OnTeamNameSelected);

            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
        }

        /// <summary>
        /// hide chat window
        /// </summary>
        public void OnHide()
        {
            compactChat.gameObject.SetActive(true);
            FillLastMessages();
            keyboard = null;
            //  Debug.Log("chatwindow closed");
            linkProcessor.OnLinkClickEvent.RemoveListener(OnTeamNameSelected);

            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
        }

        public void OnShowTeamInfo()
        {

            if (_selectedLinkData != null)
            {
                PopupsManager.Instance.SetPreloadDelaySeconds(0f);
                _battleController.LoadAttackTeamInfo(_selectedLinkData[0],OnGetAttackTeamInfo);
            }

            ResetSelectedTeamMenu();

            Debug.Log("ShowTeamInfo");
        }

        private void OnGetAttackTeamInfo(AttackTeamModel model)
        {
          
            PopupsManager.Instance.AddPopup(PopupType.AttackTeamPopup)
                .SetData(model)
                .HideButtons(!model.IsCanChallenged,false)
                .SetCallbacks(() =>
                {
                    _battleController.GetMatchInfo(() =>
                    {
                        EventsManager.Instance.Call(EnemyItemRenderer.EnemyTeamSelectedToAttack,
                            model.TeamInfo.UserId);

                    }, true);
                });
        }

        public void AnswerToTeam()
        {
            if (Application.isEditor)
            {

                editorInputField.text = teamNameTitle.text + ", ";
            }
            else
            {
                
                keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false);
                keyboard.text = teamNameTitle.text + ", ";
            }

            ResetSelectedTeamMenu();

        }

        public void LockChatForSelectedTeam()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            if (!string.IsNullOrEmpty(teamNameTitle.text))
            {
               _adminController.LockChatForPlayer(teamNameTitle.text, () =>
               {
                   StartCoroutine(SendModerMessage(this.GetLocaleText("game.common.chat.chatlockedforteam",teamNameTitle.text)));
                   ResetSelectedTeamMenu();
               });
            }
            Debug.Log("team chat locked");
        }

        private IEnumerator SendModerMessage(string message,float delay = 2f)
        {
           yield return new WaitForSeconds(delay);
           SendChatMessage(message);
        }

        public void RemoveSelectedMessage()
        {
            if (_selectedLinkData != null)
            {
                _adminController.RemoveChatMessage(_selectedLinkData[0],
                                                    long.Parse(_selectedLinkData[1]));
            }
            ResetSelectedTeamMenu();

            Debug.Log("message removed");
        }
        
        private void  FillMessages(bool add = false)
        {
            messagesField.text = _chatController.ChatBuffer.ToString();
            StartCoroutine(ScrollRectMessageField());
        }

        private IEnumerator ScrollRectMessageField()
        {
            yield return new WaitForEndOfFrame();
            messageScroller.verticalNormalizedPosition = 0f;
        }

        private void ResetSelectedTeamMenu()
        {
            chatMenu.gameObject.SetActive(false);
            teamNameTitle.text = "";
            _selectedLinkData = null;
        }

       // private Stack<string> lastMessagesStack;

        private void FillLastMessages()
        {
            List<string> lines = _chatController.LastChatMessages;
            lastMessages.text = "";
            for (int i = 0; i < lines.Count; i++)
            {
                lastMessages.text += lines[i] + '\n';
            }            
        }

        private void OnTeamNameSelected(TMP_TextLinkProcessor link)
        {
            if (link != null)
            {
                //if locked skip show menu
                if(!_adminController.IsChatAvailable)return;
                _selectedLinkData = link.LinkId.Split('_');
                SoundManager.Instance.PlaySFX("click");
                Vector2 pos = link.LinkPosition;
                pos.x = 0f;
                chatMenu.gameObject.SetActive(true);
                chatMenu.anchoredPosition = pos;
                teamNameTitle.text = _selectedLinkData[2];
            }
            else
            {
                ResetSelectedTeamMenu();
            }
        }
    }
}
