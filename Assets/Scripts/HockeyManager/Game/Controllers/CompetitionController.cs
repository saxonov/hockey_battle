﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Competitions;
using SCTools.EventsSystem;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class CompetitionController:IGameController,ICheckMessages,IDisposable, IGSResponceHandler
    {

        public const string CompetitionWinEvent2Args = "CompetitionController.CompetitionWinEvent2Args";
        public const string NeedUpdateCompetitionsEvent = "CompetitionController.NeedUpdateCompetitions";

        private IGameRoot _root;

        private List<GSData> _winnerDataList = null;
        private bool _needUpdateCompetitions = true;

        private List<CompetitionModel> _competitions;

    
        public List<CompetitionModel> Competitions
        {
            get { return _competitions; }
        }

        public bool NeedUpdateCompetitions
        {
            set { _needUpdateCompetitions = value; }
        }


        public void LoadCompetitions(UnityAction callback,bool forceUpdate = false)
        {
            if (!_needUpdateCompetitions && !forceUpdate)
            {
                callback();
                return;
            }
            _needUpdateCompetitions = false;
            var request = new LogEventRequest();
            request.SetEventKey("GET_COMPETITIONS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });
        }


        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.CompetitionsData))
            {
                _competitions = _competitions ?? new List<CompetitionModel>();
                _competitions.Clear();
                var list = scriptData.GetGSDataList(GS_ScriptData.CompetitionsData);
                foreach (var gsData in list)
                {
                    _competitions.Add(new CompetitionModel(gsData));
                }
            }
        }

        private void NotifyWinner(GSData data)
        {

            string type = data.GetString("type");
            int position = data.GetInt("position").Value;
            EventsManager.Instance.Call(CompetitionWinEvent2Args, type, position);
        }

        //public void CheckWins()
        //{
            
        //    while (_competitionWinnersData.Count > 0)
        //    {
        //        GSData data = _competitionWinnersData[0];
        //        string type = data.GetString("type");
        //        int position = data.GetInt("position").Value;
        //        EventsManager.Instance.Call(CompetitionWinEvent2Args, type, position);
        //        _competitionWinnersData.RemoveAt(0);
        //    }
        //}

        public void Initialize(IGameRoot root)
        {
            _root = root;
            NewHighScoreMessage.Listener += OnNewHighScoreChangedHandler;
            GlobalRankChangedMessage.Listener += OnTop3RankChangedHandler;
        }

        public void Reset()
        {
            _needUpdateCompetitions = true;
        }

        public void CheckWinners()
        {
            if (_winnerDataList != null)
            {
                foreach (var gsData in _winnerDataList)
                {
                    NotifyWinner(gsData);
                }
                _winnerDataList.Clear();
                _winnerDataList = null;
            }
        }

        private void OnNewHighScoreChangedHandler(NewHighScoreMessage message)
        {
            if (message.LeaderboardShortCode == "CMPT_GOALS_LB" ||
           message.LeaderboardShortCode == "CMPT_EXP_LB" ||
           message.LeaderboardShortCode == "CMPT_COINS_LB")
            {
              //  _root.DismissMessage(message);
                _needUpdateCompetitions = true;
                EventsManager.Instance.Call(NeedUpdateCompetitionsEvent);
            }
        }

        private void OnTop3RankChangedHandler(GlobalRankChangedMessage message)
        {
            if (message.LeaderboardShortCode == "CMPT_GOALS_LB" ||
                message.LeaderboardShortCode == "CMPT_EXP_LB" ||
                message.LeaderboardShortCode == "CMPT_COINS_LB")
            {
                //_root.DismissMessage(message);
                _needUpdateCompetitions = true;
                EventsManager.Instance.Call(NeedUpdateCompetitionsEvent);
            }
        }

        public void Dispose()
        {
            NewHighScoreMessage.Listener -= OnNewHighScoreChangedHandler;
            GlobalRankChangedMessage.Listener -= OnTop3RankChangedHandler;
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.CompetitionWinnerMsg)
            {

               _needUpdateCompetitions = true;
                if (_root.IsGameStarted && _root.IsGameScreen())
                {
                    NotifyWinner(msgData);
                }
                else
                {
                    _winnerDataList = _winnerDataList ?? new List<GSData>();
                    _winnerDataList.Add(msgData);
                }
            }

            if (msgCode == GS_Messages.CompetitionResetedMsg || msgCode == GS_Messages.CompetitionRefreshMsg)
            {
                _needUpdateCompetitions = true;
                EventsManager.Instance.Call(NeedUpdateCompetitionsEvent);

            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.CompetitionResetedMsg || 
                   msgType == GS_Messages.CompetitionWinnerMsg || 
                   msgType == GS_Messages.CompetitionRefreshMsg;
        }

    }
}
