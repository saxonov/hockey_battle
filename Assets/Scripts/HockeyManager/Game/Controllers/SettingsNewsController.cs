﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace HockeyManager.Game.Controllers
{
    public class SettingsNewsController:IGameController,IGSResponceHandler,IPauseHandler
    {
        
        public const string MusicOnOffPrefKey = "musicOnOffPrefKey";

        private GameSettingsData _gameSettingsData = new GameSettingsData();
        private IGameRoot _root;

    

        private List<NewsData> _news;

        public void Initialize(IGameRoot root)
        {
            _root = root;
        }

        public void Reset()
        {
            _newsLoaded = false;
        }

        private bool _newsLoaded = false;


        public void BindSocialNetwork(SocialAccountType type)
        {
           // _root.NetManager.CurrentAccountType = type;
           // _root.NetManager.
        }

        public void ResetTeam()
        {
            GSRequestData rdata = new GSRequestData();
            rdata.AddBoolean("matches", true);
            rdata.AddBoolean("trainings", true);
            rdata.AddBoolean("buildings", true);
            rdata.AddBoolean("team", true);
            rdata.AddBoolean("rewards", true);
            rdata.AddBoolean("equipment", true);
            rdata.AddBoolean("personal", true);

            var request = new LogEventRequest();
            request.SetEventKey("CLEAR_TEST");
            request.SetEventAttribute("VALUES", rdata);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    Debug.Log("UserTeam reset complete! ");
                    PlayerPrefs.DeleteAll();
                    PlayerPrefs.Save();
#if UNITY_EDITOR
                    EditorApplication.isPlaying = false;
#else
                        Application.Quit();
#endif
                }
            });
        }

#if UNITY_EDITOR
        public void AddTestRewards(int count = 10)
        {
            var request = new LogEventRequest();
            request.SetEventKey("ADD_TEST_REWARDS");
            request.SetEventAttribute("COUNT", count);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {

                }
            });
        }

        //#if UNITY_EDITOR_WIN
      
        //#endif
#endif
        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        public void LoadNews(UnityAction callback)
        {
            if (_newsLoaded)
            {
                callback();
            }
            _newsLoaded = true;
            var request = new LogEventRequest();
            request.SetEventKey("LOAD_NEWS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            }, null, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        public void SaveGameSettings(UnityAction callback = null)
        {
            if (!_gameSettingsData.isChanged) return;
            PlayerPrefs.SetInt(MusicOnOffPrefKey, Convert.ToInt32(_gameSettingsData.music));
            PlayerPrefs.Save();

            GSRequestData requestData = new GSRequestData();
            requestData.AddBoolean("music", _gameSettingsData.music);
            requestData.AddBoolean("sound", _gameSettingsData.sound);
            requestData.AddBoolean("notification", _gameSettingsData.notification);
            requestData.AddNumber("locale", (int)_gameSettingsData.locale);
            requestData.AddBoolean("tutorial", _gameSettingsData.tutorialPassed);

            var request = new LogEventRequest();
            request.SetEventKey("UPDATE_SETTINGS");
            request.SetEventAttribute("DATA", requestData);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
            Debug.Log("GameSettings saved");
            _gameSettingsData.isChanged = false;
        }



        public GameSettingsData GameSettingsData
        {
            get { return _gameSettingsData; }
        }

        public List<NewsData> News
        {
            get { return _news; }
        }

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.GameSettingsData))
            {
                GSData settingsData = scriptData.GetGSData(GS_ScriptData.GameSettingsData);

                _gameSettingsData.music = !settingsData.ContainsKey("music") || settingsData.GetBoolean("music").Value;
                _gameSettingsData.notification = !settingsData.ContainsKey("notification") || settingsData.GetBoolean("notification").Value;
                _gameSettingsData.sound = !settingsData.ContainsKey("sound") || settingsData.GetBoolean("sound").Value;
                _gameSettingsData.tutorialPassed = !settingsData.ContainsKey("tutorial") || settingsData.GetBoolean("tutorial").Value;

                if (settingsData.ContainsKey("locale"))
                {
                    _gameSettingsData.locale = (LocaleType)settingsData.GetInt("locale").Value;
                }
                LocaleManager.Instance.CurrentLocaleType = _gameSettingsData.locale;
                SoundManager.Instance.MuteSFX = !_gameSettingsData.sound;
                SoundManager.Instance.MuteMusic = !_gameSettingsData.music;
                GameData.IsTutorialPassed = _gameSettingsData.tutorialPassed;
            }

            if (scriptData.ContainsKey(GS_ScriptData.NewsData))
            {
                _news = _news ?? new List<NewsData>();
                _news.Clear();
                List<GSData> list = scriptData.GetGSDataList(GS_ScriptData.NewsData);
                for (int i = 0; i < list.Count; i++)
                {
                    _news.Add(JsonUtility.FromJson<NewsData>(list[i].JSON));
                }
            }
        }

        public void OnPaused()
        {
            SaveGameSettings();
        }

    }
}
