﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Components
{
    public class NavigateToPage :MonoBehaviour,IPointerClickHandler,IPointerUpHandler,IPointerDownHandler
    {
        [SerializeField] private NavigationPageTypes _page = NavigationPageTypes.None;

        public Button button;
        public Toggle toggle;
        
        void OnEnable()
        {
            if (button != null)
            {
                button.onClick.AddListener(OnButtonClickHandler);

            }

            if (toggle != null)
            {
                toggle.onValueChanged.AddListener(OnToggleChangedHandler);
            }
        }

        public void SetPage(NavigationPageTypes page)
        {
            _page = page;
        }

        void OnDisable()
        {
            if (button != null)
            {
                button.onClick.RemoveListener(OnButtonClickHandler);
            }
            if (toggle != null)
            {
                toggle.onValueChanged.RemoveListener(OnToggleChangedHandler);
            }
        }

        private void OnButtonClickHandler()
        {
            PageNavigatorManager.Instance.GotoPage(_page);
        }

        private void OnToggleChangedHandler(bool value)
        {
            if (value)
            {
                PageNavigatorManager.Instance.GotoPage(_page);
            }
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            if (button == null && toggle == null)
            {
                PageNavigatorManager.Instance.GotoPage(_page);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
          //Debug.Log("pointer up");
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //Debug.Log("pointerdown ");
        }
    }
}
