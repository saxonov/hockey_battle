﻿using UnityEngine;

namespace SCTools.Helpers
{
    [RequireComponent(typeof(Camera))]
    public class ScreenAdjust : MonoBehaviour
    {

        public float TargetWidth = 1920f;
        public float TargetHeight = 1080f;

        public float PixelsPerUnit = 100f;

        private Camera currentCamera;

        void Awake()
        {
            currentCamera = GetComponent<Camera>();
        }
        // Use this for initialization
        void Start()
        {

            if (currentCamera.orthographic)
            {

                float desiredRatio = TargetWidth / TargetHeight;
                float currentRatio = (float)Screen.width / Screen.height;

                if (currentRatio >= desiredRatio)
                {
                    // Our resolution has plenty of width, so we just need to use the height to determine the camera size
                    currentCamera.orthographicSize = TargetHeight / PixelsPerUnit / 2;
                }
                else
                {
                    // Our camera needs to zoom out further than just fitting in the height of the image.
                    // Determine how much bigger it needs to be, then apply that to our original algorithm.

                    float differenceInSize = desiredRatio / currentRatio;
                    // float needPoints =;
                    PixelsPerUnit /= differenceInSize;
                    currentCamera.orthographicSize = TargetHeight/ PixelsPerUnit/2;
                }

            }
        }


    }


}
