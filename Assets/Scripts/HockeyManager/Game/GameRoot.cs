﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.GameSparks;
using HockeyManager.Game.GameSparks.Impl;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace HockeyManager.Game
{

    public class GameRoot : MonoBehaviour, IGameRoot
    {
        private const string LocaleLoadPrefKey = "LocalesLoadData";

        #region Singleton

        private static GameRoot _instance;

      
     // Use this for initialization
        private void Awake()
        {

            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (_instance == null)
            {
                 _instance = this;
                 DontDestroyOnLoad(gameObject);
            }
            Init();
        }
        
        public static IGameRoot Instance
        {
            get { return _instance; }
        }

        #endregion

       // public bool sendClientLogs = true;

        private RewardsAchievementsController _rewards;
        private List<IGameController> _controllers;

        private DateTime _pausedDateTime = DateTime.Now;
        private bool _isPaused;
  

        private static bool _isGameStarted;

        private GSAuthorizationBehavior _authtorization;
        private GSManager _netManager = null;
    
        //team journal
     
        private bool _adsShowingNow = false;

        private void Init()
        {
            if (_isGameStarted)
            {
                return;
                
            }
                
              

            Debug.Log("Init Gameroot");

                _rewards = new RewardsAchievementsController();

                _controllers = new List<IGameController>(12);

                _controllers.Add(new TeamController());
                _controllers.Add(_rewards);
                _controllers.Add(new MapController());
                _controllers.Add(new EPSController());
                _controllers.Add(new TrainingsController());
                
                _controllers.Add(new HallItemsController());
                _controllers.Add(new TournamentBattleController());
                _controllers.Add(new MatchBattleController());
                _controllers.Add(new CompetitionController());
                _controllers.Add(new ChatController());
                _controllers.Add(new SettingsNewsController());
                _controllers.Add(new GSAdminController());

            foreach (var gameController in _controllers)
            {
                gameController.Initialize(this);
            }

            GS.GameSparksAvailable += OnGSAvailableHandler;
        }

        private void OnLoginCompleteHandler()
        {
            _netManager.OnUserAuthenticatedEvent.RemoveListener(OnLoginCompleteHandler);
            LoadLocales();
        }

        void Start()
        {

            if (_isGameStarted)
            {
                return;
            }

            Application.targetFrameRate = 60;

            SoundManager.Instance.MuteMusic = PlayerPrefs.HasKey(SettingsNewsController.MusicOnOffPrefKey) && !Convert.ToBoolean(PlayerPrefs.GetInt(SettingsNewsController.MusicOnOffPrefKey));
            SoundManager.Instance.PlayMusic("intro", 0.5f, true,true);

            LocaleManager.SetDefaultLocale(LocaleType.EN);
            LocaleManager.AddSupportLocale(LocaleType.RU);

            _netManager = gameObject.AddComponent<GSManager>();
            _authtorization = GetComponent<GSAuthorizationBehavior>();

            _netManager.responceControllers = _controllers.OfType<IGSResponceHandler>().ToArray();
            _netManager.checkMessageControllers = _controllers.OfType<ICheckMessages>().ToArray();

            _netManager.OnUserAuthenticatedEvent.AddListener(OnLoginCompleteHandler);


            Debug.Log("Gameroot started!");
        }

        private void LoadLocales()
        {
            Debug.Log("Load Locales");

            if (GameData.IsNewTeam)
            {
                LocaleManager.Instance.CurrentLocaleType = LocaleType.None;
            }
            //download from gs
            GetDownloadableRequest request = new GetDownloadableRequest();
            request.SetShortCode("LOCALES");
            request.Send((responce) =>
            {
                if (responce != null && !responce.HasErrors)
                {
                   // Debug.Log("Locale download url is: " + responce.Url);
                    StartCoroutine(StartLoadLocales(responce.Url, responce.LastModified.Value));
                }
                else
                {
                    LocaleManager.Instance.LocalLoadLocale();
                    StartGame();
                }
            });

        }

        IEnumerator StartLoadLocales(string url, DateTime lastModified)
        {
            //TODO: optimize loading ?save on Resources or cache
            WWW w = new WWW(url);

            yield return w;
            if (w.error != null)
            {
                Debug.LogWarning("Error loading locaSetLocaleles: " + w.error);
                LocaleManager.Instance.LocalLoadLocale();
                StartGame();
            }
            else
            {
                LocaleManager.Instance.SetLocalesData(w.text);
                StartGame();
            }
        }




        void OnDestroy()
        {
            
                GS.GameSparksAvailable -= OnGSAvailableHandler;
                
        }
    

        void OnApplicationFocus(bool hasfocus)
        {
            _isPaused = !hasfocus;
          
        }

        void OnApplicationPause(bool pause)
        {
            _isPaused = pause;
            
            if (_isPaused && IsCanRestart)
            {
                Debug.Log("Start restart timer");
                _pausedDateTime = DateTime.Now;
               
                GS.Disconnect();

                //clean memory
               // GC.Collect();
               // Resources.UnloadUnusedAssets();

                foreach (var gameController in _controllers)
                {
                    IPauseHandler pauseController = gameController as IPauseHandler;
                    if (pauseController != null)
                    {
                        pauseController.OnPaused();
                    }
                }

                EventsManager.Instance.Call(GlobalEventTypes.GamePaused);
            }
            else if (!_isPaused && IsCanRestart)
            {
                DateTime now = DateTime.Now;
                TimeSpan span = (now - _pausedDateTime);
             //   _pausedDateTime = DateTime.Now;
                //GameTimer.TimeStamp += (long)span.TotalSeconds; 
                //check half minute for restart activity
                if (isGameScreen && _isGameStarted)
                {
                    Debug.Log("Restart activity! skipped time: " + span.Hours + "h" + span.Minutes + "m" + span.Seconds + "s");
                    foreach (var gameController in _controllers)
                    {
                        gameController.Reset();
                    }

                    EventsManager.Instance.Call(GlobalEventTypes.GameUnpaused);

                    _netManager.SendRestartEvent(_rewards.RewardsCount);

                }
            }
            Debug.Log("is paused " + _isPaused);
        }


        private void OnGSAvailableHandler(bool value)
        {
            if (IsCanRestart && value && !_isPaused)
            {
                _netManager.SendRestartEvent(_rewards.RewardsCount);
            }
        }

      

        public T GetController<T>() where T:IGameController
        {
            Type type = typeof(T);
            return (T) _controllers.Find(c=> c.GetType() == type);
        }

        private bool IsCanRestart
        {
            get { return SceneManager.GetActiveScene().name != UISettings.PreloadScene && 
                    !_adsShowingNow && !_authtorization.IsStartConnectionToSocialPlatform; }
        }

        private bool isGameScreen
        {
            get { return SceneManager.GetActiveScene().name == UISettings.MainScene; }
        }

    

        public bool IsGameScreen()
        {
            return SceneManager.GetActiveScene().name == UISettings.MainScene;
        }

        //private AsyncOperation _sceneLoadOp;

        private void StartGame()
        {
            _isGameStarted = true;
            if (GameData.IsNewTeam)
            {
                SceneManager.LoadSceneAsync(UISettings.StartOnceScene);
            }
            else
            {
                if (SceneManager.GetActiveScene().name != UISettings.MainScene)
                {
                    StartCoroutine(LoadGameScene(UISettings.MainScene));
                }
            }
        }

        private IEnumerator LoadGameScene(string scene)
        {
            AsyncOperation op = SceneManager.LoadSceneAsync(scene);
            op.allowSceneActivation = false;

            yield return new WaitWhile(() => op.isDone);
            op.allowSceneActivation = true;
            yield return new WaitForSeconds(1.5f);
            _netManager.UpdateFromTeamJournal();
            yield return null;
        }

        public bool IsGameStarted
        {
            get { return _isGameStarted; }
        }

        public bool IsPaused
        {
            get { return _isPaused; }
        }

        
        public IGSManager NetManager
        {
            get { return _netManager; }
        }

        public IGSAuthentication Authtorization
        {
            get { return _authtorization; }
        }
    }
}

