﻿using UnityEngine;

namespace HockeyManager.Game.UI.Components
{
  
    [RequireComponent(typeof(LogoImage))]
    public class HSVShaderTuner : MonoBehaviour
    {
#pragma warning disable CS0169 // Поле "HSVShaderTuner.hsvMaterial" никогда не используется.
        private Material hsvMaterial;
#pragma warning restore CS0169 // Поле "HSVShaderTuner.hsvMaterial" никогда не используется.

        [SerializeField]private LogoImage source;

        // Use this for initialization
        void Start()
        {
            if (source == null)
            {
                source = GetComponent<LogoImage>();
            }
            
            
        }

         public void ChangeHUE(float value)
        {
            if (source == null)
            {
                source = GetComponent<LogoImage>();
            }
            source.ModifiedMaterial.SetFloat("_Hue",value);
        }

        public void ChangeSaturation(float value)
        {
            if (source == null)
            {
                source = GetComponent<LogoImage>();
            }
            source.ModifiedMaterial.SetFloat("_Saturation", value);
        }

      
    }

}
