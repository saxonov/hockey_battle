﻿using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class CupTeamItemRenderer:MonoBehaviour,IPointerClickHandler
    {

        public const string CupTeamItemSelected1ArgEvent = "CupTeamItemRenderer.cupTeamItemSelected";

        [SerializeField] private Image selection;
        [SerializeField] private Text counterLabel;
        [SerializeField] private Text teamNameLabel;
        [SerializeField] private Text winsLabel;
        [SerializeField] private Text lossLabel;
        [SerializeField] private Text goalsLabel;
        [SerializeField] private Text pointsLabel;

        [SerializeField] private LogoImage teamLogo;

        private TournamentUserModel _model;

        public void InvertSelection(bool value)
        {
            if (value)
            {
                selection.color = new Color(0f,0f,0f,1f);
            }
            else
            {
                selection.color = new Color(0f, 0f, 0f, 0f);
            }
        }

        public void SetModel(TournamentUserModel model)
        {
            _model = model;
            counterLabel.text = _model.Position.ToString();
            teamNameLabel.text = _model.TeamInfo.Name + LocaleManager.SPACE + this.GetLocaleText("game.common.shortlevel2",model.Level);
            winsLabel.text = _model.Wins.ToString();
            lossLabel.text = _model.Loss.ToString();
            goalsLabel.text = _model.TotalGoals.ToString();
            pointsLabel.text = _model.Points.ToString();

            teamLogo.sprite = GameSettings.Instance.GetLogo128(_model.TeamInfo.Logo);

            teamLogo.material = new Material(GameSettings.Instance.HsvShader);

            var hsvTuner = teamLogo.gameObject.AddComponent<HSVShaderTuner>();
            hsvTuner.ChangeHUE(_model.TeamInfo.LogoColor);
            hsvTuner.ChangeSaturation(_model.TeamInfo.LogoSaturation);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            

            if (!eventData.dragging)
            {
                EventsManager.Instance.Call(CupTeamItemSelected1ArgEvent, _model);
            }
        }

       
    }
}
