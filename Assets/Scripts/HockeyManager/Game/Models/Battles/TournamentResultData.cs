﻿using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Impl;
using SCTools.Localization;

namespace HockeyManager.Game.Models.Battles
{
    public class TournamentResultData:IGSParse
    {

        private long _startTime;
        private string _cupName;
        private int _totalSteps;
        private List<TournamentUserModel> _players = new List<TournamentUserModel>();

        public List<TournamentUserModel> Players
        {

            get
            {
                return _players;
            }
        }

        public TournamentResultData(GSData data)
        {
            Parse(data);
        }

        public int TotalSteps
        {
            get { return _totalSteps; }
        }

        public string GetCupName()
        {
            if (LocaleManager.Instance.HasKey(_cupName))
            {
                return LocaleManager.Instance.GetText(_cupName);
            }
            return _cupName;
        }

        public long StartTimeStamp
        {
            get { return _startTime; }
        }

        public void Parse(GSData data)
        {
            _startTime = data.GetLong("startTime").Value;
            _cupName = data.GetString("name");
            _totalSteps = data.GetInt("totalSteps").Value;

            var list = data.GetGSDataList("players");

            _players.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                _players.Add(new TournamentUserModel(list[i]));
            }
        }
    }
}
