﻿using System.Collections.Generic;
using HockeyManager.TutorialSystem.Base;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

namespace HockeyManager.TutorialSystem
{
    public class TutorialMask : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
    {
        [SerializeField] private string _tutorObjectTag = "TutorObject";
        [SerializeField] private string _tutorAimTag = "TutorAim";

#pragma warning disable CS0414 // Полю "TutorialMask.arrow" присвоено значение, но оно ни разу не использовано.
        [SerializeField] private RectTransform arrow = null;
#pragma warning restore CS0414 // Полю "TutorialMask.arrow" присвоено значение, но оно ни разу не использовано.
       

        [SerializeField] private bool gotoNextStep = false;

        [SerializeField] private RectTransform showTarget = null;

        private PointerEventData _checkPointer;

        private TutorialObject _currentObject = null;

        private RectTransform rtransform;

        void Start()
        {
            rtransform = rtransform ?? gameObject.GetComponent<RectTransform>();
        }

        private void Awake()
        {
            _checkPointer = new PointerEventData(null);
        }
        
        public string TutorObjectTag
        {
            get { return _tutorObjectTag; }
        }

        public RectTransform ShowTarget
        {
            set
            {
                showTarget = value; 
                Update();
            }
        }

        private void SetToTarget()
        {
            if (showTarget != null)
            {
                transform.position = showTarget.position;
            }
        }

        void OnValidate()
        {
            SetToTarget();
        }

        void Update()
        {
            SetToTarget();
        }

        void OnEnable()
        {
            Update();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.pointerCurrentRaycast.gameObject != null && eventData.pointerCurrentRaycast.gameObject.CompareTag(_tutorAimTag))
            {
                _checkPointer.position = Input.mousePosition;// CrossPlatformInputManager.mousePosition;
               
                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(_checkPointer, results);

                foreach (var raycastResult in results)
                {
                  //  Debug.Log(raycastResult.gameObject.tag);
                    if (raycastResult.gameObject.CompareTag(_tutorObjectTag))
                    {
                        _currentObject = _currentObject ?? raycastResult.gameObject.GetComponent<TutorialObject>();
                        if (_currentObject == null)
                        {
                            Debug.LogErrorFormat("TutorialMask target object must contain TutorialObject , {0}",
                                raycastResult.gameObject.name);
                        }
                        else
                        {
                            _currentObject.pointerEventData = eventData;
                            _currentObject.OnDownState();

                        }
                    }
                }

            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData != null && eventData.pointerCurrentRaycast.gameObject != null 
                && eventData.pointerCurrentRaycast.gameObject.CompareTag(_tutorAimTag))
            {
                    if (_currentObject != null)
                    {
                        if (gotoNextStep)
                        {
                            gameObject.SetActive(false);
                            TutorialManager.Instance.NextStep();
                        }
                        _currentObject.pointerEventData = eventData;
                        _currentObject.OnUpState();
                        _currentObject = null;
                        TutorialManager.Instance.HideBackground();
                       
                       
                }
            }

        }
    }
}
