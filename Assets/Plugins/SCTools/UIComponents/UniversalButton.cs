﻿using System.Collections.Generic;
using SCTools.Helpers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SCTools.UI
{
   public class UniversalButtonEvent : UnityEvent<UniversalButton>{}

    public class UniversalButton:MonoBehaviour,IPointerClickHandler
    {
        [SerializeField] private Text _label;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _hitArea;
        [Header("Toggle")]
        [SerializeField] private bool _isToggle = false;
        [SerializeField] private Image checkMark = null;
        [SerializeField] private Color checkColor = Color.white;
        [SerializeField] private Color uncheckColor = Color.white;
        [SerializeField]private bool _selected = false;

        [Space(10f)]

        [SerializeField] private Selectable[] _selectables;

        [SerializeField] private string clickSound = "click";

        [SerializeField]private string _id = null;
        [SerializeField]private bool _enabled = true;

        private RectTransform rectTransform;

        [SerializeField]private object _data;

        private List<PointerClickHelper> _clickChilds; 

        private UniversalButtonEvent _onCustomClick = new UniversalButtonEvent();

        public UnityEvent OnClick;

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if(_selected == value)return;
                _selected = value;
                if (checkMark != null)
                {
                    checkMark.enabled = _selected;
                }
                else
                {
                    _hitArea.color = _selected ? checkColor : uncheckColor;
                }
            }
        }

   

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                ChangeEnabledState();
            }
        }

        public UniversalButtonEvent OnCustomClick
        {
            get
            {
                return _onCustomClick;
            }
        }

        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public bool IsToggle
        {
            get { return _isToggle; }
        }

        public string GetId()
        {
            return _id;
        }

        public void SetId(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                _id = id;
                gameObject.name = _id;
            }
          
        }

        void Awake()
        {
            _clickChilds = new List<PointerClickHelper>();
            OnClick = new UnityEvent();
        }

        void Start()
        {
            rectTransform = gameObject.GetComponent<RectTransform>();

            SetId(_id);

            if (_selectables == null)
            {
                _selectables = GetComponentsInChildren<Selectable>();
            }

            if (_hitArea == null)
            {
                _hitArea = gameObject.GetComponent<Image>();
                if (_hitArea == null)
                {
                    _hitArea = gameObject.AddComponent<Image>();
                }
                _hitArea.rectTransform.anchorMin = rectTransform.anchorMin;
                _hitArea.rectTransform.anchorMax = rectTransform.anchorMax;
                _hitArea.color = new Color(1f,1f,1f,0f);
            }

            ChangeEnabledState();

           

            if (_isToggle && checkMark != null)
            {
                _clickChilds.Add(checkMark.gameObject.AddComponent<PointerClickHelper>());
                checkMark.enabled = _selected;
            }

            if (_label != null)
            {
                _clickChilds.Add(_label.gameObject.AddComponent<PointerClickHelper>());
                _label.raycastTarget = false;
            }

            if (_icon != null)
            {
                _clickChilds.Add(_icon.gameObject.AddComponent<PointerClickHelper>());
            }

            foreach (var pointerClickHelper in _clickChilds)
            {
                pointerClickHelper.button = this;
            }
        }

        void OnDestroy()
        {
            _clickChilds.Clear();
            _onCustomClick.RemoveAllListeners();
            OnClick.RemoveAllListeners();

        }

        private void ChangeEnabledState()
        {
            if (_hitArea != null)
            {
                _hitArea.raycastTarget = _enabled;
            }
            if (_selectables != null)
            {
                for (int i = 0; i < _selectables.Length; i++)
                {
                    if (_selectables[i] != null)
                    {
                        _selectables[i].interactable = _enabled;
                    }
                   
                }
            }
            if (_clickChilds != null)
            {
                foreach (var pointerClickHelper in _clickChilds)
                {
                    pointerClickHelper.Enabled = _enabled;
                }
            }
             
        }

        public void SetLabel(string text)
        {
            if (_label != null)
            {
                _label.text = text;
            }
        }

        public void SetIcon(Sprite icon)
        {
            if (_icon != null)
            {
                _icon.sprite = icon;
            }
          
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData != null)
            {
                SoundManager.SoundManager.Instance.PlaySFX(clickSound);
            }
            if (_isToggle)
            {
                _selected = !_selected;
                if (checkMark != null)
                {
                    checkMark.enabled = _selected;

                }
                else
                {
                    _hitArea.color = _selected ? checkColor : uncheckColor;
                }
              
            }
            if (OnClick != null)
            {
                OnClick.Invoke();
            }

            if (_onCustomClick != null)
            {
                _onCustomClick.Invoke(this);
            }
        }


        void OnValidate()
        {
            if (_isToggle)
            {

                if (checkMark != null)
                {
                    _selected = checkMark.enabled;
                    checkMark.enabled = !checkMark.enabled;
                }
                else
                {
                    _hitArea.color = _selected ? checkColor : uncheckColor;
                }
            }

            ChangeEnabledState();
        }
    }


 
}
