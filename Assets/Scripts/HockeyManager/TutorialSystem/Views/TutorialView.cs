﻿using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.TutorialSystem.Views
{
    public class TutorialView:MonoBehaviour
    {
        [SerializeField] protected string _stepId;
#pragma warning disable CS0649 // Полю "TutorialView.text1" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text text1;
#pragma warning restore CS0649 // Полю "TutorialView.text1" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "TutorialView._saveStep" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию false.
        [SerializeField] private bool _saveStep;
#pragma warning restore CS0649 // Полю "TutorialView._saveStep" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию false.

#pragma warning disable CS0649 // Полю "TutorialView.tutorialMask" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TutorialMask tutorialMask;
#pragma warning restore CS0649 // Полю "TutorialView.tutorialMask" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "TutorialView.arrow" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject arrow;
#pragma warning restore CS0649 // Полю "TutorialView.arrow" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        [SerializeField] private float _showDelay = 0f;
     
        public string Step
        {
            get { return _stepId; }
        }

        public bool SaveStep
        {
            get { return _saveStep; }
        }

        public Text Text
        {
            get { return text1; }
        }

        public float ShowDelay
        {
            get { return _showDelay; }
        }

        public void OnNextStep()
        {
            TutorialManager.Instance.NextStep();
        }

        public void HideArrow()
        {
            if (arrow != null)
            {
                arrow.gameObject.SetActive(false);
            }
        }

        public void SetShowTarget(RectTransform target)
        {
            if (tutorialMask != null)
            {
                tutorialMask.gameObject.SetActive(true);
                if (arrow != null)
                {
                    arrow.gameObject.SetActive(true);
                }
                tutorialMask.ShowTarget = target;
                
            }

            if (text1 != null)
            {
                text1.transform.parent.gameObject.SetActive(true);
            }

        }
    }
}
