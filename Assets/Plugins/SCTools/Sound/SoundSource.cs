﻿using UnityEngine;
using System.Collections;

namespace SCTools.SoundManager
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundSource : MonoBehaviour
    {
        private AudioSource _source;

        public AudioSource Source
        {
            get
            {
                if(_source == null)
                {
                    _source = GetComponent<AudioSource>();
                }
                return _source;
            }
        }

        void OnDestroy()
        {
            _source = null;
        }
    }

}
