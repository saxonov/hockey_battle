﻿using UnityEngine;
using System.Collections;

namespace SCTools.SoundManager
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(AudioSource))]
    public class SoundTester : MonoBehaviour
    {
        // Use this for initialization
        
        public bool playSound;

        private AudioSource source;

        public AudioClip CurrentClip
        {
            get { return source.clip; }
        }

        void Start()
        {
            source = gameObject.AddComponent<AudioSource>();
            if (Application.isPlaying)
            {
                DestroyImmediate(gameObject);
            }
        }

        private void OnValidate()
        {
            if (playSound && source)
            {
                source.Play();
                playSound = false;
            }
        }
    }

}
