﻿using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Trainings;
using SCTools.EventsSystem;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class TrainingsController: IGameController,ICheckMessages, IGSResponceHandler
    {

        public const string UpdateTrainingsEvent = "TrainingsController.updateTrainingsEvent";
        public const string NewTrainingsNotifyEvent = "TrainingsController.newTrainingsNotifyEvent";

        public const string OnTrainingsListRefreshComplete = "TrainingsController.OnTrainingsListRefreshComplete";

        private IGameRoot _root;

        private List<TrainingModel> _availableTrainings;
        private List<TrainingModel> _currentEffects;

        private bool _needRefresh = true;

        private TrainingModel _currentUpgrade = null;
        
        private int _trainingsCount = 0;

        private int _nextGenerateTrainingsTime = 0;

        private int _refreshCount = 0;

        private TeamController _team;
        private EPSController _epsController;

        public void Initialize(IGameRoot root)
        {
            _root = root;
            _currentEffects = new List<TrainingModel>();
            _epsController = _root.GetController<EPSController>();
            _team = _root.GetController<TeamController>();
        }

        public void Reset()
        {
            _needRefresh = true;
        }

        public TrainingEffect GetTotalEffect()
        {
            TrainingEffect currentEffect = new TrainingEffect();
            _currentEffects.ForEach(effect =>
            {
                if (!effect.IsUpgrading)
                {
                    currentEffect.attack += effect.Attack;
                    currentEffect.deffence += effect.Deffence;

                }
            });
            return currentEffect;
        }

        public int NextGenerateTrainingsTime
        {
            get { return _nextGenerateTrainingsTime; }
        }

        public List<TrainingModel> AvailableTrainings
        {
            get { return _availableTrainings; }
        }

        public int RefreshCount
        {
            get { return _refreshCount; }
        }

        public int TrainingsCount
        {
            get { return _trainingsCount; }
        }


        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.TrainingsData))
            {
                GSData data = scriptData.GetGSData(GS_ScriptData.TrainingsData);

                _refreshCount = data.ContainsKey("refreshCount") ? data.GetInt("refreshCount").Value : _refreshCount;

                _availableTrainings = _availableTrainings ?? new List<TrainingModel>();
                TrainingModel model = null;
                var available = data.GetGSDataList("available");
                if (available != null)
                {
                    _availableTrainings.Clear();
                    foreach (var gsData in available)
                    {
                        model = new TrainingModel(gsData);
                       _availableTrainings.Add(model);
                       
                    }
                   // UpdateEffectTime();
                }

                _trainingsCount = _availableTrainings.Count;
                EventsManager.Instance.Call(OnTrainingsListRefreshComplete);

                bool upgradeEffectExist = false;
                List<GSData> currentEffects = data.GetGSDataList("currentEffects");
                //always exist, empty list
                if (currentEffects != null)
                {
                    _currentEffects.Clear();
                    foreach (var currentEffect in currentEffects)
                    {
                        if (_currentUpgrade != null && _currentUpgrade.UID == currentEffect.GetString("uid"))
                        {
                            upgradeEffectExist = true;
                            _currentUpgrade.Parse(currentEffect);
                        }
                        model = new TrainingModel(currentEffect);
                        _currentEffects.Add(model);
                    }

                    if (_currentUpgrade != null)
                    {
                        //if effect is removed
                        if (!upgradeEffectExist)
                        {
                            _currentUpgrade.Parse(null);
                            _currentUpgrade = null;
                        }
                        else if (!_currentUpgrade.IsUpgrading)
                        {
                            _currentUpgrade = null;
                        }
                    }

                   
                }
              
                _nextGenerateTrainingsTime = data.GetInt("endGenerateTime").HasValue ? 
                                             data.GetInt("endGenerateTime").Value : 
                                             _nextGenerateTrainingsTime;
            }
        }

       

        public TrainingModel GetCurrentUpgrade()
        {
            return _currentUpgrade;
        }

        public void EndTrainingsUpgrade(UnityAction<bool> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("END_TRAINING_PROCESS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _team.NeedUpdateTeamInfo = true;
                    callback(true);
                }
                else
                {
                    _team.RefreshBalance();
                    _needRefresh = true;
                    callback(false);
                }
            });
        }

        public void RefreshTrainings(bool refreshForGold,bool current,UnityAction callback)
        {
            long timerLeft = GameTimer.TimeLeftSeconds(_nextGenerateTrainingsTime);
            if (!_needRefresh && !refreshForGold && timerLeft > 0)
            {
                callback();
                return;
            }
            GSRequestData data = new GSRequestData();
            data.AddBoolean("refresh", refreshForGold);
            data.AddBoolean("current", current);
            var request = new LogEventRequest();
            request.SetEventKey("GET_TRAININGS");
            request.SetEventAttribute("DATA",data);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                  
                  //check active upgrades at start load
                    if (current && _currentUpgrade == null)
                    {
                        _currentUpgrade = _currentEffects.Find(effect => effect.IsUpgrading);
                        if (_currentUpgrade != null)
                        {
                          //  _currentUpgrade.OnTimerLeftSecondsEvent.AddListener(OnTrainingUpgradeComplete);
                            _currentUpgrade.StartUpgradeTimer();
                        }
                    }
                    _needRefresh = false;
                    callback();
                   
                }
            });
        }

        public void UpdateEffectTime()
        {
            _epsController.GetEmployee(EmployeeType.SecondCouch, (employee) =>
            {
                if (employee != null && employee.Level > 0)
                {
                    foreach (var availableTraining in _availableTrainings)
                    {
                        availableTraining.BonusEffectTime =
                            Mathf.FloorToInt(MathUtils.CalcPercentValue(availableTraining.EffectTimeSeconds,
                                employee.CurrentBonusEffect));
                    }

                    if (_currentUpgrade != null)
                    {
                        _currentUpgrade.BonusEffectTime =
                            Mathf.FloorToInt(MathUtils.CalcPercentValue(_currentUpgrade.EffectTimeSeconds,
                                employee.CurrentBonusEffect));
                    }
                }
            });


        }

        public void StartTraining(string uid,bool fast, UnityAction<bool> callback)
        {
            TrainingModel.UpgradeStarted = true;
            //_currentUpgrading = null;
            var request = new LogEventRequest();
            request.SetEventKey("START_TRAINING");
            request.SetEventAttribute("UID", uid);
            request.SetEventAttribute("FAST", fast ? 1 : 0);
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _currentUpgrade = _currentEffects.Find(effect => effect.IsUpgrading);

                    if (!fast)
                    {
                        // _currentUpgrade.OnTimerLeftSecondsEvent.AddListener(OnTrainingUpgradeComplete);
                        _currentUpgrade.StartUpgradeTimer();
                    }
                    callback(true);
                    TrainingModel.UpgradeStarted = false;
                }
                else
                {
                    GSData error = responce.Errors.GetGSData(GS_ScriptData.ErrorData);
                    if (error.GetInt("id").Value == GS_Errors.ERROR_NOT_ENOUGH_ENERGY)
                    {
                        _team.RefreshBalance();
                    }
                    else
                    {
                        _needRefresh = true;
                        EventsManager.Instance.Call(UpdateTrainingsEvent);
                    }
                    callback(false);

                }
            });
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.TrainingAvailableMsg)
            {
                _needRefresh = true;
                if (msgData != null) _nextGenerateTrainingsTime = msgData.GetInt("endTime").Value;
                EventsManager.Instance.Call(UpdateTrainingsEvent);
                EventsManager.Instance.Call(NewTrainingsNotifyEvent);
            }

            if (msgCode == GS_Messages.TrainingEffectEndMsg)
            {
                _needRefresh = true;
                if (_root.IsGameScreen())
                {
                    EventsManager.Instance.Call(UpdateTrainingsEvent);
                }
            }

            if (msgCode == GS_Messages.TrainingUpgradeComplete)
            {

                _needRefresh = true;
                RefreshTrainings(false, true, () =>
                {
                     EventsManager.Instance.Call(UpdateTrainingsEvent);
                });
            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.TrainingAvailableMsg || 
                   msgType == GS_Messages.TrainingEffectEndMsg || 
                   msgType == GS_Messages.TrainingUpgradeComplete;
        }

    }
}
