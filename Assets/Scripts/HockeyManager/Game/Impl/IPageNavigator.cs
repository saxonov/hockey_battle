﻿using HockeyManager.Game.Dict;
using UnityEngine;

namespace HockeyManager.Game.Impl
{
    public interface IPageNavigator
    {
        void AddPageChangeHandler(IPageChangeHandler pageChangeHandler);
        void RemovePageChangeHandler(IPageChangeHandler pageChangeHandler);

        void ReloadSelectedPage();
        void GotoPage(NavigationPageTypes pageType);
        // void GotoPage(string goName);

        Canvas Canvas { get; }

        bool CancelMovePage { get; set; }

        NavigationPageTypes LastVisitedPage { get; }

        NavigationPageTypes SelectedPage { get; }

        void LockControlls();
        void UnLockControlls(float delay = 0);
    }
}
