﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SCTools.Helpers
{
    public class ClickEvent:MonoBehaviour,IPointerClickHandler
    {

        public UnityEvent clickCallback;


        public void OnPointerClick(PointerEventData eventData)
        {
            if (clickCallback != null)
            {
                clickCallback.Invoke();
            }
        }
    }
}
