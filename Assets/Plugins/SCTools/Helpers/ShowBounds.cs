﻿#if(UNITY_EDITOR)
using UnityEditor;
#endif
using UnityEngine;

namespace SCTools.Helpers
{


    [ExecuteInEditMode]
    public class ShowBounds : MonoBehaviour
    {
        private Bounds _bounds;

        private string rendererName;

        public string childName = null;

#if (UNITY_EDITOR)
        [MenuItem("GameObject/Utils/ShowBounds", false, 0)]
        static void AddShowBounds(MenuCommand command)
        {
            GameObject go = (GameObject)command.context;
            go.AddComponent<ShowBounds>();
        }
#endif



        public Bounds Bounds
        {
            get { return _bounds; }
        }

        public string RendererName
        {
            get { return rendererName; }
        }


        // Use this for initialization
        void Start()
        {

        }

        void OnEnable()
        {
            if (Application.isPlaying)
            {
                Destroy(this);

            }
            else if (Application.isEditor)
            {
                UpdateBounds();
            }
        }

        private void Error()
        {
            _bounds = new Bounds(Vector3.zero, Vector3.zero);
            rendererName = "";
        }

        // Update is called once per frame
        public void UpdateBounds()
        {
            if (Application.isEditor)
            {
                if (!string.IsNullOrEmpty(childName))
                {
                    Transform child = transform.FindChild(childName);
                    if (child == null)
                    {

                        childName = null;
                        Error();
                    }
                    else
                    {
                        var col2d = child.GetComponent<Collider2D>();
                        var col = child.GetComponent<Collider>();
                        var rend = child.GetComponent<Renderer>();
                        if (rend)
                        {
                            rendererName = rend.name;
                            _bounds = rend.bounds;
                        }
                        else
                        {

                            if (col)
                            {
                                rendererName = col.name;
                                _bounds = col.bounds;
                            }
                            else if (col2d)
                            {
                                rendererName = col2d.name;
                                _bounds = col2d.bounds;
                            }
                            else
                            {
                                childName = null;
                                Error();
                            }


                        }
                    }

                }
                else
                {
                    var r = GetComponentInChildren<Renderer>();
                    if (r != null)
                    {
                        _bounds = r.bounds;
                        rendererName = r.name;
                    }
                    else
                    {
                        var c2 = GetComponentInChildren<Collider2D>();
                        var c = GetComponentInChildren<Collider>();
                        if (c != null)
                        {
                            _bounds = c.bounds;
                            rendererName = c.name;
                        }
                        else if (c2 != null)
                        {
                            _bounds = c2.bounds;
                            rendererName = c2.name;
                        }
                        else
                        {
                            Error();
                        }
                    }
                }

            }

        }
    }

}
