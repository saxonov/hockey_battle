﻿using HockeyManager.Game.Models.Shop;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Views.Components
{
    public class PurchaseItemBox:MonoBehaviour,IPointerClickHandler
    {
        public const string OnBuyProductEvent = "onBuyProductEvent";

#pragma warning disable CS0649 // Полю "PurchaseItemBox.priceLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text priceLabel;
#pragma warning restore CS0649 // Полю "PurchaseItemBox.priceLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "PurchaseItemBox.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl goldPanel;
#pragma warning restore CS0649 // Полю "PurchaseItemBox.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        
        private PurchaseItemModel _model;
        
        public void SetProduct(PurchaseItemModel product)
        {
            _model = product;
            if (_model.Product != null)
            {
                priceLabel.text = _model.Product.metadata.localizedPriceString;
            }
           
            goldPanel.SetText(_model.GoldToBuy);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_model != null)
            {
                EventsManager.Instance.Call(OnBuyProductEvent, _model);
            }
        }
    }
}
