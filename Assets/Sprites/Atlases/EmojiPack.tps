<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>E:/UNITY/HockeyBattle/Assets/Sprites/Atlases/EmojiPack.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>512</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../EmojiPack.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f32d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f373.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f374.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f37b.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f389.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f393.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f3c5.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f3c6.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f3d2.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f3d6.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f3dd.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f413.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f427.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f43c.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f446.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f451.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f477.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f47a.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f47b.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f47d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f48b.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f490.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4a3.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4a4.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4a5.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4b0.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4b8.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4e2.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f4fa.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f51d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f525.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f54a.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f601.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f602.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f607.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f608.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f60d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f60e.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f60f.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f61a.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f61d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f621.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f62b.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f62c.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f62d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f64f.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f694.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6ab.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6cb.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6e1.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6e3.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6e9.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f6eb.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f911.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f918.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f91e.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f922.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f947.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f948.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f949.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f94a.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f98d.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/1f9c0.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/2615.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/2620.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/2622.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/2623.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/26bd.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Эмодзи/270a.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>D:/Dropbox/HockeyBattle/Эмодзи</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
