[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1016:MarkAssembliesWithAssemblyVersion")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Scope = "type", Target = "HockeyManager.Game.GameRoot")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1405:ComVisibleTypeBaseTypesShouldBeComVisible", Scope = "type", Target = "HockeyManager.Game.TimerLeftSecondsEvent")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "type", Target = "HockeyManager.Game.Controllers.CompetitionController")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "HockeyManager.Game.Controllers.CompetitionController.#Dispose()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "type", Target = "HockeyManager.Game.Controllers.TeamController")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "HockeyManager.Game.Controllers.TeamController.#Dispose()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Dict.BaseGameObjectSettingsInfo.#_previews")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Dict.IconData.#icon")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Dict.RewardIconData.#icon")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Models.Map.BuildingModel.#_effect")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1405:ComVisibleTypeBaseTypesShouldBeComVisible", Scope = "type", Target = "HockeyManager.Game.UI.Components.LogoImage")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "type", Target = "HockeyManager.Game.UI.Popups.BasePopup")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopup.#Dispose()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#controls")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#holders")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#noButton")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#textField")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#title")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.UI.Popups.BasePopupControls.#yesButton")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Views.Popups.CompetitionRewardsPopup+WinnerPostiionRewards.#goldRewards")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields", Scope = "member", Target = "HockeyManager.Game.Views.Popups.CompetitionRewardsPopup+WinnerPostiionRewards.#moneyReward")]
// Этот файл используется анализом кода для поддержки атрибутов 
// SuppressMessage, примененных в этом проекте.
// Подавления на уровне проекта либо не имеют целевого объекта, либо для них задан 
// конкретный объект и область пространства имен, тип, член и т. д.
//
// Чтобы добавить подавление к этому файлу, щелкните правой кнопкой мыши сообщение в результатах анализа кода, 
// выберите команду "Подавить сообщение" и щелкните пункт 
// "В файле блокируемых сообщений".
// Нет необходимости вручную добавлять подавления к этому файлу.

