﻿#undef TEST_LOGIN_USER

using System;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

using HockeyManager.Game.Dict;
using HockeyManager.Game.GameSparks.Impl;
#if UNITY_IOS || UNITY_EDITOR
using SA.Common.Models;
#endif 
using SCTools.EventsSystem;
using UnityEngine;

namespace HockeyManager.Game.Managers
{
    public class GSAuthorizationBehavior:MonoBehaviour,IGSAuthentication
    {
        private const string SocialAccountLoginTypePrefKey = "SocialAccountLoginTypePrefKey";

        private SocialAccountType _loginType = SocialAccountType.None;

        private Action<AuthenticationResponse> OnLoginResultCallback;

        private bool _isStartConnectionTOSocialPlatform = false;

#if UNITY_ANDROID
        private bool _googlePlayGSLogined = false;
        private bool _googlePlayLogined = false;
        //private bool _waitForAuthToken = false;
#endif
#if UNITY_IOS || UNITY_EDITOR
    
        private bool _gameCenterLogined = false;
#endif

        private bool _deviceAuthtorizationComplete = false;

        public SocialAccountType LoginType
        {
            get { return _loginType; }
            set { _loginType = value; }
        }

        public bool IsStartConnectionToSocialPlatform
        {
            get { return _isStartConnectionTOSocialPlatform; }
        }

        void Awake()
        {
            LoadSaveLoginType();

#if UNITY_ANDROID && !UNITY_EDITOR

            PlayGamesPlatform.DebugLogEnabled = false;
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration
                .Builder()
                 .AddOauthScope("https://www.googleapis.com/auth/games")
                 .RequestServerAuthCode(false)
            .Build();

            PlayGamesPlatform.InitializeInstance(config);
          
            PlayGamesPlatform.Activate();
#endif
            

#if UNITY_IOS
            if(_loginType == SocialAccountType.None)
            {
                _loginType = SocialAccountType.GameCenter;
            }
#endif
            Debug.LogFormat("Authentication type loaded: {0}", _loginType.ToString());
        }

        private void LoadSaveLoginType(bool reset = false)
        {
            if (PlayerPrefs.HasKey(SocialAccountLoginTypePrefKey) && !reset)
            {
                
                _loginType = (SocialAccountType)PlayerPrefs.GetInt(SocialAccountLoginTypePrefKey);

            }
            if (reset)
            {
                PlayerPrefs.DeleteKey(SocialAccountLoginTypePrefKey);
            }
            PlayerPrefs.SetInt(SocialAccountLoginTypePrefKey, (int)_loginType);
            PlayerPrefs.Save();
            Debug.Log("Authentication type setted: " + _loginType);
        }

        public void Login(Action<AuthenticationResponse> callback)
        {
            OnLoginResultCallback = callback;
            Login();
        }

        public void Login()
        {
            
            switch (_loginType)
            {
                case SocialAccountType.GooglePlayGames:
                    GooglePlayLogin();
                    break;
                case SocialAccountType.GameCenter:
                    GameCenterLogin();
                    break;
                default:
                    if (!_deviceAuthtorizationComplete)
                    {
                        _deviceAuthtorizationComplete = true;
                        DeviceAuthorization();
                    }
                break;
            }
            LoadSaveLoginType(true);
        }


        #region  GuestLogin



        private void DeviceAuthorization()
        {
            EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended,0);

#if (TEST_LOGIN_USER)

                                  // new AuthenticationRequest().SetUserName("Test2").SetPassword("test").Send(OnLoginResultHandler);
#else
            var request = new DeviceAuthenticationRequest();
            request.SetScriptData(CreateLoginScriptData());
            request.Send((responce) =>
            {
                if (responce.HasErrors)
                {
                    EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);

                }
                if (OnLoginResultCallback != null)
                {
                    OnLoginResultCallback.Invoke(responce);

                }
                OnLoginResultCallback = null;
                EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,0);
            });
            //Debug.LogFormat("GS: Login data:{0}",request.JSONString);
#endif
        }

      

        #endregion

        #region  GPG



        private void GooglePlayLogin()
        {

#if UNITY_ANDROID

            if (!_googlePlayGSLogined)
            {
                _googlePlayGSLogined = true;
                _isStartConnectionTOSocialPlatform = true;
                Debug.Log("Start GooglePlayGame login");
                _googlePlayLogined = PlayGamesPlatform.Instance.IsAuthenticated();
                if (_googlePlayLogined)
                {

                    Debug.LogFormat("Google play authenticated already! name:{0}",
                     PlayGamesPlatform.Instance.GetUserDisplayName());
                    SendGooglePlayGsRequest();
                }
                else
                {
                    Social.localUser.Authenticate((success, result) =>
                    {

                        if (success)
                        {
                            _googlePlayLogined = true;
                            Debug.LogFormat("GPG authentication success! " + result + " name:" +
                                            PlayGamesPlatform.Instance.GetUserDisplayName());
                            SendGooglePlayGsRequest();
                        }
                        else
                        {
                            EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                            _isStartConnectionTOSocialPlatform = false;
                            Debug.LogFormat("Social: {0} Authentication Error: {1}", _loginType.ToString(), result);
                            _loginType = SocialAccountType.None;
                            Login();
                        }
                    });
                }
            }
            else
            {
                EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                LoadSaveLoginType(true);
            }
#endif
        }

#if UNITY_ANDROID
        private void SendGooglePlayGsRequest()
        {
            string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            //string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            if (!string.IsNullOrEmpty(authCode))
            {
                //Debug.LogFormat("Send GooglePlayConnectRequest: token:{0}", authCode);
                EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended,0);
                var request = new GooglePlayConnectRequest();
                request.SetDoNotLinkToCurrentPlayer(false);
                request.SetDisplayName(PlayGamesPlatform.Instance.GetUserDisplayName());
                request.SetCode(authCode);
                request.SetErrorOnSwitch(false);
                request.SetRedirectUri("https://www.gamesparks.com/oauth2callback");
                request.SetScriptData(CreateLoginScriptData());
                request.Send((responce) =>
                {
                    
                    if (responce.HasErrors)
                    {
                        EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                        EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                        Debug.LogError("GS: Google play login error!(Guest login) " + responce.Errors.JSON);
                        _googlePlayGSLogined = false;
                        _loginType = SocialAccountType.None;
                        Login();
                    }
                    else
                    {
                     
                        Debug.Log("GS: Google play login success! " + responce.JSONString);
                        EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, true);
                        if (OnLoginResultCallback != null)
                        {
                            OnLoginResultCallback.Invoke(responce);
                        }
                        OnLoginResultCallback = null;

                        EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,0);
                    }
                   
                    _isStartConnectionTOSocialPlatform = false;
                });
            }
            else
            {
                EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                Debug.Log("GPG Auth code or token is empty or Null! Login as quest");
                _googlePlayGSLogined = false;
                _loginType = SocialAccountType.None;
                Login();
            }

        }

#endif

        #endregion

        #region  GameCenter

        private void GameCenterLogin()
        {
#if UNITY_IOS || UNITY_EDITOR
            _isStartConnectionTOSocialPlatform = true;

            //not sure if enter here
            if (GameCenterManager.IsInitialized)
            {
                Debug.Log("GameCenter already authenticated!");

                GameCenterManager.OnPlayerSignatureRetrieveResult += SendGameCenterLoginRequest;
                GameCenterManager.RetrievePlayerSignature();
                return;
            }
            GameCenterManager.OnAuthFinished += OnGameCenterAuthComplete;
            GameCenterManager.Init();

           
#endif
        }

#if UNITY_EDITOR || UNITY_IOS
        /// <summary>
        /// GameCenter Login callback handler
        /// </summary>
        /// <param name="result"></param>
        /// <param name="PublicKey"></param>
        private void OnGameCenterAuthComplete(Result result)
        {
            GameCenterManager.OnAuthFinished -= OnGameCenterAuthComplete;
            if (result.IsSucceeded)
            {
                Debug.Log("GameCenter Authentication success!");
                GameCenterManager.OnPlayerSignatureRetrieveResult += SendGameCenterLoginRequest;
                GameCenterManager.RetrievePlayerSignature();
            }
            else
            {
                _gameCenterLogined = false;
                EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                _isStartConnectionTOSocialPlatform = false;
                Debug.LogErrorFormat("Social: {0} Authentication Fail, Error code: {1}:{2}", _loginType.ToString(), result.Error.Code, result.Error.Message);
                _loginType = SocialAccountType.None;
                Login();
            }
        }

        private void SendGameCenterLoginRequest(GK_PlayerSignatureResult result)
        {
            GameCenterManager.OnPlayerSignatureRetrieveResult -= SendGameCenterLoginRequest;
            if (!_gameCenterLogined)
            {
                if (result.IsSucceeded)
                {
                    string signature = Convert.ToBase64String(result.Signature);
                    string salt = Convert.ToBase64String(result.Salt);
                   // Debug.LogFormat("<p>GameCenter retrieve signature cumplete, publicKey:{0}<br>,timestamp:{1}<br>,salt:{2}<br>,signature:{3}</p>", result.PublicKeyUrl,result.Timestamp,salt,signature);
                    _gameCenterLogined = true;
                    EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended,0);

                    //start G
                    var request = new GameCenterConnectRequest();
                    request.SetDoNotLinkToCurrentPlayer(false);
                    request.SetExternalPlayerId(GameCenterManager.Player.Id);
                    request.SetDisplayName(GameCenterManager.Player.DisplayName);
                    request.SetErrorOnSwitch(false);
                    request.SetSignature(signature);
                    request.SetSalt(salt);
                    request.SetTimestamp(result.Timestamp);
                    request.SetPublicKeyUrl(result.PublicKeyUrl);
                    request.SetScriptData(CreateLoginScriptData());
                    request.Send((responce) =>
                    {
                        if (responce.HasErrors)
                        {
                            _gameCenterLogined = false;
                            EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                            EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                            Debug.LogFormat("<p>GS: GameCenter login error(Guest login):{0}</p>", responce.Errors.JSON);

                            _loginType = SocialAccountType.None;
                            Login();
                        }
                        else
                        {
                            EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, true  );
                            Debug.LogFormat("<p>GS: GameCenter login success:{0}</p>", responce.AuthToken);

                            if (OnLoginResultCallback != null)
                            {
                                OnLoginResultCallback.Invoke(responce);

                            }
                            OnLoginResultCallback = null;
                            EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,0);
                        }

                        _isStartConnectionTOSocialPlatform = false;
                    });
                }
                else
                {
                    EventsManager.Instance.Call(GlobalEventTypes.SocialAccountConnectedResult, false);
                    _gameCenterLogined = false;
                    Debug.LogFormat("<p>GameCenter retrieve auth info error(Guest login):{0}:{1}</p>", result.Error.Code, result.Error.Message);

                    _loginType = SocialAccountType.None;
                    Login();
                }

            }
        }
#endif

        #endregion
        private GSRequestData CreateLoginScriptData()
        {
            var requestData = new GSRequestData();
            requestData.Add("DeviceOS", SystemInfo.operatingSystem);
            requestData.Add("Version", Application.version);
            requestData.Add("LoginType",_loginType.ToString());
            return requestData;
        }

    }
}
