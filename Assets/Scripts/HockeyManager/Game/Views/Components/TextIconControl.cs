﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Components
{

    public class TextIconControl : MonoBehaviour
    {
        [SerializeField]
        private Text _label;

        private Image _icon;

        private Tweener _textTweener1;
        private Tweener _textTweener2;

        private Tweener _iconTweener;
        
        void Awake()
        {
            _icon = GetComponentInChildren<Image>();
        }

        public Image GetIcon()
        {
            return _icon;
        }

        public void SetIcon(Sprite sp)
        {
            if (_icon != null)
            {
                _icon.gameObject.SetActive(sp != null);
                _icon.sprite = sp;
            }
            
        }

        public Tweener AnimateIcon(int count = 1)
        {
            if (_icon && (_iconTweener == null || !_iconTweener.IsActive()))
            {
                _iconTweener = _icon.rectTransform.DOPunchScale(Vector3.one*1.02f, 0.1f, count).SetAutoKill(true);
            }
            return _iconTweener;
        }

        public void SetText(string value,char separator,params int[] values)
        {
            string targetValue = value;
            for (int i = 0; i < values.Length; i++)
            {
                targetValue += value[i] + separator;
            }
            _label.text = targetValue;
        }

        public void SetText(string value)
        {
            _label.text = value;
        }

        public void SetText(int value)
        { 
            _label.text = value.ToString();
        }

        public void SetText(float value)
        {
            _label.text = value.ToString("F0");
        }

        public Tweener AnimateValue(int value,float duration = 1f)
        {
            //if (_textTweener1 != null && (_textTweener1.IsPlaying() || _textTweener1.IsActive()))
            //{
            //    _textTweener1.Kill();
            //}
            int targetValue;
            if (int.TryParse(_label.text,out targetValue))
            {
                _textTweener1 = DOTween.To(() => targetValue, v => targetValue = v, value, 1f).OnUpdate(() =>
                {
                    _label.text = targetValue.ToString();

                }).SetAutoKill(true);
                return _textTweener1;
            }
            return null;
        }
        public Tweener AnimateValue(float duration,char separator,int value1,int value2)
        {
            //if (_textTweener2 != null && (_textTweener2.IsPlaying() || _textTweener2.IsActive()))
            //{
            //    _textTweener2.Kill();
            //}
            int targetValue = 0;
            int index = _label.text.IndexOf(separator.ToString(), StringComparison.Ordinal);
            if (index >= 0)
            {
                int.TryParse(_label.text.Substring(0, index), out targetValue);
            }
           
            _textTweener2 = DOTween.To(() => targetValue, v => targetValue = v, value1, 1f).OnUpdate(() =>
                {
                    _label.text = targetValue.ToString() + separator + value2;

                }).SetAutoKill(true);

            return _textTweener2;

        }
    }

}
