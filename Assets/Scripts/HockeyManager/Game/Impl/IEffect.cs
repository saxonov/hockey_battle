﻿using GameSparks.Core;

namespace HockeyManager.Game.Impl
{
    public interface IEffect
    {

        float GetEffectValue(string prop);
        float GetEffectValue(params string[] prop);

      
    }
}
