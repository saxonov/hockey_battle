﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace SCTools.Helpers
{
    [RequireComponent(typeof(Canvas))]
    public class UILogger:MonoBehaviour
    {
        [SerializeField]private string email;
        private static UILogger _instance;

        private StringBuilder logs;


        [SerializeField] private GameObject holder;
        [SerializeField] private Text logTextField;
        [SerializeField] private Button sendMailButton;

        private string warningFormat = "<color='#FFFF00FF'>{0}</color>";
        private string errorFormat = "<color='#FF0000FF'>{0}</color>";

        public bool enableLogs = true;

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            Init();
        }

        private void Init()
        {
            
        }

        void OnDestroy()
        {
            _instance = null;
            logs = null;
        }

        void Start()
        {
            if (!enableLogs)
            {
                Destroy(gameObject);
            }
            else
            {
                logTextField.supportRichText = true;
            }
        }


        void OnEnable()
        {
            Application.logMessageReceived += OnLogsRecievedHandler;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= OnLogsRecievedHandler;
        }

        public void OnSendMailButton()
        {
            try
            {
                UnityUtils.SendEmail(email, "Test", logs.ToString());
            }
            catch (Exception e)
            {
               Debug.LogException(e);
            }
           
        }

        private void OnLogsRecievedHandler(string condition, string stacktrace, LogType type)
        {
            string typeStr = type.ToString();

            if (type == LogType.Warning)
            {
                logs.Append(string.Format(warningFormat, condition)).AppendLine();

            }else if (type == LogType.Exception)
            {
                logs.Append(string.Format(errorFormat, condition)).AppendLine();
                logs.Append(string.Format(errorFormat, stacktrace)).AppendLine();
            }
            else
            {
                logs.Append(condition).AppendLine();
            }

            logTextField.text = logs.ToString();
        }


        public void Show()
        {
            holder.gameObject.SetActive(true);
            logTextField.text = logs.ToString();
        }

        public void Hide()
        {
            holder.gameObject.SetActive(false);
            logTextField.text = "";
            
        }


    }
}
