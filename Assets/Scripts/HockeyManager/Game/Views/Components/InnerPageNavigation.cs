﻿using System.Collections.Generic;
using HockeyManager.Game.Dict;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Components
{
    [RequireComponent(typeof(ToggleGroup))]
    public class InnerPageNavigation:MonoBehaviour
    {

#pragma warning disable CS0649 // Полю "InnerPageNavigation.toggleButtonPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject toggleButtonPrefab;
#pragma warning restore CS0649 // Полю "InnerPageNavigation.toggleButtonPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "InnerPageNavigation.fakeButtonPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject fakeButtonPrefab;
#pragma warning restore CS0649 // Полю "InnerPageNavigation.fakeButtonPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "InnerPageNavigation.spacerPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject spacerPrefab;
#pragma warning restore CS0649 // Полю "InnerPageNavigation.spacerPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "InnerPageNavigation.provider" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private InnerPageButtonData[] provider;
#pragma warning restore CS0649 // Полю "InnerPageNavigation.provider" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        
        private ToggleGroup group;

        private RectTransform holder;

        private List<Toggle> _childs = new List<Toggle>();

        void Awake()
        {
            
            group = GetComponent<ToggleGroup>();
            holder = GetComponent<RectTransform>();

            Build();
        }


        void OnEnable()
        {
            if (!GameData.IsTutorialPassed)
            {
               EventsManager.Instance.Add(TutorialManager.TutorialStepChanged,OnTutorialStepChanged); 
            }
        }

        void OnDisable()
        {
            EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
        }

        private void Clear()
        {
            while (_childs.Count > 0)
            {
                    Toggle toggle = _childs[0];
                    toggle.onValueChanged.RemoveAllListeners();
                    group.UnregisterToggle(toggle);
                
                Destroy(_childs[0].gameObject);
                _childs.RemoveAt(0);
            }
            holder.RemoveChildren();
        }

        private void Build()
        {
            Clear();

            for (int i = 0; i < provider.Length; i++)
            {
                if (!string.IsNullOrEmpty(provider[i].localeLabel))
                {
                    if (provider[i].gotoPage != NavigationPageTypes.None)
                    {
                        AddButton(provider[i].localeLabel,provider[i].isSelected,provider[i].gotoPage,i+1);
                        
                    }
                    else
                    {
                        AddFakeButton(provider[i].localeLabel);
                    }
                }
                else
                {
                    AddSpacer();
                }
            }
        }

        private void AddFakeButton(string localeLabel)
        {
            RectTransform child = holder.AddChild(fakeButtonPrefab, false);
            child.GetComponentInChildren<UITextLocalizator>().SetLocaleKeys(true,localeLabel);
        }

        private void AddSpacer()
        {
            RectTransform spacer = holder.AddChild(spacerPrefab,false);
            spacer.name = "spacer";
        }

        private void AddButton(string localeLabel,bool select,NavigationPageTypes pageType,int num)
        {
            Toggle toggle = holder.AddChild(toggleButtonPrefab, false).GetComponent<Toggle>();
            group.RegisterToggle(toggle);
           
            toggle.group = group;
            toggle.isOn = select;
            toggle.gameObject.name = "button_" + num;
            toggle.gameObject.AddComponent<NavigateToPage>().SetPage(pageType);
            toggle.GetComponentInChildren<UITextLocalizator>().SetLocaleKeys(true,localeLabel);
            _childs.Add(toggle);

            if (!GameData.IsTutorialPassed)
            {
                toggle.gameObject.tag = "TutorInnerNavButton" + num;
                toggle.gameObject.AddComponent<TutorialToggle>();
                OnTutorialStepChanged();
            }
        }
      
        public void SelectButton(int number, bool value)
        {
            Toggle child = _childs.Find(c => c.gameObject.name == "button_" + number);
            if (child != null && child.isOn != value)
            {
                child.isOn = value;
            }
            else
            {
                if (number <= provider.Length && number > 0)
                {
                    provider[number-1].isSelected = true;
                }
            }
        }

        public void ShowButton(int number, bool value)
        {
            Toggle child = _childs.Find(c => c.gameObject.name == "button_" + number);
            if (child != null)
            {
                child.gameObject.SetActive(value);
            }
        }

        public void EnableIndicator(int number, bool value)
        {
            Toggle child = _childs.Find(c => c.gameObject.name == "button_" + number);
            if (child != null)
            {
                Transform  indicator = child.transform.FindChild("HBox").FindChild("Indicator");
                if (indicator != null)
                {
                    indicator.gameObject.SetActive(value);
                }

            }
        }

        public void LockNavButtons(bool lockValue)
        {
            _childs.ForEach((t)=>
                {
                   t.interactable = !lockValue;
                }
            );
          
        }

        public void EnableButton(int number, bool value)
        {
           Toggle child = _childs.Find(c => c.gameObject.name == "button_" + number);
            if (child != null)
            {
                child.interactable = value;
            }
        }



        private void OnTutorialStepChanged()
        {
            //hack fix
            if (GameData.IsTutorialPassed) return;
            if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.GotoPersonalPage))
            {
                Toggle button = _childs.Find((b) => b.gameObject.name == "button_2");
                if (button != null)
                {
                    TutorialManager.Instance.CurrentView.SetShowTarget(button.graphic.rectTransform);
                }
            }
            else if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.GotoSkillsPage))
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                Toggle button = _childs.Find((b) => b.gameObject.name == "button_3");
                if (button != null)
                {
                    TutorialManager.Instance.CurrentView.SetShowTarget(button.graphic.rectTransform);
                }
            }
        }
    }
}
