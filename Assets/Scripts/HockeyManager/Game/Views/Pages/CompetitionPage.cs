﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Competitions;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Pages
{
    public class CompetitionPage :MonoBehaviour
    {
        [SerializeField] private ScrollRect competitionsScroller;
        [SerializeField] private GameObject competitionItemRendererPrefab;
        [SerializeField] private GameObject competitionWinnerItemRendererPrefab;
        [SerializeField] private GameObject emptyItemRendererPrefab;

        [SerializeField] private InnerPageNavigation navBar;

        //[Range(25,120)]
        //[SerializeField] private int refreshRateSeconds = 25;

        //private long _endUpdateTime = 0;

        private CompetitionController _controller;
        // Use this for initialization
        void Awake()
        {
            _controller = GameRoot.Instance.GetController<CompetitionController>();
            CompetitionItemRenderer.winnerItemRendererPrefab = competitionWinnerItemRendererPrefab;
            CompetitionItemRenderer.emptyItemRendererPrefab = emptyItemRendererPrefab;
        }

        void Start()
        {
            _controller.CheckWinners();
        }

        private void OnCompetitionExpiredHandler()
        {
           _controller.LoadCompetitions(BuildCompetitionList,true);
        }

        void OnEnable()
        {
            EventsManager.Instance.Add(CompetitionItemRenderer.OnCompetitionTimerEndEvent, OnCompetitionExpiredHandler);
            EventsManager.Instance.Add<string, int>(CompetitionController.CompetitionWinEvent2Args, OnCompetitionWinHandler);

            EventsManager.Instance.Add(CompetitionController.NeedUpdateCompetitionsEvent, UpdateCompetitions);
          //  PageNavigatorManager.OnPageChangedEvent.AddListener(OnPageNavigationChanged);
        }

        void OnDisable()
        {
         //   PageNavigatorManager.OnPageChangedEvent.RemoveListener(OnPageNavigationChanged);

            EventsManager.Instance.Remove(CompetitionItemRenderer.OnCompetitionTimerEndEvent, OnCompetitionExpiredHandler);
            EventsManager.Instance.Remove(CompetitionController.NeedUpdateCompetitionsEvent, UpdateCompetitions);
            EventsManager.Instance.Remove<string, int>(CompetitionController.CompetitionWinEvent2Args, OnCompetitionWinHandler);
        }
      
        private void OnCompetitionWinHandler(string type, int position)
        {
            if(!GameData.IsTutorialPassed)return;

            string title = "";
            Sprite icon = GameSettings.Instance.GetIconTextCompetitionType(type,out title);

            string positionStr =  position.ToString().GetUITextSizeFormated(100).AddPrefix('\n');
            string placeStr = LocaleManager.Instance.GetText("game.main.competitions.winposition", positionStr);
            PopupsManager.Instance.AddPopup(PopupType.CompetitionWinnerPopup)
                .SetText(placeStr)
                .SetTextIconControls(title.ToUpper())
                .SetTextIconControls(icon);

            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Competition)
            {
                _controller.LoadCompetitions(BuildCompetitionList);
            }
        }

        private void UpdateCompetitions()
        {
            if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Competition)
            {
                _controller.LoadCompetitions(BuildCompetitionList);

            }
        }

        public void OnShow()
        {
            _controller.LoadCompetitions(BuildCompetitionList);
        }

        public void OnHide()
        {
            competitionsScroller.content.RemoveChildren();
        }
       
        private void BuildCompetitionList()
        {
            competitionsScroller.content.RemoveChildren();
            List<CompetitionModel> competitions = _controller.Competitions;
            if(competitions == null)return;
            competitions.Sort();
            for (int i = 0; i < competitions.Count; i++)
            {
                if (competitions[i] == null) continue;
                CompetitionItemRenderer item =
                    competitionsScroller.content.AddChild(competitionItemRendererPrefab, false)
                        .GetComponent<CompetitionItemRenderer>();
                item.SetModel(competitions[i]);
            }
        }
    }


}
