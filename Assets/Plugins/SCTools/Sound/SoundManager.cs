﻿using System.Collections.Generic;
using UnityEngine;

namespace SCTools.SoundManager
{
    public class SoundManager : MonoBehaviour
    {
       // private const string MutePrefKey = "SoundManager.mutePrefKey";
       
        [SerializeField]private SoundData[] sfxLibrary;
        [SerializeField]private SoundData[] musicLibrary;
        
        [SerializeField]private AudioSource sfxSource;
        [SerializeField]private AudioSource musicSource;
        [SerializeField]private float _fadeMusicVolumeSpeed = 0.05f;

        private bool _muteSounds;
        private bool _muteSFX = false;
        private bool _muteMusic = false;

        private static bool _initialized = false;
        private static SoundManager _instance;

        private List<AudioSource> _musicSources;

       // private Dictionary<GameObject, AudioSource> cachedSources;

        private bool _isFadeVolumeNow = false;
        private float _fadeVolumeToValue = 0f;
        private int _fadeVolumeDirection =1;

        public static SoundManager Instance
        {
            get { return _instance; }
        }

        public bool MuteMusic
        {
            set
            {
                _muteMusic = value;

                musicSource.mute = _muteMusic;

                foreach (var msource in _musicSources)
                {
                    msource.mute = _muteMusic;
                }
             }
            get { return _muteMusic; }
        }

        public bool MuteSFX
        {
            set
            {
                _muteSFX = value;
                sfxSource.mute = value;
            }
            get { return _muteSFX; }
        }

        public bool MuteSounds
        {
            get { return _muteSounds; }
            set
            {
                if(_muteSounds == value)return;
                _muteSounds = value;
                if (_muteSounds)
                {
                    MuteAllSounds();
                }
                else
                {
                    UnMuteAllSounds();
                }
            }
        }

        public void MuteAllSounds()
        {
            _muteSounds = true;
            AudioListener.pause = true;
            AudioListener.volume = 0;
        }
        public void UnMuteAllSounds()
        {
            _muteSounds = false;
            AudioListener.pause = false;
            AudioListener.volume = 1f;
        }
        


        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;

            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
            
            if (!_initialized)
            {
                _initialized = true;

                _musicSources = new List<AudioSource>();
            }
        }


        public void ClearMusicSources()
        {
            if (_musicSources != null)
            {
                _musicSources.Clear();
            }
        }

        public void FadeMusicVolume(string soundName,float volume = 0.5f)
        {
            if(musicSource.volume == volume)return;
            _fadeVolumeToValue = volume;
            if (musicSource.volume > _fadeVolumeToValue)
            {
                _fadeVolumeDirection = -1;
            }
            else
            {
                _fadeVolumeDirection = 1;
            }
            _isFadeVolumeNow = true;

        }

        void Update()
        {
            if (_isFadeVolumeNow)
            {
                musicSource.volume += (Time.deltaTime*_fadeMusicVolumeSpeed)*_fadeVolumeDirection;
                if ((musicSource.volume - _fadeVolumeToValue) <= 0.01f)
                {
                    musicSource.volume = _fadeVolumeToValue;
                    _isFadeVolumeNow = false;
                }
            }
        }

        public void StopMusic(string soundName)
        {
            if (musicSource.isPlaying && musicSource.clip.name == soundName)
            {
                musicSource.Stop();
            }
            else
            {
                foreach (var msource in _musicSources)
                {
                    if (msource.clip != null && msource.clip.name == soundName)
                    {
                        msource.Stop();
                        _musicSources.Remove(msource);
                        break;
                    }
                }
            }

         
        }

        public void PlayMusicOnGameobject(string soundName,GameObject go, float volume = 1f)
        {
            if(go.Equals(gameObject))return;
            AudioSource msource = _musicSources.Find((s) => s.gameObject != null && s.gameObject.Equals(go));
            for (int i = 0; i < musicLibrary.Length; i++)
            {
                if (musicLibrary[i].soundName == soundName)
                {
                    if (msource == null)
                    {
                        msource = go.GetComponent<AudioSource>();
                        if (msource == null)
                        {
                            msource = go.AddComponent<AudioSource>();
                        }
                        _musicSources.Add(msource);
                    }
                  
                    musicLibrary[i].soundClip.name = musicLibrary[i].soundName;
                    msource.clip = musicLibrary[i].soundClip;
                    msource.volume = musicLibrary[i].volume >= 0f ? musicLibrary[i].volume : volume;
                    msource.mute = _muteMusic;
                    if (!msource.isPlaying)
                    {
                        msource.Play();
                    }
                    return;
                }
            }

            Debug.LogWarning("Play Music sound name not found: " + soundName);
        }

        public void PlayMusic(string soundName, float volume = 1f,bool loop = false,bool playOnAwake = false)
        {
            for (int i = 0; i < musicLibrary.Length; i++)
            {
                if (musicLibrary[i].soundName == soundName)
                {
                    musicLibrary[i].soundClip.name = musicLibrary[i].soundName;
                    musicSource.playOnAwake = playOnAwake;
                    musicSource.loop = loop;
                    musicSource.clip = musicLibrary[i].soundClip;
                    musicSource.volume = musicLibrary[i].volume >= 0f ? musicLibrary[i].volume : volume;
                    musicSource.mute = _muteMusic;

                    if (!musicSource.isPlaying)
                    {
                        musicSource.Play();
                    }
                    return;
                }
            }
            Debug.LogWarning("Play Music sound name not found: " + soundName);
        }

        

        public void PlaySFXOnGameObject(string soundName,GameObject go)
        {
            if(go.Equals(gameObject))return;

            for (int i = 0; i < sfxLibrary.Length; i++)
            {
                if(sfxLibrary[i].soundName == soundName)
                {
                   SoundSource source = go.GetComponent<SoundSource>();
                   if(source == null)
                    {
                        source = go.AddComponent<SoundSource>();
                    }

                    if (sfxLibrary[i].volume > 0)
                    {
                        source.Source.volume = sfxLibrary[i].volume;
                    }
                    source.Source.mute = _muteSFX;
                    if (source.Source.loop)
                    {
                        source.Source.clip = sfxLibrary[i].soundClip;
                        source.Source.Play();
                    }
                    else
                    {
                        source.Source.PlayOneShot(sfxLibrary[i].soundClip);

                    }
                    return;
                }
            }
            Debug.LogWarning("Play SFX sound name not found: " + soundName);
        }

        public void OnPlaySFX(string soundName)
        {
            PlaySFX(soundName, 1f);
        }

        public void PlaySFX(string soundName, float pitch = 1f, float volume = 1f)
        {
            for (int i = 0; i < sfxLibrary.Length; i++)
            {
                if (sfxLibrary[i].soundName == soundName)
                {
                    sfxSource.loop = false;
                    sfxSource.pitch = pitch;
                    if (sfxLibrary[i].volume > 0)
                    {
                        sfxSource.PlayOneShot(sfxLibrary[i].soundClip, sfxLibrary[i].volume);
                    }
                    else
                    {
                        sfxSource.PlayOneShot(sfxLibrary[i].soundClip, volume);
                    }
                  
                    return;
                }

            }
            Debug.LogWarning("Play SFX sound name not found: " + soundName);
        }
    }


}
