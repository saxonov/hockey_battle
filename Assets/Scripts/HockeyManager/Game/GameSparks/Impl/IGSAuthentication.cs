﻿using HockeyManager.Game.Dict;

namespace HockeyManager.Game.GameSparks.Impl
{
    public interface IGSAuthentication
    {
        void Login();
      
        SocialAccountType LoginType { get; set; }
    }
}
