﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Base
{
    public class AttackTeamModel: IGSParse
    {
        protected int _matchWins;
        protected int _matchLost;
        protected int _cupsWins;
        protected int _cupsLost;
        protected int _totalGoals;

        protected int _competitionsWins;
        protected int _competitionsLost;

        protected int _level;
        protected int _totalExp;

        private GSData _rawData;

        protected RankType _rank = RankType.Beginner;
        
        public bool isOnline;

        public bool IsCanChallenged { get; private set; }

        protected TeamInfoModel _teamInfo;

        public TeamInfoModel TeamInfo
        {
            get { return _teamInfo; }
            set { _teamInfo = value; }
        }

        public int TotalExp
        {
            get { return _totalExp; }
        }


        public int Level
        {
            get { return _level; }
        }

        public RankType Rank
        {
            get { return _rank; }
        }

        public int CompetitionsWins
        {
            get { return _competitionsWins; }
        }

        public int CompetitionsLost
        {
            get { return _competitionsLost; }
        }

        public int TotalGoals
        {
            get { return _totalGoals; }
        }

        public int CupsLost
        {
            get { return _cupsLost; }
        }

        public int CupsWins
        {
            get { return _cupsWins; }
        }

        public int MatchLost
        {
            get { return _matchLost; }
        }

        public int MatchWins
        {
            get { return _matchWins; }
        }

        public GSData RawData
        {
            get { return _rawData; }
        }

        public virtual void Parse(GSData data)
        {
            if(data == null)return;
            _rawData = data;

           

            if (data.ContainsKey("isCanChallenged"))
            {
                IsCanChallenged = data.GetBoolean("isCanChallenged").Value;
            }
           

            if (data.GetBoolean("info").HasValue && data.GetBoolean("info").Value)
            {
                if (data.ContainsKey("TEAM"))
                {
                    
                    _teamInfo = new TeamInfoModel(data.GetString("userId"));
                    _teamInfo.Parse(data.GetString("TEAM"));
                }

                _rank = (RankType)data.GetInt("RANK").Value;
                _level = data.GetInt("LEVEL").Value;
                _totalExp = data.GetInt("TOTAL_EXP").Value;
            }

            if (data.GetBoolean("stat").HasValue && data.GetBoolean("stat").Value)
            {
                _matchWins = data.GetInt("matchesWins").Value;
                _matchLost = data.GetInt("matchesLost").Value;
                _cupsWins = data.GetInt("cupsWins").Value;
                _cupsLost = data.GetInt("cupsLost").Value;
                _totalGoals = data.GetInt("totalGoals").Value;
                _competitionsWins = data.GetInt("competitionsWins").Value;
                _competitionsLost = data.GetInt("competitionsLost").Value;
            }
        }
    }
}