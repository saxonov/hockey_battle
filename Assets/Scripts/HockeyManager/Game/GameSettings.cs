﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using SCTools.Localization;
using TMPro;

namespace HockeyManager.Game
{
    public class GameSettings : MonoBehaviour,IGameSettings
    {
        //prefabs
        [SerializeField] private GameObject _buildingProgressPrefab;
        [SerializeField] private GameObject _eqpCaptionPrefab;
        [SerializeField] private GameObject _eqpProgressPrefab;

        [SerializeField] private Shader hsvShader;
        [SerializeField]private BuildingSettingsObject _buildingSettings;
        [SerializeField]private EquipmentSettingsObject _equipmentSettings;
        [SerializeField]private PersonalSettingsData _personalSettings;
        [SerializeField]private HallItemsSettingsData _hallItemsSettings;

        [SerializeField] private GameIconsList _gameIconsList;
        [SerializeField] private LogoIconsSettingsData _logosList;

       // [SerializeField] private TMP_SpriteAsset emojiAsset;

        private Dictionary<MapBuildingsType, MapBuildingSettingsInfo> _buildingsDataDict;

        private static GameSettings _instance;

        //public BuildingSettingsObject BuildingSettings
        //{
        //    get { return _buildingSettings; }
        //}

        //public EquipmentSettingsObject EquipmentSettings
        //{
        //    get { return _equipmentSettings; }
        //}

        //public PersonalSettingsData PersonalSettings
        //{
        //    get { return _personalSettings; }
        //}

        //public HallItemsSettingsData HallItemsSettings
        //{
        //    get { return _hallItemsSettings; }
        //}

        public Shader HsvShader
        {
            get { return hsvShader; }
        }

        public GameObject BuildingProgressPrefab
        {
            get { return _buildingProgressPrefab; }
        }

        public static IGameSettings Instance
        {
            get { return _instance; }
        }

        public GameObject EqpCaptionPrefab
        {
            get { return _eqpCaptionPrefab; }
        }

        public GameObject EqpProgressPrefab
        {
            get { return _eqpProgressPrefab; }
        }

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            _buildingsDataDict = _buildingSettings.BuildingsSettingsInfo.ToDictionary(key => key.Type, value => value);
        }

        void Start()
        {
            //StringBuilder sb = new StringBuilder();
            //foreach (var tmpSprite in emojiAsset.spriteInfoList)
            //{
            //    sb.Append(char.ConvertFromUtf32(tmpSprite.unicode));
            //    sb.Append(',');
            //}

            //UISettings.SetSupportedEmojiList(sb.ToString().PadRight(1));
        }

        public Sprite GetIconTextCompetitionType(string type, out string label)
        {
            Sprite icon = null;
            label = null;
            switch (type)
            {
                case GameData.CoinsTypeCompetition:
                    icon = GetIcon(IconsEnum.Money);
                    label = LocaleManager.Instance.GetText("game.main.competitions.moremoney").ToUpper();
                    break;
                case GameData.GoalsTypeCompetition:
                    icon = GetIcon(IconsEnum.Goals);
                    label = LocaleManager.Instance.GetText("game.main.competitions.moregoals").ToUpper();
                    break;
                case GameData.ExpTypeCompetition:
                    icon = GetIcon(IconsEnum.Exp);
                    label = LocaleManager.Instance.GetText("game.main.competitions.moreexp").ToUpper();
                    break;
            }
            return icon;
        }

        public Sprite GetIcon(int currencyType)
        {
            if (_gameIconsList.icons == null) return null;
            if (currencyType == GameData.CurrencyCoins)
            {
                return _gameIconsList.icons.Find(icon => icon.type == IconsEnum.Money).icon;
            }
            if (currencyType == GameData.CurrencyGold)
            {
                return _gameIconsList.icons.Find(icon => icon.type == IconsEnum.Gold).icon;
            }
            return null;
        }


        public Sprite GetIcon(IconsEnum type)
        {
            if (_gameIconsList.icons == null) return null;
            return _gameIconsList.icons.Find(icon => icon.type.Equals(type)).icon;
        }

        public Sprite GetRewardIcon(RewardType type)
        {
            if (_gameIconsList.rewardIcons == null) return null;
            return _gameIconsList.rewardIcons.Find(icon => icon.type.Equals(type)).icon;
        }

        public EquipmentsSettingsInfo GetEquipmentData(EquipmentType type)
        {
            for (int i = 0; i < _equipmentSettings.equipments.Length; i++)
            {
                if (_equipmentSettings.equipments[i].Type.Equals(type))
                {
                    return _equipmentSettings.equipments[i];
                }
            }
            return null;
        }

  
        public MapBuildingSettingsInfo GetBuildingData(MapBuildingsType type)
        {
            if (_buildingsDataDict.ContainsKey(type))
            {
                return _buildingsDataDict[type];
            }
            return null;
        }

        public EmployeeSettingsInfo GetEmployeeData(EmployeeType type)
        {
            for (int i = 0; i < _personalSettings.employees.Length; i++)
            {
                if (_personalSettings.employees[i].Type.Equals(type))
                {
                    return _personalSettings.employees[i];
                }
            }
            return null;
        }

        public HallItemInfo GetHallItemInfo(int id)
        {
            for (int i = 0; i < _hallItemsSettings.items.Length; i++)
            {
                if (_hallItemsSettings.items[i].Id == id)
                {
                    return _hallItemsSettings.items[i];
                }
            }
            return null;
        }

        public string GetRankLocaleKey(RankType rank)
        {
            string localeKey = "";
            switch (rank)
            {
                case RankType.Beginner:
                    localeKey = "game.main.rank.beginner";
                    break;
                case RankType.Amateur:
                    localeKey = "game.main.rank.amateur";
                    break;
                case RankType.Professional:
                    localeKey = "game.main.rank.professional";
                    break;
                case RankType.Master:
                    localeKey = "game.main.rank.master";
                    break;
                case RankType.TopStar:
                    localeKey = "game.main.rank.topstar";
                    break;
            }
            return localeKey;
        }

        public Sprite GetLogo64(int id)
        {
            return GetLogo(_logosList.Logo64,id);
        }

        public Sprite GetLogo256(int id)
        {
            return GetLogo(_logosList.Logos256,id);
        }

        public Sprite GetLogo128(int id)
        {
            return GetLogo(_logosList.Logos128,id);
        }

        private Sprite GetLogo(Sprite[] logos, int index)
        {
            if (logos.Length-1 >= index)
            {
                return logos[index];
            }
            return null;
        }

    }

}
