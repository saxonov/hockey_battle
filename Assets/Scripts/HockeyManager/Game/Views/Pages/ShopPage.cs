﻿

#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
 //#define RECEIPT_VALIDATION
#endif


using System.Collections.Generic;
using GameSparks.Api.Requests;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.GameSparks.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Shop;
using HockeyManager.Game.UI.Components;
using HockeyManager.Views.Components;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif


namespace HockeyManager.Game.Views.Pages
{

    public class ShopPage : MonoBehaviour,IStoreListener
    {

        [SerializeField] private ScrollRect scrollRect;

        [SerializeField] private PurchaseItemBox[] priceBoxes;

        [Header("VIP")]
        [SerializeField] private VipShieldButton[] vipButtons;
        [SerializeField] private TextIconControl vipTimer;
        [SerializeField] private Text vipActiveLabel;
       

        [Header("Shield")]
        [SerializeField] private VipShieldButton[] shieldButtons;
        [SerializeField] private TextIconControl shieldTimer;
        [SerializeField] private Text shieldActiveLabel;

        private IGSManager gsManager;
        private TeamController _team;
     
        //  private bool _shopGoodsUpdated = false;

        private IStoreController _storeController;
        //   private IExtensionProvider extensionProvider;

#pragma warning disable 0414
        private bool m_IsGooglePlayStoreSelected;
        private bool m_PurchaseInProgress;
#pragma warning restore 0414
        //   private bool m_IsSamsungAppsStoreSelected;
        // private bool m_IsCloudMoolahStoreSelected;

        //  private string m_LastTransationID;
        //  private string m_LastReceipt;
        // private string m_CloudMoolahUserName;
        private IAppleExtensions m_AppleExtensions;
        private IAppleConfiguration appleConfigurration;
      
       // private string m_LastTransationID = null;
#if RECEIPT_VALIDATION
        private CrossPlatformValidator validator;
     
#endif

        void Awake()
        {
            _team = GameRoot.Instance.GetController<TeamController>();

            gsManager = GameRoot.Instance.NetManager;

            var module = StandardPurchasingModule.Instance();

            // The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
            // developer ui (initialization, purchase, failure code setting). These correspond to 
            // the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
            
            var builder = ConfigurationBuilder.Instance(module);

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                appleConfigurration = builder.Configure<IAppleConfiguration>();

                Debug.Log("IOS app store can make payments: " + appleConfigurration.canMakePayments);

            }


            // This enables the Microsoft IAP simulator for local testing.
            // You would remove this before building your release package.
            //builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;
            if (Application.platform == RuntimePlatform.Android)
            {
                builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiF6qxl5sya4xZHty0j1535MdhX/WBdgRlCjjtYDGmUZP+FXz+WYezkEo5zkEb9/uUlYhhYw7/YSWHx83dbzUtSejRxuJe7VQ9oHo31v4YWUAqZSs0/Lh61SbzvLOkjgE6OK2cyFGg19U0woChNaHnBpSY0cfs8xfw02WoHtCfSqHqYOAGUl26VISAmqlLxmuH0QGH6uXQZJmPoR9AU+C3xNHmY9P8N39DTgybVOZvmXxpqTqavL0AOZKoSTbMIfScZc3sepO7Pw7q2CgzeqfNNbeoru3yrWu+GkHu7sJJkX3tjGa3lBtoNehsv2hNygPUm7aM2TVqBLs2ljbZr7VawIDAQAB");
                m_IsGooglePlayStoreSelected = Application.platform == RuntimePlatform.Android && module.androidStore == AndroidStore.GooglePlay;

            }



            // CloudMoolah Configuration setings 
            // All games must set the configuration. the configuration need to apply on the CloudMoolah Portal.
            // CloudMoolah APP Key
            // builder.Configure<IMoolahConfiguration>().appKey = "d93f4564c41d463ed3d3cd207594ee1b";
            // CloudMoolah Hash Key
            //builder.Configure<IMoolahConfiguration>().hashKey = "cc";
            // This enables the CloudMoolah test mode for local testing.
            // You would remove this, or set to CloudMoolahMode.Production, before building your release package.
            // builder.Configure<IMoolahConfiguration>().SetMode(CloudMoolahMode.AlwaysSucceed);
            // This records whether we are using Cloud Moolah IAP. 
            // Cloud Moolah requires logging in to access your Digital Wallet, so: 
            // A) IAPDemo (this) displays the Cloud Moolah GUI button for Cloud Moolah
            // m_IsCloudMoolahStoreSelected = Application.platform == RuntimePlatform.Android && module.androidStore == AndroidStore.CloudMoolah;

            // Define our products.
            // In this case our products have the same identifier across all the App stores,
            // except on the Mac App store where product IDs cannot be reused across both Mac and
            // iOS stores.
            // So on the Mac App store our products have different identifiers,
            // and we tell Unity IAP this by using the IDs class.
            builder.AddProduct("gold.pack.100", ProductType.Consumable);
            builder.AddProduct("gold.pack.250", ProductType.Consumable);
            builder.AddProduct("gold.pack.750", ProductType.Consumable);
            builder.AddProduct("gold.pack.1650", ProductType.Consumable);
            builder.AddProduct("gold.pack.3750", ProductType.Consumable);
            builder.AddProduct("gold.pack.10000", ProductType.Consumable);



            // Write Amazon's JSON description of our products to storage when using Amazon's local sandbox.
            // This should be removed from a production build.
            //  builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);

            // This enables simulated purchase success for Samsung IAP.
            // You would remove this, or set to SamsungAppsMode.Production, before building your release package.
            //builder.Configure<ISamsungAppsConfiguration>().SetMode(SamsungAppsMode.AlwaysSucceed);
            // This records whether we are using Samsung IAP. Currently ISamsungAppsExtensions.RestoreTransactions
            // displays a blocking Android Activity, so: 
            // A) Unity IAP does not automatically restore purchases on Samsung Galaxy Apps
            // B) IAPDemo (this) displays the "Restore" GUI button for Samsung Galaxy Apps
            // m_IsSamsungAppsStoreSelected = module.androidStore == AndroidStore.SamsungApps;


            // This selects the GroupId that was created in the Tizen Store for this set of products
            // An empty or non-matching GroupId here will result in no products available for ShowFixedPreloaderpurchase
            //  builder.Configure<ITizenStoreConfiguration>().SetGroupId("100000085616");


#if RECEIPT_VALIDATION
		validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
#endif


           PopupsManager.Instance.ShowFixedPreloader();

            // Now we're ready to initialize Unity IAP.
            UnityPurchasing.Initialize(this, builder);
          
        }

        void Start()
        {
          
            scrollRect.gameObject.SetActive(false);

            InitializeVipAndShields();
        }

     
        private void InitializeVipAndShields()
        {

            bool hasVip = _team.HasVip;
            bool hasShield = _team.HasProtectShield;

            for (int i = 0; i < vipButtons.Length; i++)
            {
                vipButtons[i].gameObject.SetActive(false);
                shieldButtons[i].gameObject.SetActive(false);
            }

            if (hasVip)
            {
                EnableVip();
            }

            if (hasShield)
            {
                EnableShield();
            }
          
            var request = new LogEventRequest();
            request.SetEventKey("GET_VIPS_SHIELDS_EVT");

            gsManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    var vips = responce.ScriptData.GetGSDataList("VipsData");
                    var shields = responce.ScriptData.GetGSDataList("ShieldsData");
                    for (int i = 0; i < vips.Count; i++)
                    {
                        vipButtons[i].gameObject.SetActive(!hasVip);
                        vipButtons[i].SetData(JsonUtility.FromJson<VirtualGoods.TimelimitVirtualGood>(vips[i].JSON));
                        shieldButtons[i].gameObject.SetActive(!hasShield);
                        shieldButtons[i].SetData(JsonUtility.FromJson<VirtualGoods.TimelimitVirtualGood>(shields[i].JSON));
                    }
                }
                  
            });
        }

        private void EnableShield()
        {
            for (int i = 0; i < shieldButtons.Length; i++)
            {
                shieldButtons[i].gameObject.SetActive(false);
            }
            shieldTimer.gameObject.SetActive(true);
            shieldActiveLabel.gameObject.SetActive(true);
            UpdateActiveShield();
        }

        private void EnableVip()
        {
            for (int i = 0; i < vipButtons.Length; i++)
            {
                vipButtons[i].gameObject.SetActive(false);
            }
            vipTimer.gameObject.SetActive(true);
            vipActiveLabel.gameObject.SetActive(true);
            UpdateActiveVip();
        }
       
        private void UpdateActiveVip()
        {
            if (_team.HasVip)
            {
                long leftSeconds = GameTimer.TimeLeftSeconds(_team.TeamData.vip.timeLimit);
                vipTimer.SetText(GameTimer.GetGameTimeString(leftSeconds));
                if (leftSeconds <= 0)
                {
                    vipTimer.gameObject.SetActive(false);
                    vipActiveLabel.gameObject.SetActive(false);
                    for (int i = 0; i < vipButtons.Length; i++)
                    {
                        vipButtons[i].gameObject.SetActive(true);
                    }
                    _team.DisableVip();
                }
            }
            else
            {
                if (vipTimer.gameObject.activeSelf)
                {
                    vipTimer.gameObject.SetActive(false);
                    vipActiveLabel.gameObject.SetActive(false);
                    for (int i = 0; i < vipButtons.Length; i++)
                    {
                        vipButtons[i].gameObject.SetActive(true);
                    }
                }
            }
        }

        private void UpdateActiveShield()
        {
            if (_team.HasProtectShield)
            {
                long leftSeconds = GameTimer.TimeLeftSeconds(_team.TeamData.protectShield.timeLimit);
                shieldTimer.SetText(GameTimer.GetGameTimeString(leftSeconds));
                if (leftSeconds <= 0)
                {
                    shieldTimer.gameObject.SetActive(false);
                    shieldActiveLabel.gameObject.SetActive(false);
                    for (int i = 0; i < shieldButtons.Length; i++)
                    {
                        shieldButtons[i].gameObject.SetActive(true);
                    }
                    _team.DisableShield();
                }
            }
            else
            {
                if (shieldTimer.gameObject.activeSelf)
                {
                    shieldTimer.gameObject.SetActive(false);
                    shieldActiveLabel.gameObject.SetActive(false);
                    for (int i = 0; i < shieldButtons.Length; i++)
                    {
                        shieldButtons[i].gameObject.SetActive(true);
                    }
                }
            }
        }

        void OnEnable()
        {
            EventsManager.Instance.Add(GameTimer.TikTakSecondEvent,OnTikTakSecondsHandler);
            EventsManager.Instance.Add<PurchaseItemModel>(PurchaseItemBox.OnBuyProductEvent,OnBuyProductClickHandler);
            //EventsManager.Instance.Add(PageNavigatorManager.PageNavigationChanged,OnPageChangedHandler);
            EventsManager.Instance.Add<VirtualGoods.TimelimitVirtualGood>(VipShieldButton.VipShieldEffectSelected,OnVipShieldBuyClickHandler);
        }

     

        public void OnDisable()
        {
            EventsManager.Instance.Remove(GameTimer.TikTakSecondEvent, OnTikTakSecondsHandler);
            EventsManager.Instance.Remove<PurchaseItemModel>(PurchaseItemBox.OnBuyProductEvent, OnBuyProductClickHandler);
            //EventsManager.Instance.Remove(PageNavigatorManager.PageNavigationChanged, OnPageChangedHandler);
            EventsManager.Instance.Remove<VirtualGoods.TimelimitVirtualGood>(VipShieldButton.VipShieldEffectSelected, OnVipShieldBuyClickHandler);
        }

        private void OnVipShieldBuyClickHandler(VirtualGoods.TimelimitVirtualGood vipOrShield)
        {
            SoundManager.Instance.PlaySFX("click");
            if (_team.CanBuyForGold(vipOrShield.goldPrice))
            {
                var request = new BuyVirtualGoodsRequest();
                request.SetCurrencyType(GameData.CurrencyGold);
                request.SetQuantity(1);
                request.SetShortCode(vipOrShield.type);
                gsManager.RunBuyVirtualEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        if (vipOrShield.type.Contains(VirtualGoods.VIP))
                        {
                            SoundManager.Instance.PlaySFXOnGameObject("buyvip", gameObject);
                            _team.NeedUpdateTeamInfo = true;
                            EnableVip();
                        }
                        if (vipOrShield.type.Contains(VirtualGoods.PROTECT_SHIELD))
                        {
                            SoundManager.Instance.PlaySFXOnGameObject("buyshield", gameObject);
                            _team.NeedUpdateTeamInfo = true;
                            EnableShield();
                        }
                    }
                    else
                    {
                        _team.RefreshBalance();
                    }
                });
            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup).SetTitle("game.main.popup.title.notenoughgold").HideButtons(true, false);
            }
        }

        private void OnTikTakSecondsHandler()
        {
            UpdateActiveShield();
            UpdateActiveVip();
        }


        //private void OnPageChangedHandler()
        //{
        //    if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Shop)
        //    {

        //    }
        //    else
        //    {
                
        //    }
        //}

        private void OnBuyProductClickHandler(PurchaseItemModel model)
        {
            PopupsManager.Instance.ShowFixedPreloader();
            m_PurchaseInProgress = true;
            SoundManager.Instance.PlaySFX("click");
            
            _storeController.InitiatePurchase(model.Product);
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            
            Debug.Log("Billing failed to initialize! " + error);
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    Debug.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Debug.Log("No products available for purchase!");
                    break;
            }
            PopupsManager.Instance.RemoveFixedPreloader();
        }


        /// <summary>
        /// This will be called when a purchase completes.
        /// </summary>
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            Debug.Log("Purchase processing: " + e.purchasedProduct.definition.id);
          //  Debug.Log("Receipt: " + e.purchasedProduct.receipt);

          //  m_LastTransationID = e.purchasedProduct.transactionID;
          //  m_LastReceipt = e.purchasedProduct.receipt;
           

#if RECEIPT_VALIDATION
		// Local validation is available for GooglePlay and Apple stores
		if (m_IsGooglePlayStoreSelected ||
			Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) {
			try {
				var result = validator.Validate(e.purchasedProduct.receipt);
				Debug.Log("Receipt is valid. Contents:");
				foreach (IPurchaseReceipt productReceipt in result) {
					Debug.Log(productReceipt.productID);
					Debug.Log(productReceipt.purchaseDate);
					Debug.Log(productReceipt.transactionID);

					GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google) {
                        Debug.Log(google.purchaseState);
						Debug.Log(google.purchaseToken);
					}

					AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple) {
                        
						Debug.Log(apple.originalTransactionIdentifier);
						Debug.Log(apple.subscriptionExpirationDate);
						Debug.Log(apple.cancellationDate);
						Debug.Log(apple.quantity);
					}
				}
			} catch (IAPSecurityException) {
				Debug.Log("Invalid receipt, not unlocking content");
				return PurchaseProcessingResult.Complete;
			}
		}
#endif

            // CloudMoolah purchase completion / finishing currently requires using the API 
            // extension IMoolahExtension.RequestPayout to finish a transaction.
            //if (m_IsCloudMoolahStoreSelected)
            //{
            //    // Finish transaction with CloudMoolah server
            //    m_MoolahExtensions.RequestPayOut(e.purchasedProduct.transactionID,
            //        (string transactionID, RequestPayOutState state, string message) => {
            //            if (state == RequestPayOutState.RequestPayOutSucceed)
            //            {
            //            // Finally, finish transaction with Unity IAP's local
            //            // transaction log, recording the transaction id there
            //            m_Controller.ConfirmPendingPurchase(e.purchasedProduct);

            //            // Unlock content here.
            //        }
            //            else
            //            {
            //                Debug.Log("RequestPayOut: failed. transactionID: " + transactionID +
            //                    ", state: " + state + ", message: " + message);
            //            // Finishing failed. Retry later.
            //        }
            //        });
            //}

            // You should unlock the content here.


            if (m_IsGooglePlayStoreSelected)
            {
                //  Debug.Log("buy reciept " + e.purchasedProduct.receipt);
                var wrapper = e.purchasedProduct.receipt.HashtableFromJson();
                string payLoad = (string) wrapper["Payload"];
                var details = payLoad.HashtableFromJson();
                var json = (string) details["json"]; // This is the INAPP_PURCHASE_DATA information
                var sig = (string) details["signature"]; // This is the INAPP_DATA_SIGNATURE information
                var request = new GooglePlayBuyGoodsRequest();
                request.SetSignature(sig);
                request.SetSignedData(json);
                request.Send((responce) =>
                {
                    if (responce.HasErrors)
                    {
                        EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                        Debug.Log(responce.Errors.JSON);
                        gsManager.Logger.SaveClientLogs("GS: Buy innapp error!");
                    }
                    else
                    {
                       // EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,request.GetHashCode());
                        _team.UpdateFromGSResponce(responce.ScriptData);
                        _team.NeedUpdateTeamInfo = true;
                        m_PurchaseInProgress = false;
                        _storeController.ConfirmPendingPurchase(e.purchasedProduct);
                        // m_PurchaseInProgress = false;
                    }
                    PopupsManager.Instance.RemoveFixedPreloader();
                });
            }else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (e.purchasedProduct.hasReceipt)
                {
                    Debug.Log("Start buy ios product " + e.purchasedProduct.receipt);
                    var recieptData = e.purchasedProduct.receipt.HashtableFromJson();
                    IOSBuyGoodsRequest iosbuyrequest = new IOSBuyGoodsRequest();
                    iosbuyrequest.SetReceipt(recieptData["Payload"].ToString());
                    iosbuyrequest.SetSandbox(true);
                    iosbuyrequest.Send((responce =>
                    {
                        if (responce.HasErrors)
                        {
                          
                            EventsManager.Instance.Call(GlobalEventTypes.ServerResponceError, responce.Errors);
                            Debug.Log(responce.Errors.JSON);
                            gsManager.Logger.SaveClientLogs("GS: Buy innapp error!");
                        }
                        else
                        {
                           // EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,iosbuyrequest.GetHashCode());
                            _team.UpdateFromGSResponce(responce.ScriptData);
                            _team.NeedUpdateTeamInfo = true;
                            m_PurchaseInProgress = false;
                            _storeController.ConfirmPendingPurchase(e.purchasedProduct);
                        }
                        PopupsManager.Instance.RemoveFixedPreloader();
                    }));
                }
                else
                {
                    PopupsManager.Instance.RemoveFixedPreloader();
                }
            }else if (Application.isEditor)
            {
                var request = new BuyVirtualGoodsRequest();
                request.SetQuantity(1);
                request.SetCurrencyType(GameData.CurrencyGold);
                request.SetShortCode(VirtualGoods.TEST_GOLD_GOOD);
                gsManager.RunBuyVirtualEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        Debug.Log("test gold added!");
                    }
                    PopupsManager.Instance.RemoveFixedPreloader();
                });
            }

           // GooglePlayReceipt google = (GooglePlayReceipt) e.purchasedProduct;

            // Indicate if we have handled this purchase. 
            //   PurchaseProcessingResult.Complete: ProcessPurchase will not be called
            //     with this product again, until next purchase.
            //   PurchaseProcessingResult.Pending: ProcessPurchase will be called 
            //     again with this product at next app launch. Later, call 
            //     m_Controller.ConfirmPendingPurchase(Product) to complete handling
            //     this purchase. Use to transactionally save purchases to a cloud
            //     game service. 
            Debug.Log("purchase complete!");
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
        {
            PopupsManager.Instance.RemoveFixedPreloader();
            Debug.Log("Purchase failed: " + item.definition.id);
            Debug.Log(r);
            gsManager.Logger.SaveClientLogs("IAP: Buy innapp failed!");
            m_PurchaseInProgress = false;
          
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
           

            _storeController = controller;
           // extensionProvider = extensions;
            
            m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
          //  m_SamsungExtensions = extensions.GetExtension<ISamsungAppsExtensions>();
           // m_MoolahExtensions = extensions.GetExtension<IMoolahExtension>();

           

            // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
            // On non-Apple platforms this will have no effect; OnDeferred will never be called.
            m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);

            //Debug.Log("Available items:");
            //foreach (var item in controller.products.all)
            //{
            //    if (item.availableToPurchase)
            //    {
            //        Debug.Log(string.Join(" - ",
            //            new[]
            //            {
            //            item.metadata.localizedTitle,
            //            item.metadata.localizedDescription,
            //            item.metadata.isoCurrencyCode,
            //            item.metadata.localizedPrice.ToString(),
            //            item.metadata.localizedPriceString,
            //            item.transactionID,
            //            item.receipt
            //            }));
            //    }
            //}
          
            Debug.Log("IAP initialized");
#if UNITY_IOS
              m_AppleExtensions.RestoreTransactions((success) =>
                {
                    if (success)
                    {
                        Debug.Log("restore ios transaction ");
                        gsManager.Logger.SaveClientLogs("IOS Restore transaction complete!");
                    }

                });
            
#endif
            LoadGoldPacks();
            
        }

        private void LoadGoldPacks()
        {
            //EventsManager.Instance.Call(GlobalEventTypes.ServerRequestSended);
            ListVirtualGoodsRequest request = new ListVirtualGoodsRequest();
            request.SetIncludeDisabled(false);
            request.SetTags(new List<string>() { "gold" });
            gsManager.RunListVirtualGoodsRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    List<PurchaseItemModel> goldPacks = new List<PurchaseItemModel>();

                    bool isIOSAppleStore = Application.platform == RuntimePlatform.IPhonePlayer;

                    foreach (var virtualGood in responce.VirtualGoods)
                    { 
                        string productID = isIOSAppleStore ? virtualGood.IosAppStoreProductId : virtualGood.GooglePlayProductId;
                        Product p = _storeController.products.WithStoreSpecificID(productID);
                        PurchaseItemModel model = new PurchaseItemModel((int)virtualGood.Currency1Cost.Value,p);
                        goldPacks.Add(model);
                    }

                    goldPacks.Sort();
                    for (int i = 0; i < goldPacks.Count; i++)
                    {
                        if (i < priceBoxes.Length)
                        {
                            priceBoxes[i].gameObject.SetActive(true);
                            priceBoxes[i].SetProduct(goldPacks[i]);
                        }
                        else
                        {
                            Debug.LogWarning("New gold packs added!");
                            break;
                        }
                    }
                    scrollRect.gameObject.SetActive(true);
                   // EventsManager.Instance.Call(GlobalEventTypes.ServerResponceRecieved,request.GetHashCode());
                }
                PopupsManager.Instance.RemoveFixedPreloader();
            });
           
        }


   
        /// <summary>
        /// iOS Specific.
        /// This is called as part of Apple's 'Ask to buy' functionality,
        /// when a purchase is requested by a minor and referred to a parent
        /// for approval.
        /// 
        /// When the purchase is approved or rejected, the normal purchase events
        /// will fire.
        /// </summary>
        /// <param name="item">Item.</param>
        private void OnDeferred(Product item)
        {
           
            Debug.Log("Purchase deferred: " + item.definition.id);
        }

   
    }
}
