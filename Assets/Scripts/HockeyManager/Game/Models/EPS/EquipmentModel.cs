﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Base;

namespace HockeyManager.Game.Models.EPS
{
    public sealed class EquipmentModel:BaseGameEntityModel
    {
        /// <summary>
        /// default maxLevel = 27
        /// </summary>
        public static int MaxLevel = 27;

        private EquipmentType _id;
        private int _attack;
        private int _deffence;

        private int _level = 0;
        private EquipmentUpgradeModel _upgradeModel;

        public EquipmentModel(GSData data)
        {
            _entityType = EntityType.Equipment;

            _id = (EquipmentType)data.GetInt("id").Value;
            Parse(data);
        }

        public override void Parse(GSData data)
        {
            _attack = data.GetInt("attack").Value;
            _deffence = data.GetInt("deffence").Value;
            _level = data.GetInt("level").HasValue ? data.GetInt("level").Value : _level;
            base.Parse(data);
        }

        public int Attack
        {
            get { return _attack; }
        }

        public int Deffence
        {
            get { return _deffence; }
        }

        public EquipmentType Id
        {
            get { return _id; }
        }
       
        public EquipmentUpgradeModel UpgradeModel
        {
            get { return _upgradeModel; }
            set { _upgradeModel = value; }
        }

        public int Level
        {
            get { return _level; }
        }
    }
}
