﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Helpers;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Views.Components
{
    public class VipShieldButton:MonoBehaviour,IPointerClickHandler
    {
        public const string VipShieldEffectSelected = "vipShieldEffectSelected";

#pragma warning disable CS0169 // Поле "VipShieldButton.caption" никогда не используется.
        [SerializeField] private Text caption;
#pragma warning restore CS0169 // Поле "VipShieldButton.caption" никогда не используется.
#pragma warning disable CS0649 // Полю "VipShieldButton.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl pricePanel;
#pragma warning restore CS0649 // Полю "VipShieldButton.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VipShieldButton.localeCaption" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private UITextLocalizator localeCaption;
#pragma warning restore CS0649 // Полю "VipShieldButton.localeCaption" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        //[SerializeField] private VipShiledType _type;

        //public VipShiledType Type
        //{
        //    get { return _type; }
        //}

        private VirtualGoods.TimelimitVirtualGood _vipShieldData;

        public void SetData(VirtualGoods.TimelimitVirtualGood vipOrShield)
        {
            _vipShieldData = vipOrShield;
            pricePanel.SetText(vipOrShield.goldPrice);
            localeCaption.SetLocaleKeys(true,vipOrShield.localKey);
            //pricePanel.SetText(goldPrice);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            EventsManager.Instance.Call(VipShieldEffectSelected,_vipShieldData);
        }
    }
}
