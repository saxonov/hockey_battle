﻿using DG.Tweening;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.Views.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Equipments
{
    [RequireComponent(typeof(Image))]
    public class Equipment:MonoBehaviour,IPointerClickHandler
    {
        public const string OnEquipmentSelectedHandler = "OnEquipmentSelectedEvent";
        public const string OnEquipmentModelChangedHandler = "OnEquipmentModelChangedEvent";

        public static RectTransform hudHolder;
        
        [SerializeField] private Vector2[] positions;
        [SerializeField] private Sprite[] states;
        
        [SerializeField] private RectTransform hudAnchor;

        private Image _image;

        private TextFitter caption;
        private Slider upgradeProgress;

        private int _currentState = 0;

        private EquipmentModel _model;
        private bool _isInProgress = false;

        
        void OnEnable()
        {
            if (_model != null)
            {
                OnModelChangeHandler();
            }
        }

        private void AddCaption()
        {
            GameObject captionPrefab = GameSettings.Instance.EqpCaptionPrefab;
            RectTransform rtcaption = hudHolder.AddChild(captionPrefab, false);
            Vector2 localPosVector2 = hudHolder.InverseTransformPoint(hudAnchor.position);
            rtcaption.anchoredPosition = localPosVector2;
            caption = rtcaption.GetComponent<TextFitter>();
        }

        public void SetModel(EquipmentModel model)
        {
            if (_model == null)
            {
                _model = model;
                _model.OnModelChangedEvent.AddListener(OnModelChangeHandler);

                OnModelChangeHandler();

            }
            
           
        }

        private void ChangeState()
        {
            _image = _image ?? GetComponent<Image>();
            _currentState = UISettings.GetBuildingEquipmentState(_model.Level,EquipmentModel.MaxLevel);
            _image.sprite = states[_currentState];

            if (positions != null && positions.Length == 3)
            {
                _image.rectTransform.anchoredPosition = positions[_currentState];
            }
        }

        private void CheckLockStates()
        {
            if (_model.Level <= 2)
            {
                float alphaValue = _model.Level > 1 ? 1f : 0.5f;
                if (_model.Id == EquipmentType.Pullover)
                {
                    _image.material.SetFloat("_Alpha", alphaValue);
                }
                else
                {
                    _image.color = new Color(1f, 1f, 1f, alphaValue);
                }
            }
            else
            {
                if (_model.Id == EquipmentType.Pullover)
                {
                    _image.material.SetFloat("_Alpha", 1f);

                }else if(_image.color.a <= 1f)
                {
                    _image.color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }

        private void SetProgress()
        {
            if (!_isInProgress)
            {
                if (upgradeProgress == null)
                {
                    RectTransform rtprogress = hudHolder.AddChild(GameSettings.Instance.EqpProgressPrefab, false);
                    //fix for scates
                    if (_model.Id == EquipmentType.Skates)
                    {
                        Vector2 pivot = rtprogress.pivot;
                        pivot.y = -0.7f;
                        rtprogress.pivot = pivot;
                    }
                    Vector2 localPosVector2 = hudHolder.InverseTransformPoint(hudAnchor.position);
                    rtprogress.anchoredPosition = localPosVector2;
                    upgradeProgress = rtprogress.GetComponent<Slider>();
                }

                _isInProgress = true;
               
                long secondsLeft = GameTimer.TimeLeftSeconds(_model.EndUpgradeTimeSeconds);
                    upgradeProgress.minValue = 0;
                    upgradeProgress.maxValue = _model.UpgradeTimeSeconds;
                    upgradeProgress.DOValue(_model.UpgradeTimeSeconds - secondsLeft, 1f, true);
               
                _model.OnTimerLeftSecondsEvent.AddListener(OnTikTakHandler);
                OnTikTakHandler(secondsLeft);
            }
         
            SetCaption();
        }

        private void OnTikTakHandler(long secondsLeft)
        {
            if (secondsLeft > 0)
            {
                upgradeProgress.DOValue(_model.UpgradeTimeSeconds - secondsLeft, 2f);
               
            }
            else
            {
                ResetProgress();
            }


        }

        private void ResetProgress()
        {
            _isInProgress = false;
            if (upgradeProgress != null)
            {
                Destroy(upgradeProgress.gameObject);
            }
            upgradeProgress = null;
            SetCaption();
            CheckLockStates();
        }

        private void SetCaption()
        {
            if (caption == null)
            {
                AddCaption();
            }

            string eqpName = GameSettings.Instance.GetEquipmentData(_model.Id).GetName();
             caption.SetText(eqpName + LocaleManager.SPACE + 
                             LocaleManager.Instance.GetText("game.common.shortlevel2", _model.Level));
        }

        private void OnModelChangeHandler()
        {
            if (gameObject.activeInHierarchy)
            {
                ChangeState();
                if (_model.IsUpgrading)
                {
                    SetProgress();
                }
                else
                {
                    ResetProgress();
                }
                
                CheckLockStates();
                EventsManager.Instance.Call(OnEquipmentModelChangedHandler, _model);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData != null)
            {
                SoundManager.Instance.PlaySFX("click");
            }
            EventsManager.Instance.Call(OnEquipmentSelectedHandler, _model);
        }
    }
}
