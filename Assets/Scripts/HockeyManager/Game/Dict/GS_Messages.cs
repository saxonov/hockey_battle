﻿namespace HockeyManager.Game.Dict
{
    public static class GS_Messages
    {
        public const string BuildingType    = "building";
        //public const string EquipmentsType  = "equipment";
        //public const string EmployeeType    = "employee";
        //public const string TrainingType    = "training";


        //admin message types
        public const string LockChatType           = "CHAT_LOCKED";
        public const string ModeratorType          = "MODERATOR";
        public const string PenaltyGoldBalanceType = "PENALTY_GOLD_BALANCE";
        //
        public const string RewardAddedMsg          = "REWARD_ADDED_MSG";

        public const string MatchStartedMsg          = "MATCH_STARTED_MSG";
        public const string MatchWonMsg              = "MATCH_WON_MSG";
        public const string MatchLostMsg             = "MATCH_LOST_MSG";
        public const string MatchCanceledMsg         = "MATCH_CANCELED_MSG";
        public const string MatchYouChallengedMsg    = "YOU_CHALLENGED_MSG";

        public const string JournalActionAdded       = "JOURNAL_ACTION_ADDED";

        public const string BuildingUpgradeComplete  = "BUILDING_UPGRADE_COMPLETE";
        public const string EquipmentUpgradeComplete = "EQUIPMENT_UPGRADE_COMPLETE";
        public const string EmployeeUpgradeComplete  = "EMPLOYEE_UPGRADE_COMPLETE";
        public const string TrainingUpgradeComplete  = "TRAINING_UPGRADE_COMPLETE";

        public const string TournamentPlayerChangedMsg    = "TOURNAMENT_PLAYER_CHANGED_MSG";
        public const string TournamentRoundCompleteMsg    = "TOURNAMENT_ROUND_COMPLETE_MSG";
        public const string TournamentStartMsg            = "TOURNAMENT_START_MSG";
        public const string TournamentCupExpiredMsg       = "TOURNAMENT_EXPIRED_MSG";
        public const string TournamentCupCompleteMsg      = "TOURNAMENT_COMPLETE_MSG";

        public const string CompetitionWinnerMsg = "CMPT_WINNERS_MSG";
        public const string CompetitionResetedMsg = "CMPT_RESETED_MSG";
        public const string CompetitionRefreshMsg = "CMPT_REFRESH_MSG";

        public const string ChatMsg              = "CHAT_MSG";
        public const string ChatMsgRemoved       = "CHAT_MSG_REMOVED";

        public const string AdminMsg   = "ADMIN_MSG";

        public const string BuildingUpdateMsg    = "BUILDING_UPDATE_MSG";
        public const string TrainingAvailableMsg = "TRAININGS_AVAILABLE_MSG";
        public const string TrainingEffectEndMsg = "TRAINING_EFFECT_END_MSG";

        public const string AlertMsg = "ALERT_MSG";

        //base upgrade message
        public const string UpgradeCompleteMsg = "UPGRADE_COMPLETE_MSG";
    }
}
