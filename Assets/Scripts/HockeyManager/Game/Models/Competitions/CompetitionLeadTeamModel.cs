﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Competitions
{
    public class CompetitionLeadTeamModel:IGSParse,IComparable<CompetitionLeadTeamModel>
    {

        private TeamInfoModel _teamInfo;

        private int _score;
        private int _position;

        public CompetitionLeadTeamModel(GSData data)
        {
            _teamInfo = new TeamInfoModel(data.GetString("id"));
            _teamInfo.Parse(data.GetString("team"));
            Parse(data);
        }

        public int Score
        {
            get { return _score; }
        }

        public int Position
        {
            get { return _position; }
        }

        public TeamInfoModel TeamInfo
        {
            get { return _teamInfo; }
        }

        public void Parse(GSData data)
        {
            _position = data.GetInt("position").Value;
            _score = data.GetInt("score").Value;
        }

        public int CompareTo(CompetitionLeadTeamModel other)
        {
            return _position - other.Position;
        }
    }
}
