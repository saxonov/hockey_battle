﻿using System;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.UI.Popups
{
    public interface IPopup : IDisposable 
    {
       PopupType Type { get; set; }

        //  BasePopupControls PopupControls { get; }
        object GetData();

        IPopup InjectController(IGameController controller);

       GameObject gameObject { get; }
        IPopup SetMinutesTickUpdate(UnityAction<IPopup> tickCallback);
        IPopup HideButtons(bool yesButton, bool noButton);
        IPopup SetCallbacks(UnityAction applyCallback, UnityAction cancelCallback = null);
        IPopup SetTitle(string title,bool localizable = true);
        IPopup SetTextIconControls(params string[] values);
        IPopup SetTextIconControls(params int[] values);
        IPopup SetTextIconControls(params Sprite[] icons); 
        IPopup SetData(object data);
       // IPopup HidePrices(params bool[] values);
        IPopup SetText(string text, bool localized = true);
        IPopup SetButtonLabels(string yesLocaleKey = null, string noButtonLocaleKey = null);
        IPopup LockButtons(bool lockYesButton = false,bool lockNoButton = false);
        IPopup SetHolderVisibility(string holderName, bool value);
        IPopup SetVisibilityTextIconControl(string controlName, bool visible);
    }
}
