﻿using HockeyManager.Game.Dict;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Buttons
{
   
   

    public class AccountButton:MonoBehaviour,IPointerClickHandler
    {
        public const string AccountButtonClickEvent = "AccountButton.AccountButtonClickEvent";

        private bool _isLocked = false;

        [SerializeField] private SocialAccountType _type;

        private bool _isBinded;

        private GameObject infoCaption;

        void Start()
        {
            infoCaption = transform.FindChild("InfoCaption").gameObject;
        }

        
        public bool IsBinded
        {
            get { return _isBinded; }
            set
            {
                _isBinded = value;
                infoCaption = infoCaption ?? transform.FindChild("InfoCaption").gameObject;
                infoCaption.gameObject.SetActive(_isBinded);
            }
        }

        public SocialAccountType Type
        {
            get { return _type; }
        }

        public bool IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            if(IsLocked)return;
            SoundManager.Instance.PlaySFX("click");
            EventsManager.Instance.Call(AccountButtonClickEvent);
        }
    }
}
