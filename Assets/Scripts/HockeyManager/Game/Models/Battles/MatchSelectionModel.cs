﻿using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{
    public class MatchSelectionModel:IGSParse
    {
        private int _energyCost;
        private int _acceptTimeSeconds;
        private int _battleTimeSeconds;

        private List<MatchPlayerModel> _enemies;

        public List<MatchPlayerModel> Enemies
        {
            get { return _enemies; }
        }

        public int AcceptTimeSeconds
        {
            get { return _acceptTimeSeconds; }
        }

        public int EnergyCost
        {
            get { return _energyCost; }
        }

        public int BattleTimeSeconds
        {
            get { return _battleTimeSeconds; }
        }


        public void Parse(GSData data)
        {
            if (data.ContainsKey("energyCost"))
            {
                _energyCost = data.GetInt("energyCost").Value;
            }
            if (data.ContainsKey("acceptTime"))
            {
                _acceptTimeSeconds = data.GetInt("acceptTime").Value;
            }

            if (data.ContainsKey("battleTimeSeconds"))
            {
                _battleTimeSeconds = data.GetInt("battleTimeSeconds").Value;
            }

            if (data.ContainsKey("teams"))
            {
                List<GSData> list = data.GetGSDataList("teams");
                _enemies = _enemies ?? new List<MatchPlayerModel>(3);
                _enemies.Clear();
                foreach (var gsData in list)
                {
                    _enemies.Add(new MatchPlayerModel(gsData));
                }
            }
        }
    }
}
