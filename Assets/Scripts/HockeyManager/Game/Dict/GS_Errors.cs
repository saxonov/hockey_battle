﻿namespace HockeyManager.Game.Dict
{
    public static class GS_Errors
    {
       public const string ErrorBuildingsCategory = "buildings";
       public const string ErrorPersonalCategory = "personal";
       public const string ErrorEquipmentsCategory = "equipment";
       public const string ErrorHallInventoryCategory = "inventory";
       public const string ErrorTrainingsCategory = "trainings";
       public const string ErrorMatchPvpCategory = "match";
       public const string ErrorTournamentsCategory = "tournament";
       public const string ErrorOtherCategory = "other";



       public const int ERROR_NOT_ENOUGH_ENERGY = 1010;

       public const int TEAM_CANNOT_BE_ATTACKED = 31;
       public const int ERROR_RESET_SKILLPOINTS = 1006;
       public const int TOURNAMENT_NOT_EXIST = 38;
       public const int EQUIPMENT_UPGRADE_NOT_EXIST = 55;

        public const int BUILDING_UPGRADE_NOT_EXIST = 0;
        public const int BUILDING_UPGRADE_IN_PROGRESS = 2;

        

        public static bool NeedProcessError(int type)
        {
            return type == TEAM_CANNOT_BE_ATTACKED || 
                   type == TOURNAMENT_NOT_EXIST ||
                   type == ERROR_RESET_SKILLPOINTS ||
                   type == EQUIPMENT_UPGRADE_NOT_EXIST ||
                   type == BUILDING_UPGRADE_NOT_EXIST;
        }
    }
}
