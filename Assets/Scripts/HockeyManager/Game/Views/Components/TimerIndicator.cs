﻿using HockeyManager.Game;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Gam.UI.Components
{
    [RequireComponent(typeof(Image))]
    public class TimerIndicator : MonoBehaviour
    {
        private Image indicator;

        private long _endTime = 0;
        private int timeSeconds = 0;
        private bool timerStarted = false;

        void Awake()
        {
            indicator = GetComponent<Image>();
            indicator.fillClockwise = false;
            indicator.fillAmount = 0f;
            //timerStarted = true;
        }

        public void StartTimer(long endTime,int seconds)
        {
            if (timerStarted)
            {
                return;
            }
            _endTime = endTime;
            indicator.enabled = true;
            timeSeconds = seconds;
            indicator.fillAmount = 1f;
            timerStarted = true;
        }

        public void StopTimer()
        {
            if (timerStarted)
            {
                timerStarted = false;
                indicator.fillAmount = 0f;
                indicator.enabled = false;
                
            }
        }

        void Update()
        {
            if (timerStarted)
            {
              //  checkTime -= Time.deltaTime;
                int timeLeftSec = (int)GameTimer.TimeLeftSeconds(_endTime);
                indicator.fillAmount = (timeLeftSec * 1f)/ timeSeconds;
                if (indicator.fillAmount <= 0f)
                {
                    indicator.enabled = false;
                    timerStarted = false;
                    //checkTime = timeSeconds;

                    //GameRoot.Instance.RemoveUpdateCallback(UpdateTimer);
                }
            }
        }

       
    }
}
