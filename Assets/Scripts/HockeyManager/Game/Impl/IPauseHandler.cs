﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HockeyManager.Game.Impl
{
    interface IPauseHandler
    {

        void OnPaused();
    }
}
