﻿using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{
    public class TournamentCupSettingsModel:IGSParse
    {
        private int _cupNameCharactersLen;
        private int[] _players;
        private int[] _rounds;
        private int[] _energyPrices;
        private int _expiryTime;
        private int _rondTime;

        private int _priceFeeCounts = 0;
        private int _priceFeePerCount = 0;
        /// <summary>
        /// start difference from min level to max level
        /// </summary>
        private int _startLevelRange = 3;

        /// <summary>
        /// max min custom range from - to + value
        /// </summary>
        private int _maxMinLevelRange = 7;

        public int[] Players
        {
            get { return _players; }
        }

        public int[] Rounds
        {
            get { return _rounds; }
        }

        public int[] EnergyPrices
        {
            get { return _energyPrices; }
        }

        public int ExpiryTime
        {
            get { return _expiryTime; }
        }

        public int RondTime
        {
            get { return _rondTime; }
        }

        public int CupNameCharactersLen
        {
            get { return _cupNameCharactersLen; }
        }

        public int MaxMinLevelRange
        {
            get { return _maxMinLevelRange; }
        }

        public int StartLevelRange
        {
            get { return _startLevelRange; }
        }

        public int PriceFeeCounts
        {
            get { return _priceFeeCounts; }
        }

        public int PriceFeePerCount
        {
            get { return _priceFeePerCount; }
        }

        public void Parse(GSData data)
        {
            _cupNameCharactersLen = data.GetInt("cupNameLen").Value;
            //_defaultPriceFee = data.GetInt("defaultPriceFee").Value;
            _players = data.GetIntList("players").ToArray();
            _rounds = data.GetIntList("rounds").ToArray();
            _energyPrices = data.GetIntList("energyPrices").ToArray();
            _expiryTime = data.GetInt("expireTimeSec").Value;
            _rondTime = data.GetInt("roundTimeSec").Value;

            _priceFeePerCount = data.GetInt("priceFeePerCount").Value;
            _priceFeeCounts = data.GetInt("priceFeeCounts").Value;

            _startLevelRange = data.GetInt("startMinMaxRangeLevel").Value;
            _maxMinLevelRange = data.GetInt("minMaxRangeLevel").Value;
        }
    }
}
