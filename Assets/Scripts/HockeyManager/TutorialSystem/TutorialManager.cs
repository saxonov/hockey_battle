﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameSparks.Api.Requests;
using HockeyManager.Game;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.TutorialSystem.Data;
using HockeyManager.TutorialSystem.Views;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using SuperSocket.ClientEngine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.TutorialSystem
{
    public class TutorialManager : MonoBehaviour
    {
        public const string LastStepPrefKey = "TutorialSystem.LastStepIndex";
        private static TutorialManager _instance;

        public const string TutorialComplete = "TutorialSystem.tutorialComplete";
        public const string TutorialStepChanged = "TutorialSystem.tutorialStepChanged";

        [SerializeField] private string[] scenario;

        [SerializeField] private Image holderImage;

        [SerializeField] private RectTransform holder;

        [SerializeField] private RectTransform stepsHolder;

        [SerializeField] private string _currentStep = null;

        private List<TutorialView> _steps;

        private TutorialView _currentView = null;

        private int _stepIndex = 0;

        private Color originColor;

        private MapController _map;
        private TeamController _team;
        private SettingsNewsController _settings;

        public static TutorialManager Instance
        {
            get { return _instance; }
        }

        public string CurrentStep
        {
            get { return _currentStep; }
        }

        public TutorialView CurrentView
        {
            get { return _currentView; }
        }

        public bool IsCurrentViewStep(string step)
        {
            return _currentView != null && _currentView.Step == step;
        }

        public bool IsCurrentViewStep(params string[] steps)
        {
            return _currentView != null && steps.Contains(_currentView.Step);
        }

        private void Awake()
        {
            _instance = this;
            _map = GameRoot.Instance.GetController<MapController>();
            _team = GameRoot.Instance.GetController<TeamController>();
            _settings = GameRoot.Instance.GetController<SettingsNewsController>();
        }

        private void Start()
        {

            holder.gameObject.SetActive(true);

            originColor = holderImage.color;

            HideBackground();

            FillStepsHolder();

            //if (!GameRoot.Instance.IsGameStarted)
            //{
            //    EventsManager.Instance.Add(GlobalEventTypes.UserAuthenticated,StartTutorial);
            //}
            //else
            //{
                StartTutorial();
            //}
        }

        IEnumerator StartTutorialRoutine()
        {
            yield return new WaitForSeconds(0.5f);

            if (!string.IsNullOrEmpty(_currentStep))
            {
                _stepIndex = scenario.IndexOf(_currentStep, 0, scenario.Length);
                if (_stepIndex != -1 && scenario[_stepIndex] == TutorStepTypes.EmptyStep)
                {
                    _stepIndex++;
                    _currentStep = scenario[_stepIndex];
                    GotoStep(_currentStep);
                }
              
             
            }
            else
            {

                LoadStep((step) =>
                {
                    _stepIndex = step;//PlayerPrefs.HasKey(LastStepPrefKey) ? PlayerPrefs.GetInt(LastStepPrefKey) : 0;
                    if (scenario[_stepIndex] == TutorStepTypes.EmptyStep)
                    {
                        _stepIndex++;
                    }
                    _currentStep = scenario[_stepIndex];
                    GotoStep(_currentStep);
                });

            }
           
            yield return null;
        }

        private void FillStepsHolder()
        {
            _steps = new List<TutorialView>();

            int len = stepsHolder.childCount;
            for (int i = 0; i < len; i++)
            {
                _steps.Add(stepsHolder.GetChild(i).GetComponent<TutorialView>());
            }

            foreach (var tutorialView in _steps)
            {
                tutorialView.gameObject.SetActive(false);
            }


        }

        private void OnStepChanged()
        {
            if(_currentView == null)return;
            
            switch (_currentStep)
            {
                case TutorStepTypes.Intro:
                    SoundManager.Instance.PlaySFX("flytofans");
                break;
                case TutorStepTypes.FinishTraining:
                    
                    if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Training)
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Training);
                    }
                    break;
                case TutorStepTypes.EndTutorial:

                    if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Matches)
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Matches);
                    }
                    break;
                case TutorStepTypes.StartTraining:
                    if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Training)
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Training);
                    }
                break;
                case TutorStepTypes.GotoHomePageFromSkills:
                case TutorStepTypes.GotoHomePageFromRewards:
                case TutorStepTypes.GotoHomeFromTrainings:

                    if (PageNavigatorManager.Instance.SelectedPage == NavigationPageTypes.Home)
                    {
                        if (IsNextStepEmpty())
                        {
                            NextStep(2);
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                    else
                    {
                        if (_currentStep == TutorStepTypes.GotoHomeFromTrainings)
                        {
                            _currentView.Text.text = LocaleManager.Instance.GetText("game.main.tutorial.step14", _team.TeamData.TeamInfo.Name);
                        }
                    }

                    break;
                case TutorStepTypes.FinishCashbox:
                    IBuildingModel cashbox = _map.GetBuildingModel(MapBuildingsType.Cashbox, GameData.CashboxTutorialPosition);
                    if (cashbox != null && cashbox.IsBuilded)
                    {
                        if (IsNextStepEmpty())
                        {
                            NextStep(2);
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                    break;
                case TutorStepTypes.CollectCashbox:
                    if (!_map.GetBuildingModel(MapBuildingsType.Cashbox, GameData.CashboxTutorialPosition).IsCanCollect)
                    {
                        if (IsNextStepEmpty())
                        {
                            NextStep(2);
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                    break;
                case TutorStepTypes.FinishIceArena:
                    IBuildingModel icearena = _map.GetBuildingModel(MapBuildingsType.IceArena);
                    if (icearena != null && icearena.IsBuilded)
                    {
                        if (IsNextStepEmpty())
                        {
                            NextStep(2);
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                    break;
                case TutorStepTypes.FinishShop:
                    IBuildingModel shop = _map.GetBuildingModel(MapBuildingsType.Shop);
                    if (shop != null && shop.IsBuilded)
                    {
                        if (IsNextStepEmpty())
                        {
                            NextStep(2);
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                    break;
                case TutorStepTypes.CollectShop:
                    if (!_map.GetBuildingModel(MapBuildingsType.Shop).IsCanCollect)
                    {
                        NextStep();
                    }
                    break;
                case TutorStepTypes.ShowRewardsPage:
                    _currentView.Text.text = LocaleManager.Instance.GetText("game.main.tutorial.step3", _team.TeamData.TeamInfo.Name);
                    break;
                case TutorStepTypes.GotoSkillsPage:
                case TutorStepTypes.FinishMainCouch:
                case TutorStepTypes.HireMainCouch:
                    if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Personal)
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Personal);
                    }
                    break;
                case TutorStepTypes.ApplySkillPoints:
                    if (PageNavigatorManager.Instance.SelectedPage != NavigationPageTypes.Skills)
                    {
                        NextStep();
                    }
                    break;
            }
        }

        private void StartTutorial()
        {
          //  EventsManager.Instance.Remove(GlobalEventTypes.UserAuthenticated,StartTutorial);

            if (!GameData.IsTutorialPassed)
            {
                ShowBackground();
                StartCoroutine(StartTutorialRoutine());

            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        private void OnDestroy()
        {
            _instance = null;
            _team = null;
            _settings = null;
            _map = null;
        }
       
        public void FinishTutorial()
        {
            //PlayerPrefs.DeleteKey(LastStepPrefKey);
           // PlayerPrefs.Save();
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _settings.GameSettingsData.tutorialPassed = true;
            _settings.GameSettingsData.isChanged = true;
            _settings.SaveGameSettings(() =>
            {
                GameData.IsTutorialPassed = true;
                EventsManager.Instance.Call(TutorialComplete);
            });
           
            GameRoot.Instance.GetController<RewardsAchievementsController>().LoadRewards(null,true);

            Destroy(gameObject);

        }

        public void NextStep(int increment = 1)
        {
            increment = Mathf.Max(increment, 1);
            _stepIndex += increment;
            if (_stepIndex < scenario.Length)
            {
                _currentStep = scenario[_stepIndex];
                GotoStep(_currentStep);
            }

        }
      
        public bool IsNextStepEmpty()
        {
            if (_stepIndex+1 < scenario.Length)
            {
                return scenario[_stepIndex + 1] == TutorStepTypes.EmptyStep;
            }
            return true; 
        }
        
        private void GotoStep(string step)
        {
            if (_currentView != null)
            {
                StopCoroutine(JumpToStep(_currentView.Step));
                _currentView.gameObject.SetActive(false);
               _currentView = null;
            }
            StartCoroutine(JumpToStep(step));

        }


        IEnumerator JumpToStep(string step)
        {
            _currentView = _steps.Find((v) => v.Step == step);
            if (_currentView != null)
            {
                yield return new WaitForSeconds(_currentView.ShowDelay);
                if (_currentView.SaveStep)
                {
                   SaveStep(_stepIndex);
                }
                _currentView.gameObject.SetActive(true);
                ShowBackground();
            }

            EventsManager.Instance.Call(TutorialStepChanged);
            OnStepChanged();
            Debug.Log("CurrentTutorial step: " + step);
            yield return null;
        }


        private void LoadStep(UnityAction<int> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_TUTORIAL_STEP");
            request.SetDurable(true);
            GameRoot.Instance.NetManager.RunLogEventRequest(request, (responce) =>
            {
                int step = 0;
                if (responce.ScriptData != null && responce.ScriptData.ContainsKey(GS_ScriptData.TutorialStepData))
                {
                    step = responce.ScriptData.GetInt(GS_ScriptData.TutorialStepData).Value;
                }
                callback(step);
            });
        }

        private void SaveStep(int step)
        {
            var request = new LogEventRequest();
            request.SetEventAttribute("STEP", step);
            request.SetEventKey("SAVE_TUTORIAL_STEP");
            request.SetDurable(true);
            GameRoot.Instance.NetManager.RunLogEventRequest(request);
        }

        public void ShowBackground()
        {
            if (holderImage != null && holderImage.color.a < originColor.a)
            {
                holderImage.color = originColor;
            }
        }

        public void HideBackground()
        {
            if (holderImage != null)
            {
                holderImage.color = Color.clear;
            }
        }
    }
}
