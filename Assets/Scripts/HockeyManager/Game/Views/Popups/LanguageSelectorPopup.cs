﻿using System;
using HockeyManager.Game.UI.Popups;
using SCTools.Localization;
using SCTools.UI;
using SCTools.UIComponents;
using UnityEngine;

namespace HockeyManager.Game.Views.Popups
{
    public class LanguageSelectorPopup:BasePopup
    {

        [SerializeField] private UniversalButtonsToggleGroup toggleGroup;

        void Start()
        {
            toggleGroup.SelectButton(LocaleManager.Instance.CurrentLocaleType.ToString().ToLower());
        }

        void OnEnable()
        {
            toggleGroup.OnItemSelected.AddListener(OnItemSelectedHandler);
        }

        void OnDisable()
        {
            toggleGroup.OnItemSelected.RemoveListener(OnItemSelectedHandler);
        }

        private void OnItemSelectedHandler(UniversalButton button)
        {
            LocaleType type = (LocaleType)Enum.Parse(typeof (LocaleType), button.GetId().ToUpper());
            LocaleManager.Instance.CurrentLocaleType = type;
        }
    }
}
 