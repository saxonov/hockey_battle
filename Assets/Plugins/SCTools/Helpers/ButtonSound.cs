﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace SCTools.Helpers
{

    using SoundManager;

    public class ButtonSound : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {

        public enum ClickEventType
        {
            PointerDown,
            PointerUp,
            None

        }

        [SerializeField]
        private ClickEventType clickType = ClickEventType.PointerUp;

        [SerializeField]private string soundName;
        [SerializeField]private float pitch = 1f;
        [SerializeField] private float volume = 1f;

        [Tooltip("if Button or Toggle attached check for interactable")]
        public bool lockButtonSound = false;

        private Toggle toggleAttached;
        private Button butonAttached;

        void Start()
        {
            butonAttached = GetComponent<Button>();
            toggleAttached = GetComponent<Toggle>();


            if (clickType == ClickEventType.None)
            {
                if (butonAttached != null)
                {
                    butonAttached.onClick.AddListener(OnButtonClickHandler);

                }else if (toggleAttached != null)
                {
                    toggleAttached.onValueChanged.AddListener(OnToggleChangedHanlder);
                }
            }
        }

    

        void OnDestroy()
        {
            if (butonAttached != null)
            {
                butonAttached.onClick.RemoveListener(OnButtonClickHandler);
            }

            if (toggleAttached != null)
            {
                toggleAttached.onValueChanged.RemoveListener(OnToggleChangedHanlder);
            }
        }

        private void OnButtonClickHandler()
        {
              SoundManager.Instance.PlaySFX(soundName,pitch,volume);
        }

        private void OnToggleChangedHanlder(bool value)
        {
            SoundManager.Instance.PlaySFX(soundName, pitch, volume);
        }


        public void OnPointerUp(PointerEventData eventData)
        {
            //if(butonAttached && butonAttached.interactable)
            if (lockButtonSound)
            {
                if (butonAttached != null && !butonAttached.interactable)
                {
                    return;
                }
                if (toggleAttached != null && !toggleAttached.interactable)
                {
                    return;
                }
            }
            if (clickType == ClickEventType.PointerUp && Input.touchCount <= 1)
            {
                SoundManager.Instance.PlaySFX(soundName, pitch, volume);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (lockButtonSound)
            {
                if (butonAttached != null && !butonAttached.interactable)
                {
                    return;
                }
                if (toggleAttached != null && !toggleAttached.interactable)
                {
                    return;
                }
            }
            if (clickType == ClickEventType.PointerDown && Input.touchCount <= 1)
            {
                SoundManager.Instance.PlaySFX(soundName, pitch, volume);
            }
        }
    }

}