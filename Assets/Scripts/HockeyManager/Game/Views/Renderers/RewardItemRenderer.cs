﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem;
using HockeyManager.TutorialSystem.Data;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class RewardItemRenderer:MonoBehaviour
    {

        public const string OnGainRewardClicked = "RewardItemRenderer.OnGainRewardClicked";
       
#pragma warning disable CS0649 // Полю "RewardItemRenderer.spinner" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Animator spinner;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.spinner" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardItemRenderer.rewardLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text rewardLabel;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.rewardLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "RewardItemRenderer.rewardIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Image rewardIcon;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.rewardIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardItemRenderer.rewardFill" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private Image rewardFill;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.rewardFill" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "RewardItemRenderer.takeButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button takeButton;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.takeButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardItemRenderer.awardPanels" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl[] awardPanels;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.awardPanels" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "RewardItemRenderer.hallItemLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text hallItemLabel;
#pragma warning restore CS0649 // Полю "RewardItemRenderer.hallItemLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private RectTransform tutorialHelper;

        [HideInInspector] public bool isShifting = false;

        private RewardModel _model;

        private RectTransform rtransform;

        public RewardModel Model
        {
            get { return _model; }
        }

        public RectTransform Rtransform
        {
            get
            {
                rtransform = rtransform ?? gameObject.GetComponent<RectTransform>();
                return rtransform;
            }
        }
        
        void Awake()
        {
           spinner.gameObject.SetActive(false);
           
            hallItemLabel.gameObject.SetActive(false);
            hallItemLabel.color = UISettings.GreenTextColor;

            takeButton.onClick.AddListener(OnTakeClickHandler);
        }

        private void OnTakeClickHandler()
        {
            SoundManager.Instance.PlaySFXOnGameObject("pickupreward",gameObject);
            takeButton.gameObject.SetActive(false);
            spinner.gameObject.SetActive(true);
            EventsManager.Instance.Call(OnGainRewardClicked, this);
        }
        
        public void SetModel(RewardModel model)
        {
            _model = model;
            takeButton.gameObject.SetActive(true);
            spinner.gameObject.SetActive(false);

            for (int i = 0; i < awardPanels.Length; i++)
            {
                awardPanels[i].SetText(0);
                awardPanels[i].gameObject.SetActive(false);
            }
            gameObject.name = _model.DateTimestamp.ToString();
            rewardIcon.sprite = GameSettings.Instance.GetRewardIcon(_model.Type);


            if (model.Type == RewardType.MatchLooseReward)
            {
                rewardFill.color = new Color32(42, 32, 23, byte.MaxValue);
            }
            else
            {
                rewardFill.color = new Color32(34, 38, 24, byte.MaxValue);
            }

            SetRewardData(_model.RewardData);
        }

        public void SetDescription(string text)
        {
            rewardLabel.text = text;

            if (_model != null)
            {
                if (!string.IsNullOrEmpty(_model.RewardData.relics))
                {
                    hallItemLabel.text = "+" + LocaleManager.Instance.GetText(_model.RewardData.relics + "_hallitem");
                    hallItemLabel.gameObject.SetActive(true);
                }
                else
                {
                    hallItemLabel.gameObject.SetActive(false);
                }
            }
            
        }

        private void SetRewardData(RewardData reward)
        {
            RewardData rwd = reward;
            IconsEnum iconType = IconsEnum.Money;
            for (int i = 0; i < awardPanels.Length; i++)
            {
                var rwdValue = -1;
                if (rwd.coins > 0)
                {
                    rwdValue = rwd.coins;
                    iconType = IconsEnum.Money;
                    rwd.coins = 0;

                }else if (rwd.exp > 0)
                {
                    rwdValue = rwd.exp;
                    iconType = IconsEnum.Exp;
                    rwd.exp = 0;

                }else if (rwd.fans > 0)
                {
                    rwdValue = rwd.fans;
                    iconType = IconsEnum.Fans;
                    rwd.fans = 0;

                }else if (rwd.gold > 0)
                {
                    rwdValue = rwd.gold;
                    iconType = IconsEnum.Gold;
                    rwd.gold = 0;
                }

                if (rwdValue != -1)
                {
                    awardPanels[i].gameObject.SetActive(true);
                    awardPanels[i].SetText(rwdValue);
                    awardPanels[i].SetIcon(GameSettings.Instance.GetIcon(iconType));
                }
            }
        }

        void OnEnable()
        {
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Add(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                OnTutorialStepChanged();
            }
            else
            {
                if (tutorialHelper != null)
                {
                    Destroy(tutorialHelper.gameObject);
                    tutorialHelper = null;
                }

            }
        }

        private void OnTutorialStepChanged()
        {
            if (TutorialManager.Instance.IsCurrentViewStep(TutorStepTypes.TakeReward))
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                TutorialManager.Instance.CurrentView.SetShowTarget(tutorialHelper);
            }
        }

    }
}
