﻿using HockeyManager.Game.Dict;
using UnityEngine;

namespace HockeyManager.Game.Impl
{

    public interface IGameSettings
    {
        EquipmentsSettingsInfo GetEquipmentData(EquipmentType type);
        MapBuildingSettingsInfo GetBuildingData(MapBuildingsType type);
        EmployeeSettingsInfo GetEmployeeData(EmployeeType type);
        HallItemInfo GetHallItemInfo(int id);

        GameObject EqpProgressPrefab { get; }
        GameObject EqpCaptionPrefab { get; }
        GameObject BuildingProgressPrefab { get; }

        //BuildingSettingsObject BuildingSettings { get; }
        //EquipmentSettingsObject EquipmentSettings { get; }
        //PersonalSettingsData PersonalSettings { get; }
        //HallItemsSettingsData HallItemsSettings { get; }

        Sprite GetIcon(IconsEnum type);
        Sprite GetIcon(int currencyType);
        Sprite GetRewardIcon(RewardType type);

        Sprite GetLogo64(int id);
        Sprite GetLogo128(int id);
        Sprite GetLogo256(int id);

        Sprite GetIconTextCompetitionType(string type, out string label);

        Shader HsvShader { get; }

        string GetRankLocaleKey(RankType rank);
    }
}
