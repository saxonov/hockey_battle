﻿using System.Collections.Generic;
using GameSparks.Core;
using UnityEngine;

namespace HockeyManager.Game.Dict
{
    public class GS_Constants
    {
        private bool _isInitialized = false;
        private int _maxSkillPoints = 0;

        private int _maxTeamLevel = 0;

    //  private int _skillPointsForEquipmentUpgrade = 0;
        private int _energyStartTime = 0;
        private int _arenaUpgradeFansLimitPercent = 0;
        private int _maxBuildingsOnMapCount = 0;
        private int _resetSkillsGoldPrice = 0;
        private int _cashboxGoldPriceForFans = 0;
        private int _trainingsRefreshGoldPrice = 0;
       // private int _trainingsRefreshCount = 0;

        private int _bankExchangeGoldToMoney;
        private int _bankExchangeMoneyToGold;

        private int _moderLockChatTimeSeconds = 0;

        private int _teamNameChangeGoldPrice;

        public int MaxSkillPoints
        {
            get { return _maxSkillPoints; }
        }

        public int MaxTeamLevel
        {
            get { return _maxTeamLevel; }
        }

        public int EnergyStartTime
        {
            get { return _energyStartTime; }
        }

        public int ArenaUpgradeFansLimitPercent
        {
            get { return _arenaUpgradeFansLimitPercent; }
        }

        public int MaxBuildingsOnMapCount
        {
            get { return _maxBuildingsOnMapCount; }
        }

        public int ResetSkillsGoldPrice
        {
            get { return _resetSkillsGoldPrice; }
        }

        public int CashboxGoldPriceForFans
        {
            get { return _cashboxGoldPriceForFans; }
        }

        public int TrainingsRefreshGoldPrice
        {
            get { return _trainingsRefreshGoldPrice; }
        }

        public int BankExchangeGoldToMoney
        {
            get { return _bankExchangeGoldToMoney; }
        }

        public int BankExchangeMoneyToGold
        {
            get { return _bankExchangeMoneyToGold; }
        }

        public int TeamNameChangeGoldPrice
        {
            get { return _teamNameChangeGoldPrice; }
        }

        public int ModerLockChatTimeSeconds
        {
            get { return _moderLockChatTimeSeconds; }
        }

        //public int TrainingsRefreshCount
        //{
        //    get { return _trainingsRefreshCount; }
        //}

        public void Initialize(GSData data)
        {
            if (data == null || _isInitialized) return;

            if (data.ContainsKey("maxSkillPoints"))
            {
                _maxSkillPoints = data.GetInt("maxSkillPoints").Value;
            }

            if (data.ContainsKey("lockChatTimeSeconds"))
            {
                _moderLockChatTimeSeconds = data.GetInt("lockChatTimeSeconds").Value;
            }

            if (data.ContainsKey("bankGoldToMoney") && data.ContainsKey("bankMoneyToGold"))
            {
                _bankExchangeGoldToMoney = data.GetInt("bankGoldToMoney").Value;
                _bankExchangeMoneyToGold = data.GetInt("bankMoneyToGold").Value;
            }

            if (data.ContainsKey("teamNameChangeGoldPrice"))
            {
                _teamNameChangeGoldPrice = data.GetInt("teamNameChangeGoldPrice").Value;
            }

            _trainingsRefreshGoldPrice = data.GetInt("trainingsRefreshGoldPrice").Value;
            _resetSkillsGoldPrice = data.GetInt("resetSkillsGoldPrice").Value;
            _cashboxGoldPriceForFans = data.GetInt("cashboxGoldPriceForFans").Value;
            _maxBuildingsOnMapCount = data.GetInt("maxBuildingsOnMap").Value;
            _arenaUpgradeFansLimitPercent = data.GetInt("arenaUpgradeFansLimitPercent").Value;
            _energyStartTime = data.GetInt("startEnergyTimeSeconds").Value;
            _maxTeamLevel = data.GetInt("maxTeamKevel").Value;

            if (data.ContainsKey("viewData"))
            {
                UISettings.SetViewData(data.GetGSData("viewData").GetGSDataList("eqpBuildingsViewStates"));
            }
            else
            {
                //for old server side
                TextAsset safeConfigJson = Resources.Load<TextAsset>("Data/equipmentsBuildingsStatesConfig");
                //Debug.Log(safeConfigJson.text);

                GSRequestData reqData = new GSRequestData();
                reqData.AddJSONStringAsObject("viewData", safeConfigJson.text);
                UISettings.SetViewData(reqData.GetGSDataList("viewData"));
            }

            _isInitialized = true;
        }

    }
}
