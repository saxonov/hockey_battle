﻿using System;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class GSAdminController:IGameController,ICheckMessages
    {

        public const string NeedUpdateGame = "GSAdminController.NeedUpdateGame";
        public const string ChatAvailableEvent = "GSAdminController.chatAvailable";
        public const string PenaltyGoldBalanceEvent = "GSAdminController.goldBalancePenalty";
        public const string ModeratorValueChangeEvent = "GSAdminController.moderatorChangeValue";

        private IGameRoot _root;

        private bool _needUpdateBuild = false;

        private long _endChatLockedTime = 0;

        private bool _isChatAvailable = true;

        private bool _isModerator = false;

        private TeamController _team;

        public void Initialize(IGameRoot root)
        {
            _root = root;
            _team = _root.GetController<TeamController>();
        }

        public void Reset()
        {
           
        }


        public bool NeedUpdateBuild
        {
            get { return _needUpdateBuild; }
        }


        public bool IsChatAvailable
        {
            get { return _isChatAvailable; }
        }

        public long EndChatLockedTime
        {
            get { return _endChatLockedTime; }
        }

        public bool IsModerator
        {
            get { return _isModerator; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teamIdName">name or player id</param>
        public void LockChatForPlayer(string teamName,UnityAction callback)
        {
            if (!_isModerator)
            {
                throw new Exception("Wrong operation!, Only Moderator can lock chat for teams!");
            }
            var request = new LogEventRequest();
            request.SetEventKey("LOCK_CHAT");
            request.SetEventAttribute("TEAM_NAME",teamName);
            request.SetEventAttribute("SECONDS_TIME",GameData.GSConstData.ModerLockChatTimeSeconds);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });
        }

        public void RemoveChatMessage(string playerId, long time)
        {
            if (!_isModerator)
            {
                throw new Exception("Wrong operation!, Only Moderator can remove chat messages!");
            }
            var request = new LogEventRequest();
            request.SetEventKey("REMOVE_MESSAGE");
            request.SetEventAttribute("USER_ID", playerId);
            request.SetEventAttribute("TIME", time);
            _root.NetManager.RunLogEventRequest(request);

        }

        public void CheckIsChatAvailable(bool resetAvailableChat = false)
        {
            if (resetAvailableChat)
            {
                _isChatAvailable = true;
            }
            var request = new LogEventRequest();
            request.SetEventKey("CHAT_AVAILABLE");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors && responce.ScriptData.ContainsKey(GS_ScriptData.ChatAvaialbleData)) 
                {
                    GSData data = responce.ScriptData.GetGSData(GS_ScriptData.ChatAvaialbleData);
                    if (data != null)
                    {
                        _endChatLockedTime = data.GetLong("unlockTime").Value;
                        _isChatAvailable = GameTimer.TimeLeftSeconds(_endChatLockedTime) <= 0;
                        EventsManager.Instance.Call(ChatAvailableEvent);
                    }

                }
            });
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.AdminMsg)
            {
                string type = msgData.GetString("type");

                switch (type)
                {
                    case GS_Messages.ModeratorType:
                        _isModerator = msgData.GetBoolean("value").Value;
                        EventsManager.Instance.Call(ModeratorValueChangeEvent);
                    break;
                    case GS_Messages.LockChatType:
                        _endChatLockedTime = msgData.GetLong("unlockTime").Value;
                        _isChatAvailable = GameTimer.TimeLeftSeconds(_endChatLockedTime) <= 0;
                        EventsManager.Instance.Call(ChatAvailableEvent);
                    break;
                    case GS_Messages.PenaltyGoldBalanceType:
                        _team.RefreshBalance();
                        EventsManager.Instance.Call(PenaltyGoldBalanceEvent);
                    break;
                }
            }
            if (msgCode == GS_Messages.AlertMsg)
            {
                string newVersion = msgData.GetString("version");
                if (Application.version != newVersion)
                {
                    _needUpdateBuild = true;
                    EventsManager.Instance.Call(NeedUpdateGame);
                }
            }
            
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return  data != null && (msgType == GS_Messages.AdminMsg || msgType == GS_Messages.AlertMsg);
        }
        
    }
}
