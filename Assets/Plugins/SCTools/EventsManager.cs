﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

namespace SCTools.EventsSystem
{

    public class UnityGenericEvent<T> : UnityEvent<T>{};
    public class UnityGenericEvent<T,U> : UnityEvent<T,U>{};


  /// <summary>
    /// 
    /// </summary>
    public class GameEventData
    {
       
    }

    public class EventTParam<T> : UnityEvent<T> { }
    public class EventTUParam<T,U> : UnityEvent<T,U>{}

    public interface IEventDispatcher
    {
        bool HasEventListeners();
        void ClearEvents();
    }

    /// <summary>
    /// 
    /// </summary>
    public class EventDispatcher<T>:IEventDispatcher where T:GameEventData
    {

        private event Action<T> callbackEvent;

        public void AddEventListener(Action<T> listener)
        {
            //check for duplicate listeners
            if (callbackEvent == null || !callbackEvent.GetInvocationList().Contains(listener))
            {
                callbackEvent += listener;
            }
        }

        public void ClearEvents()
        {
            callbackEvent = null;
        }


        public void RemoveEventListener(Action<T> listener)
        {
            if (callbackEvent != null)
            {
                callbackEvent -= listener;
            }
        }

        public bool HasEventListeners()
        {
            return callbackEvent != null;

        }

        public void DispatchEvent(T data)
        {

            if (callbackEvent != null)
            {
                //Debug.Log(gameEvent.GetInvocationList().Length);

                try
                {
                    callbackEvent(data);
                }
                catch (Exception)
                {
                    Debug.LogErrorFormat("listener {0} of target {1} not exist anymore", callbackEvent.Method.Name, callbackEvent.Target);
                    foreach (var @delegate in callbackEvent.GetInvocationList())
                    {
                        if (@delegate.Method.Equals(callbackEvent.Method))
                        {
                            callbackEvent -= (Action<T>)@delegate;
                        }
                    }

                }
            }

        }
    }
    /// <summary>
    /// TODO: need to test
    /// </summary>
    public sealed class EventsManager
    {

        #region Singleton init

        private static EventsManager _instance;

        EventsManager()
        {
            Initialize();
        }

        private void Initialize()
        {
            
        }

        public static EventsManager Instance
        {
            get { return _instance ?? (_instance = new EventsManager()); }
        }

        #endregion

        private Dictionary<string, List<Delegate>> events = new Dictionary<string, List<Delegate>>();
        private Dictionary<string, UnityEventBase> unityEvents = new Dictionary<string, UnityEventBase>();


        private Dictionary<int,IEventDispatcher> dispatchers = new Dictionary<int, IEventDispatcher>();
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public bool HasDispatcher(GameObject go)
        {
            int UID = go.GetInstanceID();
            return dispatchers.ContainsKey(UID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="go"></param>
        public void RemoveDispatcher(GameObject go)
        {
            int UID = go.GetInstanceID();
            if (dispatchers.ContainsKey(UID))
            {
                IEventDispatcher d = dispatchers[UID];
                d.ClearEvents();
                dispatchers.Remove(UID);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public EventDispatcher<T> GetDispatcher<T>(GameObject go) where T:GameEventData
        {
            int UID = go.GetInstanceID();
            IEventDispatcher dispatcher;
            if (!dispatchers.TryGetValue(UID, out dispatcher))
            {
                dispatcher = new EventDispatcher<T>();
                dispatchers.Add(UID,dispatcher);
            }
            return dispatcher as EventDispatcher<T>;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="toRemove"></param>
        private void RemoveInternalCallback(string type,Delegate toRemove)
        {
            if (events.ContainsKey(type))
            {
                List<Delegate> list = events[type];
                if (list.Contains(toRemove))
                {
                    list.Remove(toRemove);
                }
                if (list.Count == 0)
                {
                    events.Remove(type);
                }
            }
        }

        private void AddInternalCallback(string type, Delegate callback)
        { 
            if (!events.ContainsKey(type))
            {
                events.Add(type, new List<Delegate>());
            }
            events[type].Add(callback);
        }

        public void AddEvent(string type, UnityAction action)
        {
            if (!unityEvents.ContainsKey(type))
            {
                unityEvents[type] = new UnityEvent();
            }
           ((UnityEvent) unityEvents[type]).AddListener(action);
        }

        public void AddEvent<T>(string type, UnityAction<T> action)
        {
            if (!unityEvents.ContainsKey(type))
            {
                unityEvents[type] = new EventTParam<T>();
            }
           ((EventTParam<T>)unityEvents[type]).AddListener(action);
        }

        public void RemoveEvent(string type, UnityAction action)
        {
            if (unityEvents.ContainsKey(type))
            {
                ((UnityEvent)unityEvents[type]).RemoveListener(action);
                
            }
        }

        public void RemoveEvent<T>(string type, UnityAction<T> action)
        {
            if (unityEvents.ContainsKey(type))
            {
                ((EventTParam<T>)unityEvents[type]).RemoveListener(action);
            }
        }

        public void CallEvent(string type)
        {
            if (unityEvents.ContainsKey(type))
            {
                ((UnityEvent)unityEvents[type]).Invoke();
            }
        }
        public void CallEvent<T>(string type,T arg)
        {
            if (unityEvents.ContainsKey(type))
            {
                ((EventTParam<T>)unityEvents[type]).Invoke(arg);
            }
        }

        public void Add<T>(string type, Action<T> handler)
        {
            AddInternalCallback(type,handler);
        }

        public void Add(string type, Action handler)
        {
            AddInternalCallback(type, handler);
        }

        public void Add<T, U>(string type, Action<T, U> handler)
        {
            AddInternalCallback(type, handler);
        }
        
        public void Remove(string type, Action handler)
        {
            RemoveInternalCallback(type, handler);
        }

        public void Remove<T>(string type, Action<T> handler)
        {
            RemoveInternalCallback(type, handler);

        }

        public void Remove<T, U>(string type, Action<T, U> handler)
        {
            RemoveInternalCallback(type, handler);
        }

        public void RemoveType(string type)
        {
            if (events.ContainsKey(type))
            {
                events[type].Clear();
            }
        }

        public bool Call<T>(string type, T arg0)
        {
            List<Delegate> list;
            if (events.TryGetValue(type, out list))
            {
                int len = list.Count;
                list = list.GetRange(0, len);
                foreach (var @delegate in list)
                {
                    var callback = (Action<T>)@delegate;
                    if (callback != null)
                    {
                        callback.Invoke(arg0);
                    }

                }
            }
            return false;
        }


        public bool Call<T, U>(string type, T arg0, U arg1)
        { 
            List<Delegate> list;
            if (events.TryGetValue(type, out list))
            {
                int len = list.Count;
                list = list.GetRange(0, len);
                foreach (var @delegate in list)
                {
                    var callback = (Action<T,U>)@delegate;
                    if (callback != null)
                    {
                        callback.Invoke(arg0, arg1);
                    }
                  
                }
            }
            return false;
        }

        public bool HasEventType(string type)
        {
            return events.ContainsKey(type);
        }

        public bool Call(string type)
        {
            if (events.ContainsKey(type))
            {

                int len = events[type].Count;
                List<Delegate> list = events[type].GetRange(0,len);
               
                foreach (var @delegate in list)
                {
                    var callback = (Action)@delegate;
                    if (callback != null)
                    {
                        callback.Invoke();
                    }
                }

            }
            return false;
        }

        public void Clear()
        {
           events.Clear();
            dispatchers.Clear();
        }

    }


}
