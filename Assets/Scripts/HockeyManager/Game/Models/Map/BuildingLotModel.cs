﻿using System;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Game.Models.Map
{
    [Serializable]
    public class BuildingLotModel:IComparable<BuildingLotModel>
    {

        [SerializeField] private MapConstructionLot[] sites = null;


        [SerializeField] private int _sortIndex = 0;


        //[Tooltip("Predefined static place or not")] [SerializeField] private bool _isFixed = false;
        //need to assign in server side as is
#pragma warning disable CS0649 // Полю "BuildingLotModel._id" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию 0.
        [SerializeField] private int _id;
#pragma warning restore CS0649 // Полю "BuildingLotModel._id" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию 0.

        private int _arenaUnlockLevel = 0;

        private IBuildingModel _building;

        public int ArenaUnlockLevel
        {
            get { return _arenaUnlockLevel; }
            set { _arenaUnlockLevel = value; }
        }


        public bool IsBuildingExist()
        {
            return _building != null;
        }

        public IBuildingModel Building
        {
            get { return _building; }
            set
            {
                if (_building == null)
                {
                    _building = value;
                    if (_building.Id == MapBuildingsType.AdBanner)
                    {
                        _sortIndex -= 1;
                    }
                }
              
            }
        }

        public int Id
        {
            get { return _id; }
        }

        //public bool IsFixed
        //{
        //    get { return _isFixed; }
        //}

        public MapConstructionLot[] Sites
        {
            get { return sites; }
        }

        public int SortIndex
        {
            get
            {
                return _sortIndex;
            }
        }

        //public bool CheckType(MapBuildingsType type)
        //{
        //    for (int i = 0; i < sites.Length; i++)
        //    {
        //        if (sites[i].type == type || type == MapBuildingsType.None)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

      

        public int CompareTo(BuildingLotModel other)
        {
             int currentSortIndex = _sortIndex;
            int otherSortIndex = other.SortIndex;
            
            return otherSortIndex - currentSortIndex;
        }
    }
}
