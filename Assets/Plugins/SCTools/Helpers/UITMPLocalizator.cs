﻿using SCTools.Localization;
using TMPro;
using UnityEngine;

namespace  SCTools.Helpers
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UITMPLocalizator:MonoBehaviour
    {
        [SerializeField] private string locale_key;

        //[Tooltip("Use for many locale_keys")]
        //[SerializeField] private char separator = ' ';


        [SerializeField]private string postfix;
        [SerializeField]private string prefix;

        public bool allCapital;
        private TextMeshProUGUI textField;

        // Use this for initialization
        void Awake()
        {
            
            textField = GetComponent<TextMeshProUGUI>();
            OnLocaleChanged();
        }

        void OnEnable()
        {
            LocaleManager.Instance.LanguageChanged += OnLocaleChanged;
            OnLocaleChanged();
        }

        void OnDisable()
        {
            LocaleManager.Instance.LanguageChanged -= OnLocaleChanged;
        }

        public void SetLocaleKeys(bool upperCase, string key)
        {
            locale_key = key;
            allCapital = upperCase;
            OnLocaleChanged();


        }

        private void OnLocaleChanged()
        {
            if (textField != null)
            {
                LocaleManager locales = LocaleManager.Instance;
                string resultText = locales.GetText(locale_key);
      
                resultText = allCapital ? resultText.ToUpper() : resultText;

              
                if (!string.IsNullOrEmpty(postfix))
                {
                    resultText = resultText.AddPostfix(postfix);
                }

                if (!string.IsNullOrEmpty(prefix))
                {
                    resultText = resultText.AddPrefix(prefix);
                }



                textField.text = resultText;
            }

        }
        void OnValidate()
        {
            OnLocaleChanged();
        }

    }
}