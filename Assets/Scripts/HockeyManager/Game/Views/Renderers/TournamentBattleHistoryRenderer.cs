﻿using System;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Battles;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class TournamentBattleHistoryRenderer:MonoBehaviour,IPointerClickHandler
    {
        public const string OnTournamentHistoryRecordSelected = "TournamentBattleHistoryRenderer.OnTournamentHistoryRecordSelected";

        [SerializeField] private Text dateLabel;
        [SerializeField] private Text timeLabel;
        [SerializeField] private Text cupNameLabel;
        [SerializeField] private Text resultLabel;

        [SerializeField] private Image dateField;
        [SerializeField] private Image timeField;
        [SerializeField] private Image cupNameField;
        [SerializeField] private Image resultField;


        [SerializeField]private Color firstColumnColor;
        [SerializeField]private Color firstColumnInvertedColor;
        [SerializeField]private Color secondColumnColor;
        [SerializeField]private Color secondColumnInvertedColor;
        
        private TournamentResultData _resultData;

        public void SetData(TournamentResultData resultData)
        {
            _resultData = resultData;
            DateTime date = (new DateTime(1970, 1, 1, 0, 0, 0,DateTimeKind.Utc)).AddSeconds(_resultData.StartTimeStamp);
            dateLabel.text = date.ToString("dd/MM/yy");
            timeLabel.text = date.ToLocalTime().ToString("HH:mm");

            var player = _resultData.Players.Find(p => p.TeamInfo.IsMe());

            cupNameLabel.text = _resultData.GetCupName();
            string victory = LocaleManager.Instance.GetText("game.common.victory");
            string participation = LocaleManager.Instance.GetText("game.common.participation");
            resultLabel.text = player.Position == 1 ? victory.GetUITextColorFormated(UISettings.GreenTextColor) : participation;

        }

        public void InvertColors(int index)
        {
            if (index % 2 != 0)
            {
                dateField.enabled = true;
                cupNameField.enabled = true;
                dateField.color = firstColumnInvertedColor;
                timeField.color = secondColumnInvertedColor;
                cupNameField.color = firstColumnInvertedColor;
                resultField.color = secondColumnInvertedColor;
            }
            else
            {
                dateField.color = firstColumnColor;
                timeField.color = secondColumnColor;
                cupNameField.color = firstColumnColor;
                resultField.color = secondColumnColor;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
           SoundManager.Instance.PlaySFX("click");
            EventsManager.Instance.Call(OnTournamentHistoryRecordSelected, _resultData);
        }
    }
}
