﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.UI.Pages;

namespace HockeyManager.Game.Managers
{

    public class PageNavigatorManager : MonoBehaviour,IPageNavigator
    {
        private bool _cancelMovePage = false;

        private bool _isLocked = false;

        [SerializeField] private Canvas _canvas;
        [SerializeField]private List<BasePage> _pages;
        [SerializeField] private CanvasGroup _mainUIGroup;
   
        private NavigationPageTypes _lastPage = NavigationPageTypes.None;

        private NavigationPageTypes _selectedPage = UISettings.StartPage;

        private List<IPageChangeHandler> _pageHandlers;

        private static IPageNavigator _instance;

        //public static UnityGenericEvent<bool> OnMainUIPageControlsLockChangeEvent = new UnityGenericEvent<bool>();

        void Awake()
        {
            _pageHandlers = new List<IPageChangeHandler>();
            _instance = this;
        }

        public bool IsRewardsNavigation
        {
            get { return _selectedPage == NavigationPageTypes.Rewards || _selectedPage == NavigationPageTypes.Missions; }
        }

        public bool IsEPSNavigation
        {
            get
            {
                return _selectedPage == NavigationPageTypes.CloakRoom ||
                       _selectedPage == NavigationPageTypes.EPS ||
                       _selectedPage == NavigationPageTypes.Personal ||
                       _selectedPage == NavigationPageTypes.Skills;
            }
        }

        public NavigationPageTypes SelectedPage
        {
            get { return _selectedPage; }
        }


        public bool CancelMovePage
        {
            get { return _cancelMovePage; }
            set { _cancelMovePage = value; }
        }

        public NavigationPageTypes LastVisitedPage
        {
            get { return _lastPage; }
        }

        public static IPageNavigator Instance
        {
            get { return _instance; }
        }

        public Canvas Canvas
        {
            get { return _canvas; }
        }

        void Start()
        {
            if (_pages == null)
            {
                _pages = new List<BasePage>();
                for (int i = 0; i < transform.childCount; i++)
                {
                    BasePage page = transform.GetChild(i).GetComponent<BasePage>();
                    _pages.Add(page);
                }
            }

            _pages.ForEach(c =>
            {
                c.Hide();
            });
            
            GotoPage(_selectedPage);
        }

        public void LockControlls()
        {
            if(_isLocked)return;
            _isLocked = true;
            _cancelMovePage = true;
            _mainUIGroup.blocksRaycasts = false;
            _mainUIGroup.interactable = false;
           // OnMainUIPageControlsLockChangeEvent.Invoke(true);
        }

        public void UnLockControlls(float delay = 0)
        {
            if(!_isLocked)return;
            DOVirtual.DelayedCall(delay, () =>
            {
                _isLocked = false;
                _cancelMovePage = false;
                _mainUIGroup.blocksRaycasts = true;
                _mainUIGroup.interactable = true;
                // OnMainUIPageControlsLockChangeEvent.Invoke(false);
            });
        }

        public void AddPageChangeHandler(IPageChangeHandler pageChangeHandler)
        {
            if (!_pageHandlers.Contains(pageChangeHandler))
            {
                _pageHandlers.Add(pageChangeHandler);
            }
        }

        public void RemovePageChangeHandler(IPageChangeHandler pageChangeHandler)
        {
            if (_pageHandlers.Contains(pageChangeHandler))
            {
                _pageHandlers.Remove(pageChangeHandler);
            }
        }

        public void ReloadSelectedPage()
        {
            BasePage page = _pages.Find(p => p.NavigationId == _selectedPage);
            page.Hide();
            page.Show();
        }

        public void GotoPage(NavigationPageTypes pageType)
        {

            _selectedPage = pageType;

            if (_cancelMovePage)
            {
                _pageHandlers.ForEach(p =>
                {
                    p.OnPageChanged(pageType, _lastPage);
                });
                return;
            }

          

            if (_selectedPage != _lastPage)
            {
                
                _pages.ForEach(c => c.Hide());
               
                 BasePage page = _pages.Find(p => p.NavigationId == _selectedPage);
                if (page != null)
                {
                    page.Show();
                }
                else
                {
                    Debug.LogError("Page " + _selectedPage + " not added to pages list!");
                }
                
                _pageHandlers.ForEach(p =>
                {
                    p.OnPageChanged(_selectedPage,_lastPage);
                });

                _lastPage = pageType;
                Debug.LogFormat("Goto page {0}", pageType.ToString());
            }
        }


#if UNITY_ANDROID
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape) && GameData.IsTutorialPassed)
            {
                if (_selectedPage != NavigationPageTypes.Home)
                {
                    GotoPage(NavigationPageTypes.Home);
                }
            }
        }
#endif
        void OnDestroy()
        {
            _instance = null;
            _pageHandlers.Clear();
            
        }

        //public void GotoPage(string goName)
        //{
        //    GotoPage((NavigationPageTypes)Enum.Parse(typeof(NavigationPageTypes), goName));
        //}
    }
}