﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.UI.Popups;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Views.Popups
{
    public class CustomBasePopup:MonoBehaviour,IPopup
    {
        private object _data;

        private UnityEvent OnCloseEvent;
        private UnityEvent OnApplyEvent;

        public virtual void Dispose()
        {
            _data = null;
            if (OnCloseEvent != null)
            {
                OnCloseEvent.RemoveAllListeners();
            }
            if (OnApplyEvent != null)
            {
                OnApplyEvent.RemoveAllListeners();
            }
        }

        public PopupType Type { get; set; }

        public object GetData()
        {
            return _data;
        }

        public virtual void OnApply()
        {
            if (OnApplyEvent != null)
            {
                OnApplyEvent.Invoke();
            }
        }

        public virtual void OnClose()
        {
            if (OnCloseEvent != null)
            {
                OnCloseEvent.Invoke();
            }
        }

        public IPopup InjectController(IGameController controller)
        {
            SetController(controller);
            return this;
        }

        protected virtual void SetController(IGameController controller)
        {
            //TODO implementation
        }

        public IPopup SetMinutesTickUpdate(UnityAction<IPopup> tickCallback)
        {
            throw new System.NotImplementedException("SetMinutesTickUpdate");
        }

        public IPopup HideButtons(bool yesButton, bool noButton)
        {
            throw new System.NotImplementedException("HideButtons");
        }

        public virtual IPopup SetCallbacks(UnityAction applyCallback, UnityAction cancelCallback = null)
        {
            if (applyCallback != null)
            {
                OnApplyEvent = OnApplyEvent ?? new UnityEvent();
                OnApplyEvent.AddListener(applyCallback);
            }
            if (cancelCallback != null)
            {
                OnCloseEvent = OnCloseEvent ?? new UnityEvent();
                OnCloseEvent.AddListener(cancelCallback);
            }
            return this;
        }

        public virtual IPopup SetTitle(string title, bool localizable = true)
        {
            throw new System.NotImplementedException("SetTitle");
        }

        public IPopup SetTextIconControls(params string[] values)
        {
            throw new System.NotImplementedException("SetTextIconControls");
        }

        public IPopup SetTextIconControls(params int[] values)
        {
            throw new System.NotImplementedException("SetTextIconControls");
        }

        public IPopup SetTextIconControls(params Sprite[] icons)
        {
            throw new System.NotImplementedException("SetTextIconControls");
        }

        public IPopup SetData(object data)
        {
            _data = data;
            UpdateData();
            return this;
        }

        protected virtual  void UpdateData()
        {
            
        }

        public IPopup SetText(string text, bool localized = true)
        {
            throw new System.NotImplementedException("SetText");
        }

        public IPopup SetButtonLabels(string yesLocaleKey = null, string noButtonLocaleKey = null)
        {
            throw new System.NotImplementedException("SetButtonLabels");
        }

        public IPopup LockButtons(bool lockYesButton = false, bool lockNoButton = false)
        {
            throw new System.NotImplementedException("LockButtons");
        }

        public IPopup SetHolderVisibility(string holderName, bool value)
        {
            throw new System.NotImplementedException("SetHolderVisibility");
        }

        public IPopup SetVisibilityTextIconControl(string controlName, bool visible)
        {
            throw new System.NotImplementedException("SetVisibilityTextIconControl");
        }
    }
}
