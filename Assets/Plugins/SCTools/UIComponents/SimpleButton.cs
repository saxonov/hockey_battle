﻿using UnityEngine;
using UnityEngine.UI;

namespace SCTools.UI
{

    /// <summary>
    /// Simple wrapper helper for Button Component
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class SimpleButton : MonoBehaviour
    {

        private Button _button;
        private Text label;

        [SerializeField]private GameObject[] showGroup = null;
        [SerializeField]private GameObject[] hideGroup = null;
      
        public Text Label
        {
            get { return label; }
        }

        public void ShowGroup(bool value)
        {
            if (showGroup != null && hideGroup != null)
            { 
                int i;
                for (i = 0; i < showGroup.Length; i++)
                {
                    showGroup[i].SetActive(value);
                }
                for (i = 0; i < hideGroup.Length; i++)
                {
                    hideGroup[i].SetActive(!value);
                }
            }
        }

        public bool HasLabel
        {
            get { return label != null; }
        }

        public Button Button
        {
            get { return _button; }
        }

        // Use this for initialization
        void Awake()
        {
            _button = GetComponent<Button>();
            label = GetComponentInChildren<Text>();
        }


    }

}
