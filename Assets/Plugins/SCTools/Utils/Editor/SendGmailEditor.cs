﻿using UnityEditor;
using UnityEngine;

namespace SCTools.Utils.Editor
{
    [CustomEditor(typeof(SendGmail))]
    public class SendGmailEditor:UnityEditor.Editor
    {
        private SendGmail gmailComp;

      

        public override void OnInspectorGUI()
        {

            serializedObject.Update();
            gmailComp = (SendGmail) target;
            EditorGUIUtility.fieldWidth = 0;
            EditorGUIUtility.labelWidth = 0;
            SerializedProperty ricipients = serializedObject.FindProperty("ricipientsAdresses");
            SerializedProperty fromAddress = serializedObject.FindProperty("fromAddress");
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(ricipients, true);
            EditorGUILayout.PropertyField(fromAddress, true);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }


            


            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Gmail Password");
            gmailComp.gmailPassword = EditorGUILayout.PasswordField(gmailComp.gmailPassword);
            EditorGUILayout.EndHorizontal();
    
        }
    }
}
