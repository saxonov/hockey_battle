#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("BQ3fPOn9+28Bge8dVlVN3mNIDeL8y8LHz8DNy47BwI7axsfdjs3L3KEzk12F54a0ZlBgGxegd/CyeGWTx8jHzc/ax8HAju/b2sbB3Mfa158blANaoaCuPKUfj7iA2nuSo3XMuMqbjbvlu/ezHTpZWDIwYf4Ub/b+nr+orfuqpL2k797ewsuO58DNgJ/ewsuO7cvc2sfIx83P2sfBwI7v28zCy47d2s/Ays/cyo7ay9zD3Y7POzDUogrpJfV6uJmdZWqh42C6x393mNFvKft3CTcXnOxVdnvfMNAP/KaFqK+rq6msr7iwxtra3t2UgYHZGbUTPeyKvIRpobMY4zLwzWblLrmOz8DKjs3L3NrHyMfNz9rHwcCO3tHvBjZXf2TIMorFv34NFUq1hG2xbs2d2VmUqYL4RXShj6B0FN234Rum8J4sr7+orfuzjqosr6aeLK+qnsLLjufAzYCfiJ6KqK37qqW9s+/eEFrdNUB8yqFl1+GadgyQV9ZRxWaEKOYoWaOvr6urrp7Mn6Wep6it+4DuCFnp49Gm8J6xqK37s42qtp649wmrp9K57vi/sNp9GSWNlekNe8Hr0LHixf447ydq2sylvi3vKZ0kL9nZgM/e3sLLgM3Bw4HP3t7Cy83Po6inhCjmKFmjr6+rq66tLK+vrvLcz83ax83Ljt3az9rLw8vA2t2AntSeLK/YnqCorfuzoa+vUaqqrayvqqi9rPv9n72ev6it+6qkvaTv3t4uuoV+x+k62KdQWsUjgO4IWenj0We33FvzoHvR8TVci60U+yHj86Nf3sLLjvzBwdqO7e+esLmjnpiempyYN+KD1hlDIjVyXdk1XNh82Z7hb5ucn5qenZj0uaOdm56cnpecn5qeqULTly0l/Y59lmofETThpMVRhVIsr66op4Qo5ihZzcqrr54vXJ6EqLieuqit+6qtvaPv3t7Cy478wcHaJbcncFflwlupBYyerEa2kFb+p33JIaYajlllAoKOwd4Yka+eIhntYYGeL22opoWor6urqaysni8YtC8d2sbB3Mfa15+4nrqorfuqrb2j797Xjs/d3dvDy92Oz83Ny97az8DNyx+e9kL0qpwixh0hs3DL3VHJ8MsSnZj0nsyfpZ6nqK37qqi9rPv9n72orfuzoKq4qrqFfsfpOtinUFrFI4KOzcvc2sfIx83P2suO3sHCx83XikxFfxnecaHrT4lkX8PWQ0kbubmeLKoVniytDQ6trK+srK+snqOopyHdL85otfWngTwcVurmXs6WMLtbsSstK7U3k+mZXAc17iCCeh8+vHbAyo7NwcDKx9rHwcDdjsHIjtvdy47t754sr4yeo6inhCjmKFmjr6+vqJ6hqK37s72vr1Gqq56tr69RnrMGctCMm2SLe3eheMV6DIqNv1kPAtrHyMfNz9rLjszXjs/A147ez9zajsHIjtrGy47axsvAjs/e3sLHzc+rrq0sr6GuniyvpKwsr6+uSj8Hp7E/dbDp/kWrQ/DXKoNFmAz54vtC53bYMZ26yw/ZOmeDrK2vrq8NLK+Inoqorfuqpb2z797ewsuO7cvc2pOIyY4kncRZoyxhcEUNgVf9xPXK/gQke3RKUn6nqZke29uP");
        private static int[] order = new int[] { 24,38,22,11,23,20,9,23,42,18,58,35,59,45,17,57,33,35,50,38,31,52,25,36,47,46,28,46,32,38,50,48,36,51,34,59,51,52,52,48,40,51,42,50,44,53,47,55,49,50,53,52,54,53,54,59,57,59,58,59,60 };
        private static int key = 174;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
