﻿using HockeyManager.Game.Dict;
using HockeyManager.HockeyManager.Game.Models.EPS;
using SCTools.EventsSystem;
using SCTools.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class SkillItemRenderer:MonoBehaviour
    {
        public const string OnAddPointClickEvent = "OnAddPointsClickEvent";
           
#pragma warning disable CS0649 // Полю "SkillItemRenderer.nameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private UITextLocalizator nameLabel;
#pragma warning restore CS0649 // Полю "SkillItemRenderer.nameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "SkillItemRenderer.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text counterLabel;
#pragma warning restore CS0649 // Полю "SkillItemRenderer.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "SkillItemRenderer.maxPointsProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Slider maxPointsProgress;
#pragma warning restore CS0649 // Полю "SkillItemRenderer.maxPointsProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "SkillItemRenderer.addButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button addButton;
#pragma warning restore CS0649 // Полю "SkillItemRenderer.addButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.



        private SkillModel _model;

        public void SetModel(SkillModel model)
        {
            if (_model == null)
            {
                _model = model;
                _model.OnModelChanged.AddListener(OnModelChangedHandler);
                nameLabel.SetLocaleKeys(true,_model.LocaleKeyName);
                counterLabel.text = _model.Points.ToString();
                maxPointsProgress.minValue = 0;
                maxPointsProgress.maxValue = GameData.GSConstData.MaxSkillPoints;
                maxPointsProgress.wholeNumbers = true;
                maxPointsProgress.value = _model.Points;
            }
        }

        private void OnModelChangedHandler()
        {
            counterLabel.text = _model.Points.ToString();
            maxPointsProgress.value = _model.Points;
        }

        void Start()
        {
           addButton.onClick.AddListener(OnAddButtonClickHandler);
        }

        void OnDestroy()
        {
            addButton.onClick.RemoveAllListeners();
            if (_model != null)
            {
                _model.OnModelChanged.RemoveAllListeners();
                _model = null;
            }
        }
        
        private void OnAddButtonClickHandler()
        {
            if (_model != null)
            {
                EventsManager.Instance.Call(OnAddPointClickEvent, _model);
            }
        }
    }
}
