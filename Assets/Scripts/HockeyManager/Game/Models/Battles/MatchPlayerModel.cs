﻿using GameSparks.Core;
using HockeyManager.Game.Impl;

namespace HockeyManager.Game.Models.Battles
{
    public class MatchPlayerModel:IGSParse
    {

        private int _level;
        private int _leaderBoardPosition;
        private bool _isOnline = false;
        private int _levelAwardPercent = 0;

        private TeamInfoModel _teamInfo;

        public MatchPlayerModel(GSData data)
        {
            _teamInfo = new TeamInfoModel(data);
            Parse(data);
        }

        public TeamInfoModel TeamInfo
        {
            get { return _teamInfo; }
        }

        public bool IsOnline
        {
            get { return _isOnline; }
        }

        public int Level
        {
            get { return _level; }
        }

        public int LeaderBoardPosition
        {
            get { return _leaderBoardPosition; }
        }

        public int LevelAwardPercent
        {
            get { return _levelAwardPercent; }
        }

        public void Parse(GSData data)
        {
            _teamInfo.Parse(data);

            _isOnline = data.ContainsKey("isOnline") && data.GetBoolean("isOnline").Value;
           
            _level = data.GetInt("level").Value;

            if (data.ContainsKey("rankPosition"))
            {
                _leaderBoardPosition = data.GetInt("rankPosition").HasValue ? data.GetInt("rankPosition").Value : 0;
            }
            
            if (data.ContainsKey("levelAward%"))
            {
                _levelAwardPercent = data.GetInt("levelAward%").Value;
            }

        }
    }
}
