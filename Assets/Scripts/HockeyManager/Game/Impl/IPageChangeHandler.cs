﻿using HockeyManager.Game.Dict;

namespace HockeyManager.Game.Impl
{
    public interface IPageChangeHandler
    {
        void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev);
    }
}
