﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SCTools.Localization;

namespace SCTools.Helpers
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Text))]
    public class UITextLocalizator : MonoBehaviour
    {
        [SerializeField] private string locale_key;
        [SerializeField] private string locale_key2;
        [SerializeField] private string locale_key3;
        [SerializeField] private string locale_key4;

        [SerializeField] private string[] args;

        //[Tooltip("Use for many locale_keys")]
        //[SerializeField] private char separator = ' ';

        [Tooltip("Add 0 to reset")]
        [SerializeField] private char postfix = '\0';
        [Tooltip("Add 0 to reset")]
        [SerializeField] private char prefix = '\0';

        public bool allCapital;
        private StringBuilder sbuilder;
        private Text textField;
        // Use this for initialization
        void Awake()
        {
            textField = GetComponent<Text>();
            OnLocaleChanged();
        }

      
        void OnEnable()
        {
            LocaleManager.Instance.LanguageChanged += OnLocaleChanged;
            OnLocaleChanged();
        }
        
        void OnDisable()
        {
            LocaleManager.Instance.LanguageChanged -= OnLocaleChanged;
        }

        public void SetLocaleKeys(bool upperCase,string key1,string key2 = null,string key3 = null,string key4 = null)
        {
            locale_key = key1;
            locale_key2 = key2;
            locale_key3 = key3;
            locale_key4 = key4;
            allCapital = upperCase;
            OnLocaleChanged();

           
        }

        private void OnLocaleChanged()
        {
            if (textField != null)
            {
                string resultText = null;
                sbuilder = sbuilder ?? new StringBuilder();

                if (args == null)
                {
                    args = new string[1];
                }

                sbuilder.Append(LocaleManager.Instance.GetText(locale_key,args));

                if (!string.IsNullOrEmpty(locale_key2))
                {
                    sbuilder.Append(LocaleManager.SPACE).Append(LocaleManager.Instance.GetText(locale_key2,args));
                }

                if (!string.IsNullOrEmpty(locale_key3))
                {
                    sbuilder.Append(LocaleManager.SPACE).Append(LocaleManager.Instance.GetText(locale_key3, args));
                }

                if (!string.IsNullOrEmpty(locale_key4))
                {
                    sbuilder.Append(LocaleManager.SPACE).Append(LocaleManager.Instance.GetText(locale_key4, args));
                }

                resultText = allCapital ? sbuilder.ToString().ToUpper() : sbuilder.ToString();

                if (postfix == '0')
                {
                    postfix = '\0';
                }

                if (prefix == '0')
                {
                    prefix = '\0';
                }
                if (postfix != '\0')
                {
                    resultText = resultText.AddPostfix(postfix);
                }

                if (prefix != '\0')
                {
                    resultText = resultText.AddPrefix(prefix);
                }

            

                textField.text = resultText;
              
                sbuilder = null;
            }
            
        }
        void OnValidate()
        {
            OnLocaleChanged();
        }

        
    }

}
