﻿using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace SCTools.UI
{
    [ExecuteInEditMode]
    public class TextFitter:MonoBehaviour
    {
        [SerializeField] private Text textfield;

        [SerializeField] private RectTransform fitTarget;

        [SerializeField] private float paddingLeftRight = 0f;
        [SerializeField] private float paddingTopBottom = 0f;

        [SerializeField] private bool vertical = true;
        [SerializeField] private bool horizontal = true;

        void Start()
        {
            FitToText();
        }

        void OnEnable()
        {
            LocaleManager.Instance.LanguageChanged += OnLanguageChangedHandler;
            OnLanguageChangedHandler();
        }

        void OnDisable()
        {
            LocaleManager.Instance.LanguageChanged -= OnLanguageChangedHandler;
        }



        private void OnLanguageChangedHandler()
        {
            FitToText();
        }

        public void FitToText()
        {
            if (fitTarget != null && textfield != null)
            {
                textfield.horizontalOverflow = horizontal ? HorizontalWrapMode.Overflow : HorizontalWrapMode.Wrap;
                textfield.verticalOverflow = vertical ? VerticalWrapMode.Overflow : VerticalWrapMode.Truncate;
                textfield.resizeTextForBestFit = false;

                textfield.text = textfield.text.PadLeft(1).PadRight(1);
                Vector2 sizeDelta = fitTarget.sizeDelta;
                sizeDelta.x = horizontal ? Mathf.FloorToInt(textfield.preferredWidth + paddingLeftRight) : sizeDelta.x;
                sizeDelta.y = vertical ? Mathf.FloorToInt(textfield.preferredHeight + paddingTopBottom) : sizeDelta.y;
                fitTarget.sizeDelta = sizeDelta;
            }
            
        }

        public void SetPaddings(float leftRight = 0,float topBottom = 0)
        {
            paddingTopBottom = topBottom;
            paddingLeftRight = leftRight;
            FitToText();
        }

        public void SetFontSize(int size)
        {
            if (textfield != null)
            {
                textfield.fontSize = size;
                FitToText();
            }
        }

        public void SetText(string value)
        {
            if (textfield != null)
            {
                textfield.text = value;
                FitToText();
            }
        }

#if UNITY_EDITOR || UNITY_EDITOR_64
        void Update()
        {
            if (!Application.isPlaying)
            {
                FitToText();
            }
        }
#endif
        void OnValidate()
        {
            FitToText();
        }

    }
}
