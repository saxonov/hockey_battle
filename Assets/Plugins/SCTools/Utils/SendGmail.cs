﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace SCTools.Utils
{
    public class SendGmail:MonoBehaviour
    {
        public string fromAddress;
        public string[] ricipientsAdresses;
        public string gmailPassword;

        public void Send(string subject,string body)
        {
            

                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(fromAddress);
                for (int i = 0; i < ricipientsAdresses.Length; i++)
                {
                    mail.To.Add(ricipientsAdresses[i]);
                }

                mail.Subject = subject;
                mail.Body = body;

                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                smtpServer.Port = 587;
                smtpServer.Credentials = new System.Net.NetworkCredential(fromAddress, gmailPassword) as ICredentialsByHost;
                smtpServer.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    { return true; };

            try
            {
                smtpServer.Send(mail);
                Debug.Log("success");
            }
            catch (Exception e)
            {
               Debug.LogWarning(e.Message);
            }

        }

    }
}
