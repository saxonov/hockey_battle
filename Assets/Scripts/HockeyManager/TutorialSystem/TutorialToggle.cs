﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem.Base;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.TutorialSystem
{
    [RequireComponent(typeof(Toggle))]
    public class TutorialToggle:TutorialObject
    {
       
        private Toggle toggle;

        public NavigateToPage pageToNavigator;

        void Start()
        {
            if (GameData.IsTutorialPassed)
            {
                Destroy(this);
                return;
            }

            toggle = GetComponent<Toggle>();
            pageToNavigator = gameObject.GetComponent<NavigateToPage>();
        }

        public override void OnUpState()
        {
            if (toggle == null)
            {
                toggle = GetComponent<Toggle>();
            }
            SoundManager.Instance.PlaySFX("click");
            toggle.isOn = !toggle.isOn;
            toggle.OnDeselect(null);
            if(pageToNavigator != null)
            {
                pageToNavigator.OnPointerClick(pointerEventData);
            }
            // toggle.OnPointerClick(null);
        }

        public override void OnDownState()
        {
            if (toggle == null)
            {
                toggle = GetComponent<Toggle>();
            }
            toggle.Select();
        }
    }
}
