﻿using System;
using System.Linq;
using SCTools.EventsSystem;
using UnityEngine;
using Object = UnityEngine.Object;


public static class ExtensionMethods {

    public static Transform AddChild(this Transform holder, Transform child)
    {
        child.SetParent(holder);
        child.localPosition = Vector3.zero;
        child.localRotation = Quaternion.identity;
        return child;
    }
    public static Transform AddChild(this Transform holder, GameObject childPrefab)
    {
        Transform newChild = GameObject.Instantiate(childPrefab).transform;
        newChild.SetParent(holder);
        newChild.localPosition = Vector3.zero;
        newChild.localRotation = Quaternion.identity;
        newChild.localScale = Vector3.one;
        return newChild;
    }

    public static RectTransform AddChild(this RectTransform rectHolder, GameObject childPrefab,bool worldPos)
    {
        RectTransform child = Object.Instantiate(childPrefab).GetComponent<RectTransform>();
        child.SetParent(rectHolder, worldPos);
        child.localScale = Vector3.one;
        return child;
    }

    public static Transform AddChild(this Transform holder,string newName)
    {
        GameObject newChild = new GameObject(newName);
        return AddChild(holder,newChild.transform);
    }

    public static Transform RemoveChild(this Transform holder, string childName)
    {
        Transform child = holder.FindChild(childName);
        if (child != null && child.IsChildOf(holder))
        {
            Object.Destroy(child.gameObject);
        }
        return child;
    }

    public static void RemoveChild(this Transform holder, Transform child)
    {
        if (child != null && child.IsChildOf(holder))
        {
           Object.Destroy(child.gameObject);
        }
    }

    public static Bounds GetBounds(this Transform holder, string childName)
    {
        Transform child = holder.FindChild(childName);
        if (child != null)
        {
            return GetBounds(child);
        }
        return GetBounds(holder);
    }

    public static Bounds GetBounds(this Transform holder)
    {
        Collider collider = holder.GetComponent<Collider>();
        if (collider != null)
        {
            return collider.bounds;
        }
        Collider2D col = holder.transform.GetComponent<Collider2D>();
        if (col != null)
        {
            return col.bounds;
        }
        Renderer renderer = holder.GetComponent<Renderer>();
        if (renderer != null)
        {
            return renderer.bounds;
        }
        return new Bounds(Vector3.one * 0.5f, Vector3.one);
    }

    public static void HideChildren(this Transform holder)
    {
        int len = holder.childCount;
        for (int i = 0; i < len; i++)
        {
            holder.GetChild(i).gameObject.SetActive(false);
        }
    }

    public static void RemoveChildren(this Transform holder)
    {
        while (holder.childCount > 0)
        {
            Object.DestroyImmediate(holder.GetChild(0).gameObject);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="holder"></param>
    /// <param name="exeptChildNames">name,name,name</param>
    public static void RemoveChildren(this Transform holder, string exeptChildNames)
    {
        string[] names = exeptChildNames.Split(',');
        int count = holder.childCount;
        while (count > 0)
        {
            var child = holder.GetChild(0);
            if (!names.Contains(child.gameObject.name))
            {
                Object.DestroyImmediate(child.gameObject);
            }
            count--;
        }

    }

    public static string AddPrefix(this string s, char sign)
    {
        return sign + s;
    }

    public static string AddPostfix(this string s, char sign)
    {
        return s + sign;
    }

    public static string AddPrefix(this string s, string sign)
    {
        return sign + s;
    }

    public static string AddPostfix(this string s, string sign)
    {
        return s + sign;
    }

    public static string GetUITextSizeFormated(this string s,int size)
    {
        return string.Format("<size='{0}'>{1}</size>",size,s);
    }

    public static string GetUITextColorFormated(this string s,Color32 color)
    {
        string hexcolor = "#" + ColorUtility.ToHtmlStringRGB(color);
        return string.Format("<color='{0}'>{1}</color>", hexcolor, s);
    }


    public static string GetUITMPColorFormated(this string s, Color32 color)
    {
        string hexcolor = "#" + ColorUtility.ToHtmlStringRGB(color);
        return string.Format("<color={0}>{1}</color>", hexcolor, s);
    }


    public static EventDispatcher<T> Dispatcher<T>(this GameObject go) where T:GameEventData
    {
        return EventsManager.Instance.GetDispatcher<T>(go);
    }

    public static void ClearDispatcher(this GameObject go)
    {
        EventsManager.Instance.RemoveDispatcher(go);
    }

    //public static void AddEventListener(this GameObject go,GameEventDelegate listener)
    //{
    //    var dispatcher = GameEventsManager.Instance.GetDispatcher(go);
    //    dispatcher.AddEventListener(listener);
    //}

    //public static void RemoveEventListener(this GameObject go,GameEventDelegate listener)
    //{
    //    var dispatcher = GameEventsManager.Instance.GetDispatcher(go);
    //    dispatcher.RemoveEventListener(listener);
    //}


    //public static void DispatchEvent(this GameObject go,GameEventData eventData)
    //{
    //    var dispatcher = GameEventsManager.Instance.GetDispatcher(go);
    //    if (dispatcher.HasEventListeners())
    //    {
    //        dispatcher.DispatchEvent(eventData);
    //    }

    //}

    //public static void AddCallback(this MonoBehaviour behavior,string type,Callback handler)
    //{
    //    EventsManager.Instance.Add(type,handler);
    //}

    //public static void AddCallback<T>(this MonoBehaviour behavior, string type, Callback<T> handler)
    //{
    //    EventsManager.Instance.Add(type, handler);
    //}

    //public static void AddCallback<T,U>(this MonoBehaviour behavior, string type, Callback<T, U> handler)
    //{
    //    EventsManager.Instance.Add(type, handler);
    //}

    //public static void RemoveCallback(this MonoBehaviour behavior, string type, Callback handler)
    //{
    //    EventsManager.Instance.Remove(type, handler);
    //}

    //public static void RemoveCallback<T>(this MonoBehaviour behavior, string type, Callback<T> handler)
    //{
    //    EventsManager.Instance.Remove(type, handler);
    //}

    //public static void RemoveCallback<T,U>(this MonoBehaviour behavior, string type, Callback<T,U> handler)
    //{
    //    EventsManager.Instance.Remove(type, handler);
    //}

    //public static bool TriggerCallback(this MonoBehaviour behavior, string type)
    //{
    //    return EventsManager.Instance.Call(type);
    //}

    //public static bool TriggerCallback<T>(this MonoBehaviour behavior, string type,T arg0)
    //{
    //    return EventsManager.Instance.Call(type,arg0);
    //}

    //public static bool TriggerCallback<T,U>(this MonoBehaviour behavior, string type,T arg0,U arg1)
    //{
    //   return EventsManager.Instance.Call(type,arg0,arg1);
    //}
}
