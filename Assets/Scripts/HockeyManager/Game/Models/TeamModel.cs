﻿using HockeyManager.Game.Dict;

namespace HockeyManager.Game.Models
{

   
    public class TeamModel
    {
        private BalanceInfoData _balance;

        private TeamInfoModel _teamInfo = null;

        public long energyEndTime = 0;
        public long energyTime = 0;
        public long energyTimeBoosterEffect = 0;

        public RankType rank = RankType.Beginner;

        public int maxLevelExp = 0;
        public int exp = 0;
        public int level = 0;

        //
     
        public int fans = 0;
        public int maxFans = 0;

        public int maxEnergy = 0;
        public int energy = 0;

        public int coins = 0;
        public int gold = 0;

        public VirtualGoods.TimelimitVirtualGood vip = null;
        public VirtualGoods.TimelimitVirtualGood protectShield = null;

        public BalanceInfoData GetBalanceInfo()
        {
            _balance.fans = fans;
            _balance.coins = coins;
            _balance.energy = energy;
            _balance.maxEnergy = maxEnergy;
            _balance.gold = gold;
            _balance.maxFans = maxFans;
            return _balance;
        }

        public TeamInfoModel TeamInfo
        {
            get
            {
                return _teamInfo;
            }
         
        }

        public TeamModel(string userId)
        {
            _teamInfo = new TeamInfoModel(userId);
        }

        public override string ToString()
        {
            return "name: " + _teamInfo.Name;
        }

        //public int GetNextLevelExp()
        //{
        //    GSData nextLevelData = nextExpLevels.First();
        //    return nextLevelData.GetInt("needExp").Value;
        //}

    }
}

