﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using SCTools.EventsSystem;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Components
{
    public class SpinnerLoader : MonoBehaviour
    {

        [SerializeField] private Text label;
        [SerializeField] private Image spinner;

        void Start()
        {
            label.text = LocaleManager.Instance.GetText("game.preloader.loading");
        }

        void OnEnable()
        {
            EventsManager.Instance.Add<GSData>(GlobalEventTypes.ServerResponceError,OnErrorResponceRecieved);
            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionLost,OnLostNetConnection);
            EventsManager.Instance.Add(GlobalEventTypes.InternetConnectionExist,OnConnectToInternet);
        }
        
        void OnDisable()
        {
            EventsManager.Instance.Remove<GSData>(GlobalEventTypes.ServerResponceError, OnErrorResponceRecieved);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionLost, OnLostNetConnection);
            EventsManager.Instance.Remove(GlobalEventTypes.InternetConnectionExist, OnConnectToInternet);
        }

        private void OnErrorResponceRecieved(GSData data)
        {
            //label.color = Color.red;
            label.color = Color.red;
        }

        private void OnConnectToInternet()
        {
            //label.text = LocaleManager.Instance.GetText("game.preloader.loading");
            //label.color = Color.white;
            spinner.color = Color.white;
        }

        private void OnLostNetConnection()
        {
           // label.text = LocaleManager.Instance.GetText("game.common.networklost");
          //  label.color = Color.red;
            spinner.color = Color.red;
        }

    }
}
