﻿using HockeyManager.Game.Dict;
using UnityEngine;

public class BuildingSettingsObject : ScriptableObject
{
#pragma warning disable CS0649 // Полю "BuildingSettingsObject._buildingsSettingsInfo" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
    [SerializeField] private MapBuildingSettingsInfo[] _buildingsSettingsInfo;
#pragma warning restore CS0649 // Полю "BuildingSettingsObject._buildingsSettingsInfo" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
     
   //public string GetDescription(MapBuildingsType type)
    //{
    //    for (int i = 0; i < buildingsData.Length; i++)
    //    {
    //        if (buildingsData[i].type == type)
    //        {
    //            return LocaleManager.Instance.GetText(buildingsData[i].descriptionLocaleKey);
    //        }
    //    }
    //    return "";
    //}

    //public string GetName(MapBuildingsType type)
    //{
    //    for (int i = 0; i < buildingsData.Length; i++)
    //    {
    //        if (buildingsData[i].type == type)
    //        {
    //            return LocaleManager.Instance.GetText(buildingsData[i].nameLocaleKey);
    //        }
    //    }
    //    return "";
    //}

    //public Sprite[] GetPreview(MapBuildingsType type)
    //{
    //    for (int i = 0; i < buildingsData.Length; i++)
    //    {
    //        if (buildingsData[i].type == type)
    //        {
    //            return buildingsData[i].previews;
    //        }
    //    }
    //    return null;
    //}

    public MapBuildingSettingsInfo[] BuildingsSettingsInfo
    {
        get { return _buildingsSettingsInfo; }
    }

}