﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using SCTools.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{


    public class NewsItemRenderer:MonoBehaviour
    {

#pragma warning disable CS0649 // Полю "NewsItemRenderer.titleLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text titleLabel;
#pragma warning restore CS0649 // Полю "NewsItemRenderer.titleLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "NewsItemRenderer.descriptionLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text descriptionLabel;
#pragma warning restore CS0649 // Полю "NewsItemRenderer.descriptionLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.


        public void SetData(NewsData data)
        {

            DateTime date = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(data.timestamp);

            titleLabel.text = date.ToString("dd/MM/yy").AddPostfix(LocaleManager.SPACE);
            titleLabel.text += LocaleManager.Instance.GetText(data.title).AddPrefix(LocaleManager.SPACE);
            descriptionLabel.text = LocaleManager.Instance.GetText(data.desc);
        }
    }
}
