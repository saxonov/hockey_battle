﻿using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using HockeyManager.Game.Dict;
using UnityEngine.Events;

namespace HockeyManager.Game.GameSparks.Impl
{
    public interface IGSManager
    {
        // void SendPushNotification(int sendAfterSeconds);

       // SocialAccountType CurrentAccountType { get;}

        GSClientLogger Logger { get; }

        bool InternetExist { get; }

        void RunListVirtualGoodsRequest(ListVirtualGoodsRequest request, UnityAction<ListVirtualGoodsResponse> callback);
        void RunLeaderboardRequest(AroundMeLeaderboardRequest request, UnityAction<AroundMeLeaderboardResponse> callback);
        void RunLogEventRequest(LogEventRequest request, UnityAction<LogEventResponse> Callback = null, string errorMsg = null, bool isBackgroundRequest = false);
        void RunBuyVirtualEventRequest(BuyVirtualGoodsRequest request, UnityAction<BuyVirtualGoodResponse> Callback = null, string errorMsg = null);
        void RunCustomLogEventRequest(LogEventRequest request, UnityAction<LogEventResponse> Callback, bool isBackgroundRequest = false);
    }
}
