﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.HallItems;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class HallSlotItemRenderer:MonoBehaviour,IPointerClickHandler
    {
        public const string HallItemSlotSelected = "hallItemSlotSelected";

#pragma warning disable CS0649 // Полю "HallSlotItemRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image icon;
#pragma warning restore CS0649 // Полю "HallSlotItemRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private HallItemSlotModel _slotModel;

        void Awake()
        {
            icon.color = new Color(1f,1f,1f,0.35f);
        }

        public void SetSlotData(HallItemSlotModel model)
        {
            _slotModel = model;

            if (!_slotModel.IsLocked && _slotModel.ItemId == -1)
            {
                icon.enabled = false;
            }
           
            _slotModel.OnItemChangeEvent.AddListener(OnItemChangedHandler);
        }

        private void OnItemChangedHandler()
        {
            if (_slotModel.ItemId != -1)
            {
                icon.color = Color.white;
                icon.enabled = true;
                icon.sprite = GameSettings.Instance.GetHallItemInfo(_slotModel.ItemId).GetPreview();
            }
            else
            {
                icon.enabled = false;
            }
           
            
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            EventsManager.Instance.Call(HallItemSlotSelected, _slotModel);
        }
    }
}
