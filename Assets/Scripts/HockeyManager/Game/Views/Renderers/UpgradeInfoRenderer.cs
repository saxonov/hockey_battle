﻿using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.Models.EPS;
using HockeyManager.Game.Models.Map;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.Views.Components;
using HockeyManager.Models.Map;
using SCTools.Localization;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class UpgradeInfoRenderer:MonoBehaviour
    {
        public static UnityAction<EquipmentModel> OnEquipmentUpgradeClicked;
            

        [Header("BankInfo")]
        [SerializeField]private BankInfoExchangePanel bankInfoPanel;

        [Header("UpgradeInfo")]
        [SerializeField]private Text levelText;
        [SerializeField]private Text maxLevelReachedLabel;
        [SerializeField]private Image previewIcon;
        [SerializeField]private Text description;
        [SerializeField]private Text upgradeRequirementsText;
        [SerializeField]private TextIconControl upgradePricePanel;
        [SerializeField]private TextIconControl upgradeTimePanel;
        [SerializeField]private Button upgradeButton;
        [SerializeField]private Image upgradeBackground;

        [Space(10)]
        [Header("ArenaInfo")]
        [SerializeField]private GameObject arenaPanel;
        [SerializeField]private TextIconControl attackBonus;
        [SerializeField]private TextIconControl deffenceBonus;
        [SerializeField]private Text fansPercents;
        [SerializeField]private Button fansTakeButton;

        [Space(10)]
        [Header("IconTextEffect")]
        [SerializeField]private GameObject iconTextEffectPanel;
        [SerializeField]private Text iconTextEffectLabel;
        [SerializeField]private TextIconControl iconTextEffect;

        [Space(10)]
        [Header("TextEffect")]
        [SerializeField]private GameObject textEffectPanel;
        [SerializeField]private Text textEffectPanelLabel;
        [SerializeField]private Button navigateButton;
        private Text navigateButtonLabel;

        [Space(10)]
        [Header("EquipmentsInfo")]
        [SerializeField]private GameObject eqpPanel;
        [SerializeField]private TextIconControl eqpAttack;
        [SerializeField]private TextIconControl eqpDeffence;

        private EquipmentModel _equipmentModel;
        private BuildingUpgradeInfo _buildingUpgradeModel;

        private TeamController _teamController;
        private HallItemsController _hallController;
        private MapController _map;

        void OnDestroy()
        {
           _buildingUpgradeModel = null;
            _teamController = null;
            _hallController = null;
            _map = null;
            upgradeButton.onClick.RemoveAllListeners();
            fansTakeButton.onClick.RemoveAllListeners();
        }

        void Awake()
        {
            _teamController = GameRoot.Instance.GetController<TeamController>();
            _hallController = GameRoot.Instance.GetController<HallItemsController>();
            _map = GameRoot.Instance.GetController<MapController>();

            bankInfoPanel.gameObject.SetActive(false);
            eqpPanel.SetActive(false);
            arenaPanel.SetActive(false);
            textEffectPanel.SetActive(false);
            iconTextEffectPanel.SetActive(false);
            maxLevelReachedLabel.gameObject.SetActive(false);
            navigateButtonLabel = navigateButton.GetComponentInChildren<Text>();
        }

        public void SetModel(BaseGameEntityModel model)
        {
            if(model == null)return;

            if (model.EntityType == EntityType.Building)
            {
                SetBuildingData(model as IBuildingModel);

            }else if (model.EntityType == EntityType.Equipment)
            {
                SetEquipmentData(model as EquipmentModel);
            }
        }


        private void SetBuildingData(IBuildingModel buildingModel)
        {

            _buildingUpgradeModel = MapManager.Instance.GetUpgrade(buildingModel);

            MapBuildingSettingsInfo buildingSettingsInfo = GameSettings.Instance.GetBuildingData(buildingModel.Id);

            LocaleManager locales = LocaleManager.Instance;

            previewIcon.sprite = buildingSettingsInfo.GetPreview(buildingModel.Level, buildingModel.MaxLevel);
            description.text = buildingSettingsInfo.GetDescription();
            levelText.text = locales.GetText("game.common.level", buildingModel.Level);

            if (buildingModel.Level == buildingModel.MaxLevel)
            {
                upgradeBackground.gameObject.SetActive(false);
                maxLevelReachedLabel.gameObject.SetActive(true);
            }
            else
            {
                upgradeTimePanel.SetText(GameTimer.GetGameTimeString(_buildingUpgradeModel.TimeSeconds));
                upgradePricePanel.SetText(_buildingUpgradeModel.Price.ToString());

                upgradeButton.onClick.AddListener(OnBuildingUpgradeClickHandler);
            }

       

            switch (buildingModel.Id)
            {
                    case MapBuildingsType.IceArena:
                        
                        arenaPanel.SetActive(true);
                        upgradeBackground.enabled = true;
                        attackBonus.SetText(locales.GetText("game.common.attack") + LocaleManager.SPACE + buildingModel.GetEffectValue(BuildingModel.ArenaAttackProp) + LocaleManager.PERCENT);
                        deffenceBonus.SetText(locales.GetText("game.common.deffence") + LocaleManager.SPACE + buildingModel.GetEffectValue(BuildingModel.ArenaDeffenceProp) + LocaleManager.PERCENT);

                        float fillPercent = 0f;
                        fansTakeButton.interactable = _teamController.NeedFansToUpgrade(out fillPercent);
                        TeamModel team = _teamController.TeamData;

                        if (fansTakeButton.interactable)
                        {
                            string blockColor = ColorUtility.ToHtmlStringRGBA(UISettings.BlockTextColor);
                            fansTakeButton.onClick.AddListener(OnTakeFansClickedhandler);
                            fansPercents.text = team.fans + "/" + team.maxFans + string.Format(" (<color='#{0}'>{1}%</color>)",blockColor,Mathf.Floor(fillPercent));

                            string fillFansLabel = locales.GetText("game.common.full") + LocaleManager.SPACE + GameData.GSConstData.ArenaUpgradeFansLimitPercent + LocaleManager.PERCENT;
                            upgradeRequirementsText.text = locales.GetText("game.main.building.upgrades.requirements", fillFansLabel);
                            upgradeRequirementsText.color = UISettings.BlockTextColor;
                        }
                        else
                        {
                            string greenColor = ColorUtility.ToHtmlStringRGBA(UISettings.GreenTextColor);
                            fansPercents.text = team.fans + "/" + team.maxFans + string.Format(" (<color='#{0}'>{1}%</color>)", greenColor, Mathf.Floor(fillPercent));
                            upgradeRequirementsText.text = locales.GetText("game.main.building.upgrades.requirementscomplete");
                            upgradeRequirementsText.color = UISettings.GreenTextColor;
                        }
                    break;
                    case MapBuildingsType.Bank:

                        bankInfoPanel.gameObject.SetActive(true);
                        bankInfoPanel.OnBuyClickEvent.AddListener(OnBankExchangeHandler);
                        bankInfoPanel.SetMaxExchangeRates(buildingModel);
                       
                       // iconTextEffectPanel.SetActive(true);
                       // iconTextEffectLabel.text = LocaleManager.Instance.GetText("game.main.building.upgrades.moneyinbank");
                       // iconTextEffect.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Money));
                        //iconTextEffect.SetText(buildingModel.GetEffectValue(BuildingModel.BankLimitProp));
                    break;
                    case MapBuildingsType.AdBanner:
                        iconTextEffectPanel.SetActive(true);
                        iconTextEffectLabel.text = LocaleManager.Instance.GetText("game.main.building.upgrades.fansbonus");
                        iconTextEffect.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Fans));
                       
                        iconTextEffect.SetText(locales.GetText("game.common.perhour", buildingModel.GetEffectValue(BuildingModel.FansBonusProp)));
                    break;
                    case MapBuildingsType.Kiosk:
                    case MapBuildingsType.Shop:
                        iconTextEffectPanel.SetActive(true);
                        iconTextEffectLabel.text = LocaleManager.Instance.GetText("game.main.building.upgrades.salesbonus");
                        iconTextEffect.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Money));
                        float coinsPerTime = buildingModel.GetEffectValue(BuildingModel.CoinsPerTimeProp);
                        coinsPerTime = _teamController.HasVip ? coinsPerTime*2 : coinsPerTime;
                        iconTextEffect.SetText(locales.GetText("game.common.per5minutes",coinsPerTime));
                    break;
                    case MapBuildingsType.Cashbox:
                        iconTextEffectPanel.SetActive(true);
                        iconTextEffectLabel.text = LocaleManager.Instance.GetText("game.main.building.upgrades.fansbonus");
                        iconTextEffect.SetIcon(GameSettings.Instance.GetIcon(IconsEnum.Fans));
                        float totalEffect = buildingModel.GetEffectValue(BuildingModel.FansPerTimeProp,BuildingModel.BannersBonusProp);
                        totalEffect += MathUtils.CalcPercentValue(totalEffect,_hallController.GetTotalFansBonus());
                        totalEffect = _teamController.HasVip ? totalEffect*2 : totalEffect;
                        
                       // int hallitemBonus = buildingModel.GetEffectValue(BuildingModel.HallItemsBonusProp);
                        iconTextEffect.SetText(locales.GetText("game.common.perhour",totalEffect));
                    break;
                    case MapBuildingsType.Base:
                        textEffectPanel.SetActive(true);
                        int eqpMinlvl = (int)buildingModel.GetEffectValue(BuildingModel.EquipmentMinLevelProp);
                        int eqpMaxlvl = (int)buildingModel.GetEffectValue(BuildingModel.EquipmentMaxLevelProp);

                        textEffectPanelLabel.text = locales.GetText("game.main.building.upgrades.equiplevelsbonus", eqpMinlvl + "-" + eqpMaxlvl);
                        navigateButtonLabel.text = locales.GetText("game.common.gotocloakroombutton");
                        navigateButton.onClick.AddListener(() =>
                        {
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.CloakRoom);
                        });
                    break;
                    case MapBuildingsType.Hall:
                        textEffectPanel.SetActive(true);
                        textEffectPanelLabel.text = locales.GetText("game.main.building.upgrades.relicscellbonus", buildingModel.GetEffectValue(BuildingModel.HallCellsProp));
                        navigateButtonLabel.text = locales.GetText("game.common.gotohallbutton");
                        navigateButton.onClick.AddListener(() =>
                        {
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.HallOfFame);
                        });
                    break;
            }


            //check buildings unlock levels
            //with arena level
            if (buildingModel.Id != MapBuildingsType.IceArena && buildingModel.Level < buildingModel.MaxLevel)
            {
                int arenaLevel = _map.GetArenaLevel();
                if (arenaLevel < _buildingUpgradeModel.UnlockLevel)
                {
                    string arenaName = locales.GetText("game.common.arena");
                    string needArenaevel = arenaName + LocaleManager.SPACE + locales.GetText("game.common.shortlevel2", _buildingUpgradeModel.UnlockLevel);
                    string reqText = locales.GetText("game.main.building.upgrades.requirements", needArenaevel);
                    upgradeRequirementsText.text = reqText;
                    upgradeRequirementsText.color = UISettings.BlockTextColor;
                    //upgradeButton.interactable = false;
                }
                else
                {
                    upgradeRequirementsText.text = locales.GetText("game.main.building.upgrades.requirementscomplete");
                    upgradeRequirementsText.color = UISettings.GreenTextColor;
                }
            }
           

        }

        private void OnBankExchangeHandler()
        {
            if (_teamController.CanBuyForCurrency(bankInfoPanel.CurrencyBuyType, bankInfoPanel.CurrentPrice))
            {
                _map.BankExchanger(bankInfoPanel.ExchangeCurrencyType, bankInfoPanel.SelectedAmountToBuy);

            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.NotEnoughMoneyPopup)
                  .SetTitle("game.main.popup.title.notenoughmoney")
                  .SetButtonLabels(LocaleManager.Instance.GetText("game.main.building.shop").ToUpper())
                  .SetCallbacks(() =>
                  {
                      PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                  });
            }
        }


        private void SetEquipmentData(EquipmentModel model)
        {
            _equipmentModel = model;
        
            LocaleManager locales = LocaleManager.Instance;

            EquipmentsSettingsInfo eqpInfo = GameSettings.Instance.GetEquipmentData(_equipmentModel.Id);
         
            previewIcon.sprite = eqpInfo.GetPreview(model.Level,EquipmentModel.MaxLevel);
            description.text = eqpInfo.GetDescription();
            levelText.text = locales.GetText("game.common.level", model.Level);
          

            eqpPanel.SetActive(true);

            string currentAttack = locales.GetText("game.common.attack") + LocaleManager.SPACE + _equipmentModel.Attack;
            string currentDeffence = locales.GetText("game.common.deffence") + LocaleManager.SPACE + _equipmentModel.Deffence;


            if (model.Level >= EquipmentModel.MaxLevel)
            {
                maxLevelReachedLabel.gameObject.SetActive(true);
                upgradeBackground.gameObject.SetActive(false);
                eqpAttack.SetText(currentAttack.GetUITextSizeFormated(60));
                eqpDeffence.SetText(currentDeffence.GetUITextSizeFormated(60));
                return;
            }



            string upgradeAttack = (_equipmentModel.UpgradeModel.Attack - _equipmentModel.Attack).ToString().AddPrefix('+').GetUITextColorFormated(UISettings.GreenTextColor);
            string upgradeDeffence = (_equipmentModel.UpgradeModel.Deffence - _equipmentModel.Deffence).ToString().AddPrefix('+').GetUITextColorFormated(UISettings.GreenTextColor);

            eqpAttack.SetText(currentAttack.AddPostfix('\n').GetUITextSizeFormated(60) + upgradeAttack.GetUITextSizeFormated(50));
            eqpDeffence.SetText(currentDeffence.AddPostfix('\n').GetUITextSizeFormated(60) + upgradeDeffence.GetUITextSizeFormated(50));


            upgradeTimePanel.SetText(GameTimer.GetGameTimeString(_equipmentModel.UpgradeModel.TimeSeconds));
            upgradePricePanel.SetText(_equipmentModel.UpgradeModel.Price.ToString());
            

            int baseLevel = _map.GetBaseEquipementMaxMinLevel();
            if (baseLevel <= model.Level)
            {
                int lvl = baseLevel == -1 ? 1 : _map.GetBuildingModel(MapBuildingsType.Base).Level + 1;
                MapBuildingSettingsInfo baseBuilding = GameSettings.Instance.GetBuildingData(MapBuildingsType.Base);
                string baseName = baseBuilding.GetName() + LocaleManager.SPACE + locales.GetText("game.common.shortlevel2", lvl);
                upgradeRequirementsText.text = locales.GetText("game.main.building.upgrades.requirements", baseName);
                upgradeRequirementsText.color = UISettings.BlockTextColor;
            }
            else
            {
                upgradeRequirementsText.text = locales.GetText("game.main.building.upgrades.requirementscomplete");
                upgradeRequirementsText.color = UISettings.GreenTextColor;
                // string greenColor = ColorUtility.ToHtmlStringRGBA(UISettings.GreenTextColor);
            }
            upgradeButton.onClick.AddListener(OnEquipmentUpgradeHandler);
        }

        private void OnEquipmentUpgradeHandler()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            OnEquipmentUpgradeClicked.Invoke(_equipmentModel);
        }

        private void OnBuildingUpgradeClickHandler()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            MapManager.Instance.UpgradeBuilding(_buildingUpgradeModel);
        }
        
        private void OnTakeFansClickedhandler()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            _map.LoadBuyArenaFansInfo(OnArenaFansInfoLoaded);
        }

        private void OnArenaFansInfoLoaded()
        {
            MapManager.Instance.ShowTakeFansPopup(() =>
            {
                PageNavigatorManager.Instance.ReloadSelectedPage();
            });
        }
    }
}
