﻿using System.Collections.Generic;
using System.Linq;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Map;
using HockeyManager.Models.Map;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class MapController : IGameController, ICheckMessages,IGSResponceHandler
    {
        public const string BuildingUpgradeComplete = "OnBuildingUpgraded";

        private List<BuildingUpgradeInfo> _upgrades;
        private List<IBuildingModel> _buildings = null;

        private List<GSData> _arenaLockedLots = null;

        private HallItemsController _hall;

        private IGameRoot _root;
        private TeamController _teamController;

        private IBuildingModel _currentUpgrade = null;
        private bool _allUpgradesLoaded;

        public List<IBuildingModel> Buildings
        {
            get { return _buildings; }
        }

        //public List<BuildingUpgradeInfo> Upgrades
        //{
        //    get { return _upgrades; }
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position">MapBuilding lot position</param>
        /// <returns>unlock arena level for construct building</returns>
        public int GetArenaUnlockLevel(int position)
        {
            if (_arenaLockedLots == null || _arenaLockedLots.Count == 0)
            {
                return 0;
            }
            foreach (var arenaLockedLot in _arenaLockedLots)
            {
                if (arenaLockedLot.GetIntList("lots").Contains(position))
                {
                    return arenaLockedLot.GetInt("unlockLevel").Value;
                }
            }
            return 0;
        }

        public void LoadBuyArenaFansInfo(UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_ARENA_FANS_INFO");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }

        public void BankExchanger(int currencyType,int amount)
        {
            var request = new LogEventRequest();
            request.SetEventKey("BANK_EXCHANGER");
            request.SetEventAttribute("CURRENCY", currencyType);
            request.SetEventAttribute("AMOUNT", amount);
            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    _teamController.RefreshBalance();
                }
            });
        }

        public void BuyArenaFans(int fansCount,UnityAction callback)
        {
             var request = new LogEventRequest();
             request.SetEventKey("BUY_ARENA_FANS");
             request.SetEventAttribute("VALUE", fansCount);
             request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
            });

        }

        private bool _loadCollectablesStarted = false;

        public void LoadCollectables()
        {
            if(_loadCollectablesStarted)return;
            if (_buildings != null && _buildings.Exists(b=>b.IsCollectable))
            {
                _loadCollectablesStarted = true;
                var request = new LogEventRequest();
                request.SetEventKey("LOAD_COLLECTABLE_BUILDINGS");
                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    _loadCollectablesStarted = false;
                },null,true);
            }
           
        }

        /// <summary>
        /// Load all buildings
        /// </summary>
        private void LoadBuildingsData(int id = -1, int pos = -1,UnityAction callback = null)
        {
            var request = new LogEventRequest();// LogEventRequest_LOAD_MAP_DATA();
            request.SetEventKey("LOAD_MAP_DATA");
            request.SetEventAttribute("ID", id);
            request.SetEventAttribute("POS", pos);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }
        
        public int GetBaseEquipementMaxMinLevel()
        {
            int maxLevel = -1;
            IBuildingModel baseModel = GetBuildingModel(MapBuildingsType.Base);
            if (baseModel != null)
            {
                int effectValue = (int)baseModel.GetEffectValue(BuildingModel.EquipmentMaxLevelProp);
                if (effectValue != 0)
                {
                    maxLevel = effectValue;
                }
            }
            return maxLevel;
        }

        public int GetArenaLevel()
        {
            IBuildingModel arena = GetBuildingModel(MapBuildingsType.IceArena,GameData.ArenaBuildingPosition);
            if (arena != null)
            {
                return arena.Level;
            }
            return -1;
        }


        public void LoadUpgradeData(int id, int pos, UnityAction callback = null)
        {
            IBuildingModel building = GetBuildingModel((MapBuildingsType) id, pos);
            if (building != null && building.Level == building.MaxLevel)
            {
                if (callback != null)
                {
                    callback();
                }
                return;
            }

            var request = new LogEventRequest(); //LogEventRequest_GET_BUILDING_UPGRADE().Set_ID(id).Set_POS(pos).Send((responce) =>
            request.SetEventKey("GET_BUILDING_UPGRADE");
            request.SetEventAttribute("ID", id);
            request.SetEventAttribute("POS", pos);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    var upgradeData = responce.ScriptData.GetGSData(GS_ScriptData.BuildingsUpgradesData);
                    if (upgradeData != null)
                    {
                        upgradeData.BaseData.Add("pos", pos);
                        BuildingUpgradeInfo info = _upgrades.Find(u => (int)u.Id == upgradeData.GetInt("id").Value && u.Pos == pos);
                        if (info != null)
                        {
                            info.Parse(upgradeData);
                        }
                        else
                        {
                            info = new BuildingUpgradeInfo(upgradeData);
                            _upgrades.Add(info);
                        }
                    }
                    else
                    {
                        //for not constructed;
                        List<GSData> upgradeList = responce.ScriptData.GetGSDataList(GS_ScriptData.BuildingsUpgradesData);
                        foreach (var gsData in upgradeList)
                        {
                            gsData.BaseData.Add("pos", pos);
                            BuildingUpgradeInfo info = _upgrades.Find(u => (int)u.Id == gsData.GetInt("id").Value && u.Pos == pos);
                            if (info != null)
                            {
                                info.Parse(gsData);
                            }
                            else
                            {
                                info = new BuildingUpgradeInfo(gsData);
                                _upgrades.Add(info);
                            }

                        }
                    }
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }

        public void LoadUpgrades(UnityAction callback)
        {
            if (_allUpgradesLoaded)
            {
                if (callback != null)
                {
                    callback();
                }
                return;
            }
            _allUpgradesLoaded = true;
            LogEventRequest request = new LogEventRequest();
            request.SetEventKey("GET_BUILDINGS_UPGRADES");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    List<GSData> upgrades = responce.ScriptData.GetGSDataList(GS_ScriptData.BuildingsUpgradesData);
                    foreach (var upgrade in upgrades)
                    {
                            BuildingUpgradeInfo info =
                                _upgrades.Find(
                                    u =>
                                        (int) u.Id == upgrade.GetInt("id").Value && u.Pos == upgrade.GetInt("pos").Value);
                            if (info != null)
                            {
                                info.Parse(upgrade);
                            }
                            else
                            {
                                info = new BuildingUpgradeInfo(upgrade);
                                _upgrades.Add(info);
                            }
                    }
                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }


        public bool UpgradeExist(BuildingLotModel lotModel)
        {
            return _upgrades.Exists((upgr) =>
            {
                if (lotModel.IsBuildingExist())
                {
                    if ((upgr.Level - lotModel.Building.Level) == 1 && upgr.Pos == lotModel.Building.Pos)
                    {
                        return true;
                    }
                }
                else
                {
                    foreach (var site in lotModel.Sites)
                    {
                        if (upgr.Id.Equals(site.type) && upgr.Pos == lotModel.Id)
                        {
                            return true;
                        }

                    }
                }

                return false;
            });
        }

        public void SendUpgradeRequest(BuildingUpgradeInfo upgradeData, bool fastUpgrade,UnityAction<IBuildingModel> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("UPGRADE_BUILDING");
            request.SetEventAttribute("ID", (int)upgradeData.Id);
            request.SetEventAttribute("POS", upgradeData.Pos);
            request.SetEventAttribute("FAST", fastUpgrade ? 1 : 0);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (upgradeData.Id == MapBuildingsType.IceArena)
                    {
                        _allUpgradesLoaded = false;
                    }

                    if (!fastUpgrade)
                    {
                        
                        _currentUpgrade = GetBuildingModel(upgradeData.Id, upgradeData.Pos);
                        // _currentUpgrade.OnTimerLeftSecondsEvent.AddListener(OnUpgradeCompleteHandler);
                        _currentUpgrade.StartUpgradeTimer();
                        callback(_currentUpgrade);

                    }
                    else
                    {
                        _teamController.NeedUpdateTeamInfo = true;
                        _currentUpgrade = null;
                        callback(GetBuildingModel(upgradeData.Id, upgradeData.Pos));
                    }
                }
                else
                {
                    _teamController.RefreshBalance();
                }
            });
        }
        
        public void Collect(IBuildingModel model,out int coinsToAdd,out int fansToAdd,UnityAction callback)
        {
            coinsToAdd = 0;
            fansToAdd = 0;


            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("ID", (int)model.Id);
            request.SetEventAttribute("POS", model.Pos);
            request.SetDurable(true);

            if (model.Id == MapBuildingsType.Cashbox)
            {

                fansToAdd = (int)model.GetEffectValue(BuildingModel.FansCollectedProp);
                //building.Model.GetEffectValue(BuildingModel.FansMaxqueueProp, out fansCount);
                coinsToAdd = (int)model.GetEffectValue(BuildingModel.CoinsProp);
                //int admanBonus = 0;
                //EmployeeModel adman = _epsController.GetEmployee(EmployeeType.Adman);
                //if (adman != null)
                //{
                //    admanBonus = adman.CurrentBonusEffect;
                //}
                //coinsCount = coinsCount/coinsToAdd;
                //from server side
                //var adMan = getPersonal().getEmployee(ADMAN);
                //var coins = building.effect.fans * CASHBOX_PRICE_FOR_FANS;
                //coinsToAdd = fansToAdd*GameData.GSConstData.CashboxGoldPriceForFans;
                //coinsToAdd += Mathf.RoundToInt(MathUtils.CalcPercentValue(coinsToAdd, admanBonus));

                //Debug.Log("Cashbox collect " + coinsToAdd);
                request.SetEventKey("COLLECT_CASHBOX");
                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        //model.Parse(responce.ScriptData.GetGSData(GS_ScriptData.BuildingsData));
                        callback();
                    }
                },null,true);

            }
            else if (model.Id == MapBuildingsType.Shop || model.Id == MapBuildingsType.Kiosk)
            {
                coinsToAdd = (int)model.GetEffectValue(BuildingModel.CoinsProp);
                request.SetEventKey("COLLECT_SHOP_KIOSK");
                request.SetDurable(true);
                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        //model.Parse(responce.ScriptData.GetGSData(GS_ScriptData.BuildingsData));
                        callback();
                    }
                },null,true);
            }
            else
            {
                Debug.LogError("wrong collect building type " + model.Id);
            }


        }

        public void EndUpgradeProcess(UnityAction<bool> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("END_BUILDING_UPGRADE_PROCESS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _teamController.NeedUpdateTeamInfo = true;
                    callback(true);
                }
                else
                {
                    callback(false);
                    _currentUpgrade = GetCurrentUpgrade();
                    if (_currentUpgrade != null)
                    {
                        LoadBuildingsData((int)_currentUpgrade.Id, _currentUpgrade.Pos);
                    }
                    //LoadBuildingsData();
                }
            });
        }

        public bool isHallOfFameExist()
        {
            IBuildingModel hall = GetBuildingModel(MapBuildingsType.Hall);
            return hall != null && hall.IsBuilded;
        }

        public IBuildingModel GetBuildingModel(MapBuildingsType id, int pos = -1)
        {
            if (pos == -1)
            {
                return _buildings.Find(b => b.Id.Equals(id));
            }
            return _buildings.Find(b => b.Id.Equals(id) && b.Pos == pos);
        }


        private IBuildingModel GetBuildingModel(GSData building)
        {
            if (_buildings.Count == 0 || building == null) return null;
            MapBuildingsType id = building.GetInt("id").HasValue ? (MapBuildingsType)building.GetInt("id").Value : MapBuildingsType.None;
            int pos = building.GetInt("pos").HasValue ? building.GetInt("pos").Value : -1;
            return _buildings.Find(b => b.Id.Equals(id) && b.Pos == pos);
        }

        public IBuildingModel GetCurrentUpgrade()
        {
            return _buildings.Find(b => b.IsUpgrading);
        }

        public BuildingUpgradeInfo GetUpgrade(MapBuildingsType type, int pos)
        {
            return _upgrades.Find(upgrade => upgrade.Id.Equals(type) && upgrade.Pos == pos);
        }
   
        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.BuildingUpgradeData))
            {
                GSData upgrade = scriptData.GetGSData(GS_ScriptData.BuildingUpgradeData);
                if (upgrade != null)
                {
                    BuildingUpgradeInfo info = _upgrades.Find(u => (int)u.Id == upgrade.GetInt("id").Value && u.Pos == upgrade.GetInt("pos").Value);
                    if (info != null)
                    {
                        info.Parse(upgrade);
                    }
                    else
                    {
                        info = new BuildingUpgradeInfo(upgrade);
                        _upgrades.Add(info);
                    }
                }
            }

            if (scriptData.ContainsKey(GS_ScriptData.BuildingsData))
            {
                //load all buildings builded and upgraded
                var buildings = scriptData.GetGSDataList(GS_ScriptData.BuildingsData);
                if (buildings != null)
                {
                    _buildings.Clear();
                    foreach (var building in buildings)
                    {
                        var b = new BuildingModel(building);
                        _buildings.Add(b);

                        if (b.Id == MapBuildingsType.Hall)
                        {
                            _hall.UnlockSlots(b);
                        }

                        if (b.IsUpgrading && _currentUpgrade == null)
                        {

                            _currentUpgrade = b;
                           // _currentUpgrade.OnTimerLeftSecondsEvent.AddListener(OnUpgradeCompleteHandler);
                            _currentUpgrade.StartUpgradeTimer();
                        }
                    }
                }
                else
                {
                    var building = scriptData.GetGSData(GS_ScriptData.BuildingsData);
                    if (building != null)
                    {
                        var model = GetBuildingModel(building);
                        if (model != null)
                        {
                           model.Parse(building);
                        }
                        else
                        {
                            model = new BuildingModel(building);
                            _buildings.Add(model);
                        }

                        if (model.Id == MapBuildingsType.Hall)
                        {
                            _hall.UnlockSlots(model);
                        }
                        if (!model.IsUpgrading)
                        {
                            _currentUpgrade = null;
                            if (model.Id == MapBuildingsType.IceArena)
                            {
                                _teamController.SetMaxFans((int)model.GetEffectValue(BuildingModel.ArenaMaxFansProp));
                            }
                            EventsManager.Instance.Call(BuildingUpgradeComplete, model);
                        }
                    }
                }
            }

            if (scriptData.ContainsKey(GS_ScriptData.BuildingsCollectablesData))
            {
                List<GSData> collectables = scriptData.GetGSDataList(GS_ScriptData.BuildingsCollectablesData);
                foreach (var gsData in collectables)
                {
                    IBuildingModel model = GetBuildingModel(gsData);
                    if (model != null)
                    {
                        model.Parse(gsData);
                    }
                }
            }

            if (scriptData.ContainsKey(GS_ScriptData.ArenaLockedLotsData))
            {
                _arenaLockedLots = scriptData.GetGSDataList(GS_ScriptData.ArenaLockedLotsData);
            }
            
        }
      
        public void Initialize(IGameRoot root)
        {
            _root = root;


            _upgrades = new List<BuildingUpgradeInfo>();
            _buildings = new List<IBuildingModel>();

            _teamController = _root.GetController<TeamController>();

            _hall = _root.GetController<HallItemsController>();
        }
        
        public void Reset()
        {
            //
        }




        public void CheckMessage(GSData msgData, string msgCode)
        {
            if (msgCode == GS_Messages.BuildingUpgradeComplete)
            {
                _currentUpgrade = GetCurrentUpgrade();
                if (_currentUpgrade != null)
                {
                    LoadBuildingsData((int)_currentUpgrade.Id, _currentUpgrade.Pos);
                }
            }

            //check for income money or fans and upgrade complete
            if (msgData != null)
            {
                
                GSData building = msgData.GetGSData("data");
                IBuildingModel model;
                if (building != null)
                {
                    model = GetBuildingModel(building);
                    if (model != null)
                    {
                        model.Parse(building);
                    }
                }
                else
                {
                    List<GSData> buildings = msgData.GetGSDataList("data");
                    if (buildings != null)
                    {
                        foreach (var gsData in buildings)
                        {
                            model = GetBuildingModel(gsData);
                            if (model != null)
                            {
                                model.Parse(gsData);
                            }

                        }
                    }

                }
            }
            _teamController.NeedUpdateTeamInfo = true;
            
        }

        public bool CanCheckMessage(string msgType,GSData data = null)
        {
            return (data != null && data.GetString("type") == GS_Messages.BuildingType) || msgType == GS_Messages.BuildingUpgradeComplete;
        }


        //private void OnUpgradeCompleteHandler(long seconds)
        //{
        //    if (seconds == 0)
        //    {
        //        if (_currentUpgrade != null)
        //        {
        //            _root.CheckAndLoadJournalActions((result) =>
        //            {
        //                if (!result)
        //                {
        //                    _root.StartCoroutine(CheckUpgradeComplete());
        //                }
        //            },GS_Messages.BuildingUpgradeComplete);
        //        }
        //    }
        //}

        //IEnumerator CheckUpgradeComplete()
        //{
        //    yield return new WaitForSeconds(2f);
        //    OnUpgradeCompleteHandler(0);
        //     yield return null;
        //}
    }
}
