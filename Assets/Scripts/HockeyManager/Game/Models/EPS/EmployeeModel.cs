﻿using System;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Base;

namespace HockeyManager.Game.Models.EPS
{
    public sealed class EmployeeModel:BaseGameEntityModel,IEffect,IComparable<EmployeeModel>
    {


        public const string TacticBonusEffect = "tacticbonus";
        public const string TrainingBonusEffect = "trainingbonus";
        public const string CashboxBonusEffect = "cashboxbonus";
        public const string EnergyTimeBonusEffect = "energytimebonus";

        private EmployeeType _id;
        private int _maxLevel;

        private int _level;
        private int _currentBonusEffect = 0;

        private MainCouchTactic _bonusState = MainCouchTactic.TacticBalance;

        private GSData nextUpgrade;

        public EmployeeModel(GSData data)
        {
            _entityType = EntityType.Personal;
            _id = (EmployeeType)data.GetInt("id").Value;
            _maxLevel = data.GetInt("maxLevel").Value;
           
            Parse(data);
        }

        public int UpgradeGoldPrice
        {
            get { return nextUpgrade != null && nextUpgrade.GetInt("time").HasValue ? nextUpgrade.GetInt("time").Value * 2 : 0; }
        }

   

        public int MaxLevel
        {
            get { return _maxLevel; }
        }

        public EmployeeType Id
        {
            get { return _id; }
        }

        public int NextUpgradeTimeSeconds
        {
            get { return nextUpgrade != null && nextUpgrade.GetInt("time").HasValue ? nextUpgrade.GetInt("time").Value * 60 : 0; }
        }

        public int UnlockLevel
        {
            get
            {
                return nextUpgrade != null && nextUpgrade.GetInt("unlockLevel").HasValue ? nextUpgrade.GetInt("unlockLevel").Value : 0;
            }
        }

        public int NextUpgradeLevel
        {
            get
            {
                return nextUpgrade != null && nextUpgrade.GetInt("level").HasValue ?  nextUpgrade.GetInt("level").Value : 0; ;
            }
        }

        public int UpgradePrice
        {
            get
            {
                return nextUpgrade != null && nextUpgrade.GetInt("price").HasValue ? nextUpgrade.GetInt("price").Value : 0 ;
            }
        }

        public int CurrentBonusEffect
        {
            get { return _currentBonusEffect; }
        }

        public MainCouchTactic BonusState
        {
            get { return _bonusState; }
        }

        public int Level
        {
            get { return _level; }
        }

        public override void Parse(GSData data)
        {
           _currentBonusEffect = data.GetInt("bonus").Value;
            _level = data.GetInt("level").HasValue ? data.GetInt("level").Value : _level;
            if (data.ContainsKey("bonusState"))
            {
                _bonusState = (MainCouchTactic) data.GetInt("bonusState").Value;
            }

            if (_level < _maxLevel)
            {
                nextUpgrade = data.GetGSData("nextUpgrade");
            }
            else
            {
                nextUpgrade = null;
            }
         

             base.Parse(data);
        }


        public float GetEffectValue(string prop)
        {
            if (nextUpgrade != null && nextUpgrade.ContainsKey(prop))
            {
                return nextUpgrade.GetFloat(prop).Value;
            }
            return 0f;
        }

        public float GetEffectValue(params string[] prop)
        {
            return 0f;
        }

        public int CompareTo(EmployeeModel other)
        {
            return _id - other.Id;
        }
    }
}
