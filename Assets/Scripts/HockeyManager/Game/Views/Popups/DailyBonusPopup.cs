﻿using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Popups;

namespace HockeyManager.Game.Views.Popups
{
    public class DailyBonusPopup:BasePopup
    {
        private RewardModel _dailyBonusReward;

       

        protected override void UpdateData()
        {
            _dailyBonusReward = (RewardModel) _data;
            _basePopupControls.controls[0].SetText(_dailyBonusReward.RewardData.coins.ToString());
        }
        
    }
}
