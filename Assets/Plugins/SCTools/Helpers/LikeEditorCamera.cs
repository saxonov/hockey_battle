﻿using UnityEngine;
using System.Collections;
using SCTools;

namespace SCTools.Helpers
{

    [RequireComponent(typeof(Camera))]
    public class LikeEditorCamera : MonoBehaviour
    {

        public float ZoomFactor = 10f;

        [Range(-90f, 90f)]
        public float MaxYOrbitLimit = 45;
        public float OrbitXSpeed = 50f;
        public float OrbitYSpeed = 100f;

        public float LookAroundSpeed = 5f;
        public float MoveSpeed = 5f;
        public float PanSpeed = 10f;


        private float _distanceOrbit = 10f;

        public bool ShowGUIHelp = false;
        public Transform OrbitTarget;

        private Camera _camera;
        private Transform _transform;

        private bool _isTinting;
        private bool _isOrbit;
        private bool _isLook;
        private bool _isMove;
        private bool _isPan;

        private Vector3 _movePos;
        private Vector3 _mouseLastPosition;

        private float _x = 0f;
        private float _y = 0f;

        private Vector3 _eulerAngles;
        private float _yaw;
        private float _pitch;
        private Vector3 _orbitPoint;

        private GUIStyle labelStyle;

        // Use this for initialization
        void Start()
        {
            labelStyle = new GUIStyle();
            labelStyle.fontStyle = FontStyle.Bold;

            _transform = GetComponent<Transform>();
            _camera = GetComponent<Camera>();
            _eulerAngles = _transform.eulerAngles;
            _x = _eulerAngles.y;
            _y = _eulerAngles.x;
            _pitch = _x;
            _yaw = _y;

        }

        void OnGUI()
        {
            if (ShowGUIHelp)
            {
                GUI.Label(new Rect(10, 10, 400, 20), "Zoom:Scroll MouseWheel.", labelStyle);
                GUI.Label(new Rect(10, 30, 500, 20), "Pan:Press and hold MouseWheel Button and Move.", labelStyle);
                GUI.Label(new Rect(10, 50, 500, 20), "Orbit:Press Left Alt and hold mouse left button.", labelStyle);
                GUI.Label(new Rect(10, 70, 500, 20), "Select orbit transform:Press F key in scene to select orbit center.", labelStyle);
                GUI.Label(new Rect(10, 90, 500, 20), "Movement:Press WASD for move in directions, speed Up with Left Shift.", labelStyle);
            }
        }

        void FixedUpdate()
        {
            if (Input.GetKeyDown(KeyCode.F) && !_isOrbit && !_isTinting)
            {
                RaycastHit hit;
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, int.MaxValue))
                {
                    Renderer rend = hit.collider.gameObject.GetComponent<Renderer>();
                    if (rend)
                    {
                        if (rend.material.HasProperty("color"))
                        {
                            _isTinting = true;
                            StartCoroutine("Tint", rend.material.color);
                            rend.material.color = Color.red;
                        }


                    }
                    OrbitTarget = hit.transform;
                    _distanceOrbit = hit.distance;
                }
            }

        }


        IEnumerator Tint(Color startColor)
        {
            yield return new WaitForSeconds(0.2f);

            if (OrbitTarget != null)
            {
                _isTinting = false;
                Renderer rend = OrbitTarget.gameObject.GetComponent<Renderer>();
                if (rend)
                {
                    rend.material.color = startColor;
                }
            }
            yield return null;
        }

        void LateUpdate()
        {
            Pan();
            MoveOrZoom();
            LookAround();

            Orbit();

        }

        private void Orbit()
        {
            _orbitPoint = OrbitTarget != null ? OrbitTarget.position : Vector3.zero;
            _isOrbit = Input.GetKey(KeyCode.LeftAlt);
            if (_isOrbit && Input.GetMouseButton(0))
            {

                _x += Input.GetAxis("Mouse X") * OrbitXSpeed * _distanceOrbit * 0.02f;
                _y -= Input.GetAxis("Mouse Y") * OrbitYSpeed * 0.02f;

                _y = Utils.MathUtils.ClampAngle(_y, -MaxYOrbitLimit, MaxYOrbitLimit);


                Vector3 negDistance = new Vector3(0.0f, 0.0f, -_distanceOrbit);
                Quaternion rotation = Quaternion.Euler(_y, _x, 0);

                _movePos = rotation * negDistance + _orbitPoint;
                _transform.position = _movePos;
                _transform.rotation = rotation;

                _pitch = _x;
                _yaw = _y;
            }
        }

        private void LookAround()
        {
            _isLook = Input.GetMouseButton(1);
            if (_isLook && !_isOrbit)
            {
                _yaw -= Input.GetAxis("Mouse Y") * LookAroundSpeed;
                _pitch += Input.GetAxis("Mouse X") * LookAroundSpeed;

                _eulerAngles.x = _yaw;
                _eulerAngles.y = _pitch;
                _eulerAngles.z = 0f;
                _transform.eulerAngles = _eulerAngles;
                _x = _transform.eulerAngles.y;
                _y = _transform.eulerAngles.x;
            }
        }


        private void MoveOrZoom()
        {
            if (_isOrbit)
            {
                return;

            }
            float wheel = Input.GetAxis("Mouse ScrollWheel");
            if (wheel > 0 || wheel < 0)
            {
                _transform.Translate(0, 0, wheel * ZoomFactor, Space.Self);
            }

            _isMove = false;
            if (Input.GetKey(KeyCode.W))
            {
                _isMove = true;
                _movePos.x = 0;
                _movePos.y = 0;
                _movePos.z = MoveSpeed;

            }
            if (Input.GetKey(KeyCode.S))
            {
                _isMove = true;
                _movePos.x = 0;
                _movePos.y = 0;
                _movePos.z = -MoveSpeed;
            }
            if (Input.GetKey(KeyCode.A))
            {
                _isMove = true;
                _movePos.x = -MoveSpeed;
                _movePos.y = 0;
                _movePos.z = 0;
            }
            if (Input.GetKey(KeyCode.D))
            {
                _isMove = true;
                _movePos.x = MoveSpeed;
                _movePos.y = 0;
                _movePos.z = 0;
            }
            if (_isMove && !_isPan)
            {

                bool speedUp = Input.GetKey(KeyCode.LeftShift);
                _movePos = _movePos * Time.deltaTime;
                if (speedUp)
                {
                    _movePos = _movePos * 2f;
                }

                _transform.Translate(_movePos, Space.Self);

            }



        }

        private void Pan()
        {
            if (_isMove)
            {
                return;

            }
            _isPan = Input.GetMouseButton(2);
            if (Input.GetMouseButtonDown(2))
            {
                _mouseLastPosition = Input.mousePosition;
            }
            if (_isPan)
            {
                Vector3 delta = _camera.ScreenToViewportPoint(Input.mousePosition - _mouseLastPosition);
                _movePos.x = -(delta.x * PanSpeed);
                _movePos.y = -(delta.y * PanSpeed);
                _movePos.z = 0f;
                _transform.Translate(_movePos, Space.Self);
                _mouseLastPosition = Input.mousePosition;
            }
            _isPan = false;


        }
    }

}
