﻿using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.HallItems;
using HockeyManager.Game.Models.Map;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class HallItemsController:IGameController, IGSResponceHandler
    {
        //public const string SLotsUnlocked = "slotsUnlocked";

        private IGameRoot _root;

        private List<HallItemSlotModel> _slots; 
        private List<HallItemModel> _allItems;

        private bool _hallItemsLoaded;

        private int _activeCount;
        private float _totalFansBonus;

        private TeamController _team;

        public List<HallItemSlotModel> Slots
        {
            get { return _slots; }
        }

        public int ActiveSlotsCount
        {
            get { return _activeCount; }
        }

        public float GetTotalFansBonus()
        {
            _totalFansBonus = 0f;
            List<HallItemModel> activeItems = _allItems.FindAll(i => i.Active && i.Slot != -1);
            foreach (var hallItemModel in activeItems)
            {
                _totalFansBonus += hallItemModel.GetEffectValue(HallItemModel.FansBoosterEffect);
            }
            return _totalFansBonus;
        }

        public float GetTotalEffect(string effectType)
        {
            List<HallItemModel> activeItems = _allItems.FindAll(i => i.Active && i.Slot != -1);

            float totalValuePercent = 0f;
            foreach (var hallItemModel in activeItems)
            {
                totalValuePercent += hallItemModel.GetEffectValue(effectType);
            }
            return totalValuePercent;
        }

        public void LoadItems(UnityAction callback,bool forceUpdate = false)
        {
            _totalFansBonus = 0;
            if (_hallItemsLoaded && !forceUpdate)
            {
                if (callback != null)
                {
                    callback();
                }
                
                return;
            }

            if (!forceUpdate)
            {
                _hallItemsLoaded = true;
            }

            var request = new LogEventRequest();
            request.SetEventKey("GET_HALL_ITEMS");
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    _activeCount = 0;
                    for (int i = 0; i < _allItems.Count; i++)
                    {
                        if (_allItems[i].Active)
                        {
                            var i1 = i;
                            HallItemSlotModel slot = _slots.Find(s => s.SlotId == _allItems[i1].Slot);
                            slot.SetItem(_allItems[i].Id);
                            _activeCount++;
                        }
                    }

                    if (callback != null)
                    {
                        callback();
                    }
                }
            });
        }

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.HallItemsData))
            {
                List<GSData> rawList = scriptData.GetGSDataList(GS_ScriptData.HallItemsData);
                if (rawList != null)
                {
                    foreach (var gsData in rawList)
                    {
                        int id = gsData.GetInt("id").Value;
                        HallItemModel model = _allItems.Find(item => item.Id == id);
                        if (model == null)
                        {
                            _allItems.Add(new HallItemModel(gsData));
                        }
                        else
                        {
                            model.Parse(gsData);
                            //_root.GetController<TeamController>().NeedUpdateTeamInfo = true;
                        }

                    }
                }
                else
                {
                    GSData itemData = scriptData.GetGSData(GS_ScriptData.HallItemsData);
                    HallItemModel model = _allItems.Find(item => item.Id == itemData.GetInt("id").Value);
                    if(model != null)
                    {
                        model.Parse(itemData);
                    }
                }
               
            }

            if (scriptData.ContainsKey(GS_ScriptData.HallItemsFansBonusData))
            {
                _totalFansBonus = scriptData.GetGSData(GS_ScriptData.HallItemsFansBonusData).GetInt(HallItemModel.FansBoosterEffect).Value;
            }
        }
        public void BuyItem(HallItemModel model,UnityAction callback)
        {
            BuyVirtualGoodsRequest request = new BuyVirtualGoodsRequest();
            request.SetCurrencyType(model.CurrencyType);
            request.SetQuantity(1);
            request.SetShortCode(model.ShortCode);
            request.SetDurable(true);

            _root.NetManager.RunBuyVirtualEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    _team.NeedUpdateTeamInfo = true;
                    callback();
                }
                
            });
        }


        public List<HallItemModel> GetAvailableItems()
        {
            List<HallItemModel> filtered = _allItems.FindAll(item => !item.Disabled && !item.Active);
            filtered.Sort();
            return filtered;
        }

        public bool RemoveItemFromSlot(HallItemSlotModel slot)
        {
            if (slot.ItemId != -1)
            {
                _totalFansBonus = 0;
                HallItemModel model = _allItems.Find(m => m.Slot == slot.SlotId);
                model.Slot = -1;
                model.Active = false;
                slot.SetItem(-1);
                _activeCount--;
                SendActivateItemRequest(model.ShortCode,0,slot.SlotId);
                // UpdateChanges(model);
            }
            return false;
        }



        public bool AddItemToSlot(HallItemModel model,HallItemSlotModel slot)
        {
            if (slot != null && !slot.IsLocked)
            {
                _totalFansBonus = 0;
                slot.SetItem(model.Id);
                model.Slot = slot.SlotId;
                model.Active = true;
                _activeCount ++;
               // UpdateChanges(model);
                SendActivateItemRequest(model.ShortCode,1,slot.SlotId);
                return true;
            }
            return false;
        }

        public bool HasEmptySlot()
        {
            return _slots.Exists(s => !s.IsLocked && s.ItemId == -1);
        }
        
        public void Initialize(IGameRoot root)
        {
            _root = root;
            _team = _root.GetController<TeamController>();
            _allItems = new List<HallItemModel>();
            BuildSlots();
        }

        public void Reset()
        {
            _hallItemsLoaded = false;
        }
        private void SendActivateItemRequest(string sc,int active,int slotId)
        {
            var request = new LogEventRequest();
            request.SetEventKey("ACTIVATE_HALL_ITEM");
            request.SetEventAttribute("SHORT_CODE",sc);
            request.SetEventAttribute("ACTIVE",active);
            request.SetEventAttribute("SLOT_ID",slotId);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    _team.NeedUpdateTeamInfo = true;
                }
            });
        }

        private void BuildSlots()
        {
            _slots = new List<HallItemSlotModel>();
            for (int i = 0; i < GameData.MaxHallInventorySlots; i++)
            {
                _slots.Add(new HallItemSlotModel(i + 1));
            }
        }

        public void UnlockSlots(IBuildingModel hallBuilding)
        {
            if (hallBuilding != null)
            {
                var cellsCount = hallBuilding.GetEffectValue(BuildingModel.HallCellsProp);
                for (int i = 0; i < cellsCount; i++)
                {
                    _slots[i].IsLocked = false;
                }
            }
        }

    }
}
