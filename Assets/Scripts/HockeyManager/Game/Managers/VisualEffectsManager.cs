﻿using System.Collections.Generic;
using DG.Tweening;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Managers
{
    public class VisualEffectsManager : MonoBehaviour
    {
        //private const string FreeToFlyObjectName = "FreeToFly";
       // private const string FlyObjectName = "FlyNow";

        public const string AllTweensCompleteEvent = "allTweensCompleteEvent";

        public UnityEvent OnAllTweensComplete = new UnityEvent();
     
        private const float FlyTimeDuration = 1f;

        public const int FlyObjectsCountLimit = 10;

        public const string FlyCoinEffectComplete = "flyCoinEffectComplete";
        public const string FlyGoldEffectComplete = "flyGoldEffectComplete";
        public const string FlyFanEffectComplete = "flyFanEffectComplete";
        //public const string FlyEffectStarted = "flyEffectStarted";
        //public const string FlyEffectComplete = "flyEffectComplete";

        private Dictionary<Transform,Transform> buildingHolders;

        private List<Transform> _flyObjectsPool; 

        private static VisualEffectsManager _instance;

        private GameObject rewardsContainer;

        public UnityEvent OnFlyRewardCompleteEvent = new UnityEvent();

       // private UnityAction _onFlyRewardCompleteCallback = null;

#pragma warning disable CS0649 // Полю "VisualEffectsManager.mapLayer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private RectTransform mapLayer;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.mapLayer" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "VisualEffectsManager.coinsPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl coinsPanel;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.coinsPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl goldPanel;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.goldPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.fansPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl fansPanel;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.fansPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.expPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField]private TextIconControl expPanel;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.expPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

#pragma warning disable CS0649 // Полю "VisualEffectsManager.flyCoinsPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject flyCoinsPrefab;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.flyCoinsPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.flyGoldPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject flyGoldPrefab;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.flyGoldPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.flyExpPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject flyExpPrefab;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.flyExpPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "VisualEffectsManager.flySupportPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private GameObject flySupportPrefab;
#pragma warning restore CS0649 // Полю "VisualEffectsManager.flySupportPrefab" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private TeamController teamController;

        //private Tweener _coinsIconTargetTweener;
        //private Tweener _goldIconTargetTweener;
        //private Tweener _expIconTweener;
        //private Tweener _fansIconTargetTweener;

    
        private int _totalFlyTweenCount = 0;

        public static VisualEffectsManager Instance
        {
            get { return _instance; }
        }

        void OnDestroy()
        {
            _instance = null;
            buildingHolders = null;
            teamController = null;
        }

        void Awake()
        {
            _instance = this;
            _flyObjectsPool = new List<Transform>();
            buildingHolders =  new Dictionary<Transform, Transform>();
            teamController = GameRoot.Instance.GetController<TeamController>();
        }

        private int GetFlyCount(int collectValue)
        {
            int count = 0;
            if (collectValue < 100)
            {
                float percent = collectValue/100f;
                count = Mathf.FloorToInt(FlyObjectsCountLimit * percent);
            }
            else
            {
                int diff = Mathf.FloorToInt((100f/collectValue)*FlyObjectsCountLimit);
                count = FlyObjectsCountLimit - diff;
            }
            return count == 0 ? 1 : count;
        }

        public void FlyBuildingAward(Transform building,int coins,int fans)
        {
            Transform fromTarget;
            if (!buildingHolders.TryGetValue(building, out fromTarget))
            {
                GameObject go = new GameObject(building.name);
                go.transform.SetParent(transform,false);
                fromTarget = go.transform;
                buildingHolders.Add(building, fromTarget);
            }

            Update();
            
            if (coins > 0)
            {
                FlyCoins(fromTarget, GetFlyCount(coins),coins);
            }
            if (fans > 0)
            {
                FlyFans(fromTarget, GetFlyCount(fans),fans);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromTarget"></param>
        /// <param name="reward"></param>
        /// <param name="useFlyCount"></param>
        public void FlyRewards(Vector2 fromTarget, RewardData reward,bool useFlyCount = true)
        {
           
            if (rewardsContainer == null)
            {
                rewardsContainer = new GameObject("rewardsHolder");
                rewardsContainer.transform.SetParent(transform, false);

            }

            rewardsContainer.transform.position = fromTarget;

            if (reward.coins > 0)
            {
                FlyCoins(rewardsContainer.transform, GetFlyCount(useFlyCount ? reward.coins : 1), reward.coins);
            }
            if (reward.gold > 0)
            {
                FlyGold(rewardsContainer.transform, GetFlyCount(useFlyCount ? reward.gold : 1), reward.gold);
            }
            if (reward.exp > 0)
            {
                FlyExp(rewardsContainer.transform, GetFlyCount(useFlyCount ? reward.exp : 1), reward.exp);
            }
            if (reward.fans > 0)
            {
                FlyFans(rewardsContainer.transform, GetFlyCount(useFlyCount ? reward.fans : 1), reward.fans);
            }

        }

        private void FlyCoins(Transform fromTarget,int count = 1,int coinsToAdd = 0)
        {
            Transform target = coinsPanel.GetIcon().transform;
            int i;
            for (i = 0; i < count; i++)
            {
                Transform flyCoin = GetFlyObjectFromPool("coin", fromTarget, flyCoinsPrefab);
                int i1 = i;
                FlyItemInternal(flyCoin, target, i*0.1f, (flyObject) =>
                {
                  //  if (_coinsIconTargetTweener == null || !_coinsIconTargetTweener.IsActive())
                   // {
                        coinsPanel.AnimateIcon(1);
                  //  }
                    flyCoin.gameObject.SetActive(false);
                    _flyObjectsPool.Add(flyCoin);
                    SoundManager.Instance.PlaySFXOnGameObject("flytomoney", coinsPanel.gameObject);
                    if (i1 == 0)
                    {
                        teamController.AddCoins(coinsToAdd,count * FlyTimeDuration,true);
                    }
                   
                });
            }
       
        }

        private void FlyGold(Transform fromTarget, int count = 1, int goldToAdd = 0)
        {
            Transform target = goldPanel.GetIcon().transform;
            int i;
            for (i = 0; i < count; i++)
            {
                Transform flyGold = GetFlyObjectFromPool("gold",fromTarget,flyGoldPrefab);// fromTarget.AddChild(flyGoldPrefab);
                int i1 = i;
                FlyItemInternal(flyGold, target, i * 0.1f, (flyObject) =>
                {
                   // if (_goldIconTargetTweener == null || !_goldIconTargetTweener.IsActive())
                   // {
                        goldPanel.AnimateIcon();
                   // }
                    flyGold.gameObject.SetActive(false);
                    _flyObjectsPool.Add(flyGold);
                    SoundManager.Instance.PlaySFXOnGameObject("flytogold", goldPanel.gameObject);
                    if (i1 == 0)
                    {
                        
                        teamController.AddGold(goldToAdd, count * FlyTimeDuration, true);
                    }

                });
            }

        }


        private void FlyExp(Transform fromTarget, int count = 1, int expToAdd = 0)
        {
            Transform target = expPanel.GetIcon().transform;
            int i;
            for (i = 0; i < count; i++)
            {
                Transform flyExp =  GetFlyObjectFromPool("exp",fromTarget,flyExpPrefab);
                int i1 = i;
                FlyItemInternal(flyExp, target, i * 0.1f, (flyObject) =>
                {
                    //if (_expIconTweener == null || !_expIconTweener.IsActive())
                    //{
                       expPanel.AnimateIcon();
                    //}
                    flyExp.gameObject.SetActive(false);
                    _flyObjectsPool.Add(flyExp);
                    SoundManager.Instance.PlaySFXOnGameObject("flytoexp", expPanel.gameObject);
                    if (i1 == 0)
                    {
                        
                        teamController.AddExp(expToAdd, count * FlyTimeDuration, true);
                    }

                });
            }

        }

        private void FlyFans(Transform fromTarget,int count = 1,int fansToAdd = 0)
        {
            Transform target = fansPanel.GetIcon().transform;
            for (int i = 0; i < count; i++)
            {
                Transform flyFan = GetFlyObjectFromPool("fans",fromTarget,flySupportPrefab);
                var i1 = i;
                FlyItemInternal(flyFan, target, i * 0.1f, (flyObject) =>
                {
                   // if (_fansIconTargetTweener == null || _fansIconTargetTweener.IsActive())
                   // {
                       fansPanel.AnimateIcon();
                   // }
                    flyFan.gameObject.SetActive(false);
                    _flyObjectsPool.Add(flyFan);
                    SoundManager.Instance.PlaySFXOnGameObject("flytofans", fansPanel.gameObject);
                    if (i1 == 0)
                    {
                        teamController.AddFans(fansToAdd, count * FlyTimeDuration, true);
                    }
                });
            }

        }

        private void FlyItemInternal(Transform flyObject, Transform flyTarget, float delay, TweenCallback<GameObject> flyTargetComplete)
        {
            _totalFlyTweenCount++;
            flyObject.gameObject.SetActive(false);
            Sequence sequence = DOTween.Sequence();
          
            TweenParams tparams =
                new TweenParams()
                    .SetEase(Ease.InOutSine)
                    .SetAutoKill()
                    .SetDelay(delay).OnPlay(() =>
                    {
                        flyObject.gameObject.SetActive(true);

                    }).OnComplete(() =>
                    {
                        
                        flyTargetComplete.Invoke(flyObject.gameObject);
                    });
            sequence.SetAutoKill(true);
            sequence.Append(flyObject.DOMove(flyTarget.position, FlyTimeDuration).SetAs(tparams));
            sequence.Insert(0f, flyObject.transform.DOScale(Vector3.one * 3f, 0.5f).SetEase(Ease.InFlash).SetDelay(delay));
            sequence.Insert(0.6f, flyObject.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutFlash).SetDelay(delay));
            sequence.AppendCallback(OnFlyTweenComplete);

        }


        private Transform GetFlyObjectFromPool(string typeName, Transform holder, GameObject flyPrefab)
        {
            Transform tr = _flyObjectsPool.Find(t => t.gameObject.name == typeName);
            if (tr == null)
            {
                tr = holder.AddChild(flyPrefab);
                tr.gameObject.name = typeName;
            }
            else
            {
                tr.SetParent(holder);
                tr.localScale = Vector3.one;
                tr.localPosition = Vector3.zero;
                _flyObjectsPool.Remove(tr);
            }
            return tr;
        }

        private void OnFlyTweenComplete()
        {
            _totalFlyTweenCount--;
         
            if (_totalFlyTweenCount <= 0)
            {
                Debug.Log("All fly tween complete ");
                _totalFlyTweenCount = 0;

                EventsManager.Instance.Call(AllTweensCompleteEvent);

                OnAllTweensComplete.Invoke();

               
                OnFlyRewardCompleteEvent.Invoke();
                
              //  OnAllTweenFlyComplete.Invoke();
                //teamController.CheckAndApplyReservedFans();
                //teamController.CheckAndApplyReservedCoins();
                //teamController.SkipCoinsFansGoldUpdates = false;
            }
        }

        void Update()
        {
            if (buildingHolders != null && buildingHolders.Count > 0)
            {
                foreach (KeyValuePair<Transform,Transform> keyValuePair in buildingHolders)
                {
                    Vector3 pos = mapLayer.InverseTransformPoint(keyValuePair.Key.transform.position);
                    keyValuePair.Value.transform.localPosition = pos;
                }
            }
        }

    }
}
