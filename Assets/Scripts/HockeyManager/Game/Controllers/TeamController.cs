﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models;
using HockeyManager.Game.Models.Leaderboard;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;
using MathUtils = SCTools.Utils.MathUtils;

namespace HockeyManager.Game.Controllers
{
    public class TeamController:IGameController,ICheckMessages, IDisposable, IGSResponceHandler
    {
        public const string CoinsChanged = "TeamController.coinsChanged";
        public const string GoldChanged = "TeamController.goldChanged";
        public const string EnergyChanged = "TeamController.energyChanged";
        public const string LevelChanged = "TeamController.levelRankChanged";
        public const string FansChanged = "TeamController.fansChanged";
        public const string MaxFansChanged = "TeamController.maxFansChanged";
        public const string VipOrShieldUpdate = "TeamController.vipOrShieldUpdates";


        public const string LeaderBoardUpdateEvent = "TeamController.updateLeaderBoard";
        public const string CurrentActionStateChanged = "TeamController.CurrentActionStateChanged";

        private int _maxFansToBuy = -1;

        private int _totalTeamsCount = 0;

        private GSData _fansPenalty = null;

        private GameActionState _currentActionState = GameActionState.None;

        private List<LeaderTeamModel> _leadersBoard; 

        private TeamModel _team;
         
        private IGameRoot _root;

        private bool _isLevelUp = false;
        private bool _isLevelUpComplete = false;

        private bool _needUpdateTeamInfo = true;
        private int _newExp = 0;

        private GSData _currentTeamInfo;
    
        private int _rewardFans = 0;

        public TeamModel TeamData
        {
            get { return _team; }
        }

        public bool HasProtectShield 
        {
            get { return _team != null && _team.protectShield != null; }
        }


        public void DisableVip()
        {
            if (_team != null && _team.vip != null)
            {
                _team.vip = null;
                EventsManager.Instance.Call(VipOrShieldUpdate);
            }
        }
        public void DisableShield()
        {
            if (_team != null && _team.protectShield != null)
            {
                _team.protectShield = null;
                EventsManager.Instance.Call(VipOrShieldUpdate);
            }
        }

        public bool HasVip
        {
            get { return _team != null && _team.vip != null; }
        }


        public GSData GetFansPenalty()
        {
            GSData penalty = _fansPenalty;
            _fansPenalty = null;
            return penalty;
        }

        public void GetTotalTeamsCount(UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_TOTAL_TEAMS_COUNT");
            request.SetEventAttribute("DATA","");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _totalTeamsCount = responce.ScriptData.GetInt("TotalTeamsCount").Value;
                    callback();
                }
            });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="count"></param>
        public void LoadLeaderBoard(UnityAction callback,byte count)
        {
            AroundMeLeaderboardRequest request = new AroundMeLeaderboardRequest();
            request.SetEntryCount(count);
            request.SetIncludeLast(0);
            request.SetIncludeFirst(count);
            request.SetLeaderboardShortCode("TOTAL_EXP_LB");
            _root.NetManager.RunLeaderboardRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _leadersBoard.Clear();
                    int index = 0;
                    List<GSData> players = responce.ScriptData.GetGSDataList(GS_ScriptData.LeaderBoardPlayersData);
                    foreach (var leaderboardData in players)
                    {
                        _leadersBoard.Add(new LeaderTeamModel(leaderboardData, ++index));
                    }
                    callback();
                }
            });
        }

        public void LoadTeamInfo(UnityAction<GSData> callback,bool battleInfo = true,bool buildsInfo = true)
        {
            if (!_needUpdateTeamInfo && _currentTeamInfo != null)
            {
                callback(_currentTeamInfo);
                return;
            }
            _needUpdateTeamInfo = false;
            LogEventRequest request = new LogEventRequest();
            request.SetEventKey("GET_USER_STAT");
            request.SetEventAttribute("BATTLE_INFO",battleInfo ? 1 : 0);
            request.SetEventAttribute("BUILDS_STAT",buildsInfo ? 1 : 0);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    _currentTeamInfo = responce.ScriptData.GetGSData(GS_ScriptData.TeamStatisticData);
                    callback(_currentTeamInfo);
                }  
            });
        }

        public void ChangeName(string newName,UnityAction callback)
        {
            if (string.IsNullOrEmpty(newName))
            {
                throw new Exception("newName is empty!");
            }
            var request = new LogEventRequest();
            request.SetEventKey("CHANGE_TEAM_NAME");
            request.SetEventAttribute("NAME", newName);
            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                callback();
            });
        }
      
        public void RefreshBalance()
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_BALANCE");
            _root.NetManager.RunLogEventRequest(request);
        }

        public void UpdateEnergy(int energyCost = 0,UnityAction callback = null)
        {
            if (_team.energy == _team.maxEnergy)
            {
                if (callback != null)
                {
                    callback();
                }
                return;
            }

            if (_team.energy <= energyCost || energyCost == 0)
            {
                var request = new LogEventRequest();
                request.SetEventKey("UPDATE_ENERGY");
                _root.NetManager.RunLogEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        if (callback != null)
                        {
                            callback();
                        }

                    }
                },null,true);
            }
            else
            {
                if (callback != null)
                {
                    callback();
                }
            }
            
        }

        public bool IsLevelUp
        {
            get { return _isLevelUp && !_isLevelUpComplete; }
        }

        public bool IsLevelUpComplete
        {
            set { _isLevelUpComplete = value; }
        }
        
        public bool NeedUpdateTeamInfo
        {
            set { _needUpdateTeamInfo = value; }
          //  get { return _needUpdateTeamInfo; }
        }

        public List<LeaderTeamModel> LeadersBoard
        {
            get { return _leadersBoard; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaxFansToBuy
        {
            get { return _maxFansToBuy; }
        }

        public GameActionState CurrentActionState
        {
            get { return _currentActionState; }
        }

        public int TotalTeamsCount
        {
            get { return _totalTeamsCount; }
        }

        public bool CanBuyForEnergy(int energyCost)
        {
            return _team.energy >= energyCost;
        }

        public bool CanBuyForCoins(int coinsPrice)
        {
            Debug.LogFormat("need coins {0} , coins has {1}",coinsPrice,_team.coins);
            return _team.coins >= coinsPrice;
        }

        public bool CanBuyForGold(int gold)
        {
            Debug.LogFormat("need gold {0} , gold has {1}", gold, _team.gold);
            return _team.gold >= gold;
        }

        public bool CanBuyForCurrency(int currencyType, int price)
        {
            if (currencyType == GameData.CurrencyGold)
            {
                return _team.gold >= price;
            }

            return currencyType == GameData.CurrencyCoins && _team.coins >= price;
        }
        
        public void Initialize(IGameRoot root)
        {
            _root = root;
            _leadersBoard = new List<LeaderTeamModel>();
            NewHighScoreMessage.Listener += OnHighScoreChangedEvent;
            GlobalRankChangedMessage.Listener += OnGlobalRankChanged;
        }

        public void Reset()
        {
            _needUpdateTeamInfo = true;
        }

        private void OnGlobalRankChanged(GlobalRankChangedMessage scoreMessage)
        {
            if (scoreMessage.LeaderboardShortCode == "TOTAL_EXP_LB")
            {
                EventsManager.Instance.Call(LeaderBoardUpdateEvent);
              //  _root.DismissMessage(scoreMessage);
            }
        }

        private void OnHighScoreChangedEvent(NewHighScoreMessage scoreMessage)
        {
            if (scoreMessage.LeaderboardShortCode == "TOTAL_EXP_LB")
            {
                 EventsManager.Instance.Call(LeaderBoardUpdateEvent);
                //_root.DismissMessage(scoreMessage);
            }
        }

        public bool NeedFansToUpgrade(out float fillPercent)
        {
            fillPercent = MathUtils.GetPercent(_team.fans, _team.maxFans);
            return fillPercent < GameData.GSConstData.ArenaUpgradeFansLimitPercent;
        }

        public bool NeedFansToUpgrade()
        {
            return MathUtils.GetPercent(_team.fans, _team.maxFans) < GameData.GSConstData.ArenaUpgradeFansLimitPercent;
        }

        public int GetGoldPricePerFan(int count)
        {
            return count == 1 ? 1 : count/2;
        }

        public void ChekTeamNameAvailable(string teamName,UnityAction<bool> callback)
        {
            if (string.IsNullOrEmpty(teamName))
            {
                callback(false);
                return;
            }

            var request = new LogEventRequest();
            request.SetEventKey("CHECK_TEAM_NAME");
            request.SetEventAttribute("NAME", teamName);

            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                if (responce.HasErrors)
                {
                    callback(false);
                }
                else
                {
                    callback(true);
                }
            });

        }

        /// <summary>
        /// need for fly coins effect
        /// </summary>
        /// <param name="value"></param>
        /// <param name="duration"></param>
        /// <param name="add"></param>
        public void AddCoins(int value, float duration, bool add)
        {
           
                if (add)
                {
                    _team.coins += value;
                    _team.coins = Mathf.Max(0, _team.coins);
                }
                else
                {
                    _team.coins = value;
                }
                _needUpdateTeamInfo = true;
                EventsManager.Instance.Call(CoinsChanged,duration);
            
        }

        public void AddGold(int value, float duration, bool add)
        {
            
                if (add)
                {
                    _team.gold = _team.gold + value;
                }
                else
                {
                    _team.gold = value;
                }
                EventsManager.Instance.Call(GoldChanged, duration);
            
        }
        
        public void AddExp(int value, float duration, bool add)
        {
           
                if (add)
                {
                    _team.exp = _team.exp + value;
                }
                else
                {
                    _team.exp = value;
                }

                if (_team.exp != _newExp)
                {
                    Debug.Log("newExp : " + _newExp + " exp: " + _team.exp);
                    _team.exp = _newExp;
                    _newExp = 0;
                }
            _needUpdateTeamInfo = true;
            EventsManager.Instance.Call(LevelChanged, duration);
        }

        public void AddFans(int value,float duration, bool add)
        {
            
                if (add)
                {
                    //Debug.Log("fans value: " + value);
                    _team.fans = _team.fans + value;
                    _team.fans = Mathf.Max(0, _team.fans);
                   // Debug.Log("result fans value: " + _team.fans);
                }
                else
                {
                    _team.fans = value;
                }

            if (_rewardFans > 0 && _team.fans != _rewardFans)
            {
                Debug.Log("rewards fans : " + _rewardFans + " fans: " + _team.fans);
                _team.fans = _rewardFans;
                
            }
            _rewardFans = 0;
            _needUpdateTeamInfo = true;
            EventsManager.Instance.Call(FansChanged, duration);
            
        }
        
        private void UpdateTeamData(GSData teamData)
        {
            if (teamData != null)
            {
                _team = _team ?? new TeamModel(teamData.GetString("userId"));
                
                bool vipShieldUpdate = false;

                if (teamData.ContainsKey("vip"))
                {
                    vipShieldUpdate = true;
                    var vipBonusData = teamData.GetGSData("vip");
                    if (vipBonusData != null)
                    {
                        _team.vip = JsonUtility.FromJson<VirtualGoods.TimelimitVirtualGood>(teamData.GetGSData("vip").JSON);
                        GameData.HasVip = true;
                    }
                    else
                    {
                        GameData.HasVip = false;
                        _team.vip = null;
                    }

                }

                if (teamData.ContainsKey("protectShield"))
                {
                    vipShieldUpdate = true;
                    var protectShield = teamData.GetGSData("protectShield");
                    if (protectShield != null)
                    {
                        _team.protectShield = JsonUtility.FromJson<VirtualGoods.TimelimitVirtualGood>(teamData.GetGSData("protectShield").JSON);
                    }
                    else
                    {
                        _team.protectShield = null;
                    }
                }

                
                if (teamData.ContainsKey("fansPenalty"))
                {
                    _fansPenalty = teamData.GetGSData("fansPenalty");
                }

                if (teamData.ContainsKey("gameState"))
                {
                    _currentActionState = (GameActionState)teamData.GetInt("gameState").Value;
                    EventsManager.Instance.Call(CurrentActionStateChanged);
                }
              
                if (teamData.ContainsKey("isNew"))
                {
                    JsonUtility.FromJsonOverwrite(teamData.JSON,_team);
                    _team.TeamInfo.Parse(teamData);
                    _isLevelUpComplete = false;
                    return;
                }

                if (teamData.ContainsKey("maxFansToBuy"))
                {
                    _maxFansToBuy = teamData.GetInt("maxFansToBuy").Value;
                }


                if (teamData.ContainsKey("gold"))
                {
                    AddGold(teamData.GetInt("gold").Value,5f,false);
                }

                if (teamData.ContainsKey("coins"))
                {
                    AddCoins(teamData.GetInt("coins").Value, 5f, false);
                }

                if (teamData.ContainsKey("energy") || teamData.ContainsKey("energyEndTime") || 
                    teamData.ContainsKey("energyTimeBoosterEffect") || teamData.ContainsKey("energyTime"))
                {
                    _team.energyEndTime = teamData.GetLong("energyEndTime").HasValue ? teamData.GetLong("energyEndTime").Value
                        : _team.energyEndTime;
                    _team.energyTimeBoosterEffect = teamData.GetLong("energyTimeBoosterEffect").HasValue
                        ? teamData.GetLong("energyTimeBoosterEffect").Value
                        : _team.energyTimeBoosterEffect;
                    _team.energy = teamData.GetInt("energy").HasValue ? teamData.GetInt("energy").Value : _team.energy;
                    _team.energyTime = teamData.GetInt("energyTime").HasValue ? teamData.GetInt("energyTime").Value : _team.energyTime;
                    EventsManager.Instance.Call(EnergyChanged);
                }

                if (teamData.ContainsKey("rewardsFans"))
                {
                    _rewardFans = teamData.GetInt("rewardsFans").Value;
                }

                if (teamData.ContainsKey("maxFans") || teamData.ContainsKey("fans"))
                {
                    if (teamData.GetInt("maxFans").HasValue)
                    {
                        SetMaxFans(teamData.GetInt("maxFans").Value);
                    }
               
                    AddFans(teamData.GetInt("fans").HasValue ? teamData.GetInt("fans").Value : _team.fans, 5f,false);
                }
                if (teamData.GetInt("rank").HasValue)
                {
                    _team.rank = (RankType) teamData.GetInt("rank").Value;
                }

                if (teamData.GetInt("exp").HasValue)
                {
                    _newExp = teamData.GetInt("exp").Value;
                }
                
                if (teamData.GetInt("nextLevelExp").HasValue)
                {
                    int newLevel = teamData.GetInt("level").Value;
                   // int levelsAdded = newLevel - _team.level;
                    _isLevelUp = true;
                    _isLevelUpComplete = false;
                    _team.level = newLevel;
                    _team.maxLevelExp = teamData.GetInt("nextLevelExp").Value;
                }

                if (teamData.ContainsKey("name"))
                {
                    _team.TeamInfo.Name = teamData.GetString("name");
                }

                if(vipShieldUpdate)
                {
                    EventsManager.Instance.Call(VipOrShieldUpdate);
                }
            }
        }

        public void SetMaxFans(int maxFans)
        {
            if (_team.maxFans != maxFans)
            {
                _team.maxFans = maxFans;
                EventsManager.Instance.Call(MaxFansChanged, 5f);
            }
        }

        public void UpdateFromGSResponce(GSData scriptData)
        {

            if (scriptData.ContainsKey(GS_ScriptData.TeamData))
            {
                UpdateTeamData(scriptData.GetGSData(GS_ScriptData.TeamData));
            }

            if (scriptData.ContainsKey(GS_ScriptData.LastActionsData))
            {
                List<GSData> lastActions = scriptData.GetGSDataList(GS_ScriptData.LastActionsData);
                foreach (var lastAction in lastActions)
                {
                    GSData gdata = lastAction.GetGSData("data");
                    if (gdata != null && gdata.ContainsKey(GS_ScriptData.TeamData))
                    {
                        UpdateTeamData(gdata.GetGSData(GS_ScriptData.TeamData));
                    }
                }
            }
        }

        public void Dispose()
        {
            NewHighScoreMessage.Listener -= OnHighScoreChangedEvent;
            GlobalRankChangedMessage.Listener -= OnGlobalRankChanged;
        }

        public void CheckMessage(GSData msgData, string msgCode)
        {
                if (msgData.ContainsKey(GS_ScriptData.TeamData))
                {
                    UpdateTeamData(msgData.GetGSData(GS_ScriptData.TeamData));
                }
        }

        public bool CanCheckMessage(string msgType,GSData data = null)
        {
            return data != null && data.ContainsKey(GS_ScriptData.TeamData);
        }

    }
}
