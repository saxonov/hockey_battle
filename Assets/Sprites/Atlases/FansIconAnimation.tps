<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>E:/UNITY/HockeyManager/Assets/Sprites/Atlases/FansIconAnimation.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../FansIconSpriteSheet.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0000.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0001.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0002.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0003.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0004.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0005.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0006.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0007.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0008.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0009.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0010.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0011.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0012.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0013.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0014.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0015.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0016.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0017.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0018.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0019.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0020.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0021.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0022.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0023.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0024.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0025.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0026.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0027.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0028.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0029.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0030.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0031.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0032.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0033.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0034.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0035.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0036.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0037.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0038.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0039.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0040.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0041.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0042.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0043.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0044.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0045.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0046.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0047.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0048.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0049.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0050.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0051.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0052.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0053.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0054.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0055.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0056.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0057.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0058.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0059.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0060.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0061.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0062.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0063.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0064.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0065.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0066.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0067.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0068.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0069.png</key>
            <key type="filename">D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ/болел0070.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>D:/Dropbox/HockeyBattle/Иконки анимированные/БОЛЕЛЬЩИКИ НОВЫЕ</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
