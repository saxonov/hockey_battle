﻿using System;
using UnityEngine;
using System.Collections.Generic;
using SCTools.Utils;
#if UNITY_EDITOR_WIN

#endif

namespace SCTools.Localization
{
    public enum LocaleType
    {
        EN = 0,
        RU,
        DE,
        CS,//чешский
        SV,//шведский
        FR,
        FI,//финский
        ZH,
        None

    }

    //internal struct LocaleRecord
    //{
    //    public string enValue;
    //    public string ruValue;

    //}

    public class LocaleManager
    {
        public const char SPACE = '\u0020';
        public const char COLON = '\u003A';
        public const char PERCENT = '\u0025';
        public const char CR = '\u000D';
        public const char LF = '\u000A';
        //public const string NewLine = "\n";
        //public const string Tab = "\t\r";
        //public const char Comma = ',';

        //private const string LocalKeysCSharp = "LocaleKeys.cs";
        //private const string KeyConstTemplate = @"		public const string {0} = {1};";

        public Action LanguageChanged;
        public Action LocalesLoaded;

        private const string LOCALE_ASSET_NAME = "locales";

      

//#if UNITY_EDITOR_WIN
//        [MenuItem("Localization/Create Local Keys")]
//        public static void CreateLanguageKeys()
//        {
//            string filePath = Path.Combine(Application.dataPath, LocalKeysCSharp);
//            string[] keys = LocaleManager.Instance.CurrentCollection.Keys;
//            StreamWriter sw = new StreamWriter(filePath);
//            using (sw)
//            {
//                sw.WriteLine("namespace Tools.Localization");
//                sw.WriteLine("{");
//                sw.WriteLine("");
//                sw.WriteLine("\tpublic class LocaleKeys");
//                sw.WriteLine("\t{");
//                sw.WriteLine("");
//                for (int i = 0; i < keys.Length; i++)
//                {
//                    sw.WriteLine(KeyConstTemplate, keys[i].ToUpper(), "\"" + keys[i] + "\"");
//                }
//                sw.WriteLine("");
//                sw.WriteLine("\t}");
//                sw.WriteLine("");
//                sw.WriteLine("}");
//            }
//            AssetDatabase.Refresh();
//        }
//#endif

        private static LocaleType _defaultLocale = LocaleType.EN;
        private static List<LocaleType> _supportedLocales = null;
        private static LocaleManager _instance = null;

        /// <summary>
        /// set default locale type 
        /// and add to supported locales call AddSupportLocale(type)
        /// </summary>
        /// <param name="type"></param>
        public static void SetDefaultLocale(LocaleType type)
        {
            _defaultLocale = type;
            AddSupportLocale(type);
        }

        public static void AddSupportLocale(LocaleType type)
        {
            _supportedLocales = _supportedLocales ?? new List<LocaleType>();
            if (!_supportedLocales.Contains(type))
            {
                _supportedLocales.Add(type);
            }
        }

        private LocaleManager()
        {
            Initialize();
        }

        private void Initialize()
        {
            _currentLocaleType = _defaultLocale;
            _locale = _currentLocaleType.ToString();
        }


        public static LocaleManager Instance
        {
            get { return _instance ?? (_instance = new LocaleManager()); }
        }

      

        public LocaleType CurrentLocaleType
        {
            get { return _currentLocaleType; }
            set
            {
                if (_currentLocaleType == value)
                {
                    return;
                }

                if (value == LocaleType.None)
                {
                    SetLocaleFromDevice();
                }
                else
                {
                    _currentLocaleType = _supportedLocales.Contains(value) ? value : _defaultLocale;
                    
                }
                
                _locale = _currentLocaleType.ToString();
                if (LanguageChanged != null)
                {
                    LanguageChanged.Invoke();
                }
            }
        }



        

        private Dictionary<string, Dictionary<string, string>> records;

        //private StringBuilder sbuilder = new StringBuilder();
        private LocaleType _currentLocaleType;
        private string _locale;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvText">csv table format</param>
        public void SetLocalesData(string csvText)
        {
            string[][] table = CsvParser2.Parse(csvText);

            int rows = table.Length;
            int cols = table[0].Length;

            if (records == null)
            {
                records = new Dictionary<string, Dictionary<string, string>>();
            }
            records.Clear();

            for (int i = 2; i < rows; i++)
            {
                Dictionary<string, string> record = new Dictionary<string, string>();
                for (int j = 1; j < cols; j++)
                {
                    //skip empty records
                    if (table[i][j] == "")
                    {
                        break;
                    }

                    string localeType = table[0][j];
                    if (!record.ContainsKey(localeType))
                    {
                        record[localeType] = table[i][j];
                    }



                    //Debug.Log(record[localeType]);
                }

                if (table[i][0] == "")
                {
                    Debug.LogWarningFormat("LocaleManager: empty locale_key!, record:{0}", i);
                    continue;
                }

                if (!records.ContainsKey(table[i][0]))
                {
                    records.Add(table[i][0], record);
                }
                else
                {
                    Debug.LogWarning("LocaleManager Duplicate key:" + table[i][0]);
                }

            }

            Debug.Log("LocaleManager: Language loaded");
            if (LanguageChanged != null)
            {
                LanguageChanged.Invoke();
            }
        }

        public void LocalLoadLocale()
        {
            TextAsset csvAsset = Resources.Load<TextAsset>(LOCALE_ASSET_NAME);

            if (csvAsset == null)
            {
                Debug.LogWarningFormat("LocaleManager: {0} csv asset not found!", LOCALE_ASSET_NAME);
                return;
            }

            SetLocalesData(csvAsset.text);

            Resources.UnloadAsset(csvAsset);
        }


    /// <summary>
        /// Use generated LocaleKeys class
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[string key]
        {
            get
            {
                return GetText(key);
            }
        }

        public static string Numeral(int value, string v1, string v234, string v5i)
        {
            if ((value%100) < 20)
            {
                return CheckNumeral(value % 100, v1, v234, v5i);
            }
            return CheckNumeral(value % 10, v1, v234, v5i);
        }

        private static string CheckNumeral(int mod,string v1, string v234, string v5i)
        {
            if (mod == 1)
            {
                return v1;
            }
            if ((mod >= 2) && (mod <= 4))
            {
                return v234;
            }
               
            return v5i;
        }

        public bool HasKey(string locale_key)
        {
            if (string.IsNullOrEmpty(locale_key)) return false;
            return records.ContainsKey(locale_key);
        }

        /// <summary>
        ///  Use generated LocaleKeys class
        /// </summary>
        /// <param name="key"></param>
        /// <param name="formatArgs">format parameters like {0,1,2} by https://msdn.microsoft.com/ru-ru/library/system.string.format(v=vs.110).aspx#Starting </param>
        /// <returns></returns>
        public string GetText(string key, params object[] formatArgs)
        {
            string text = GetText(key);
            try
            {
                string res = string.Format(text, formatArgs);
                return res;
            }
            catch (Exception e)
            {
               Debug.LogWarningFormat("LocaleManager wrong text format: ",e.Message);
            }
            return text;
        }

        /// <summary>
        /// Get joined string 
        /// </summary>
        /// <param name="keyToSplit"> keyToSplit by comma separator</param>
        /// <returns></returns>
        public string GetSplitText(string keyToSplit)
        {
            string[] keys = keyToSplit.Split(',');
            string result = "";
            for (int i = 0; i < keys.Length; i++)
            {
                result += this[keys[i]];
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetText(string key)
        {
            if (string.IsNullOrEmpty(key) || records == null) return "UNDEFINED_KEY";
            Dictionary<string, string> record;
            if (records.TryGetValue(key, out record))
            {
                if (record.ContainsKey(_locale))
                {
                    return record[_locale];
                }
            }
            else
            {
               Debug.LogWarningFormat("LocaleManager: uknown key {0}",key);
            }
            return key;
        }

        private void SetLocaleFromDevice()
        {
            switch (Application.systemLanguage)
            {
                    case SystemLanguage.English:
                      _currentLocaleType = LocaleType.EN;
                    break;
                    case SystemLanguage.Russian:
                    _currentLocaleType = LocaleType.RU;
                    break;
                    case SystemLanguage.Czech:
                    _currentLocaleType = LocaleType.CS;
                   break;
                    case SystemLanguage.ChineseSimplified:
                    _currentLocaleType = LocaleType.ZH;
                        break;
                    case SystemLanguage.Swedish:
                        _currentLocaleType = LocaleType.SV;
                        break;
                    case SystemLanguage.German:
                        _currentLocaleType = LocaleType.DE;
                        break;
                    case SystemLanguage.French:
                        _currentLocaleType = LocaleType.FR;
                        break;
                    case SystemLanguage.Finnish:
                        _currentLocaleType = LocaleType.FI;
                        break;
                    default:
                    _currentLocaleType = _defaultLocale;
                    break;
            }

            if (!_supportedLocales.Contains(_currentLocaleType))
            {
                _currentLocaleType = _defaultLocale;
            }

            _locale = _currentLocaleType.ToString();
        }
    }
}
