﻿using DG.Tweening;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models;
using HockeyManager.Game.Models.Rewards;
using HockeyManager.Game.UI.Components;
using HockeyManager.TutorialSystem;
using HockeyManager.UI.Popups;
using HockeyManager.Views.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;


namespace HockeyManager.Game.UI
{
    public class MainScreen : MonoBehaviour,IPageChangeHandler
    {
        public static bool fastCountersUpdates = false;

        private IGameSettings settings;

        [Header("Header Panel")]
        [Space(3f)]
     
        [SerializeField]private LogoImage teamLogo;
        [SerializeField] private Button fillEnergyButton;
        [SerializeField] private Slider energyProgressBar;
        [SerializeField] private TextIconControl energyPanel;
        [SerializeField] private Text levelLabel;
        [SerializeField] private Text rankLabel;
        [SerializeField] private TextIconControl ratePanel;
        [SerializeField] private TextIconControl supportersPanel;
        [SerializeField] private TextIconControl coinsPanel;
        [SerializeField] private TextIconControl goldPanel;
        [SerializeField] private Slider expProgressBar;
        [SerializeField] private Text expMaxLevelLabel;

       // private GSAdminController _adminController;
        private TeamController _team;
        private RewardsAchievementsController _rewards;
        private TeamModel model;
        private GSAdminController _adminController;
        // Use this for initialization

  
        private bool _needUpdateEnergy = false;

        void Awake()
        {

            settings = GameSettings.Instance;
            _adminController = GameRoot.Instance.GetController<GSAdminController>();
            _team = GameRoot.Instance.GetController<TeamController>();
            _rewards = GameRoot.Instance.GetController<RewardsAchievementsController>();

            model = _team.TeamData;
        }

        void OnDisable()
        {

            EventsManager.Instance.Remove(GSAdminController.NeedUpdateGame, ShowUpdateBuildAlert);

            EventsManager.Instance.Remove(RewardsAchievementsController.DailyBonusAdded, CheckDailyBonus);
            EventsManager.Instance.Remove(TeamController.VipOrShieldUpdate, UpdateVipShield);

            EventsManager.Instance.Remove<float>(TeamController.CoinsChanged, UpdateCoins);
            EventsManager.Instance.Remove<float>(TeamController.GoldChanged, UpdateGold);
            EventsManager.Instance.Remove(TeamController.EnergyChanged, UpdateEnergyBar);
            EventsManager.Instance.Remove<float>(TeamController.LevelChanged, UpdateLevel);
            EventsManager.Instance.Remove<float>(TeamController.FansChanged, UpdateFans);
            EventsManager.Instance.Remove<float>(TeamController.MaxFansChanged, UpdateFans);

            EventsManager.Instance.Remove(GameTimer.TikTakMinuteEvent,OnMinuteEventHandler);
            EventsManager.Instance.Remove(GSAdminController.PenaltyGoldBalanceEvent, OnAdminChangeMyBalanceHandler);


            LocaleManager.Instance.LanguageChanged -= OnLocaleChangedHandler;
            fillEnergyButton.onClick.RemoveListener(OnShowBuyEnergyPopup);

        }

        private void ShowUpdateBuildAlert()
        {
            if (_adminController.NeedUpdateBuild && GameData.IsTutorialPassed)
            {
                PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                    .HideButtons(false, true)
                    .SetTitle("game.common.newbuild")
                    .SetButtonLabels(LocaleManager.Instance.GetText("game.common.update"))
                    .SetCallbacks(OnUpdateBuildAlertCloseHandler);
            }
        }

        private void OnUpdateBuildAlertCloseHandler()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.stdforland.hockeybattle");
            }
            else
            {
                Application.OpenURL("https://itunes.apple.com/ru/app/hockeybattle-%D0%BF%D0%B5%D1%80%D0%B2%D0%B0%D1%8F-%D1%85%D0%BE%D0%BA%D0%BA%D0%B5%D0%B9%D0%BD%D0%B0%D1%8F-%D1%81%D1%82%D1%80%D0%B0%D1%82%D0%B5%D0%B3%D0%B8%D1%8F/id1154406242?l=ru&ls=1&mt=8");
            }
            Application.Quit();
        }

        private void OnMinuteEventHandler()
        {
            if (_needUpdateEnergy)
            {
                _team.UpdateEnergy();
            }
        }


        void OnDestroy()
        {
            _team = null;
            _rewards = null;
             model = null;
        }

        void OnEnable()
        {

            EventsManager.Instance.Add(GSAdminController.PenaltyGoldBalanceEvent,OnAdminChangeMyBalanceHandler);

            EventsManager.Instance.Add(GSAdminController.NeedUpdateGame,ShowUpdateBuildAlert);
            EventsManager.Instance.Add(GameTimer.TikTakMinuteEvent, OnMinuteEventHandler);
            EventsManager.Instance.Add(RewardsAchievementsController.DailyBonusAdded, CheckDailyBonus);

            LocaleManager.Instance.LanguageChanged += OnLocaleChangedHandler;

            EventsManager.Instance.Add(TeamController.VipOrShieldUpdate, UpdateVipShield);
            EventsManager.Instance.Add<float>(TeamController.CoinsChanged, UpdateCoins);
            EventsManager.Instance.Add<float>(TeamController.GoldChanged, UpdateGold);
            EventsManager.Instance.Add(TeamController.EnergyChanged, UpdateEnergyBar);
            EventsManager.Instance.Add<float>(TeamController.LevelChanged, UpdateLevel);
            EventsManager.Instance.Add<float>(TeamController.FansChanged, UpdateFans);
            EventsManager.Instance.Add<float>(TeamController.MaxFansChanged, UpdateFans);
          

            fillEnergyButton.onClick.AddListener(OnShowBuyEnergyPopup);
           
        }

    

        void Start()
        {
            expMaxLevelLabel.enabled = false;

            PageNavigatorManager.Instance.AddPageChangeHandler(this);

            SoundManager.Instance.PlaySFXOnGameObject("enter",gameObject);
            SoundManager.Instance.PlayMusic("maintheme", 0.5f, true);
             
                fastCountersUpdates = true;
                UpdateLogo();
                UpdateVipShield();
                UpdateLevel();
                UpdateCoins();
                UpdateGold();
                UpdateFans();
                UpdateEnergyBar();
                CheckFansPenalty();
                fastCountersUpdates = false;
                CheckDailyBonus();
                ShowUpdateBuildAlert();

                NotifyToAccountsConnected();
        }

        private void NotifyToAccountsConnected()
        {
            if (GameData.IsTutorialPassed)
            {
                SocialAccountType loginType = GameRoot.Instance.Authtorization.LoginType;

                if (loginType == SocialAccountType.None)
                {
#if UNITY_ANDROID || UNITY_EDITOR
                    PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                        .SetTitle("game.common.android.bindaccount")
                        .SetButtonLabels("game.common.bindbutton", "game.common.closebutton")
                        .SetCallbacks(() =>
                        {
                            PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Accounts);
                        });

#endif
                }
            }
        }


        private void OnAdminChangeMyBalanceHandler()
        {
            PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                .SetTitle("game.common.admin.goldbalancecancel")
                .HideButtons(true, false)
                .SetButtonLabels(null, "game.common.closebutton");
        }



        private void CheckDailyBonus()
        {
           
            IPopup popup = null;
            RewardModel dailyBonus = _rewards.GetReward(RewardType.DailyBonusReward);
            if (dailyBonus != null && !dailyBonus.IsGained)
            {
               // EventsManager.Instance.Remove(RewardsAchievementsController.DailyBonusAdded, CheckDailyBonus);
                popup = PopupsManager.Instance.AddPopup(PopupType.DailyBonusPopup)
                    .SetData(dailyBonus)
                    .SetCallbacks(() =>
                    {
                        _rewards.GainReward(dailyBonus, null);
                        VisualEffectsManager.Instance.FlyRewards(popup.gameObject.transform.position,
                            dailyBonus.RewardData);

                    });
            }
        }
        
        private void CheckFansPenalty()
        {
            GSData penalty = _team.GetFansPenalty();
            if (penalty != null)
            {
                int days = penalty.GetInt("day").Value;
                int fansLoss = penalty.GetInt("penalty").Value;
                PopupsManager.Instance.AddPopup(PopupType.FansPenaltyPopup)
                    .SetText(LocaleManager.Instance.GetText("game.main.popup.textfield.fanslosttext"))
                    .SetTitle(LocaleManager.Instance.GetText("game.main.popup.title.fanslostpenalty", days))
                    .SetTextIconControls("-" + fansLoss);
            }
        }
       
        private void OnLocaleChangedHandler()
        {
            UpdateLevel(0.1f);
        }

        private void UpdateVipShield()
        {
            VipShieldsIndicator indicator = teamLogo.GetComponent<VipShieldsIndicator>();
            if (indicator != null)
            {
                indicator.EnableVipIndicator(_team.HasVip);
                indicator.EnableShieldIndicator(_team.HasProtectShield);
            }
        }


        private void UpdateLogo()
        {
            teamLogo.sprite = settings.GetLogo256(model.TeamInfo.Logo);

            teamLogo.material = new Material(settings.HsvShader);

            HSVShaderTuner hsv = teamLogo.GetComponent<HSVShaderTuner>();
            hsv.ChangeHUE(model.TeamInfo.LogoColor);
            hsv.ChangeSaturation(model.TeamInfo.LogoSaturation);
        }

        private void UpdateLevel(float duration = 0f)
        {
            levelLabel.text = LocaleManager.Instance.GetText("game.common.level", model.level);

            rankLabel.text = LocaleManager.Instance.GetText(settings.GetRankLocaleKey(_team.TeamData.rank));

            if (model.level >= GameData.GSConstData.MaxTeamLevel)
            {
                expProgressBar.value = 0;
                expMaxLevelLabel.enabled = true;
                ratePanel.SetText(model.exp);
            }
            else
            {
                expProgressBar.minValue = 0;
                expProgressBar.maxValue = model.maxLevelExp;
                if (fastCountersUpdates)
                {
                    expProgressBar.value = model.exp;
                    ratePanel.SetText(model.exp + " / " + model.maxLevelExp);
                }
                else
                {
                    expProgressBar.DOValue(model.exp, 1f);
                    ratePanel.AnimateValue(duration, '/', model.exp, model.maxLevelExp);
                }

               

                if (_team.IsLevelUp)
                {
                    //PopupsManager.Instance.AddPopup(PopupType.TeamLevelUpPopup)
                    //  .SetText(model.level.ToString()); 
                    SoundManager.Instance.PlaySFXOnGameObject("newlevel",levelLabel.gameObject);
                    levelLabel.GetComponent<Outline>().DOFade(1f, 0.25f).SetLoops(2).OnComplete(() =>
                    {
                        levelLabel.GetComponent<Outline>().DOFade(0f, 0.25f);
                    });
                    _team.IsLevelUpComplete = true;
                }
            }

          
        }

        private void UpdateFans(float duration = 0f)
        {
            if (model.fans > model.maxFans)
            {
                model.fans = model.maxFans;
            }
            if (fastCountersUpdates)
            {
                //supportersPanel.AnimateIcon(2);
                supportersPanel.SetText(model.fans + "/" + model.maxFans);
            }
            else
            {
                 supportersPanel.AnimateValue(duration, '/', model.fans, model.maxFans);
            }
            
        }

        private void UpdateGold(float duration = 0f)
        {
            if (fastCountersUpdates)
            {
              //  goldPanel.AnimateIcon(2);
                goldPanel.SetText(model.gold.ToString());
            }
            else
            {
                goldPanel.AnimateValue(model.gold, duration);
            }
            

        }

        private void UpdateCoins(float duration = 0f)
        {
            
            if (fastCountersUpdates)
            {
               // coinsPanel.AnimateIcon(2);
                coinsPanel.SetText(model.coins.ToString());
            }
            else
            {
                coinsPanel.AnimateValue(model.coins,duration);
            }
        }

        private void OnShowBuyEnergyPopup()
        {
            PopupsManager.Instance.AddPopup(PopupType.BuyForPricePopup)
                .SetTitle("game.main.popup.buyenergy")
                .SetButtonLabels("game.common.restorebutton")
                .SetTextIconControls(VirtualGoods.FULL_ENERGY_GOLD_PRICE)
                .SetCallbacks(OnEnergyBuyApply);
        }
        
        private void OnEnergyBuyApply()
        {
            if (_team.CanBuyForGold(VirtualGoods.FULL_ENERGY_GOLD_PRICE))
            {
                SoundManager.Instance.PlaySFX("buyenergy");
                fillEnergyButton.interactable = false;
                var request = new BuyVirtualGoodsRequest();
                request.SetQuantity(1);
                request.SetShortCode(VirtualGoods.FULL_ENERGY_GOOD);
                request.SetCurrencyType(GameData.CurrencyGold);
                GameRoot.Instance.NetManager.RunBuyVirtualEventRequest(request, (responce) =>
                {
                    if (!responce.HasErrors)
                    {
                        fillEnergyButton.interactable = true;
                    }
                });
            }
            else
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
            }
        }

        private void UpdateEnergyBar()
        {
           // Debug.Log("Energy updated :" + model.energy);
            energyProgressBar.maxValue = model.maxEnergy;
            energyProgressBar.minValue = 0;
            if (fastCountersUpdates)
            {
                energyPanel.SetText(model.energy + " / " + model.maxEnergy);
                energyProgressBar.value = model.energy;
            }
            else
            {
                energyProgressBar.DOValue(model.energy, 1f);
                energyPanel.AnimateValue(1f, '/', model.energy, model.maxEnergy);
            }

            _needUpdateEnergy = model.energy < model.maxEnergy;
        }

        public void OnPageChanged(NavigationPageTypes selected, NavigationPageTypes prev)
        {
            if (selected != NavigationPageTypes.Home)
            {
                SoundManager.Instance.FadeMusicVolume("maintheme", 0.2f);
                SoundManager.Instance.PlayMusicOnGameobject("cloakroomtheme", gameObject);
            }
            else
            {
                SoundManager.Instance.FadeMusicVolume("maintheme");
                SoundManager.Instance.StopMusic("cloakroomtheme");
            }
        }
    }


}
