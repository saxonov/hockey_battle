﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Map;
using SCTools.EventsSystem;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Map
{
    public class MapBuildingLot :MonoBehaviour,IPointerClickHandler
    {
        [SerializeField] private Image construtionSet = null;
        [SerializeField] private Image lockIcon = null;

#pragma warning disable CS0649 // Полю "MapBuildingLot._lotData" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private BuildingLotModel _lotData;
#pragma warning restore CS0649 // Полю "MapBuildingLot._lotData" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        //lock for touch select, for special cases(double tap)
        private bool _clickLocked;

       // private MapBuildingsType _costructedType = MapBuildingsType.None;

        private MapBuilding _currentBuilding = null;

        private RectTransform rtransform;
       
        public RectTransform Rtransform
        {
            get { return rtransform; }
        }

        public bool ClickLocked
        {
            set { _clickLocked = value; }
        }

      

        public BuildingLotModel LotData
        {
            get { return _lotData; }
        }

        public MapBuilding CurrentBuilding
        {
            get { return _currentBuilding; }
        }

        public void ChangeArenaLockStates()
        {
            if (_lotData.ArenaUnlockLevel > 0)
            {
                LockLot();
            }
            else
            {
                RemoveLocker();
            }
        }

        void Start()
        {
            rtransform = GetComponent<RectTransform>();
          //  LockLot();
        }

        private void LockLot()
        {
            //if (lockIcon != null)
            //{
            //    Destroy(lockIcon.gameObject);
            //    lockIcon = null;
            //    return;
            //}
            if (lockIcon != null && construtionSet != null)
            {
                lockIcon.gameObject.SetActive(true);
                construtionSet.color = UISettings.BuldingUpgradeColor;
            }
        }

        private void RemoveLocker()
        {
            if (lockIcon != null)
            {
                construtionSet.color = Color.white;
                Destroy(lockIcon.gameObject);
                lockIcon = null;
            }
        }


        public void Build(IBuildingModel model)
        {
            if (_currentBuilding == null)
            {
               // _costructedType = model.Type;
               // Debug.Log(model.Id.ToString());
                for (int i = 0; i < _lotData.Sites.Length; i++)
                {
                    if (_lotData.Sites[i].type.Equals(model.Id))
                    {
                        _lotData.Building = model;

                        GameObject prefab = GetBuildingPrefab(_lotData.Sites[i]);
                        GameObject child = Instantiate(prefab);
                        child.transform.SetParent(transform, false);
                        _currentBuilding = child.GetComponent<MapBuilding>();
                        _currentBuilding.SetModel(model);

                        if (construtionSet != null)
                        {
                            Destroy(construtionSet.gameObject);
                            construtionSet = null;
                        }

                    }
                }
            }
            
        }

        private GameObject GetBuildingPrefab(MapConstructionLot data)
        {

            string path = data.isFliped ? UISettings.MapBuildingsResourcesPath + data.type + UISettings.MapBuidlingFlipedPrefix : 
                                          UISettings.MapBuildingsResourcesPath + data.type;
            
            return Resources.Load<GameObject>(path);
        }
       
        public void OnPointerClick(PointerEventData eventData)
        {
            if (!eventData.dragging && !_clickLocked)
            {
                _clickLocked = true;
                SoundManager.Instance.PlaySFX("click");
                MapManager.Instance.SelectMapBuilding(_lotData.Id);
            }
        }
    }
}
