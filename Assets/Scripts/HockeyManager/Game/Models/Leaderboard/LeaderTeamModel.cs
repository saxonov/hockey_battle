﻿using GameSparks.Core;
using HockeyManager.Game.Models.Base;

namespace HockeyManager.Game.Models.Leaderboard
{
    public class LeaderTeamModel:AttackTeamModel
    {
        private int _index = 0;
        private int _position;
        public int Index
        {
            get { return _index; }
        }


        public int Position
        {
            get { return _position; }
        }

        public LeaderTeamModel(GSData data,int index)
        {
            _index = index;
            Parse(data);
        }

        public sealed override void Parse(GSData data)
        {

            base.Parse(data);

            if (data.ContainsKey("rank"))
            {
                _position = data.GetInt("rank").Value;
            }
        }
    }
}
