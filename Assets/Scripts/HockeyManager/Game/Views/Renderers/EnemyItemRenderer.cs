﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Battles;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class EnemyItemRenderer:MonoBehaviour
    {
        public const string EnemyTeamSelectedToAttack = "EnemyItemRenderer.enemyTeamAttackEvent";

        [SerializeField] private LogoImage logo;
        [SerializeField] private Text infoText;
        [SerializeField] private Button attackButton;
        [SerializeField] private TextIconControl energyPanel;
        [SerializeField] private TextIconControl timerPanel;
        [SerializeField] private Image selection;

        private MatchPlayerModel _model;

        void Start()
        {
            attackButton.onClick.AddListener(OnAttackButtonClickHandler);
        }

        void OnDestroy()
        {
            attackButton.onClick.RemoveListener(OnAttackButtonClickHandler);
        }

        private void OnAttackButtonClickHandler()
        {
            EventsManager.Instance.Call(EnemyTeamSelectedToAttack, _model.TeamInfo.UserId);
        }

        public void ShowSelection(bool value)
        {
            selection.enabled = value;
        }

        public void SetData(MatchPlayerModel model,int energyCost,int time)
        {
            _model = model;

            logo.material = new Material(GameSettings.Instance.HsvShader);
            logo.sprite = GameSettings.Instance.GetLogo128(model.TeamInfo.Logo);

            var tuner = logo.GetComponent<HSVShaderTuner>();
            tuner.ChangeHUE(model.TeamInfo.LogoColor);
            tuner.ChangeSaturation(model.TeamInfo.LogoSaturation);

            string online = model.IsOnline ? LocaleManager.Instance.GetText("game.common.online").GetUITextColorFormated(UISettings.GreenTextColor).GetUITextSizeFormated(35) :
                                             LocaleManager.Instance.GetText("game.common.offline").GetUITextColorFormated(UISettings.BlockTextColor).GetUITextSizeFormated(35);
            string enemyLevel;
            string awardPercent = "";
            if (model.LevelAwardPercent > 0)
            {
                awardPercent = "+" + model.LevelAwardPercent + "%";
                enemyLevel = LocaleManager.Instance.GetText("game.main.matchpage.enemystrong") + " / " + LocaleManager.Instance.GetText("game.common.reward") + " " + awardPercent;

            }else if (model.LevelAwardPercent < 0)
            {
                awardPercent = model.LevelAwardPercent + "%";
                enemyLevel = LocaleManager.Instance.GetText("game.main.matchpage.enemyweak") + "/" + LocaleManager.Instance.GetText("game.common.reward") + " " + awardPercent;

            }
            else
            {
                enemyLevel = LocaleManager.Instance.GetText("game.main.matchpage.enemyequal");

            }
            
            string level = LocaleManager.Instance.GetText("game.common.shortlevel2", model.Level);
            infoText.text = model.TeamInfo.Name.GetUITextSizeFormated(50) + " " 
                            + level + 
                            "\n" + online + 
                            "\n" + enemyLevel.GetUITextSizeFormated(30);
            energyPanel.SetText(energyCost);
            timerPanel.SetText(MathUtils.SecondsToTimeString(time));
        }

    }
}
