﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine.Events;

namespace HockeyManager.Game.Models.Base
{
    public interface IBaseGameEntityModel:IGSParse
    {
        int UpgradeTimeSeconds { get; }

        long EndUpgradeTimeSeconds { get; }

        EntityType EntityType { get; }

        bool StartUpgradeTimer();
        bool UpgradeTimerComplete { get; }
        bool IsUpgrading { get; }

        UnityEvent OnModelChangedEvent { get; }
        TimerLeftSecondsEvent OnTimerLeftSecondsEvent { get; }
    }
}
