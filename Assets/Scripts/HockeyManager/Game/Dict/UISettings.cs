﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GameSparks.Core;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Dict
{

    [Serializable]
    public struct IconData
    {
        public IconsEnum type;
        public Sprite icon;
    }

    [Serializable]
    public struct RewardIconData
    {
        public RewardType type;
        public Sprite icon;
    }

    [Serializable]
    public struct InnerPageButtonData
    {
        public string localeLabel;
        public bool isSelected;
        public NavigationPageTypes gotoPage;
    }

    public struct ViewStateData
    {
        //great than equal
        private const string GTE = "gte";
        //great than
        private const string GT = "gt";

        public int[] ranges;
        public string op;
        public int GetViewState(int currentLevel)
        {
            int state = 0;
            for (int i = 0; i < ranges.Length; i++)
                {
                    if (op == GT && currentLevel > ranges[i] || op == GTE && currentLevel >= ranges[i])
                    {
                        state = i;
                    }
                }
            return state;
        }
    }

    public enum IconsEnum
    {
        Energy,
        Gold,
        Money,
        Fans,
        Exp,
        Attack,
        Deffence,
        Goals
    }


    public enum VipShiledType
    {
        Vip5Hours,
        Vip1Day,
        Vip1Week,
        Shield5Hours,
        Shield1Day,
        Shield1Week
    }

    public enum NavigationPageTypes
    {
        Home,//карта,чат,нотификации,попапы
       
        EPS,// раздевалка
        Personal,//персонал
        Skills,//навыки
        Competition,// конкурсы
        HallOfFame,// зал славы
        News,//новости
             // Navigation,//навигация вся карта навигации
        Settings,//настройки  
        Tournament,//кубки турниры
        TournamentHistory,//история по турнирам
        СreateTournament,//создать турнир
        Matches,// матчи
        MatchesHistory,//история по матчам
        Training,// тренировки
        Rewards,// награды
        Missions,// задания
        Leaderboard,//рейтинг
        Shop,//магазин
        TeamInfo, // о команде
        MapLotInfo,
        MapBuildingInfo,
        CloakRoom,//раздевалка
        EquipmentUpgradeInfo,
        Buildings,
        Chat,
        Accounts,
        None
    }

    public enum PopupType
    {
        FinishUpgradePopup,
        RewardAdsPopup,
        BuyForPricePopup,
        ResetSkillPointsPopup,
        ServerErrorPopup,
        NotEnoughFansCoinsPopup,
        NotEnoughMoneyPopup,
        NeedLevelPopup,
        TakeFansPopup,
        ChangeTacticBonusPopup,
        LockedToChangePopup,
        AlertPopup,
        DailyBonusPopup,
        TeamLevelUpPopup,
        FansPenaltyPopup,
        MatchCanceledPopup,
        MatchCancelPopup,
        MatchWinLoosePopup,
        AttackTeamPopup,
        MatchNotificationPopup,
        BaseNotificationPopup,
        TournamentNotificationPopup,
        BlockerPopup,
        CompetitionWinnerPopup,
        CompetitionRewardsPopup,
        LanguagePopup,
        ChangeTeamNamePopup,
        None
    }

    [Serializable]
    public struct MapConstructionLot
    {
      
       public bool isFliped;
       public MapBuildingsType type;
    }


    [Serializable]
    public class BaseGameObjectSettingsInfo
    {
        [SerializeField] protected string _nameLocalKey;
     //   public string nameLocaleKey;
        [SerializeField] protected string descriptionLocaleKey;
        [SerializeField] protected Sprite[] _previews;

        public string GetName()
        {
            return LocaleManager.Instance.GetText(_nameLocalKey);
        }

        public string GetDescription()
        {
            return LocaleManager.Instance.GetText(descriptionLocaleKey);
        }

        public Sprite GetPreview()
        {
            return _previews[0];
        }

        public Sprite GetPreview(int level, int maxLevel)
        {
            var index = UISettings.GetBuildingEquipmentState(level, maxLevel);
            return _previews[index];
        }
    }
    [Serializable]
    public class EquipmentsSettingsInfo:BaseGameObjectSettingsInfo
    {
        [SerializeField] private EquipmentType _type;

        public EquipmentType Type
        {
            get { return _type; }
        }
    }

    [Serializable]
    public class EmployeeSettingsInfo : BaseGameObjectSettingsInfo
    {
        [SerializeField]
        private EmployeeType _type;

        public EmployeeType Type
        {
            get { return _type; }
        }
    }

    [Serializable]
    public class HallItemInfo:BaseGameObjectSettingsInfo
    {
        [SerializeField] private int _id;

        public int Id
        {
            get { return _id; }
        }
    }

    [Serializable]
    public class MapBuildingSettingsInfo:BaseGameObjectSettingsInfo
    {
     
        [SerializeField] private MapBuildingsType _type;

        public MapBuildingsType Type
        {
            get { return _type; }
        }

        //public string DescriptionLocaleKey
        //{
        //    get { return descriptionLocaleKey; }
        //}
    }

    public static class UISettings
    {
        public static readonly char[] CheckMask = "!-()[]:.|".ToCharArray();
        private static Dictionary<int, ViewStateData> _equipBuildingsViewStateParams;

        private  static Regex rp = new Regex(@"\p{Cs}*\p{So}*", RegexOptions.CultureInvariant | RegexOptions.Multiline);
        //Resources folders
        //public const string LogosResourcesPath = "Logos/";
       // public const string Logos256ResourcesPath = "Logos/Logos_256/";
       // public const string Logos128ResourcesPath = "Logos/Logos_128/";
       // public const string Logos64ResourcesPath = "Logos/Logos_64/";

        //Tags
        public const string FlyCoinsIconTag = "FlyCoinsIcon";
        public const string FlyFansIconTag = "FlyFansIcon";
        public const string SceneRefsTag = "SceneRefferences";

        //Scenes
        public const string MainScene = "Main";
        public const string PreloadScene = "PreloadScene";
        public const string StartOnceScene = "StartOnceScene";

        public const int MaxUpgradeProcessCount = 4;

      
        public const int MaxChatHistoryLimit = 25;

        public const NavigationPageTypes StartPage = NavigationPageTypes.Home;

        public const int MaxBuildingEquipmentViewStates = 3;
        
        //Resources paths
        public const string MapBuidlingFlipedPrefix = "_fliped";
        public const string PopupResourcesPath = "Popups/";
        public const string MapBuildingsResourcesPath = "Map/";


        public static readonly Color32 BuldingUpgradeColor = new Color32(128,128,128, byte.MaxValue);
        public static readonly Color32 LeaderboardSelectionItemColor = new Color32(29,36,18, byte.MaxValue);
        public static readonly Color32 LeaderboardNextItemColor = new Color32(27,26,22, byte.MaxValue);
        public static readonly Color32 GreenTextColor = new Color32(106, 143,43, byte.MaxValue);
        public static readonly Color32 BlockTextColor = new Color32(146, 109,60, byte.MaxValue);
        public static readonly Color32 BlockPreviewColor = new Color32(128, 128,128,128);
        public static readonly Color32 CompetitionMyPosColor = new Color32(33, 35,22, byte.MaxValue);

        public static readonly Color32 SimpleChatNameColor = new Color32(126, 125, 123, byte.MaxValue);
        public static readonly Color32 ModerChatNameColor = new Color32(121, 84, 57, byte.MaxValue);

        public const byte MaxLeaderboardRenderersCount = 200;
        public const byte MaxLeaderboardRenderersPageCount = 25;

        //private static string _emojiUnicodes;

        //public static void SetSupportedEmojiList(string emojiUnicodes)
        //{
        //    if (_emojiUnicodes == null)
        //    {
        //        _emojiUnicodes = '[' + emojiUnicodes + ']';
        //    }
        //}

        public static string RemoveEmoji(string input)
        {
            return rp.Replace(input, "");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewData"></param>
        public static void SetViewData(List<GSData> viewStates)
        {
            if (_equipBuildingsViewStateParams == null)
            {
               _equipBuildingsViewStateParams = new Dictionary<int, ViewStateData>();

               // var viewStates = viewData.GetGSDataList("eqpBuildingsViewStates");
                //error for old server side
                if (viewStates != null)
                {
                    foreach (var viewState in viewStates)
                    {
                        _equipBuildingsViewStateParams.Add(viewState.GetInt("maxLevel").Value, JsonUtility.FromJson<ViewStateData>(viewState.JSON));
                    }
                }
            }

        }

        //public const string MapBuidlingPreviewPrefix = "_preview";

        public static int GetBuildingEquipmentState(int currentLevel, int maxLevel)
        {
            ViewStateData stateData;
            if (_equipBuildingsViewStateParams.TryGetValue(maxLevel, out stateData))
            {
                return stateData.GetViewState(currentLevel);
            }
            return 0;
        }
    }
}
