﻿using DG.Tweening;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Map;
using HockeyManager.Game.UI.Components;
using HockeyManager.Models.Map;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class BuildingItemRenderer:MonoBehaviour,IPointerClickHandler
    {

#pragma warning disable CS0649 // Полю "BuildingItemRenderer.preview" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image preview;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.preview" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.lockIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image lockIcon;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.lockIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.locker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image locker;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.locker" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.questionSignIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Sprite questionSignIcon;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.questionSignIcon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.selection" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image selection;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.selection" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.title" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text title;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.title" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.requirements" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text requirements;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.requirements" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.timerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl timerPanel;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.timerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.upgradeProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Slider upgradeProgress;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.upgradeProgress" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "BuildingItemRenderer.levelLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text levelLabel;
#pragma warning restore CS0649 // Полю "BuildingItemRenderer.levelLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private BuildingLotModel _lotModel;

        private bool _canGoToMap = false;
        private int _arenaLevel;

     
        void Awake()
        {
            levelLabel.gameObject.SetActive(false);
            lockIcon.gameObject.SetActive(false);
            upgradeProgress.gameObject.SetActive(false);
            timerPanel.gameObject.SetActive(false);
            title.text = "";
            requirements.text = "";
           // preview.sprite = null;
            locker.enabled = false;
        }

        void OnDestroy()
        {
            if (_lotModel != null && _lotModel.IsBuildingExist())
            {
                _lotModel.Building.OnModelChangedEvent.RemoveListener(OnBuildingModelChanged);
                _lotModel.Building.OnTimerLeftSecondsEvent.RemoveListener(OnTikTakHandler);
            }
        }
        
        public void SetData(BuildingLotModel model)
        {
            _lotModel = model;
            _arenaLevel = MapManager.Instance.GetArenaLevel();
            if (!_lotModel.IsBuildingExist())
            {

                MapBuildingSettingsInfo info;
                
                _canGoToMap = false;
                requirements.gameObject.SetActive(true);
                preview.color = UISettings.BlockPreviewColor;

                if (_lotModel.Sites.Length > 1)
                {
                    SetMultipleTypes();
                }
                else
                {
                    info = GameSettings.Instance.GetBuildingData(_lotModel.Sites[0].type);
                    title.text = info.GetName();
                    preview.sprite = info.GetPreview();
                }

                if (_lotModel.ArenaUnlockLevel > _arenaLevel && _arenaLevel >= 0)
                {
                    lockIcon.gameObject.SetActive(true);
                    locker.enabled = true;

                    string arenaLabel = LocaleManager.Instance.GetText("game.common.arena") + LocaleManager.SPACE +
                                        LocaleManager.Instance.GetText("game.common.level2", _lotModel.ArenaUnlockLevel);
                    requirements.color = UISettings.BlockTextColor;
                    requirements.text = LocaleManager.Instance.GetText("game.main.popup.title.needlevelitem", arenaLabel);
                }
                else
                {
                    if (_lotModel.Sites.Length > 1)
                    {
                        preview.sprite = questionSignIcon;
                    }
                    locker.enabled = false;
                    _canGoToMap = true;
                    requirements.color = UISettings.GreenTextColor;
                    requirements.text = LocaleManager.Instance.GetText("game.main.building.upgrades.requirementscomplete");
                }
            }
            else
            {

                _lotModel.Building.OnModelChangedEvent.AddListener(OnBuildingModelChanged);
                OnBuildingModelChanged();
            }

           
        }

        private void SetMultipleTypes()
        {
            string titleLabel = "";
            for (int i = 0; i < _lotModel.Sites.Length; i++)
            {
                MapBuildingSettingsInfo info = GameSettings.Instance.GetBuildingData(_lotModel.Sites[i].type);
                titleLabel += info.GetName() + "/";
            }

            titleLabel = titleLabel.TrimEnd('/');
            title.text = titleLabel;
        }

        private void OnBuildingModelChanged()
        {
            _canGoToMap = false;

            IBuildingModel model = _lotModel.Building;
          
            MapBuildingSettingsInfo info = GameSettings.Instance.GetBuildingData(model.Id);

            title.text = info.GetName();

            levelLabel.gameObject.SetActive(model.Level > 0);
            levelLabel.text = LocaleManager.Instance.GetText("game.common.level2", model.Level);


            preview.sprite = info.GetPreview(model.Level, model.MaxLevel);


            BuildingUpgradeInfo upgrade = MapManager.Instance.GetUpgrade(_lotModel);
            if (upgrade != null && _arenaLevel < upgrade.UnlockLevel)
            {
                preview.color = UISettings.BlockPreviewColor;
                string arenaName = LocaleManager.Instance.GetText("game.common.arena");
                string needArenaLevel = arenaName + LocaleManager.SPACE +
                                        LocaleManager.Instance.GetText("game.common.shortlevel2", upgrade.UnlockLevel);
                string reqText = LocaleManager.Instance.GetText("game.main.building.upgrades.requirements", needArenaLevel);
                requirements.text = reqText;
                requirements.color = UISettings.BlockTextColor;

            }
            else
            {
                preview.color = Color.white;
                requirements.text = LocaleManager.Instance.GetText("game.main.building.upgrades.requirementscomplete");
                requirements.color = UISettings.GreenTextColor;
            }

            if (model.IsUpgrading)
            {
                requirements.gameObject.SetActive(false);
                levelLabel.gameObject.SetActive(false);
                timerPanel.gameObject.SetActive(true);
                upgradeProgress.gameObject.SetActive(true);
                upgradeProgress.minValue = 0f;
                upgradeProgress.maxValue = model.UpgradeTimeSeconds;
               
                long timeLeft = GameTimer.TimeLeftSeconds(model.EndUpgradeTimeSeconds);
                timerPanel.SetText(GameTimer.GetGameTimeString(timeLeft));
                upgradeProgress.DOValue(_lotModel.Building.UpgradeTimeSeconds - timeLeft, 0f);
                model.OnTimerLeftSecondsEvent.AddListener(OnTikTakHandler);
                
            }
            else
            {
                _canGoToMap = true;
                requirements.gameObject.SetActive(model.Level < model.MaxLevel);
                model.OnTimerLeftSecondsEvent.RemoveListener(OnTikTakHandler);
                levelLabel.gameObject.SetActive(true);
                timerPanel.gameObject.SetActive(false);
                upgradeProgress.gameObject.SetActive(false);
            }
        }

        private void OnTikTakHandler(long secondsLeft)
        {
            if (secondsLeft > 0)
            {
                timerPanel.SetText(GameTimer.GetGameTimeString(secondsLeft));
                upgradeProgress.DOValue(_lotModel.Building.UpgradeTimeSeconds - secondsLeft, 2f);
            }
        }


        public void SetSelection(bool value)
        {
            if (value)
            {
                Color32 color = selection.color;
                color.a = 128;
                selection.color = color;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!eventData.dragging)
            {
                SoundManager.Instance.PlaySFX("click");
                
                if (_lotModel.IsBuildingExist() && _lotModel.Building.IsUpgrading)
                {
                    MapManager.Instance.FinishUpgradeBuilding(_lotModel.Building,false);
                }
                else if(_canGoToMap)
                {

                    if (_lotModel.IsBuildingExist() && _lotModel.Building.IsCanCollect)
                    {
                        MapManager.Instance.GotoBuilding(_lotModel.Building.Pos,0.8f);
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Home);
                    }
                    else
                    {
                        MapManager.Instance.GotoBuilding(_lotModel.Id,0.8f);
                        MapManager.Instance.SelectMapBuilding(_lotModel.Id);
                    }

                }
            }
              
        }
    }
}
