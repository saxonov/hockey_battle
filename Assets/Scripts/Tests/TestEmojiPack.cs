﻿using System.Collections;
using TMPro;
using UnityEngine;

public class TestEmojiPack : MonoBehaviour
{

    public TextMeshProUGUI textField;

    public TMP_SpriteAsset spriteAsset;

    private WaitForSeconds delayRoutine;


	void Start ()
	{
        delayRoutine = new WaitForSeconds(1f);
	    StartCoroutine(StartTestRoutine());

	}

    private IEnumerator StartTestRoutine()
    {
      
        for (int i = 0; i < spriteAsset.spriteInfoList.Count; i++)
        {
            yield return delayRoutine;
            textField.text += Random.value > 0.5f ? "Test_!-" : "";
            textField.text += char.ConvertFromUtf32(spriteAsset.spriteInfoList[i].unicode);
        }
        yield return null;
    }

    // Update is called once per frame
	void Update () {
		
	}
}
