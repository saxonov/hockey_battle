﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace SCTools.Animator
{

    public class StateMachineEvent : UnityEvent<AnimatorStateInfo>
    {
    };

    public class StateMachineBehavior : StateMachineBehaviour
    {
        public bool addEnterStateEvent;
        public bool addExitStateEvent;
        public bool addUpdateStateEvent;

        public void ClearEvents()
        {
            if (EnterStateEvent != null)
            {
                EnterStateEvent.RemoveAllListeners();
            }

            if (ExitStateEvent != null)
            {
                ExitStateEvent.RemoveAllListeners();
            }

            if (UpdateStateEvent != null)
            {
                UpdateStateEvent.RemoveAllListeners();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterState"></param>
        /// <param name="exitState"></param>
        /// <param name="updateState"></param>
        public void AddEvents(UnityAction<AnimatorStateInfo> enterState = null,
                              UnityAction<AnimatorStateInfo> exitState = null,
                              UnityAction<AnimatorStateInfo> updateState = null
            )
        {
            if (addEnterStateEvent)
            {
                EnterStateEvent = new StateMachineEvent();
                EnterStateEvent.AddListener(enterState);
            }

            if (addExitStateEvent)
            {
                ExitStateEvent = new StateMachineEvent();
                ExitStateEvent.AddListener(exitState);

            }

            if (addUpdateStateEvent)
            {
                UpdateStateEvent = new StateMachineEvent();
                UpdateStateEvent.AddListener(updateState);
            }
        }

        private StateMachineEvent EnterStateEvent;
        private StateMachineEvent ExitStateEvent;
        private StateMachineEvent UpdateStateEvent;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(UnityEngine.Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

            if (EnterStateEvent != null)
            {
                EnterStateEvent.Invoke(stateInfo);
            }
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(UnityEngine.Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (UpdateStateEvent != null)
            {
                UpdateStateEvent.Invoke(stateInfo);
            }
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(UnityEngine.Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (ExitStateEvent != null)
            {
                ExitStateEvent.Invoke(stateInfo);
            }
        }


        // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}
    }

}
