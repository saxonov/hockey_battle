﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Competitions;
using HockeyManager.Game.UI.Components;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class CompetitionWinnerItemRenderer:MonoBehaviour
    {
#pragma warning disable CS0649 // Полю "CompetitionWinnerItemRenderer.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text counterLabel;
#pragma warning restore CS0649 // Полю "CompetitionWinnerItemRenderer.counterLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionWinnerItemRenderer.teamLogo" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private LogoImage teamLogo;
#pragma warning restore CS0649 // Полю "CompetitionWinnerItemRenderer.teamLogo" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionWinnerItemRenderer.teamNameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text teamNameLabel;
#pragma warning restore CS0649 // Полю "CompetitionWinnerItemRenderer.teamNameLabel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionWinnerItemRenderer.scorePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl scorePanel;
#pragma warning restore CS0649 // Полю "CompetitionWinnerItemRenderer.scorePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionWinnerItemRenderer.selection" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image selection;
#pragma warning restore CS0649 // Полю "CompetitionWinnerItemRenderer.selection" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private CompetitionLeadTeamModel _model;

        public void SetModel(CompetitionLeadTeamModel model,int index,Sprite scoreIcon)
        {
            _model = model;

            counterLabel.text = _model.Position.ToString().AddPostfix('.');
            teamNameLabel.text = _model.TeamInfo.Name;

            scorePanel.SetIcon(scoreIcon);
            scorePanel.SetText(model.Score);

            teamLogo.material = new Material(GameSettings.Instance.HsvShader);
            teamLogo.sprite = GameSettings.Instance.GetLogo64(_model.TeamInfo.Logo);
            var tuner = teamLogo.GetComponent<HSVShaderTuner>();
            tuner.ChangeHUE(_model.TeamInfo.LogoColor);
            tuner.ChangeSaturation(_model.TeamInfo.LogoSaturation);

            selection.enabled = index%2 != 0;

            if (_model.TeamInfo.IsMe())
            {
                selection.enabled = true;
                selection.color = UISettings.CompetitionMyPosColor;
            }

        }

        

    }
}
