﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.UI.Components;
using HockeyManager.Models.Map;
using HockeyManager.TutorialSystem;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Renderers
{
   
    public class MapLotInfoRenderer : MonoBehaviour
    {
        
#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Image icon;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.icon" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.labelName" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text labelName;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.labelName" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.description" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Text description;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.description" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl pricePanel;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.pricePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.timePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl timePanel;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.timePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private RectTransform tutorialHelper;

#pragma warning disable CS0649 // Полю "MapLotInfoRenderer.buildButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button buildButton;
#pragma warning restore CS0649 // Полю "MapLotInfoRenderer.buildButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.

        private MapBuildingSettingsInfo _buildingSettingsInfo;
        private BuildingUpgradeInfo _model;
        
        void Start()
        {
            buildButton.onClick.AddListener(OnBuildHandler);
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Add(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
                OnTutorialStepChanged();
            }
            else
            {
                if (tutorialHelper != null)
                {
                    Destroy(tutorialHelper.gameObject);
                    tutorialHelper = null;
                }

            }
        }

        void OnDestroy()
        {
            if (!GameData.IsTutorialPassed)
            {
                EventsManager.Instance.Remove(TutorialManager.TutorialStepChanged, OnTutorialStepChanged);
            }
            buildButton.onClick.RemoveListener(OnBuildHandler);
        }

        public void SetData(BuildingUpgradeInfo model)
        {
            _model = model;
            _buildingSettingsInfo = GameSettings.Instance.GetBuildingData(model.Id);
            icon.sprite = _buildingSettingsInfo.GetPreview(_model.Level, _model.MaxLevel);
            labelName.text = _buildingSettingsInfo.GetName().ToUpper();
            description.text = _buildingSettingsInfo.GetDescription();
            pricePanel.SetText(model.Price.ToString());
            timePanel.SetText(GameTimer.GetGameTimeString(model.TimeSeconds));

          

        }


        private void OnTutorialStepChanged()
        {
            if (TutorialManager.Instance.CurrentView != null)
            {
                TutorialManager.Instance.CurrentView.SetShowTarget(tutorialHelper);
            }
        }


        private void OnBuildHandler()
        {
            PopupsManager.Instance.SetPreloadDelaySeconds(0f);
            MapManager.Instance.UpgradeBuilding(_model);
        }
    }
}
