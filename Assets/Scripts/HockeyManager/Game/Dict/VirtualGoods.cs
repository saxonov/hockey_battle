﻿namespace HockeyManager.Game.Dict
{

    public class VirtualGoods
    {
        public const string VIP = "VIP";
        public const string PROTECT_SHIELD = "PROTECT_SHIELD";

        public const int FULL_ENERGY_GOLD_PRICE = 25;

        public const string FULL_ENERGY_GOOD = "FULL_ENERGY";

        public const string TEST_GOLD_GOOD = "TEST_GOLD";

        public class TimelimitVirtualGood
        {
            public string type;
            public int timeLimit;
            public int goldPrice;
            public string localKey;
        }

    }

    
}
