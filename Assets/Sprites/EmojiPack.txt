{"frames": [

{
	"filename": "1f3c5.png",
	"frame": {"x":128,"y":129,"w":48,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":8,"y":1,"w":48,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f3c6.png",
	"frame": {"x":1,"y":129,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f3d2.png",
	"frame": {"x":948,"y":65,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f3d6.png",
	"frame": {"x":496,"y":194,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f3dd.png",
	"frame": {"x":430,"y":194,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4a3.png",
	"frame": {"x":1,"y":194,"w":49,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":49,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4a4.png",
	"frame": {"x":354,"y":129,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4a5.png",
	"frame": {"x":176,"y":260,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4b0.png",
	"frame": {"x":110,"y":260,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4b8.png",
	"frame": {"x":182,"y":194,"w":64,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4e2.png",
	"frame": {"x":450,"y":1,"w":63,"h":59},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":63,"h":59},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f4fa.png",
	"frame": {"x":693,"y":1,"w":64,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":64,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6ab.png",
	"frame": {"x":483,"y":129,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6cb.png",
	"frame": {"x":44,"y":260,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6e1.png",
	"frame": {"x":693,"y":194,"w":54,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":0,"w":54,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6e3.png",
	"frame": {"x":318,"y":1,"w":64,"h":51},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":13,"w":64,"h":51},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6e9.png",
	"frame": {"x":65,"y":1,"w":64,"h":46},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":9,"w":64,"h":46},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f6eb.png",
	"frame": {"x":813,"y":194,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f9c0.png",
	"frame": {"x":131,"y":1,"w":63,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":8,"w":63,"h":48},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f32d.png",
	"frame": {"x":749,"y":194,"w":62,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":62,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f37b.png",
	"frame": {"x":117,"y":194,"w":63,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":63,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f43c.png",
	"frame": {"x":52,"y":194,"w":63,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":63,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f47a.png",
	"frame": {"x":784,"y":129,"w":56,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":1,"w":56,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f47b.png",
	"frame": {"x":718,"y":129,"w":64,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":64,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f47d.png",
	"frame": {"x":658,"y":129,"w":58,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":1,"w":58,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f48b.png",
	"frame": {"x":196,"y":1,"w":56,"h":50},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":5,"w":56,"h":50},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f51d.png",
	"frame": {"x":644,"y":1,"w":47,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":9,"y":1,"w":47,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f54a.png",
	"frame": {"x":944,"y":194,"w":62,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":62,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f60d.png",
	"frame": {"x":248,"y":194,"w":63,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f60e.png",
	"frame": {"x":1,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f60f.png",
	"frame": {"x":514,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f61a.png",
	"frame": {"x":642,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f61d.png",
	"frame": {"x":578,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f62b.png",
	"frame": {"x":321,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f62c.png",
	"frame": {"x":129,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f62d.png",
	"frame": {"x":257,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f64f.png",
	"frame": {"x":193,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f91e.png",
	"frame": {"x":1,"y":260,"w":41,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":0,"w":41,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f94a.png",
	"frame": {"x":879,"y":194,"w":63,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":63,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f98d.png",
	"frame": {"x":882,"y":65,"w":64,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":64,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f373.png",
	"frame": {"x":365,"y":194,"w":63,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":63,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f374.png",
	"frame": {"x":628,"y":194,"w":63,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f389.png",
	"frame": {"x":515,"y":1,"w":63,"h":60},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":63,"h":60},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f393.png",
	"frame": {"x":1,"y":1,"w":62,"h":43},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":10,"w":62,"h":43},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f413.png",
	"frame": {"x":752,"y":65,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f427.png",
	"frame": {"x":242,"y":129,"w":64,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":64,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f446.png",
	"frame": {"x":313,"y":194,"w":50,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":1,"w":50,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f451.png",
	"frame": {"x":254,"y":1,"w":62,"h":51},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":6,"w":62,"h":51},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f477.png",
	"frame": {"x":594,"y":129,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f490.png",
	"frame": {"x":580,"y":1,"w":62,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":61},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f525.png",
	"frame": {"x":887,"y":1,"w":54,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":1,"w":54,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f601.png",
	"frame": {"x":823,"y":1,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f602.png",
	"frame": {"x":759,"y":1,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f607.png",
	"frame": {"x":952,"y":129,"w":60,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":60,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f608.png",
	"frame": {"x":65,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f621.png",
	"frame": {"x":450,"y":65,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f694.png",
	"frame": {"x":385,"y":65,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f911.png",
	"frame": {"x":943,"y":1,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f918.png",
	"frame": {"x":842,"y":129,"w":43,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":0,"w":43,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f922.png",
	"frame": {"x":419,"y":129,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f947.png",
	"frame": {"x":706,"y":65,"w":44,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":1,"w":44,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f948.png",
	"frame": {"x":548,"y":129,"w":44,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":1,"w":44,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "1f949.png",
	"frame": {"x":308,"y":129,"w":44,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":1,"w":44,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "26bd.png",
	"frame": {"x":887,"y":129,"w":63,"h":63},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":63,"h":63},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "270a.png",
	"frame": {"x":65,"y":129,"w":61,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":61,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "2615.png",
	"frame": {"x":817,"y":65,"w":63,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":63,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "2620.png",
	"frame": {"x":562,"y":194,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "2622.png",
	"frame": {"x":178,"y":129,"w":62,"h":62},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":62,"h":62},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "2623.png",
	"frame": {"x":384,"y":1,"w":64,"h":58},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":64,"h":58},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
}],
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "EmojiPack.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:57c670496647214b51c27b9ab396b51c:2e80dd7789d9d755c4dde743a1a08e74:be595000493f3fcf29ebd3f7e1372af8$"
}
}
