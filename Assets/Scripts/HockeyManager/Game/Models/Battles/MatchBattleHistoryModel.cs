﻿using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;


namespace HockeyManager.Game.Models.Battles
{
    public class MatchBattleResultData:IGSParse
    {
        private bool _myTeamWinner;
        private bool _isCanceled = false;
        private string _resultString = "";
        private TeamInfoModel _enemyTeam;
        private int _startTime;


        private int _cancelPenaltyPrice; 

        public int StartTimeStamp
        {
            get { return _startTime; }
        }


        public bool MyTeamWinner
        {
            get { return _myTeamWinner; }
        }

        public string ResultString
        {
            get { return _resultString; }
        }

        public TeamInfoModel EnemyTeam
        {
            get { return _enemyTeam; }
        }

        public bool IsCanceled
        {
            get { return _isCanceled; }
        }

        public int CancelPenaltyPrice
        {
            get { return _cancelPenaltyPrice; }
        }

        public void Parse(GSData data)
        {
            _isCanceled = data.GetBoolean("isCanceled").Value;
            _startTime = data.GetInt("startTime").Value;
            _cancelPenaltyPrice = data.GetInt("cancelPenaltyPrice").Value;

            List<GSData> list = data.GetGSDataList("players");

            GSData player1 = list[0];
            GSData player2 = list[1];

            _enemyTeam = !GameData.IsMyUserId(player1.GetString("userId")) ? 
                new TeamInfoModel(player1) : 
                new TeamInfoModel(player2);

            if (player1.GetInt("goals").Value > player2.GetInt("goals").Value)
            {
                _resultString = player1.GetInt("goals").Value + ":" + player2.GetInt("goals").Value;
                _myTeamWinner = GameData.IsMyUserId(player1.GetString("userId"));
            }
            else
            {
                _resultString = player2.GetInt("goals").Value + ":" + player1.GetInt("goals").Value;
                _myTeamWinner = GameData.IsMyUserId(player2.GetString("userId"));
            }
        }
    }

    public class MatchBattleHistoryModel:IGSParse
    {
        private int _total = 0;

        private int _wins = 0;
        private int _lost = 0;
      
        private string _type;

        private List<MatchBattleResultData> _results;
        
        public int Total
        {
            get { return _total; }
        }

        public int Wins
        {
            get { return _wins; }
        }

        public int Lost
        {
            get { return _lost; }
        }

       
        public string Type
        {
            get { return _type; }
        }

       
        public List<MatchBattleResultData> Results
        {
            get { return _results; }
        }


        public void Parse(GSData data)
        {
            _total = data.GetInt("total").Value;
            _wins = data.GetInt("wins").Value;
            _lost = data.GetInt("lost").Value;

            _type = data.GetString("type");
            
            _results = _results ?? new List<MatchBattleResultData>();
            _results.Clear();
            List<GSData> list = data.GetGSDataList("history");

            foreach (var gsData in list)
            {
                MatchBattleResultData res = new MatchBattleResultData();
                res.Parse(gsData);
                _results.Add(res);
            }

            
        }
    }
}