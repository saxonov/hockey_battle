﻿using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using UnityEngine;

namespace HockeyManager.Game.Models.EPS
{
    public class EquipmentUpgradeModel:IGSParse
    {
        private int _level = 0;
        private int _price = 0;
        private int _timeSeconds = 0;
        private int _upgradeGoldPrice = 0;
        private int _attack = 0;
        private int _deffence = 0;
        private int _view = 0;
        private RewardData _reward;

        public RewardData Reward
        {
            get { return _reward; }
        }

        public int Deffence
        {
            get { return _deffence; }
        }

        public int Attack
        {
            get { return _attack; }
        }

        public int UpgradeGoldPrice
        {
            get { return _upgradeGoldPrice; }
        }

        public int TimeSeconds
        {
            get { return _timeSeconds; }
        }

        public int Price
        {
            get { return _price; }
        }

        public int Level
        {
            get { return _level; }
        }

        public int View
        {
            get { return _view; }
        }


        public void Parse(GSData data)
        {
            
            _level = data.GetInt("level").Value;
            _price = data.GetInt("price").Value;
            _timeSeconds = data.GetInt("time").Value * 60;
            _attack = data.GetInt("attack").Value;
            _deffence = data.GetInt("deffence").Value;
            _upgradeGoldPrice = data.GetInt("time").Value * 2;
           
            if (data.ContainsKey("reward"))
            {
                _reward = JsonUtility.FromJson<RewardData>(data.GetGSData("reward").JSON);
            }

        }
    }
}
