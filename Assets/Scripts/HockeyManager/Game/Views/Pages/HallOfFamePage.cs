﻿using System.Collections.Generic;
using HockeyManager.Game.Controllers;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.HallItems;
using HockeyManager.Game.Views.Components;
using HockeyManager.Game.Views.Renderers;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.SoundManager;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.UI.Pages
{
    public class HallOfFamePage :MonoBehaviour
    {
        [SerializeField] private Text activeSlotsLabel;
        [SerializeField] private ScrollRect hallItemsList;
        [SerializeField] private ScrollRect slotsGridList;
        [SerializeField] private GameObject hallItemRendererPrefab;
        [SerializeField] private GameObject slotItemRendererPrefab;

        [SerializeField] private InnerPageNavigation navBar;

        private HallItemsController _controller;
        private TeamController _team;

        void Awake()
        {
            _controller = GameRoot.Instance.GetController<HallItemsController>();
            _team = GameRoot.Instance.GetController<TeamController>();

        }

        void Start()
        {
            BuildSlots();
        }

        public void OnShow()
        {
            _controller.LoadItems(BuildItemsList);
        }

        public void OnHide()
        {
            hallItemsList.content.RemoveChildren();
        }

        private void BuildSlots() 
        {
            slotsGridList.content.RemoveChildren();

            for (int i = 0; i < _controller.Slots.Count; i++)
            {
                RectTransform itemR = slotsGridList.content.AddChild(slotItemRendererPrefab, false);
                itemR.GetComponent<HallSlotItemRenderer>().SetSlotData(_controller.Slots[i]);
            }
            activeSlotsLabel.text = LocaleManager.Instance.GetText("game.main.hallpage.activeitems", _controller.ActiveSlotsCount + "/" + GameData.MaxHallInventorySlots);
        }


        void OnDestroy()
        {
            _team = null;
            _controller = null;
        }

        void OnEnable()
        {
            EventsManager.Instance.Add<HallItemModel>(HallIItemRenderer.HallItemBuyClick,OnItemBuyClickHandler);
            EventsManager.Instance.Add<HallItemModel>(HallIItemRenderer.HallItemSelect, OnItemSelectHandler);
            EventsManager.Instance.Add<HallItemSlotModel>(HallSlotItemRenderer.HallItemSlotSelected, OnItemSlotSelected);
        }

        void OnDisable()
        {
            EventsManager.Instance.Remove<HallItemSlotModel>(HallSlotItemRenderer.HallItemSlotSelected, OnItemSlotSelected);
            EventsManager.Instance.Remove<HallItemModel>(HallIItemRenderer.HallItemSelect, OnItemSelectHandler);
            EventsManager.Instance.Remove<HallItemModel>(HallIItemRenderer.HallItemBuyClick, OnItemBuyClickHandler);
            
        }

        private void OnItemSelectHandler(HallItemModel model)
        {
            SoundManager.Instance.PlaySFX("click");
            if (_controller.HasEmptySlot())
            {
                HallItemSlotModel slot = _controller.Slots.Find(s => s.ItemId == -1);
                if (slot != null)
                {
                    _controller.AddItemToSlot(model,slot);
                    BuildItemsList();
                     activeSlotsLabel.text = LocaleManager.Instance.GetText("game.main.hallpage.activeitems",
                        _controller.ActiveSlotsCount + "/" + GameData.MaxHallInventorySlots);
                }
            }
            else
            {
                PopupsManager.Instance.AddPopup(PopupType.NeedLevelPopup)
                    .SetTitle(LocaleManager.Instance.GetText("game.main.hallpage.popup.title.cannotactivate").ToUpper())
                    .SetText("game.main.hallpage.popup.title.notemptyslots")
                    .HideButtons(true, false);
            }
        }



        private void OnItemSlotSelected(HallItemSlotModel slot)
        {
            SoundManager.Instance.PlaySFX("click");

            if (slot.IsLocked)
            {
                PopupsManager.Instance.AddPopup(PopupType.AlertPopup)
                       .SetTitle("game.main.hallpage.popup.title.slotlocked")
                       .SetButtonLabels("game.main.building.halloffame", "game.common.closebutton")
                       .SetCallbacks(() =>
                       {
                           MapManager.Instance.SelectMapBuilding(GameData.HallBuildingPosition);
                       });
                return;
            }

            if (slot.ItemId != -1 && !slot.IsLocked)
            {
              
                    _controller.RemoveItemFromSlot(slot);
                    BuildItemsList();
                    activeSlotsLabel.text = LocaleManager.Instance.GetText("game.main.hallpage.activeitems",
                        _controller.ActiveSlotsCount + "/" + GameData.MaxHallInventorySlots);
                
            }
        }


        private void OnItemBuyClickHandler(HallItemModel model)
        {
            if (model.MoneyPrice > 0)
            {
                if (_team.CanBuyForCoins(model.MoneyPrice))
                {
                    _controller.BuyItem(model, BuildItemsList);

                }
                else
                {
                    if (_team.CanBuyForGold(model.GoldPrice))
                    {
                        PopupsManager.Instance.AddPopup(PopupType.NotEnoughFansCoinsPopup)
                            .SetTitle("game.main.popup.title.notenoughmoney")
                            .SetTextIconControls(model.GoldPrice)
                            .SetButtonLabels(LocaleManager.Instance.GetText("game.common.buybutton").ToUpper())
                            .SetText("game.main.popup.title.cannotbuy").SetCallbacks(() =>
                            {
                                model.CurrencyType = GameData.CurrencyGold;
                                _controller.BuyItem(model, BuildItemsList);
                            });
                    }
                    else
                    {
                        PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
                    }
                }
            }
            else if (model.GoldPrice > 0 && _team.CanBuyForGold(model.GoldPrice))
            {
                _controller.BuyItem(model,BuildItemsList);
            }
            else
            {
                PageNavigatorManager.Instance.GotoPage(NavigationPageTypes.Shop);
            }
        }


        private void OnPageChangedHandler(NavigationPageTypes selectedPage)
        {
            if (selectedPage == NavigationPageTypes.HallOfFame)
            {
               _controller.LoadItems(BuildItemsList);
            }
            else
            {
                hallItemsList.content.RemoveChildren();
            }

        }

        private void BuildItemsList()
        {
            List<HallItemModel> avaialble = _controller.GetAvailableItems();

            hallItemsList.content.RemoveChildren();

            for (int i = 0; i < avaialble.Count; i++)
            {
                RectTransform itemR = hallItemsList.content.AddChild(hallItemRendererPrefab, false);
                itemR.GetComponent<HallIItemRenderer>().SetModel(avaialble[i]);
            }
            //hallItemsList.verticalNormalizedPosition = 1f;
            activeSlotsLabel.text = LocaleManager.Instance.GetText("game.main.hallpage.activeitems", _controller.ActiveSlotsCount + "/" + GameData.MaxHallInventorySlots);
        }

    }


}
