﻿using HockeyManager.Game.Dict;
using HockeyManager.Game.Managers;
using HockeyManager.Game.Models.Competitions;
using HockeyManager.Game.UI.Components;
using SCTools.EventsSystem;
using SCTools.Localization;
using SCTools.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Renderers
{
    public class CompetitionItemRenderer:MonoBehaviour
    {
        public const string OnCompetitionTimerEndEvent = "CompetitionItemRenderer.OnCompetitionTimerEndEvent";

        public static GameObject winnerItemRendererPrefab;
        public static GameObject emptyItemRendererPrefab;

#pragma warning disable CS0649 // Полю "CompetitionItemRenderer.winnerList" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private RectTransform winnerList;
#pragma warning restore CS0649 // Полю "CompetitionItemRenderer.winnerList" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionItemRenderer.rewardsInfoButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private Button rewardsInfoButton;
#pragma warning restore CS0649 // Полю "CompetitionItemRenderer.rewardsInfoButton" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionItemRenderer.titlePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl titlePanel;
#pragma warning restore CS0649 // Полю "CompetitionItemRenderer.titlePanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0649 // Полю "CompetitionItemRenderer.expireTimerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
        [SerializeField] private TextIconControl expireTimerPanel;
#pragma warning restore CS0649 // Полю "CompetitionItemRenderer.expireTimerPanel" нигде не присваивается значение, поэтому оно всегда будет иметь значение по умолчанию null.
#pragma warning disable CS0169 // Поле "CompetitionItemRenderer.refreshButton" никогда не используется.
        [SerializeField] private Button refreshButton;
#pragma warning restore CS0169 // Поле "CompetitionItemRenderer.refreshButton" никогда не используется.

        private CompetitionModel _model;

        void OnEnable()
        {
            rewardsInfoButton.onClick.AddListener(OnRewardsClickHandler);
            GameTimer.OnSecondsChanged.AddListener(OnTikTakHandler);
        }
        
        void OnDisable()
        {
            rewardsInfoButton.onClick.RemoveListener(OnRewardsClickHandler);
            GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
        }

        public void SetModel(CompetitionModel model)
        {
            _model = model;
            string cmptName = "";
            Sprite scoreIcon = GameSettings.Instance.GetIconTextCompetitionType(model.Type,out cmptName);

            titlePanel.SetIcon(scoreIcon);
            titlePanel.SetText(cmptName);

            winnerList.RemoveChildren();

            for (int i = 0; i < model.Winners.Count; i++)
            {
                CompetitionWinnerItemRenderer item =
                    winnerList.AddChild(winnerItemRendererPrefab, false).GetComponent<CompetitionWinnerItemRenderer>();
                item.SetModel(model.Winners[i],i,scoreIcon);
            }

            if (_model.Winners.Count == 0)
            {
                Text emptyLabel = winnerList.AddChild(emptyItemRendererPrefab, false).GetComponent<Text>();
                emptyLabel.text = LocaleManager.Instance.GetText("game.main.competitions.waitforupdaterenderer");
            }

            OnTikTakHandler();
        }


        private void OnRewardsClickHandler()
        {
            if (_model != null)
            {
                PopupsManager.Instance.AddPopup(PopupType.CompetitionRewardsPopup).SetData(_model);
            }
        }


        private void OnTikTakHandler()
        {
            if (_model != null)
            {
                long timeLeft = GameTimer.TimeLeftSeconds(_model.ExpireTime);
                if (timeLeft > 0)
                {
                    expireTimerPanel.SetText(MathUtils.SecondsToTimeString(timeLeft));
                }
                else
                {
                    GameTimer.OnSecondsChanged.RemoveListener(OnTikTakHandler);
                    EventsManager.Instance.Call(OnCompetitionTimerEndEvent);
                }
              
            }
        }
    }
}
