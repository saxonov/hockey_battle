﻿using System;
using System.Collections.Generic;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using SCTools.Localization;
using UnityEngine;

namespace HockeyManager.Game.Models.Competitions
{
    public class CompetitionModel:IGSParse,IComparable<CompetitionModel>
    {
        private string _type;
        private long _expireTime;

        private List<CompetitionLeadTeamModel> _winners; 

        private List<RewardData> _rewards; 

        public CompetitionModel(GSData data)
        {
            Parse(data);
        }

        public string Type
        {
            get { return _type; }
        }

        public long ExpireTime
        {
            get { return _expireTime; }
        }

        public List<RewardData> Rewards
        {
            get { return _rewards; }
        }

        public List<CompetitionLeadTeamModel> Winners
        {
            get { return _winners; }
        }

        public void Parse(GSData data)
        {
            _expireTime = data.GetLong("expireTime").Value;
            _type = data.GetString("type");

            _winners = _winners ?? new List<CompetitionLeadTeamModel>();
            _winners.Clear();
            var list = data.GetGSDataList("winners");
            foreach (var gsData in list)
            {
               _winners.Add(new CompetitionLeadTeamModel(gsData));
            }
            _winners.Sort();

            if (data.ContainsKey("rewards"))
            {
                _rewards = _rewards ?? new List<RewardData>();
                _rewards.Clear();
                var rewardsList = data.GetGSDataList("rewards");
                foreach (var gsData in rewardsList)
                {
                    _rewards.Add(JsonUtility.FromJson<RewardData>(gsData.JSON));
                }
            }

        }

        public int CompareTo(CompetitionModel other)
        {
            if (_expireTime < other.ExpireTime)
            {
                return -1;
            }
            if (_expireTime > other.ExpireTime)
            {
                return 1;
            }

            return 0;
        }
    }
}
