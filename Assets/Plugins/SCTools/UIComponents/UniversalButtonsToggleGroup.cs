﻿using System.Collections.Generic;
using SCTools.UI;
using UnityEngine;

namespace SCTools.UIComponents
{
    public class UniversalButtonsToggleGroup:MonoBehaviour
    {

        public UniversalButtonEvent OnItemSelected = new UniversalButtonEvent();

        [SerializeField]private List<UniversalButton> buttons = new List<UniversalButton>();
   //     [SerializeField]private string selectedId = string.Empty;

        void Start()
        {
                foreach (var universalButton in buttons)
                {
                    if (universalButton.IsToggle)
                    {
                        universalButton.OnCustomClick.AddListener(OnItemSelectedHandler);
                    }
                   
                }
        }

        private void OnItemSelectedHandler(UniversalButton button)
        {
            if (!button.Selected)
            {
                button.Selected = true;
            }
             buttons.ForEach((btn) =>
             {
                 if (btn.GetId() != button.GetId())
                 {
                     btn.Selected = false;
                 }
             });
             
            OnItemSelected.Invoke(button);
        }

        public void SelectButton(string id)
        {

            buttons.ForEach((btn) =>
            {
                if (btn.GetId() == id)
                {
                    btn.Selected = true;
                }
                else
                {
                    btn.Selected = false;
                }
            });


        }

    }

}
