﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SCTools.UI
{
    [Serializable]
    public struct Gradation
    {
        public Color color;
        public float checkPosition;
    }

    public class ProgressBar : MonoBehaviour
    {
        public Gradation[] gradations;

        public float max = 10;
        public float min = 0;

        private Image foreground;
      //  private Image background;

        private float size = 0f;
        private float targetSize;
        private float _currentProgress;

        public float CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                   _currentProgress = value;
                   UpdateProgress();
            }
        }

        // Use this for initialization
        void Awake()
        {

            foreground = transform.FindChild("foreground").GetComponent<Image>();
            //background = transform.FindChild("background").GetComponent<Image>();

            size = Mathf.Abs(foreground.rectTransform.rect.xMin) + Mathf.Abs(foreground.rectTransform.rect.xMax);
            foreground.type = Image.Type.Filled;

            int gradationCount = gradations.Length;
            float valueCount = (max - min + min)/gradationCount;
            for (int i = 0; i < gradationCount; i++)
            {
                float gradationFactor = size / ((max - min + min) / (valueCount*(gradationCount-i)));
                gradations[i].checkPosition = gradationFactor;
            }

       }

        public void SetProgress(float minimum, float maximum)
        {
            if (minimum > maximum)
            {
                max = minimum;
                min = maximum;
            }
            else
            {
                min = minimum;
                max = maximum;
            }
            CurrentProgress = max;
        }

        private void UpdateProgress()
        {
            _currentProgress = Mathf.Clamp(_currentProgress, min, max);
            float calcValue = _currentProgress / (max - min + min);
            targetSize = size * calcValue;

            for (int i = 0; i < gradations.Length; i++)
            {
                if (targetSize <= gradations[i].checkPosition && !foreground.color.Equals(gradations[i].color))
                {
                    foreground.color = gradations[i].color;
                }
            }
            foreground.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, targetSize);
        }

    }


}

