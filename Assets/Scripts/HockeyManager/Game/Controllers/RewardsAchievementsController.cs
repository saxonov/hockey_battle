﻿using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Impl;
using HockeyManager.Game.Models.Rewards;
using SCTools.EventsSystem;
using UnityEngine;
using UnityEngine.Events;

namespace HockeyManager.Game.Controllers
{
    public class RewardsAchievementsController:IGameController,ICheckMessages, IGSResponceHandler
    {
        public const string RewardAdded = "RewardsAchievementsController.addRewardEvent";
        public const string RewardsCountChanged= "RewardsAchievementsController.rewardsCountChanged";
        public const string DailyBonusAdded = "RewardsAchievementsController.dailyBonusAddedEvent";
      //  public const string RewardsNeedUpdate = "RewardsAchievementsController.rewardsNeedUpdate";
       
     
        private IGameRoot _root;

        private List<AchievementModel> _achievements;
        private int _rewardsCount = 0;
        private List<RewardModel> _rewards;

        private bool _rewardsLoaded = false;
        private bool _achievementsLoaded = false;

        public int RewardsCount
        {
            get { return _rewardsCount; }

        }

        public void Initialize(IGameRoot root)
        {
            _root = root;
            _rewards = new List<RewardModel>();
            _achievements = new List<AchievementModel>();
        }

        public void Reset()
        {
            //_rewardsCount = 0;
       
            _rewardsLoaded = false;
            //LoadRewards(() =>
            //{
               
            //    _rewardsCount = _rewards.FindAll(r => r.ShowInList).Count;
            //    EventsManager.Instance.Call(RewardsCountChanged);

            //},true);
        }

        public void GetAdsReward(UnityAction callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_ADS_REWARD");
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                   callback();
                }
            });
        }

        private void AddReward(GSData reward)
        {
            if (reward != null)
            {

                if (_rewards.Exists(
                        r => r.Uid == reward.GetInt("uid").Value))
                {
                    Debug.LogWarningFormat("Duplicate reward added! Type: {0} ",reward.GetInt("type").Value);
                    return;
                }


                _rewardsLoaded = false;

                RewardModel rwd = new RewardModel(reward);
                rwd.newAdded = true;
                _rewards.Add(rwd);

                
                UpdateRewardCount();
                
                EventsManager.Instance.Call(RewardAdded);

                if (rwd.Type == RewardType.DailyBonusReward)
                {
                    EventsManager.Instance.Call(DailyBonusAdded);
                }
                
            }
        }
        
        public List<AchievementModel> Achievements
        {
            get { return _achievements; }
        }


        public List<RewardModel> RewardsList()
        {
            return _rewards.FindAll(r => !r.IsGained && r.ShowInList && !r.isInList);
        }

        public RewardModel GetReward(RewardType type)
        {
            return _rewards.Find(r => r.Type.Equals(type));
        }

        public void RemoveAdReward()
        {
            RewardModel model = _rewards.Find((r)=>r.Type == RewardType.AdsReward);
            if (model != null)
            {
                _rewards.Remove(model);
            }
        }

        public void GainAllRewards(UnityAction<RewardData> callback)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GAIN_ALL_REWARDS");
            _root.NetManager.RunLogEventRequest(request, responce =>
            {
                if (!responce.HasErrors)
                {
                    List<int> gainedRewards = responce.ScriptData.GetIntList(GS_ScriptData.AllGainedRewards);
                    RewardData totalRewardData = JsonUtility.FromJson<RewardData>(responce.ScriptData.GetGSData(GS_ScriptData.TakeRewardData).JSON);
                    for (int i = 0; i < gainedRewards.Count; i++)
                    {
                        RewardModel gainedReward = _rewards.Find(r => r.Uid == gainedRewards[i]);
                        if (gainedReward != null)
                        {
                            gainedReward.IsGained = true;
                            CheckAndRemoveAchievements(gainedReward);
                        }
                    }
                    _rewards.RemoveAll(r => r.IsGained);
                    UpdateRewardCount();
                    callback(totalRewardData);
                }
            });
        }

        public void GainReward(RewardModel model,UnityAction<bool> callback)
        {
            if (model.Type == RewardType.DailyBonusReward && model.IsGained)
            {
                Debug.LogError("Dailybonus gained already!");
                return;
            }
            
            var request = new LogEventRequest();
            request.SetEventKey("GAIN_REWARD");
            request.SetEventAttribute("TYPE",(int)model.Type);
            request.SetEventAttribute("DATE",model.DateTimestamp);
            request.SetEventAttribute("UID", model.Uid);
            request.SetDurable(true);
            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (_rewards.Contains(model))
                    {
                        _rewards.Remove(model);
                    }
                    model.IsGained = true;

                    CheckAndRemoveAchievements(model);
                    UpdateRewardCount();

                    if (callback != null)
                    {
                        callback(true);
                    }
                }
                else
                {
                    if (_rewards.Contains(model))
                    {
                        _rewards.Remove(model);
                    }
                    model.IsGained = true;
                    UpdateRewardCount();
                    if (callback != null)
                    {
                        callback(false);
                    }
                }
              
            },null,true);
        }

        private void CheckAndRemoveAchievements(RewardModel model)
        {
            if (model.Type == RewardType.AchievementReward)
            {
                AchievementModel amodel = _achievements.Find(a => a.Id == model.CustomData.GetString("id"));
                if (amodel != null)
                {
                    _achievementsLoaded = false;
                    amodel.IsTaked = true;
                    _achievements.Remove(amodel);
                }
            }
        }

        public void LoadAchievements(UnityAction callback)
        {
            if (_achievementsLoaded)
            {
                callback();
                return;
            }

            _achievementsLoaded = true;
            var request = new LogEventRequest();
            request.SetEventKey("GET_TASKS");
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    callback();
                }
                
            });
        }

        public void LoadRewards(UnityAction callback,bool forceLoad)
        {
            if (_rewardsLoaded && !forceLoad)
            {
                if (callback != null)
                {
                    callback();
                }
                   

                return;
            }
            _rewardsLoaded = true;
            LoadInternalRewards(callback);
        }

        private void LoadInternalRewards(UnityAction callback = null)
        {
            var request = new LogEventRequest();
            request.SetEventKey("GET_REWARDS");
            request.SetDurable(true);

            _root.NetManager.RunLogEventRequest(request, (responce) =>
            {
                if (!responce.HasErrors)
                {
                    if (callback != null)
                    {
                        callback();
                    }
                  
                }
            });
        }


        private void UpdateRewardCount()
        {
            _rewardsCount = _rewards.FindAll(r => r.ShowInList).Count;
            EventsManager.Instance.Call(RewardsCountChanged);
        }
          // 
        

        public void UpdateFromGSResponce(GSData scriptData)
        {
            if (scriptData.ContainsKey(GS_ScriptData.RewardsData))
            {
                GSData reward = scriptData.GetGSData(GS_ScriptData.RewardsData);
                if (reward != null)
                {
                    AddReward(reward);
                }
                else
                {
                    var rewards = scriptData.GetGSDataList(GS_ScriptData.RewardsData);
                    foreach (var rwd in rewards)
                    {
                        RewardModel model = _rewards.Find(r => r.Uid == rwd.GetInt("uid").Value);
                        if (model == null)
                        {
                            model = new RewardModel(rwd);
                            _rewards.Add(model);
                        }
                        else
                        {
                            model.Parse(rwd);
                        }
                    }

                    UpdateRewardCount();
                   
                }

            }

            if (scriptData.ContainsKey(GS_ScriptData.AchievementsData))
            {
                var achievements = scriptData.GetGSDataList(GS_ScriptData.AchievementsData);
                foreach (var achievement in achievements)
                {
                    _achievements.Add(new AchievementModel(achievement));
                }
            }

        }

        public void CheckMessage(GSData msgData, string msgCode)
        { 

            if (msgData != null && msgData.ContainsKey(GS_ScriptData.RewardsData))
            {
                AddReward(msgData.GetGSData(GS_ScriptData.RewardsData));
            }

            if (msgCode == GS_Messages.RewardAddedMsg)
            {
                if (msgData != null)
                {
                    AddReward(msgData);
                }
                else
                {
                    _rewardsLoaded = false;
                //    EventsManager.Instance.Call(RewardsNeedUpdate);
                }

            }
        }

        public bool CanCheckMessage(string msgType, GSData data = null)
        {
            return msgType == GS_Messages.RewardAddedMsg || (data != null && data.ContainsKey(GS_ScriptData.RewardsData));
        }

    }
}
