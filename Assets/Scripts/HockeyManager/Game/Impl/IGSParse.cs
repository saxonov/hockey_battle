﻿using GameSparks.Core;

namespace HockeyManager.Game.Impl
{
    public interface IGSParse
    {
        void Parse(GSData data);
    }
}
