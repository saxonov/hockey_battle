﻿using System.Collections;
using System.Text;
using HockeyManager.Game.Dict;
using HockeyManager.Game.Models.Base;
using HockeyManager.Game.UI.Components;
using HockeyManager.Game.UI.Popups;
using SCTools.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HockeyManager.Game.Views.Popups
{
    public class AttackTeamPopup:BasePopup
    {
        [SerializeField] private LogoImage teamLogo;
        [SerializeField] private TextMeshProUGUI teamNameText;
        [SerializeField] private TextMeshProUGUI totalExpText;
        [SerializeField] private TextMeshProUGUI statisticInfo;
        [SerializeField] private Text onlineLabel;

        [SerializeField] private HorizontalLayoutGroup hbox;

        //private string lostColor = ColorUtility.ToHtmlStringRGB(UISettings.BlockTextColor);
        //private string winColor = ColorUtility.ToHtmlStringRGB(UISettings.GreenTextColor);


        IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            hbox.enabled = false;
        }

        private AttackTeamModel model;

        protected override void UpdateData()
        {
            model = (AttackTeamModel) _data;
            teamLogo.sprite = GameSettings.Instance.GetLogo256(model.TeamInfo.Logo);
            var logoTuner = teamLogo.GetComponent<HSVShaderTuner>();
            logoTuner.ChangeHUE(model.TeamInfo.LogoColor);
            logoTuner.ChangeSaturation(model.TeamInfo.LogoSaturation);
            onlineLabel.text = model.isOnline ? LocaleManager.Instance.GetText("game.common.online") : LocaleManager.Instance.GetText("game.common.offline");
            onlineLabel.color = model.isOnline ? UISettings.GreenTextColor : UISettings.BlockTextColor;

            string rankStr = this.GetLocaleText(GameSettings.Instance.GetRankLocaleKey(model.Rank));
            string levelStr = this.GetLocaleText("game.common.shortlevel2", model.Level);
            teamNameText.text = string.Format("{0}\n{1}<sup>{2}</sup>",model.TeamInfo.Name, levelStr, rankStr.GetUITMPColorFormated(UISettings.GreenTextColor));
            totalExpText.text = string.Format("<sprite=4>{0}", model.TotalExp);
            
            StringBuilder statInfo = new StringBuilder();

            statInfo.AppendLine(this.GetLocaleText("game.main.teaminfo.totalmatches",
                GetTotalWinsLost(model.MatchWins, model.MatchLost)));

            statInfo.AppendLine(this.GetLocaleText("game.main.teaminfo.totalcups",
                GetTotalWinsLost(model.CupsWins, model.CupsLost)));

            statInfo.AppendLine(this.GetLocaleText("game.main.teaminfo.totalgoals",model.TotalGoals));
            statInfo.AppendLine(this.GetLocaleText("game.main.teaminfo.competitions",
                model.CompetitionsWins.ToString().GetUITMPColorFormated(UISettings.GreenTextColor),
                model.CompetitionsLost));

            statisticInfo.text = statInfo.ToString();

        }

        private string GetTotalWinsLost(int wins,int lost)
        {
            string winsStr = wins.ToString().GetUITMPColorFormated(UISettings.GreenTextColor);
            string lostStr = lost.ToString().GetUITMPColorFormated(UISettings.BlockTextColor);
            return string.Format("{0}({1}/{2})",wins+lost,winsStr,lostStr);
        }
    }
}
