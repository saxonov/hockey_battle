﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SCTools.UI
{

    [RequireComponent(typeof(Button))]
    public class ProgressButton : MonoBehaviour
    {
        [HideInInspector]
        public UnityEvent progressComplete;

        public Image progressImage;

        public Button button;

        private Text _countText;
        private float _progressTime = 1f;
        private bool _isTimerStarted;
        private float startTime;

        public float ProgressTime
        {
            get { return _progressTime; }
            set { _progressTime = value; }
        }

        public bool IsTimerStarted
        {
            get { return _isTimerStarted; }
        }

        void Awake()
        {
            progressComplete = new UnityEvent();
            button = GetComponent<Button>();
            if (progressImage == null)
            {
                progressImage = transform.FindChild("Progress").GetComponent<Image>();
            }

            _countText = GetComponentInChildren<Text>();


        }
        // Use this for initialization
        void Start()
        {


            button.enabled = true;
            progressImage.enabled = false;
            progressImage.fillMethod = Image.FillMethod.Radial360;
            progressImage.fillClockwise = true;
            progressImage.fillAmount = 0f;
        }



        // Update is called once per frame
        void LateUpdate()
        {

            if (_isTimerStarted)
            {
                float seconds = Time.time - startTime;
                if (seconds < _progressTime)
                {
                    progressImage.fillAmount = (1f / _progressTime) * seconds;
                }
                else
                {
                    button.enabled = true;
                    progressImage.enabled = false;
                    _isTimerStarted = false;
                    _countText.color = Color.white;
                    progressComplete.Invoke();

                }

            }
        }

        public void ChangeCount(int count)
        {
            _countText.text = count.ToString();
        }

        public void StartTimer()
        {
            if (_isTimerStarted)
            {
                return;
            }
            button.enabled = false;
            _countText.color = Color.black;
            progressImage.enabled = true;
            progressImage.fillAmount = 0f;
            startTime = Time.time;
            _isTimerStarted = true;
        }



    }

}
