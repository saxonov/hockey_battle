﻿using System;
using UnityEngine;
using System.Collections;

namespace SCTools.SoundManager
{

    [Serializable]
    public class SoundData
    {
        public string soundName;
        public AudioClip soundClip;
        public float volume = -0f;
    }

}
